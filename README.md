# Darlean JS App

Library for creating JavaScript/TypeScript Darlean apps

**IMPORTANT: Darlean is still in alpha. Functionality may change in backwards-incompatible ways. Use at your own risk! We are working hard to reach beta and stable as soon as possible!**

## Functionality

The Darlean app-lib provides the following key functionality:
* Creating and registering regular and service actors.

In addition to that it provides a lot of supportive functionality that you normally do not need directly:
* Shared/exclusive locks with reentrancy and take-over functionality in module [[selock]]
* Serializeable data objects with support for text, binary and struct attachments in module [[structs]]
* Containers that contain and transfer text, binary and struct data over the network in module [[containers]]
* Binary encoding of containers in module [[binaryencoding]]
* Various types in module [[types]]
* Various utility functions (like [[parallel]], [[replaceAll]] and [[wildcardMatch]]) in module [[util]]. 

## Getting started

This library contains all the types and functionality required for implementing a NodeJS/TypeScript Darlean application.

More information: 
* Generic website: http://darlean.io
* Getting started: https://gitlab.com/darlean/darlean/-/wikis/Tutorials/Getting-started
* Creating a NodeJS app: https://gitlab.com/darlean/darlean/-/wikis/NodeJS/Creating-an-App
* API Documentation for this library: https://docs.darlean.io/js/app-lib

## Functionality

The Darlean app-lib provides the following key functionality:
* Creating and registering regular and service actors.

In addition to that it provides a lot of supportive functionality:
* Shared/exclusive locks with reentrancy and take-over functionality
* Serializeable data objects with support for text, binary and struct attachments
* Containers that contain and transfer text, binary and struct data over the network
* Binary encoding of containers
* Various utility functions (like parallel, replaceAll and wildcardMatch). 

## Installation
```
$ npm install @darlean/app-lib
```

## Usage

* See: https://gitlab.com/darlean/darlean/-/wikis/NodeJS/Creating-an-App
* API Documentation for this library: https://docs.darlean.io/js/app-lib

## Support
Please email darlean@choofy.nl

## Roadmap
The roadmap is available at https://gitlab.com/groups/darlean/-/milestones

## License
This library is licensed under the Apache V2.0 license.
