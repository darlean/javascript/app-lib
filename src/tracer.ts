import { ISegment, ISegmentOptions, ITracer } from './services/tracing';
import * as uuid from 'uuid';
import { ITraceInfo } from './interfaces/tracing';
import * as perf from 'perf_hooks';

const base = perf.performance.timeOrigin;

function now() {
  return base + perf.performance.now();
}

export class Segment implements ISegment {
  public options: ISegmentOptions;
  protected tracer: Tracer;

  constructor(tracer: Tracer, parent: Segment | undefined, options: ISegmentOptions) {
    this.tracer = tracer;
    this.options = {
      ...options,
      correlationIds: options.correlationIds ?? parent?.options.correlationIds ?? [uuid.v4()],
      id: options.id ?? uuid.v4(),
      parentSegmentId: parent?.options.id,
      startMoment: options.startMoment ?? now(),
    };
  }

  public sub(options: ISegmentOptions): ISegment {
    const seg = new Segment(this.tracer, this, options);
    if (seg.options.id) {
      this.tracer._segments.set(seg.options.id, seg);
    }
    return seg;
  }

  public finish(): void {
    this.options.endMoment = now();
    this.tracer._finish(this);
  }

  public getCorrelationIds(): string[] {
    if (!this.options.correlationIds) {
      throw new Error('No correlationids');
    }
    return this.options.correlationIds;
  }
}

export class Tracer implements ITracer {
  public _segments: Map<string, Segment>;

  constructor() {
    this._segments = new Map();
  }

  public trace(options: ITraceInfo): ISegment {
    return new Segment(this, undefined, {
      correlationIds: options.correlationIds,
      id: options.parentSegmentId,
    });
  }

  public _finish(segment: Segment): void {
    if (segment.options.id) {
      this._segments.set(segment.options?.id, segment);
    }
  }

  public extract(): ISegment[] {
    const results = Array.from(this._segments.values());
    this._segments = new Map();
    return results;
  }
}
