/**
 * Under water, containers play a key role in Darlean because they allow efficient transfer
 * of text, binary and structured information between apps. But they are almost hidden from the
 * regular app developer that uses [[structs]] to directly interact with the contained binary,
 * text and structured data.
 *
 * > In essence, containers and [[structs]] deal with the same data, it is just that containers
 * are more low-level (and closely related to the underlying messaging layer) whereas [[structs]]
 * are more high-level and easier to use in application code.
 *
 * A container contains binary, text or structured data, or can be empty (contain no data). The
 * corresponding types are `Buffer`, `string`, `object` and `undefined`, respectively.
 * Containers can be nested because they can have zero or more child containers.
 *
 * > Example of nested containers: a typical Darlean network message consists of a top level
 * struct container (C0) with the name of the receiver node as data. A child struct container (C1) contains
 * metadata about the message (like to which node to send delivery receipts). This container C1 then has
 * a struct child container (C2) with the action request data (actor type, actor id, action name). And this
 * action request container C2 has a struct child container that finally contains the data that should be
 * passed as input to the actor. When the actor also needs additional information (like binary blobs of data),
 * they can be attached as children of C2.
 *
 * Containers can be serialized and deserialized very efficiently. In particular, containers make it
 * possible to only deserialize the information that your are interested in, instead of the entire message.
 * The same holds for serializing: when composing a new container that contains existing serialized containers as
 * their (sub)children, these (sub)children can be incorporated into the final container without the need to
 * serialize and deserialize them first.
 *
 * > Example: At the messaging layer, a node only has to read the `receiver` field of the top-level container
 * to determine to which node it should forward the message to.
 * To do so, it only has to deserialize the very small *data section* of that top-level container. It then can create a
 * new container (possibly with a different `recipient` value), and just attach all child containers to that
 * new container without having to serialize and deserialize those child containers first.
 *
 * Containers are created by means of the [[container]] function. It expects the container data (a string, Buffer,
 * object or undefined) as input, and optionally expects an array of child container-likes. (A [[ContainerLike]] is
 * a value that can already is, or can easily be converted into a container, like strings, Buffers, [[struct]] objects
 * and regular objects).
 *
 * A container implements [[IContainer]], which provides methods for accessing the container data and its child
 * containers.
 *
 * The binarization of containers is implemented in [[binaryencoding]].
 *
 * @module
 */
export type ContainerKind = 'empty' | 'binary' | 'text' | 'structure';

export type BufferEncoding = 'none' | 'tlv';

// eslint-disable-next-line @typescript-eslint/ban-types
export type AnObject = Object;

export type ContainerData = AnObject | Buffer | string | undefined;

export type IContainer0 = IContainer<ContainerData, ContainerData>;
export type IContainer1<T extends ContainerData> = IContainer<T, ContainerData>;

export interface IContainerizeable<A, B> {
  _cont(): IContainer<A, B>;
}
export type IContainerizeable0 = IContainerizeable<ContainerData, ContainerData>;
export type IContainerizeable1<D extends ContainerData> = IContainerizeable<D, ContainerData>;

export type ContainerLike<A extends ContainerData, B extends ContainerData> =
  | IContainer<A, B>
  | IContainerizeable<A, B>
  | A;
export type ContainerLike0 = ContainerLike<ContainerData, ContainerData>;
export type ContainerLike1<D> = ContainerLike<D, ContainerData>;

export interface IContainer<Type extends ContainerData, Child0Type extends ContainerData> {
  __container: () => boolean;
  getData(): Type | undefined;
  getDataStruct(): Type;
  getDataBinary(): Buffer;
  getDataText(): string;
  getKind(): ContainerKind;
  getChild0(): IContainer<Child0Type, ContainerData>;
  tryGetChild0(): IContainer<Child0Type, ContainerData> | undefined;
  getChild<Type>(idx: number): IContainer<Type, ContainerData>;
  getChildCount(): number;
  getBufferEncoding(): BufferEncoding;
  getBuffer(): Buffer | undefined;
}

export function isContainer(value: unknown): boolean {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  return (value as any)?.__container ?? false;
}

export function asContainer<Type extends ContainerData, Child0Type extends ContainerData>(
  value?: ContainerLike<Type, Child0Type>,
): IContainer<Type, Child0Type> {
  return isContainer(value)
    ? (value as IContainer<Type, Child0Type>)
    : (value as unknown as IContainerizeable<Type, Child0Type>)?._cont
    ? (value as unknown as IContainerizeable<Type, Child0Type>)._cont()
    : container<Type, Child0Type>(value as Type);
}

// Takes a "primitive" (string, buffer or object) and wraps it into a container, unless it is
// already "containerizeable" (in that case, the result of _cont() is returned).
export function container<Data extends ContainerData, Child0Data extends ContainerData>(
  data?: Data,
  children?: ContainerLike0[],
): IContainer<Data, Child0Data> {
  if ((data as IContainerizeable<Data, Child0Data>)?._cont) {
    return (data as IContainerizeable<Data, Child0Data>)._cont();
  }
  return new EditableContainer<Data, Child0Data>(data, undefined, children);
}

export class EditableContainer<Type extends ContainerData, Child0Type extends ContainerData>
  implements IContainer<Type, Child0Type>
{
  protected data?: Type;
  protected kind: ContainerKind;
  protected children?: ContainerLike<ContainerData, ContainerData>[];

  public __container: () => boolean = () => true;

  constructor(data: Type | undefined, kind?: ContainerKind, children?: ContainerLike0[]) {
    this.data = data;
    if (typeof data === 'object') {
      if ((data as IContainerizeable<Type, Child0Type>)._cont) {
        throw new Error('Contained data is already assigned to a container');
      }
      (data as IContainerizeable<Type, Child0Type>)._cont = () => {
        return this as IContainer<Type, Child0Type>;
      };
    }
    if (kind) {
      this.kind = kind;
    } else {
      this.kind = Buffer.isBuffer(data)
        ? 'binary'
        : typeof data === 'string'
        ? 'text'
        : data === undefined
        ? 'empty'
        : 'structure';
    }
    if (children) {
      for (const child of children) {
        this.addChild(child);
      }
    }
  }

  public addChild(value: ContainerLike0): void {
    if (!this.children) {
      this.children = [];
    }
    this.children.push(value);
  }

  public getData(): Type | undefined {
    return this.data;
  }

  public getDataStruct(): Type {
    return this.data as Type;
  }

  public getDataBinary(): Buffer {
    return this.data as Buffer;
  }

  public getDataText(): string {
    return this.data as string;
  }

  public getBufferEncoding(): BufferEncoding {
    return 'none';
  }

  public getKind(): ContainerKind {
    return this.kind;
  }

  public getChild<Type extends ContainerData>(idx: number): IContainer<Type, ContainerData> {
    if (this.children) {
      return asContainer<Type, ContainerData>(this.children[idx] as ContainerLike<Type, ContainerData>);
    }
    throw new Error(`No child: ${idx}`);
  }

  public getChild0(): IContainer<Child0Type, ContainerData> {
    if (this.children) {
      return asContainer<Child0Type, ContainerData>(this.children[0] as ContainerLike<Child0Type, ContainerData>);
    }
    throw new Error(`No child: 0`);
  }

  public tryGetChild0(): IContainer<Child0Type, ContainerData> | undefined {
    if (this.children) {
      return asContainer<Child0Type, ContainerData>(this.children[0] as ContainerLike<Child0Type, ContainerData>);
    }
  }

  public getChildCount(): number {
    return this.children?.length || 0;
  }

  public getBuffer(): Buffer | undefined {
    return undefined;
  }
}
