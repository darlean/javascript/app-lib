/**
 * Websocket transport implementation that uses the `ws` library both for server connections
 * and for client connections.
 *
 * The `ws` library is a pure-javascript library, which may be slower than the alternative
 * `uws` library that is used in `wstransport` (without the `2`), but it has the advantage
 * of using the nodejs thread helpers, which reduces the time the main loop is blocked, which
 * is good for overall performance.
 */
import { ILogger } from './logging';
import {
  ITransport,
  INetworkMessage,
  EditableNetworkMessage,
  ContainerNetworkMessage,
  INetworkHeader,
  INetworkEnvelope,
} from './networktypes';
import { WebSocket, WebSocketServer } from 'ws';
import { BaseTransport, IPeerInfo } from './basetransport';
import { IPeerConfig } from './types';
import { decodeFromBinaryBuffer, encodeToBinaryBuffers } from './binaryencoding';
import * as net from 'net';

const PROTO_VERSION = '1.0';
const MAX_MESSAGE_SIZE = 10 * 1000 * 1000;

type WsMessage = Buffer;

type ISocketInfo = {
  sender?: string;
  pending: number;
};

interface IWsMessage {
  version: string;
  sender: string;
  blobs: { id: string; size: number }[];
}

function getRawSocket(socket: WebSocket): net.Socket {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  return (socket as any)._socket as net.Socket;
}

export class WsTransport extends BaseTransport<WebSocket, WebSocket> implements ITransport {
  private listenSocket?: WebSocketServer;
  private incomingSockets: Map<unknown, ISocketInfo>;
  private incomingWaiters: Map<unknown, Array<{ callback?: (error: unknown) => void; message?: WsMessage }>>;
  private peerReconnectTimers: Map<string, NodeJS.Timeout>;

  constructor(logger: ILogger, id: string, address: string, peers: IPeerConfig[]) {
    super(logger, id, address, peers);
    this.incomingSockets = new Map();
    this.incomingWaiters = new Map();
    this.peerReconnectTimers = new Map();
  }

  public async initIn(): Promise<void> {
    if (this.address) {
      await this.launchIncoming(this.address);
    }
  }

  public async initOut(): Promise<void> {
    for (const peer of this.peers.values()) {
      this.launchOutgoing(peer);
    }
  }

  protected async closeImpl(): Promise<void> {
    this.active = false;

    for (const timer of this.peerReconnectTimers.values()) {
      clearTimeout(timer);
    }
    this.peerReconnectTimers.clear();

    if (this.listenSocket) {
      this.logger.deep('Stopping WS server');
      this.listenSocket.close();
    }

    this.logger.deep('Closing open connections');
    for (const peer of this.peers.values()) {
      if (peer.incoming) {
        this.logger.deep('Terminating incoming socket from [Peer]', () => ({ Peer: peer.id }));
        peer.incoming.close(1012, 'Server is stopping');
        peer.incoming = undefined;
      }
      if (peer.outgoing) {
        this.logger.deep('Terminating outgoing socket to [Peer]', () => ({ Peer: peer.id }));
        peer.outgoing.close(1012, 'Server is stopping');
        peer.outgoing = undefined;
      }
    }
    this.logger.debug('Closed network');
  }

  protected async launchIncoming(address: string): Promise<number> {
    try {
      const wss = new WebSocketServer({ port: parseInt(address.split(':')[1], 10), maxPayload: MAX_MESSAGE_SIZE });
      wss.on('connection', (ws) => {
        this.logger.info('Incoming connection to [B]', () => ({ B: this.id }));
        if (!this.active) {
          ws.close(1012, 'SHUTTING_DOWN');
        }
        // TODO: Close the connection when no initial message is received within certain time

        this.incomingSockets.set(ws, {
          sender: undefined,
          pending: 0,
        });

        ws.binaryType = 'nodebuffer';
        ws.on('close', () => {
          // console.log('SERVER SIDE CLOSED');
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          ((ws as any)._socket as net.Socket).uncork();
          // clearTimeout(timer);
          const waiting = this.incomingWaiters.get(ws);
          if (waiting) {
            for (const waiter of waiting) {
              const e = new Error('Stream closed');
              if (waiter.callback) {
                waiter.callback(e);
              }
            }
            this.incomingWaiters.delete(ws);
          }

          const peerid = this.incomingSockets.get(ws)?.sender ?? '';
          this.logger.info('Incoming websocket from [Sender] closed', () => ({ Sender: peerid }));
          if (peerid) {
            this.incomingSockets.delete(ws);
            const peer = this.peers.get(peerid);
            if (peer) {
              peer.incoming = undefined;
            }
          }
        });

        ws.on('message', (value) => {
          try {
            const buffer = value as Buffer;

            let sender = this.incomingSockets.get(ws)?.sender;
            if (sender === undefined) {
              const info = JSON.parse(buffer.toString()) as IWsMessage;
              sender = info.sender;
              this.logger.deep('Received sender [Sender]', () => ({ Sender: sender }));
              this.incomingSockets.set(ws, { sender, pending: 0 });
              const peer = this.peers.get(sender);
              if (!peer) {
                const peer2: IPeerInfo<WebSocket, WebSocket> = {
                  id: sender,
                  incoming: ws,
                  kind: 'client',
                };
                this.peers.set(sender, peer2);
                this.logger.debug('Added new incoming node [Id]', () => ({ Id: sender }));
              } else {
                peer.incoming = ws;
                this.logger.deep('Received incoming stream from known node [Id]', () => ({ Id: sender }));
              }
              return;
            }

            const message = this.messageToTransportMessage(buffer);
            this.logger.deep('Incoming socket received message [Message] from [Sender]', () => ({
              Message: message,
              Sender: sender ?? '',
            }));

            if (this.handler) {
              const handler = this.handler;
              process.nextTick(async () => {
                try {
                  await handler(message);
                } catch (e) {
                  this.logger.error('Unexpected error during handling of message: [Error]', () => ({ Error: e }));
                }
              });
            }
          } catch (e) {
            this.logger.error('Error in handling socket data: [Error]', () => ({ Error: e }));
          }
        });
      });

      this.listenSocket = wss;
      return 0;
    } catch (e) {
      this.logger.warning('Failed to listen on [Address]: [Error]', () => ({ Address: address, Error: e }));
      throw e;
    }
  }

  protected writeOutgoing(socket: WebSocket, message: EditableNetworkMessage, cb?: (error: unknown) => void): void {
    // console.log('WRITE OUTGOING');
    const msg = this.messageToProtoMessage(message);
    if (msg.length > MAX_MESSAGE_SIZE) {
      throw new Error(
        `Message size ${msg.length} is larger than max allowed message size of ${MAX_MESSAGE_SIZE} bytes. The message is not sent.`,
      );
    }
    const sock = getRawSocket(socket);
    if (sock.writableCorked === 0) {
      process.nextTick(() => sock.uncork());
      sock.cork();
    }
    socket.send(msg, { binary: true }, cb);
  }

  protected writeIncoming(socket: WebSocket, message: EditableNetworkMessage, cb?: (error: unknown) => void): void {
    const msg = this.messageToProtoMessage(message);
    if (msg.length > MAX_MESSAGE_SIZE) {
      throw new Error(
        `Message size ${msg.length} is larger than max allowed message size of ${MAX_MESSAGE_SIZE} bytes. The message is not sent.`,
      );
    }
    //const info = this.incomingSockets.get(socket);
    //if (info) {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const sock = getRawSocket(socket);
    if (sock.writableCorked === 0) {
      process.nextTick(() => sock.uncork());
      sock.cork();
    }
    socket.send(msg, { binary: true }, cb);
  }

  protected launchOutgoing(peer: IPeerInfo<WebSocket, WebSocket>): void {
    this.peerReconnectTimers.delete(peer.id);
    if (!this.active) {
      return;
    }

    if (!peer.address) {
      return;
    }

    if (peer.outgoing) {
      this.logger.error('PEER [Peer] ALREADY HAS OUTGOING', () => ({ Peer: peer.id }));
      return;
    }

    this.logger.debug('Launching outgoing from [Self] to [Peer]', () => ({ Self: this.id, Peer: peer.id }));

    try {
      const socket = new WebSocket('ws://' + peer.address, {
        maxPayload: MAX_MESSAGE_SIZE,
      });
      socket.on('open', () => {
        this.logger.info('Connected from [A] to [B]', () => ({ A: this.id, B: peer.id }));
        if (peer.outgoing) {
          this.logger.error('Peer already has outgoing');
          peer.outgoing.close();
        }
        peer.outgoing = socket;

        const raw = Buffer.from(
          JSON.stringify({
            sender: this.id,
            version: PROTO_VERSION,
          } as IWsMessage),
        );
        socket.send(raw, { binary: true });
      });

      socket.on('message', (data: Buffer) => {
        try {
          // console.log('INCOMING MSG');
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          //(socket as any).pause();
          const message = data;
          this.logger.deep('Outgoing socket received message [Message]', () => ({ Message: message }));

          if (this.handler) {
            const msg2 = this.messageToTransportMessage(message);
            this.handler(msg2)
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              .then(() => (socket as any).resume())
              .catch((e) => {
                this.logger.error('Unexpected error during handling of message: [Error]', () => ({ Error: e }));
              });
          } else {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            (socket as any).resume();
          }
        } catch (e) {
          this.logger.debug('Error in logger data: [Error]', () => ({ Error: e }));
        }
      });

      socket.on('error', (e) => {
        // TODO: Emit
        this.logger.debug('Socket encountered error: [Error]', () => ({ Error: e }));
        // console.log("ERROR", e);
      });

      socket.on('close', () => {
        try {
          // console.log('CLOSED');
          socket.close();
          if (peer.outgoing) {
            peer.outgoing = undefined;
          }
          if (this.active) {
            const timer = this.peerReconnectTimers.get(peer.id);
            if (!timer) {
              this.logger.debug('Outgoing socket disconnected, trying to reconnect in 1 second');
              this.peerReconnectTimers.set(
                peer.id,
                setTimeout(() => this.launchOutgoing(peer), 1000),
              );
            }
          } else {
            this.logger.info('Outgoing socket disconnected (will not retry, we are inactive)');
          }
        } catch (e) {
          this.logger.debug('Error in socket.end: [Error]', () => ({ Error: e }));
        }
      });
    } catch (e) {
      // TODO: emit
      this.logger.debug('Socket connect error: [Error]', () => ({ Error: e }));
    }
  }

  protected messageToProtoMessage(msg: EditableNetworkMessage): WsMessage {
    const container = msg.toContainer();
    const buffers: Buffer[] = [];
    encodeToBinaryBuffers(container, buffers);
    const buffer = Buffer.concat(buffers);
    return buffer;
  }

  protected messageToTransportMessage(msg: WsMessage): INetworkMessage {
    const container = decodeFromBinaryBuffer<INetworkHeader, INetworkEnvelope>(msg, 0);
    return new ContainerNetworkMessage(container);
  }
}
