import { IActorCreateOptions, IAppInfo, IShutdownInfo } from '../..';

import * as af from '../../actorfactory';

interface IActorInfo {
  actorType: string;
  // List of apps for which this actor is enabled
  apps: Map<string, boolean>;
  dependencies: string[];
}

export class ShutdownActor extends af.BaseActor {
  protected fullAppId: string;
  protected actorInfos: Map<string, IActorInfo>;
  protected usedActors: Map<string, boolean>;
  protected status: 'running' | 'shuttingdown';

  constructor(options: IActorCreateOptions & { fullAppId: string }) {
    super(options);
    this.status = 'running';
    this.fullAppId = options.fullAppId;
    this.actorInfos = new Map();
    this.usedActors = new Map();
  }

  public async PublishInfo(info: IAppInfo): Promise<void> {
    let changed = false;

    for (const actor of info.supportedActors) {
      let ourInfo = this.actorInfos.get(actor.name);
      if (!ourInfo) {
        ourInfo = {
          actorType: actor.name,
          apps: new Map(),
          dependencies: [],
        };
        this.actorInfos.set(actor.name, ourInfo);
      }

      if (actor.status === 'disabled') {
        this.c.deep('Encountered disabled [Id] [CurrentApps]', () => ({
          Id: info.id,
          CurrentApps: Array.from(ourInfo?.apps.keys() ?? []),
        }));
        if (ourInfo.apps.has(info.id)) {
          ourInfo.apps.delete(info.id);
          if (ourInfo.apps.size === 0) {
            changed = true;
          }
        }
      } else {
        ourInfo.apps.set(info.id, true);
        ourInfo.dependencies = actor.dependencies;
        changed = true;
      }
    }

    this.c.deep('Halfway destroy info [Changed] [ActorInfos]', () => ({
      Changed: changed,
      ActorInfos: this.actorInfos.values(),
    }));

    if (changed) {
      const used: Map<string, boolean> = new Map();
      for (const ourInfo of this.actorInfos.values()) {
        if (ourInfo.apps.size > 0) {
          for (const dep of ourInfo.dependencies) {
            used.set(dep, true);
          }
        }
      }
      this.usedActors = used;
    }
  }

  public async GetShutdownInfo(): Promise<IShutdownInfo> {
    return {
      status: this.status,
      actorsBeingUsed: Array.from(this.usedActors.keys()),
    };
  }

  public async Shutdown(): Promise<void> {
    this.status = 'shuttingdown';
  }
}
