import {
  action,
  encodeKey,
  IActionContext,
  IActorCreateOptions,
  IAppInfo,
  IShutdownInfo,
  parallel,
  service,
  sleep,
} from '../..';

import { performance } from 'perf_hooks';

import * as af from '../../actorfactory';
import { ISingleLock } from '../locks/lockactor';
import { IRetrieveDispatchInfoRequest, IRetrieveDispatchInfoResponse } from '../../interfaces/dispatching';
import {
  IAcquireLockRequest,
  IAcquireLockResponse,
  IAggregatedSingleLock,
  IGetLockInfoRequest,
  IGetLockInfoResponse,
  IReleaseLockRequest,
} from '../../interfaces/locking';

// Strange as it may seem, normally you do not want retries when a lock is held by
// another holder, because it is an indication that the request is sent to the wrong
// receiver. Should you want to retry, the entire (larger) command should be retried,
// not just a retry within the lock acquire.
const DEFAULT_RETRY_TIME = 0;

export interface IRuntimeServiceCreateOptions {
  fullAppId: string;
  lockNodes: string[];
  dispatchNodes: string[];
}

@service({
  name: 'RuntimeService',
  locking: 'shared',
  dependencies: ['LockActor', 'DispatchService', 'ShutdownService'],
})
export class RuntimeService extends af.BaseActor {
  private fullAppId: string;
  private lockNodes: string[];
  private dispatchNodes: string[];
  private majority: number;

  constructor(options: IActorCreateOptions & IRuntimeServiceCreateOptions) {
    super(options);
    this.fullAppId = options.fullAppId;
    this.lockNodes = options.lockNodes;
    this.dispatchNodes = options.dispatchNodes;
    this.majority = Math.ceil((this.lockNodes.length + 0.75) * 0.5);
  }

  @action({})
  public async AcquireLock(data: IAcquireLockRequest, context: IActionContext): Promise<IAcquireLockResponse> {
    let delay = 0;
    const maxTime = performance.now() + (data.retryTime ?? DEFAULT_RETRY_TIME);
    const start = data.singleStage ? 1 : 0;
    for (let round = start; round < 10; round++) {
      const ttl = round === 0 ? 100 : data.ttl;

      const result = await this.AcquireLockRound(data, context, ttl);
      this.c.debug('After lock round for [Id] by [Requester]: [Duration]', () => ({
        Id: data.id,
        Requester: data.requester,
        Duration: result.duration,
      }));
      if (result.duration > 0) {
        if (round > 0) {
          return result;
        }
      } else {
        delay = delay === 0 ? 25 : delay * 2;

        const actualDelay = (0.5 + 0.5 * Math.random()) * delay;
        const now = performance.now();
        const newTime = now + actualDelay;
        if (newTime > maxTime) {
          return {
            duration: -1,
            holders: result.holders,
          };
        }
        await sleep(actualDelay);
      }
    }

    return {
      duration: -1,
      holders: [],
    };
  }

  @action({})
  public async ReleaseLock(data: IReleaseLockRequest, context: IActionContext): Promise<void> {
    await parallel<void, undefined>(
      this.lockNodes.map((node) => {
        return async () => {
          await context.performAction<IReleaseLockRequest, void>({
            actorType: 'LockActor',
            actorId: [node],
            actionName: 'ReleaseLock',
            data,
            receivers: [node],
            retryCount: 0,
          });
        };
      }),
      1000,
    );
  }

  @action({})
  public async InspectLocks(_data: void, context: IActionContext): Promise<IAggregatedSingleLock[]> {
    const results = await parallel<ISingleLock[], number>(
      this.lockNodes.map((node) => {
        return async () => {
          const info = await context.performAction<void, ISingleLock[]>({
            actorType: 'LockActor',
            actorId: [node],
            actionName: 'Inspect',
            receivers: [node],
            retryCount: 0,
          });
          return info;
        };
      }),
      1000,
    );

    const map = new Map<string, IAggregatedSingleLock>();
    let idx = -1;
    for (const result of results.results) {
      idx++;
      const source = this.lockNodes[idx];
      if (result.result) {
        for (const lock of result.result) {
          const id = encodeKey(lock.id);
          let existing = map.get(id);
          if (!existing) {
            existing = {
              id: lock.id,
              holders: [],
            };
            map.set(id, existing);
          }
          existing.holders.push({
            holder: lock.holder,
            ttl: lock.ttl,
            source,
          });
        }
      }
    }
    return Array.from(map.values());
  }

  @action({})
  public async GetLockInfo(data: IGetLockInfoRequest, context: IActionContext): Promise<IGetLockInfoResponse> {
    let n = 0;
    const majority = this.majority;
    const holders: string[] = [];

    await parallel<IGetLockInfoResponse, number>(
      this.lockNodes.map((node) => {
        return async (abort) => {
          const info = await context.performAction<IGetLockInfoRequest, IGetLockInfoResponse>({
            actorType: 'LockActor',
            actorId: [node],
            actionName: 'GetLockInfo',
            data,
            receivers: [node],
            retryCount: 0,
          });
          for (const holder of info.holders) {
            if (!holders.includes(holder)) {
              holders.push(holder);
            }
          }
          n++;
          if (n >= majority) {
            abort();
          }
          return info;
        };
      }),
      1000,
    );
    return {
      holders,
    };
  }

  @action({})
  public async InspectActorInfo(data: void, context: IActionContext): Promise<IAppInfo[]> {
    return await context.performAction<void, IAppInfo[]>({
      actorType: 'DispatchActor',
      actorId: [context.fullAppId],
      actionName: 'Inspect',
      receivers: [context.fullAppId],
    });
  }

  @action({})
  public async PublishInfo(info: IAppInfo, context: IActionContext): Promise<void> {
    await parallel(
      this.dispatchNodes.map((node) => {
        return async () => {
          await context.performAction({
            actorType: 'DispatchActor',
            actorId: [node],
            actionName: 'PublishInfo',
            data: info,
            receivers: [node],
            retryCount: 0,
          });
        };
      }),
      1000,
    );

    await context.performAction(
      // await context.forward(
      {
        actorType: 'ShutdownActor',
        actorId: [],
        actionName: 'PublishInfo',
        data: info,
        retryCount: 0,
      },
    );
  }

  @action({})
  public async RetrieveDispatchInfo(
    data: IRetrieveDispatchInfoRequest,
    context: IActionContext,
  ): Promise<IRetrieveDispatchInfoResponse> {
    const result = await context.performAction<IRetrieveDispatchInfoRequest, IRetrieveDispatchInfoResponse>({
      actorType: 'DispatchActor',
      actorId: [context.fullAppId],
      actionName: 'RetrieveDispatchInfo',
      data,
      receivers: [this.fullAppId],
      retryCount: 0,
    });
    return result;
  }

  public async GetShutdownInfo(data: void, context: IActionContext): Promise<IShutdownInfo> {
    return await context.performAction<void, IShutdownInfo>({
      actorType: 'ShutdownActor',
      actorId: [],
      actionName: 'GetShutdownInfo',
      retryCount: 0,
    });
  }

  public async Shutdown(data: void, context: IActionContext): Promise<void> {
    await context.performAction<void, void>({
      actorType: 'ShutdownActor',
      actorId: [],
      actionName: 'Shutdown',
    });
  }

  protected async AcquireLockRound(
    data: IAcquireLockRequest,
    context: IActionContext,
    ttl: number,
  ): Promise<IAcquireLockResponse> {
    let n = 0;
    const majority = this.majority;
    const holders: string[] = [];
    let duration: number | undefined;

    const results = await parallel<IAcquireLockResponse, number>(
      this.lockNodes.map((node) => {
        return async (abort) => {
          const info = await context.performAction<IAcquireLockRequest, IAcquireLockResponse>({
            actorType: 'LockActor',
            actorId: [node],
            actionName: 'AcquireLock',
            data,
            ttl,
            receivers: [node],
            retryCount: 0,
          });
          if (info.duration > 0) {
            n++;
            duration = duration === undefined ? info.duration : Math.min(duration, info.duration);
            if (n >= majority) {
              abort(duration);
            }
          } else {
            for (const holder of info.holders) {
              if (!holders.includes(holder)) {
                holders.push(holder);
              }
            }
            abort(-1);
          }
          return info;
        };
      }),
      1000,
    );

    if ((results.result ?? 0) > 0) {
      for (let idx = 0; idx < this.lockNodes.length; idx++) {
        const node = this.lockNodes[idx];
        const result = results.results[idx];
        if (result.result !== undefined && result.result.duration <= 0) {
          process.nextTick(async () => {
            try {
              context.performAction<IReleaseLockRequest, void>({
                actorType: 'LockActor',
                actorId: [node],
                actionName: 'ReleaseLock',
                data,
                receivers: [node],
                retryCount: 0,
              });
            } catch (e) {
              this.c.deep('Unable to release refused lock for [ActorId] on [Node]: [Error]', () => ({
                ActorId: data.id,
                Node: node,
                Error: e,
              }));
            }
          });
        }
      }
      return {
        duration: results.result ?? 0,
        holders: [data.requester],
      };
    }

    await parallel<void, undefined>(
      this.lockNodes.map((node) => {
        return async () => {
          await context.performAction<IReleaseLockRequest, void>({
            actorType: 'LockActor',
            actorId: [node],
            actionName: 'ReleaseLock',
            data,
            receivers: [node],
          });
        };
      }),
      1000,
    );
    return {
      duration: -1,
      holders,
    };
  }
}
