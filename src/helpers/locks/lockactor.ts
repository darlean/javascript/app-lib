import * as af from '../../actorfactory';
import { ActorRunnerTime } from '../../actorrunnertime';
import {
  IAcquireLockRequest,
  IAcquireLockResponse,
  IGetLockInfoRequest,
  IGetLockInfoResponse,
  IReleaseLockRequest,
} from '../../interfaces/locking';
import { SimpleLock } from './simplelock';

export interface ISingleLock {
  id: string[];
  holder?: string;
  ttl: number;
}

export class LockActor extends af.BaseActor {
  private lock?: SimpleLock;
  private time?: ActorRunnerTime;

  public async activate(): Promise<void> {
    this.time = new ActorRunnerTime(this.c.getChildLogger('Time'));
    this.lock = new SimpleLock(this.c.getChildLogger('LockImpl'), this.time, 60 * 1000);
  }

  public async deactivate(): Promise<void> {
    this.lock?.stop();
  }

  public async AcquireLock(data: IAcquireLockRequest): Promise<IAcquireLockResponse> {
    const result = await this.lock?.acquireLock(data.id, data.requester, data.ttl);
    return {
      duration: result?.[0] ?? 0,
      holders: result?.[1] ?? [],
    };
  }

  public async ReleaseLock(data: IReleaseLockRequest): Promise<void> {
    await this.lock?.releaseLock(data.id, data.requester);
  }

  public async GetLockInfo(data: IGetLockInfoRequest): Promise<IGetLockInfoResponse> {
    const result = await this.lock?.getLockHolders(data.id);
    return {
      holders: result ?? [],
    };
  }

  public async Inspect(): Promise<ISingleLock[]> {
    const result = this.lock?.inspect();
    return (
      result?.map((v) => {
        return {
          id: v.id,
          holder: v.holder,
          ttl: v.ttl,
        } as ISingleLock;
      }) || []
    );
  }
}
