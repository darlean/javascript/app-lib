import { encodeKey } from '../../util';
import { ILogger } from '../../logging';
import { ITime } from '../../types';

interface ILockInfo {
  id: string[];
  holder: string;
  until: number;
  timer: NodeJS.Timeout;
}

export interface ILockInfo2 {
  id: string[];
  holder: string;
  ttl: number;
}

// Implementation of a lock that simply uses an in-memory map. No redundancy
// (communication with other lock instances) is supported.
export class SimpleLock {
  protected locks: Map<string, ILockInfo>;
  protected duration: number;
  public time: ITime;
  public logger: ILogger;

  constructor(logger: ILogger, time: ITime, duration: number = 60 * 1000) {
    this.time = time;
    this.locks = new Map();
    this.duration = duration;
    this.logger = logger;
  }

  public stop(): void {
    for (const lock of this.locks.values()) {
      clearTimeout(lock.timer);
    }
    this.locks.clear();
  }

  public async acquireLock(id: string[], requester: string, ttl: number): Promise<[number, string[]]> {
    this.logger.deep('Try to acquire lock for [Id] by [Requester]', () => ({ Id: id, Requester: requester }));
    const idAsString = encodeKey(id);
    const current = this.locks.get(idAsString);
    const now = this.time.machineTicks();
    if (current) {
      if (current.until > now) {
        if (current.holder !== requester) {
          this.logger.deep('Rejected');
          return [0, [current.holder]];
        }
      }
    }

    const duration = ttl ?? this.duration;

    if (current) {
      clearTimeout(current.timer);
    }

    this.locks.set(idAsString, {
      holder: requester,
      id,
      until: now + duration,
      timer: setTimeout(() => {
        this.locks.delete(idAsString);
      }, 2 * duration),
    });
    this.logger.deep('Granted');
    return [duration, [requester]];
  }

  public async releaseLock(id: string[], requester: string): Promise<boolean> {
    const idAsString = encodeKey(id);
    const current = this.locks.get(idAsString);
    if (current) {
      if (current.holder === requester) {
        clearTimeout(current.timer);
        this.locks.delete(idAsString);
        return true;
      }
    } else {
      return true;
    }
    return false;
  }

  public async getLockHolders(id: string[]): Promise<string[]> {
    const idAsString = encodeKey(id);
    const current = this.locks.get(idAsString);
    const now = this.time.machineTicks();
    if (current) {
      if (current.until > now) {
        return [current.holder];
      }
    }
    return [];
  }

  public inspect(): ILockInfo2[] {
    const now = this.time.machineTicks();
    return Array.from(this.locks.values())
      .filter((v) => v.until >= now)
      .map((v) => {
        return {
          id: v.id,
          holder: v.holder,
          ttl: v.until - now,
        } as ILockInfo2;
      });
  }
}
