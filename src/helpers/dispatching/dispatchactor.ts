import {
  action,
  actor,
  DarleanError,
  ensureDarleanError,
  IActionContext,
  IActorCreateOptions,
  IAppInfo,
  IAttributes,
  IRuntime,
  ISupportedActor,
} from '../..';
import * as af from '../../actorfactory';
import { IRetrieveDispatchInfoRequest, IRetrieveDispatchInfoResponse } from '../../interfaces/dispatching';
import { IGetLockInfoResult } from '../../services/locking';

const MAX_CACHE_SIZE = 10000;

export function RuntimeError(
  codes: string[],
  message: string,
  attributes?: IAttributes,
  nested?: DarleanError[],
): DarleanError {
  return new DarleanError(['RUNTIME'].concat(codes), message, attributes, undefined, nested);
}

export function DispatchError(
  code: DispatchErrorCode,
  message: string,
  attributes?: IAttributes,
  nested?: DarleanError[],
): DarleanError {
  return RuntimeError(['DISPATCH'].concat([code]), message, attributes, nested);
}

export type DispatchErrorCode = 'ACTOR_TYPE_NOT_REGISTERED' | 'UNEXPECTED_DISPATCH_ERROR' | 'UNEXPECTED_PERFORM_ERROR';

@actor({
  name: 'DispatchActor',
  locking: 'shared',
  dependencies: ['ShutdownService'],
  appBindIndex: -1,
})
export class DispatchActor extends af.BaseActor {
  protected appInfos: Map<string, IAppInfo>;
  protected supportedActors: Map<string /*actortype*/, Map<string /*appid*/, ISupportedActor>>;
  // protected apps: Map<string, IApp>;
  // protected lock: ISharedLock;
  // Map with for each actor type, the most recently received information.
  // Note that this information contains 'static' data (that is the same
  // for each app) as well as 'dynamic' (like availability) that is different
  // between apps and only applies to the latest received info.
  protected actorInfo: Map<string /*actortype*/, ISupportedActor>;
  protected runtime: IRuntime;
  protected fullAppId: string;
  protected actorCache: Map<string, string[]>;

  constructor(options: IActorCreateOptions & { runtime: IRuntime; fullAppId: string; actors: ISupportedActor[] }) {
    super(options);
    this.runtime = options.runtime;
    this.fullAppId = options.fullAppId;
    this.appInfos = new Map();
    this.supportedActors = new Map();
    // this.apps = new Map();
    this.actorInfo = new Map();
    this.actorCache = new Map();
    this.addSupportedActors(options.fullAppId, options.actors);
  }

  // Receives information about a certain app.
  @action({
    name: 'PublishInfo',
  })
  public async PublishInfo(info: IAppInfo): Promise<void> {
    this.c.debug('Received app info for [App]: [Info]', () => ({ App: info.id, Info: info }));
    this.appInfos.set(info.id, info);
    this.addSupportedActors(info.id, info.supportedActors);
  }

  @action({
    name: 'Inspect',
  })
  public async Inspect(): Promise<IAppInfo[]> {
    return Array.from(this.appInfos.values());
  }

  // Returns app-errors as part of the result, or throws an Error for technical
  // failures of the runtime.
  @action({
    name: 'RetrieveDispatchInfo',
  })
  public async retrieveDispatchInfo(
    data: IRetrieveDispatchInfoRequest,
    context: IActionContext,
  ): Promise<IRetrieveDispatchInfoResponse> {
    const logger = this.c.getChildLogger('io.darlean.dispatchactor.RetrieveDispatchInfo.' + data.actorType);
    try {
      // TODO: Take into account that after every async call, our internal administration
      // of actors may be have changed by a parallel (in-between) call to PublishInfo.

      logger.debug('Dispatching [Action]', () => ({ Action: data }));

      const actorType = data.actorType;
      if (!actorType) {
        throw new Error('Dispatch request must contain an actorType');
      }

      const actorId = data.actorId;
      if (actorId === undefined) {
        throw new Error('Dispatch request must contain an actorId');
      }

      const actorInfo = this.actorInfo.get(actorType);
      if (!actorInfo) {
        throw DispatchError(
          'ACTOR_TYPE_NOT_REGISTERED',
          'Actor type [ActorType] is not registered to the runtime by any app',
          { ActorType: actorType },
        );
      }

      const boundNodes = [];
      const boundNode =
        actorInfo.appBindIndex === undefined
          ? undefined
          : actorInfo.appBindIndex >= 0
          ? actorId[actorInfo.appBindIndex]
          : actorId[actorId.length + actorInfo.appBindIndex];
      if (boundNode !== undefined && boundNode !== '') {
        boundNodes.push(boundNode);
      }

      if (boundNodes.length === 1) {
        return {
          apps: boundNodes,
          multiplicity: 'single',
        };
      }

      if (actorInfo.multiplicity === 'single') {
        const globalLockId = [actorType].concat(actorId);

        let lockInfo: IGetLockInfoResult | undefined;

        const logger3 = logger.getChildLogger('io.darlean.dispatchactor.getLockInfo');
        try {
          try {
            lockInfo = await this.runtime.getLockInfo(globalLockId, {
              actionHopsRemaining: context.hopsRemaining,
            });
          } catch (e) {
            this.c.info('Error while obtaining lock info while dispatching [Action]: [Error]', () => ({
              Action: action,
              Error: e,
            }));
            throw DispatchError('UNEXPECTED_DISPATCH_ERROR', 'Unable to obtain lock info', undefined, [
              ensureDarleanError(e),
            ]);
          }
        } finally {
          logger3.finish();
        }

        logger.debug('Apps currently holding lock are [CurrentApps]', () => ({ CurrentApps: lockInfo?.holders }));

        if (lockInfo.holders.length >= 1) {
          return {
            apps: lockInfo.holders,
            multiplicity: 'single',
          };
        }
      }

      const map = this.supportedActors.get(actorType);
      if (map) {
        const candidates: string[] = [];
        for (const [appid, info] of map.entries()) {
          this.c.debug('MAP ENTRY [AppId] [Info]', () => ({ AppId: appid, Info: info }));
          if (info.status === 'enabled') {
            if (!boundNode || appid === boundNode) {
              const availability = info.availability || 0;
              if (actorInfo.multiplicity === 'multiple' || availability > 0) {
                candidates.push(appid);
              }
            }
          }
        }
        if (candidates.length > 0) {
          return {
            apps: candidates,
            multiplicity: actorInfo.multiplicity,
          };
        }
      }

      return {
        apps: [],
        multiplicity: actorInfo.multiplicity,
      };
    } finally {
      logger.finish();
    }
  }

  protected addSupportedActors(appId: string, actors: ISupportedActor[]): void {
    for (const a of actors) {
      // TODO: Do this based on timestamp or version field, only if newer to avoid pingpong during updates
      this.actorInfo.set(a.name, a);
      let current = this.supportedActors.get(a.name);
      if (!current) {
        this.c.debug('MAKING CURRENT');
        current = new Map();
        this.supportedActors.set(a.name, current);
      }
      this.c.debug('ADDING TO CURRENT: [Value], [Old]', () => ({
        Value: appId,
        Old: Array.from(current?.keys() ?? []),
      }));
      current.set(appId, a);
      this.c.debug('ADDED TO CURRENT: [Value]', () => ({ Value: Array.from(current?.keys() ?? []) }));
    }
  }

  protected addToActorCache(flatId: string, appNames: string[]): void {
    if (this.actorCache.size > MAX_CACHE_SIZE) {
      let remaining = Math.round(MAX_CACHE_SIZE * 0.1);
      for (const key of this.actorCache.keys()) {
        this.actorCache.delete(key);
        remaining--;
        if (remaining < 0) {
          break;
        }
      }
    }
    this.actorCache.set(flatId, appNames);
  }
}
