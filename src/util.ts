import { IAttributes } from './types';
import { IPerformActionReq } from './interfaces/performing';

/**
 * Sleeps for [[ms]] milliseconds. When ms is 0, `setImmediate` is used so that the function
 * returns immediately after giving the NodeJS event loop the chance to process pending events.
 * @param ms The amount of milliseconds to wait
 */
export async function sleep(ms: number): Promise<void> {
  if (ms <= 0) {
    return new Promise((resolve) => {
      setImmediate(resolve);
    });
  }
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

/**
 * Replaces all occurrances of `search` within `input` with `replace`. The replacement is
 * performed in one sweep (when the result of the replacement contains occurrances of
 * 'input', they are not replaced again).
 * @param input The text in which the search and replace should take place
 * @param search The text to search for
 * @param replace The text to replace the search text with
 * @returns The input with all occurrances of search replaced by replace.
 */
export function replaceAll(input: string, search: string, replace: string): string {
  return input.split(search).join(replace);
}

/**
 * Performs a wildcard match on [[input]]. The [[mask]] can contain zero or more occurrances
 * of the wildcard character (`*`).
 * @param input The text that should be evaluated against the mask
 * @param mask The mask that is evaluated against the input
 * @returns Whether text matches with the mask.
 */
export function wildcardMatch(input: string, mask: string): boolean {
  const parts = mask.split('*');
  let lastIdx = -1;
  if (!input.startsWith(parts[0] || '')) {
    return false;
  }
  if (!input.endsWith(parts[0] || '')) {
    return false;
  }
  lastIdx = parts[0].length;
  for (let partIdx = 1; partIdx < parts.length - 1; partIdx++) {
    lastIdx = input.indexOf(parts[partIdx], lastIdx);
    if (lastIdx < 0) {
      return false;
    }
  }
  return true;
}

/**
 * Encodes an actor key into a string. Encoding is performed in such a way that later decoding
 * produces exactly the same key as result, and that lexicographical ordering still preserves the
 * original order.
 *
 * Warning: This function uses unicode characters 1-3 as delimiters and escape characters, so the output
 * of this function should only be used on places where these characters are accepted. Also, no escaping
 * of other 'typically unsafe' characters like `/` is performed. In particular, this means that the encoded
 * keys should not be used to create files/folders on file systems or web services like S3.
 * @param parts The key parts
 * @returns The encoded string
 */
export function encodeKey(parts: string[]): string {
  return parts
    .map((v) => {
      let replaced = replaceAll(v, '\u0001', '\u0001\u0002');
      replaced = replaceAll(replaced, '\u0000', '\u0001\u0003');
      return replaced;
    })
    .join('\0');
}

/**
 * Decodes a key that was previously encoded with [[encodeKey]].
 * @param key The key to be decoded
 * @returns The decoded key parts
 */
export function decodeKey(key: string): string[] {
  const parts = key.split('\0');
  return parts.map((v) => {
    let decoded = replaceAll(v, '\u0001\u0003', '\u0000');
    decoded = replaceAll(decoded, '\u0001\u0002', '\u0001');
    return decoded;
  });
}

export function actionToString(action: IPerformActionReq): string {
  return `${action.actorType}${JSON.stringify(action.actorId)}.${action.actionName}`;
}

export function actorToString(type: string, id: string[]): string {
  return `${type}${JSON.stringify(id)}`;
}

export interface ITasksResult<TaskResult, FinalResult> {
  results: ITaskResult<TaskResult>[];
  result?: FinalResult;
  status: 'completed' | 'timeout' | 'aborted';
}

export interface ITaskResult<Result> {
  result?: Result;
  error?: unknown;
}

export type ParallelAbort<FinalResult> = (finalResult?: FinalResult) => void;
export type ParallelTask<TaskResult, FinalResult> = (abort: ParallelAbort<FinalResult>) => Promise<TaskResult>;

/**
 *
 * Execute a list of tasks in parallel, with an optional timeout. Tasks can decide to
 * abort the parallel processing early. They can then also specify a 'final result'
 * value that is then returned as part of the result.
 */
export async function parallel<TaskResult, FinalResult>(
  tasks: Array<ParallelTask<TaskResult, FinalResult>>,
  timeout: number,
  maxConcurrency?: number,
): Promise<ITasksResult<TaskResult, FinalResult>> {
  return new Promise((resolve) => {
    const results: ITasksResult<TaskResult, FinalResult> = {
      results: new Array(tasks.length),
      status: 'completed',
    };

    let open = tasks.length;
    let idx = 0;
    let abort = false;
    let done = false;
    let nrunning = 0;
    const nextTick = maxConcurrency ?? 0 < 0;
    maxConcurrency = maxConcurrency === undefined ? undefined : Math.abs(maxConcurrency);

    let timer: NodeJS.Timeout | undefined;
    if (timeout > 0) {
      timer = setTimeout(() => {
        if (!done) {
          results.status = 'timeout';
          done = true;
          resolve(results);
        }
      }, timeout);
    }

    function closeOneTask() {
      open--;
      nrunning--;
      if (abort || open === 0) {
        if (timer) {
          clearTimeout(timer);
          timer = undefined;
        }
        done = true;
        resolve(results);
      }
      if (nextTick) {
        process.nextTick(() => processMore());
      } else {
        setImmediate(() => processMore());
      }
    }

    function processMore() {
      while (idx < tasks.length) {
        if (maxConcurrency === undefined || nrunning < maxConcurrency) {
          const task = tasks[idx];
          const result = results.results[idx];
          try {
            nrunning++;
            task(
              /*abort*/ (finalResult) => {
                if (done) {
                  return;
                }
                results.result = finalResult;
                results.status = 'aborted';
                abort = true;
              },
            )
              .then((value: TaskResult) => {
                if (done) {
                  return;
                }
                result.result = value;
                closeOneTask();
              })
              .catch((e) => {
                if (done) {
                  return;
                }
                result.error = e;
                closeOneTask();
              });
          } catch (e) {
            if (done) {
              return;
            }
            result.error = e;
            closeOneTask();
          }
          idx++;
        } else {
          break;
        }
      }
    }

    for (let idx = 0; idx < tasks.length; idx++) {
      const result: ITaskResult<TaskResult> = {};
      results.results[idx] = result;
    }

    process.nextTick(() => processMore());
  });
}

function getNestedMessage(error: unknown): string {
  if (typeof error === 'string') {
    return error;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const ex = error as any;
  const parts = [];
  let current = ex;
  while (current) {
    const c = current;
    current = undefined;

    if (typeof c === 'string') {
      parts.push(c);
      break;
    }
    const msg = c.message;
    if (msg && typeof msg === 'string') {
      parts.push(msg);
      const nested = c.nested;
      if (nested && Array.isArray(nested) && nested.length > 0) {
        current = nested[0];
      }
    }
  }
  return parts.join(': ');
}

// eslint-disable-next-line
export function formatAttribute(attribute: any): string {
  if (typeof attribute === 'object' && !Array.isArray(attribute)) {
    const actorType = attribute.actorType;
    const actorId = attribute.actorId;
    const actionName = attribute.actionName;

    if (actorType !== undefined && actorId !== undefined && actionName !== undefined) {
      return `${actorType}${JSON.stringify(actorId)}.${actionName}`;
    }
    if (actorType !== undefined && actorId !== undefined) {
      return `${actorType}${JSON.stringify(actorId)}`;
    }
    if (actorType !== undefined) {
      return actorType;
    }
    if (attribute.code && attribute.message) {
      return `${attribute.code}: ${getNestedMessage(attribute)}`;
    }
    if (attribute.ids && attribute.message) {
      return `${attribute.ids[attribute.ids.length - 1]}: ${getNestedMessage(attribute)}`;
    }
    if (attribute.name && attribute.message) {
      const stack = (attribute.stack as string) || '';

      //if (stack) {
      //  return stack; // Already contains name and message, but not "nested" messages
      //}
      return `${attribute.name}: ${getNestedMessage(attribute)} at ${stack}`;
    }
    return '<object/dict>';
  }
  return JSON.stringify(attribute);
}

export function formatAllAttributes(attributes?: IAttributes): IAttributes | undefined {
  if (!attributes) {
    return;
  }

  const results: IAttributes = {};
  for (const [key, value] of Object.entries(attributes)) {
    results[key] = formatAttribute(value);
  }
  return results;
}

export function replaceArguments(value: string, attributes?: IAttributes, literal = false): string {
  if (!attributes) {
    return value;
  }
  let v = value;
  for (const [key, value] of Object.entries(attributes)) {
    v = replaceAll(v, '[' + key + ']', literal ? (value as string) : formatAttribute(value));
  }
  return v;
}

export class Mutex<T> {
  protected queue: Array<(value: T | undefined) => void>;
  protected held = false;

  constructor(acquire = false) {
    this.queue = [];
    if (acquire) {
      this.held = true;
    }
  }

  public async acquire(): Promise<T | undefined> {
    if (this.held) {
      return new Promise((resolve) => {
        this.queue.push(resolve);
      });
    }
    this.held = true;
  }

  public release(value: T | undefined): boolean {
    if (this.queue.length > 0) {
      const q0 = this.queue[0];
      this.queue.splice(0, 1);
      q0(value);
      return true;
    }
    this.held = false;
    return false;
  }
}

export class MultiMutex<T> {
  protected locks: Map<string, Mutex<T>>;

  constructor() {
    this.locks = new Map();
  }

  public async acquire(key: string): Promise<T | undefined> {
    const lock = this.locks.get(key);
    if (lock) {
      return await lock.acquire();
    } else {
      const newlock = new Mutex<T>(true);
      this.locks.set(key, newlock);
    }
  }

  public release(key: string, value: T | undefined): void {
    const lock = this.locks.get(key);
    if (!lock) {
      throw new Error('NO_SUCH_LOCK');
    }
    if (!lock.release(value)) {
      this.locks.delete(key);
    }
  }
}
