import { IContext, ILogger } from './logging';
import { TransportMessageHandler, ITransport, EditableNetworkMessage, INetworkMessage } from './networktypes';
import { IPeerConfig } from './types';

export type CommandHandler<T> = (value: T) => void;

export interface IPeerInfo<Incoming, Outgoing> {
  id: string;
  address?: string;
  activeStream?: 'outgoing' | 'incoming';
  kind: 'client' | 'hub';
  incoming?: Incoming;
  outgoing?: Outgoing;
}

export class BaseTransport<Incoming, Outgoing> implements ITransport {
  protected handler?: TransportMessageHandler;
  protected id: string;
  protected address: string;
  protected logger: ILogger;
  protected peers: Map<string, IPeerInfo<Incoming, Outgoing>>;
  protected active: boolean;
  protected hub: boolean;

  constructor(logger: ILogger, id: string, address: string, peers: IPeerConfig[]) {
    this.logger = logger;
    this.id = id;
    this.address = address;
    this.peers = new Map();
    this.hub = false;
    for (const peer of peers) {
      this.peers.set(peer.id, { id: peer.id, address: peer.address, kind: 'hub' });
      if (peer.id === id) {
        this.hub = true;
      }
    }
    this.active = true;
  }

  public async init(): Promise<void> {
    await this.initIn();
    await this.initOut();
  }

  public async initIn(): Promise<void> {
    //
  }

  public async initOut(): Promise<void> {
    //
  }

  public estimateDistance(receiver: string, direct: boolean): number {
    // Note: not used for now. TODO: Clean up.
    if (!this.active) {
      return -1;
    }
    if (receiver === '') {
      // This indicates 'send message to a hub node'
      for (const peer of this.peers.values()) {
        if (peer.kind === 'hub') {
          if (!!peer.incoming || !!peer.outgoing) {
            // this.logger.debug('Estimate distance to [Receiver] via [Peer] returned 1', {Receiver: receiver, Peer: peer.id});
            return 1;
          }
        }
      }
      return -1;
    }

    const peer = this.peers.get(receiver);
    if (peer) {
      if (!!peer.incoming || !!peer.outgoing) {
        return 1;
      } else {
        return -1;
      }
    }
    return direct ? -1 : 2;
  }

  public async send(c: IContext, message: EditableNetworkMessage): Promise<void> {
    await this.sendImpl(c, message);
  }

  public setHandler(handler: TransportMessageHandler): void {
    this.handler = handler;
  }

  public async close(): Promise<void> {
    this.logger.debug('Closing network');
    this.active = false;

    await this.closeImpl();
  }

  protected async closeImpl(): Promise<void> {
    //
  }

  protected getHubPeer(): IPeerInfo<Incoming, Outgoing> | undefined {
    // First, use ourselves if we are a hub
    const peer = this.peers.get(this.id);
    if (peer && peer.kind === 'hub') {
      if (!!peer.incoming || !!peer.outgoing) {
        return peer;
      }
    }

    const candidates = [];

    // Then find any of the other hubs. We now return the first one;
    // TODO: Randomly choose one to distribute the load?
    for (const peer of this.peers.values()) {
      if (peer.kind === 'hub') {
        if (!!peer.incoming || !!peer.outgoing) {
          candidates.push(peer);
        }
      }
    }

    if (candidates.length > 0) {
      return candidates[Math.floor(Math.random() * candidates.length)];
    }
  }

  protected async sendImpl(c: IContext, message: EditableNetworkMessage): Promise<void> {
    const c0 = c.getChildContext('io.darlean.transport.SendImpl');
    try {
      const receiver = message.getReceiver();

      if (receiver === this.id) {
        process.nextTick(async () => {
          const c1 = c.getChildContext('io.darlean.transport.SendToOurselves');
          try {
            if (this.handler) {
              try {
                await this.handler(message);
              } catch (e) {
                this.logger.error('Unexpected error during handling of message: [Error]', () => ({ Error: e }));
              }
            }
          } finally {
            c1.finish();
          }
        });
        return;
      }

      if (receiver === '') {
        throw new Error(`A receiver must be specified when sending a message`);
      }

      const peer = this.peers.get(receiver) ?? (this.hub ? undefined : this.getHubPeer());
      if (!peer) {
        throw new Error(`Node [${receiver}] not known`);
      }

      this.logger.deep('Sending to peer [Peer]', () => ({ Peer: peer.id }));

      if (peer.incoming) {
        return new Promise((resolve, reject) => {
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          this.writeIncoming(peer.incoming!, message, (error) => {
            if (error) {
              reject(error);
            } else {
              resolve();
            }
          });
        });
      } else if (peer.outgoing) {
        return new Promise((resolve, reject) => {
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          this.writeOutgoing(peer.outgoing!, message, (error) => {
            if (error) {
              reject(error);
            } else {
              resolve();
            }
          });
        });
      } else {
        throw new Error(`No connection with [${peer.id}]`);
      }
    } finally {
      c0.finish();
    }
  }

  protected writeOutgoing(_socket: Outgoing, _msg: INetworkMessage, _cb?: (error: unknown) => void): void {
    //
  }

  protected writeIncoming(_socket: Incoming, _msg: INetworkMessage, _cb?: (error: unknown) => void): void {
    //
  }
}
