import * as ev from 'events';
import * as uuid from 'uuid';
import { IContext, ILogger } from '.';
import {
  EditableNetworkMessage,
  IEnvelopeCallback,
  INetwork,
  INetworkEnvelope,
  INetworkMessage,
  ITransport,
  NetworkSendOptions,
} from './networktypes';

const DEFAULT_SEND_TTL = 5 * 1000;

interface IOutstanding {
  id: string;
  options: NetworkSendOptions;
  timeout?: NodeJS.Timeout;
  resolve?: () => void;
  reject?: (reason: unknown) => void;
}

export declare interface Network {
  on(event: 'received', listener: (c: IContext, msg: INetworkMessage) => void): this;
  on(event: 'error', listener: (error: unknown) => void): this;
}

export class Network extends ev.EventEmitter implements INetwork {
  private transports: ITransport[];
  private id: string;
  private outstanding: Map<string, IOutstanding>;
  private defaultHopCount: number;
  private active: boolean;
  private logger: ILogger;
  private hub: boolean;

  constructor(logger: ILogger, id: string, hub: boolean) {
    super();
    this.id = id;
    this.logger = logger;
    this.transports = [];
    this.defaultHopCount = 4;
    this.outstanding = new Map();
    this.active = true;
    this.hub = hub;
  }

  public addTransport(t: ITransport): void {
    this.transports.push(t);
    t.setHandler(async (msg) => await this.handleIncomingMessage(this.logger, msg));
  }

  public getReturnCallback(data: string): IEnvelopeCallback {
    return {
      to: this.id,
      data,
      uid: uuid.v4(),
    };
  }

  // Send an envelope.
  // If receiver is empty string, we send it to a gateway. If we are a gateway ourselves,
  // we then send it to us.
  // If receiver is not empty, we send it to the transport with the least estimated
  // distance to the receiver (see ITransport.estimateDistance).
  // If an error occurs before the function returns), it is thrown and emitted as 'error'
  // event. If an error occurs after the function returns, it is only emitted.
  // The value of options.waitFor determines when this function returns: as soon as
  // possible, when the the message is sent away, or when the message is received by the
  // (final) receiver.
  public async send(c: IContext, msg: EditableNetworkMessage, options: NetworkSendOptions): Promise<void> {
    const c1 = c.getChildLogger('io.darlean.network.send');
    try {
      const receiver = msg.getReceiver();
      c1.debug('Network is sending envelope [Uid] to [Receiver]: [Env]', () => ({
        Uid: msg.getEnvelopeContainer().getDataStruct().uid,
        Receiver: msg.getReceiver(),
        Env: msg.getEnvelopeContainer().getDataStruct(),
      }));

      try {
        if (!this.active) {
          throw new Error('Network not active (are you shutting down?)');
        }

        if (receiver === this.id) {
          return await this.deliverMessageToOurselves(c1, msg, options);
        }

        const reqid = uuid.v4();

        const hopsRemaining = msg.getHopsRemaining();
        if (hopsRemaining <= 0) {
          throw new Error(`No hops remaining while sending to [${receiver}]`);
        }

        msg.decreaseHopsRemaining();

        if (options.waitFor === 'delivered' || options.listenerCount('delivered') > 0) {
          const envelope = msg.getEnvelopeContainer().getDataStruct();
          if (envelope.onDF === undefined) {
            envelope.onDF = [];
          }
          envelope.onDF.push(this.getReturnCallback(reqid));
        }

        if (options.waitFor === 'undeliverable' || options.listenerCount('undeliverable') > 0) {
          const envelope = msg.getEnvelopeContainer().getDataStruct();
          if (envelope.onF === undefined) {
            envelope.onF = [];
          }
          envelope.onF.push(this.getReturnCallback(reqid));
        }

        for (const transport of this.findTransports(receiver, options.direct)) {
          try {
            return await this.deliverMessageViaTransport(c1, transport, options, reqid, msg);
          } catch (e) {
            // TODO: Only emit this once instead of for each transport?
            options.emit('sent', e);
          }
        }

        // All transports failed
        throw new Error(`No transport available to [${receiver}]`);
      } catch (e) {
        options.emit('sent', e);
        this.logger.debug('Send failed: [Error]', () => ({ Error: e }));
        if (options.waitFor !== 'nothing') {
          throw e;
        }
      }
    } finally {
      c1.finish();
    }
  }

  public findTransports(_receiver: string, _direct: boolean): ITransport[] {
    return this.transports;
    /*
    // TODO: Reenable this mechanism? But improve performance!
    const logger = this.logger.getChildLogger('network.findTransports');
    try {
      const results = [];
      for (const transport of this.transports) {
        const distance = transport.estimateDistance(receiver, direct);
        // console.log('Found transport', this.id, receiver, direct, distance);
        if (distance >= 0) {
          results.push({ transport, distance });
        }
      }
      return results.sort((a, b) => a.distance - b.distance).map((v) => v.transport);
    } finally {
      logger.finish();
    }*/
  }

  // Shutsdown the network. Any errors are emitted as 'error' event and not thrown.
  public async shutdown(): Promise<void> {
    this.active = false;
    for (const transport of this.transports) {
      try {
        await transport.close();
      } catch (e: unknown) {
        this.emit('error', e);
      }
    }
    for (const outstanding of this.outstanding.values()) {
      if (outstanding.timeout) {
        clearTimeout(outstanding.timeout);
        outstanding.timeout = undefined;
      }
    }
    this.outstanding.clear();
  }

  protected async deliverMessageToOurselves(
    c: IContext,
    msg: INetworkMessage,
    options: NetworkSendOptions,
  ): Promise<void> {
    // Message to ourselves, take a shortcut here, bypass the transports...
    c.deep('Sending [Msg] to Ourselves, bypassing Transport', () => ({ Msg: msg }));
    return new Promise((resolve, reject) => {
      process.nextTick(async () => {
        const c2 = c.getChildContext('io.darlean.network.SendToOurselves');
        try {
          if (this.active) {
            await this.handleIncomingMessage(c2, msg);
            options.emit('sent');
            resolve();
          }
        } catch (e) {
          this.logger.error('Error while handling incoming message to ourselves: [Error]', () => ({ Error: e }));
          options.emit('sent');
          reject(e);
        } finally {
          c2.finish();
        }
      });
    });
  }

  protected async deliverMessageViaTransport(
    c: IContext,
    transport: ITransport,
    options: NetworkSendOptions,
    reqid: string,
    msg: EditableNetworkMessage,
  ): Promise<void> {
    // When the options indicate that the caller wants us to wait for the message
    // to be delivered or undeliverable, we add a record to our administration of
    // outstanding calls, and return a promise this will resolve when the receipt
    // message is received (or when a timeout fires, whichever comes first).
    // Otherwise, we just return a promise that resolves almost immediately (from
    // within the body of the 'nextTick' code that actually sends the message).
    let p: Promise<void> | undefined;
    let pResolve: () => void | undefined;
    let pReject: (e: unknown) => void | undefined;
    if (options.waitFor === 'delivered' || options.waitFor === 'undeliverable') {
      p = new Promise((resolve, reject) => {
        const outstanding: IOutstanding = {
          id: reqid,
          options,
          resolve,
          reject,
          timeout: setTimeout(() => {
            outstanding.timeout = undefined;
            this.outstanding.delete(reqid);
            if (outstanding.reject) {
              outstanding.reject('NETWORK_TIMEOUT');
            }
          }, options.ttl ?? DEFAULT_SEND_TTL),
        };
        this.outstanding.set(reqid, outstanding);
      });
    } else {
      p = new Promise((resolve, reject) => {
        pResolve = resolve;
        pReject = reject;
      });
    }

    // The creation of the above promise is deliberately placed before the "await transport.send" below, because during sending,
    // a fast receiver might already send a return or error message, and for that to be properly send back to
    // the caller, the promise's body (which registers the outstanding record) must already have been invoked. Note that
    // because the outstanding record requires the reject/resolve functions, this must be done from within the promise.
    // When we would not do the below call to transport.send in a "nextTick", we could have the situation in which
    // the quick receiver already sends a return or error message, and we have not yet returned the promise to our caller
    // (because we are still waiting for "await transport.send"). Result: the caller is not informed, or, in case of an
    // error, NodeJS complains with a UnhandledPromiseRejection because no "catch" handler is yet registered.
    // To eliminate this, we invoke the actual sending as soon as possible ('nextTick'), but after returning from our function.
    process.nextTick(async () => {
      const c2 = c.getChildContext('io.darlean.network.ActualSendToTransport');
      try {
        if (this.active) {
          c2.deep('Sending [Msg] to Transport', () => ({ Msg: msg }));
          await transport.send(c2, msg);
        }
        options.emit('sent');

        if (pResolve) {
          pResolve();
        }
      } catch (e) {
        const outstanding = this.outstanding.get(reqid);
        if (outstanding) {
          if (outstanding.timeout) {
            clearTimeout(outstanding.timeout);
          }
          outstanding.timeout = undefined;
          this.outstanding.delete(reqid);
          if (outstanding.reject) {
            outstanding.reject(e);
          }
        } else {
          if (pReject) {
            pReject(e);
          }
        }
      } finally {
        c2.finish();
      }
    });
    return p;
  }

  protected async handleIncomingMessage(c: IContext, message: INetworkMessage): Promise<void> {
    if (!this.active) {
      return;
    }
    const tracing = message.getTracing();
    const c2 = c.getChildContext(
      'io.darlean.network.HandleIncomingMessage',
      undefined,
      () => ({ Node: this.id }),
      tracing,
    );

    try {
      const receiver = message.getReceiver();
      if (receiver === this.id) {
        await this.handleIncomingMessageToOurselves(c2, message);
      } else {
        await this.forwardIncomingMessage(c2, message, receiver);
      }
    } catch (e) {
      c2.debug('Error in handling incoming message: [Error]', () => ({ Error: e }));
      this.emit('error', e);
    } finally {
      c2.finish();
    }
  }

  protected async handleIncomingMessageToOurselves(c: IContext, message: INetworkMessage): Promise<void> {
    // It is for us. Either as a receipt, or as a message to deliver locally
    const envelope = message.getEnvelopeContainer().getDataStruct();
    if (envelope.type === 'dlv' || envelope.type === 'fail') {
      const reqid = envelope.data;
      if (reqid) {
        const outstanding = this.outstanding.get(reqid);
        c.debug('Network received receipt for [ReqId] [Outstanding]', () => ({
          ReqId: reqid,
          Outstanding: !!outstanding,
        }));
        if (outstanding) {
          if (outstanding.timeout) {
            clearTimeout(outstanding.timeout);
            outstanding.timeout = undefined;
          }
          this.outstanding.delete(reqid);
          if (envelope.type === 'fail') {
            const error = envelope.error || 'UNDELIVERABLE';
            outstanding.options.emit('delivered', error);
            if (outstanding.reject) {
              outstanding.reject(error);
            }
          } else {
            outstanding.options.emit('delivered');
            if (outstanding.resolve) {
              outstanding.resolve();
            }
          }
        }
      } else {
        c.info('Received FAIL envelope without data field');
      }
      return;
    }

    // We have a real message to us.
    this.emit('received', c, message);

    await this.sendReceipt(c, envelope.onD, 'dlv');
    await this.sendReceipt(c, envelope.onDF, 'dlv');
  }

  protected async forwardIncomingMessage(c: IContext, message: INetworkMessage, receiver: string): Promise<void> {
    try {
      const msg = new EditableNetworkMessage(receiver, message.getEnvelopeContainer(), message.getHopsRemaining(), c);
      await this.send(c, msg, new NetworkSendOptions('sent', true));
    } catch (e) {
      c.debug('Forwarding failed: [Error] [Message]', () => ({ Error: e, Message: message }));
      const envelope = message.getEnvelopeContainer().getDataStruct();
      await this.sendReceipt(c, envelope.onF, 'fail');
      await this.sendReceipt(c, envelope.onDF, 'fail');
    }
  }

  protected async sendReceipt(
    c: IContext,
    envelopes: IEnvelopeCallback[] | undefined,
    type: 'dlv' | 'fail',
  ): Promise<void> {
    if (envelopes) {
      for (const env of envelopes) {
        try {
          const env2: INetworkEnvelope = {
            type,
            uid: env.uid,
            data: env.data,
          };
          const m = new EditableNetworkMessage(env.to, env2);
          const options = new NetworkSendOptions('nothing');
          await this.send(c, m, options);
        } catch (e) {
          c.debug('Error while sending receipt: [Error]', () => ({ Error: e }));
          this.emit('error', e);
        }
      }
    }
  }
}
