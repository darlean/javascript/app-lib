import {
  ActorRunner,
  Struct,
  DarleanError,
  encodeKey,
  ensureDarleanError,
  IAppInfo,
  ILogger,
  IRuntime,
  Logger,
  sleep,
  struct,
  toRpcError,
  WARNING_AND_UP,
  IContext,
} from '.';
import { WsTransport } from './wstransport2';
import { fromRpcError, IPeerConfig } from './types';
import {
  EditableNetworkMessage,
  INetworkMessage,
  ITransport,
  NetworkSendOptions,
  NetworkWaitFor,
} from './networktypes';
import { Network } from './network';
import * as uuid from 'uuid';
import { ActorRunnerTime } from './actorrunnertime';
import { AnObject } from './containers';
import { IActionOptions, IActionResult } from './services/performing';
import {
  IPerformActionReq,
  IPerformActionReqC,
  IPerformActionResp,
  IPerformActionRespC,
} from './interfaces/performing';
import { IRetrieveDispatchInfoRequest, IRetrieveDispatchInfoResponse } from './interfaces/dispatching';
import { DispatchError } from './helpers/dispatching/dispatchactor';
import { IAcquireLockResult, IGetLockInfoResult } from './services/locking';
import {
  IAcquireLockRequest,
  IAcquireLockResponse,
  IGetLockInfoRequest,
  IGetLockInfoResponse,
  IReleaseLockRequest,
} from './interfaces/locking';
import { MultiMutex } from './util';
import { Tracer } from './tracer';

const DEFAULT_MAX_ACTION_HOP_COUNT = 32;
const DEFAULT_ACTION_TTL = 5 * 1000;

export type PerformActionRequestWaitFor = 'nothing' | 'performed';

interface IOutstandingRequest {
  reqid: string;
  resolve: (result: IPerformActionRespC<unknown>) => void;
  reject: (error: unknown) => void;
  timeout?: NodeJS.Timeout;
}

interface IDispatchCacheItem {
  apps: string[];
  multiplicity: 'single' | 'multiple';
}

export class Application implements IRuntime {
  public cluster: string;
  public fullId: string;
  public time: ActorRunnerTime;

  private logger: ILogger;
  private network: Network;
  private transport: ITransport;
  private address: string;
  private gateways: IPeerConfig[];
  private gatewayNodes: string[];
  private outstandingRequests: Map<string, IOutstandingRequest>;
  private runner: ActorRunner;
  private transportSettleDelay: number;
  private dispatchCache: Map<string, IDispatchCacheItem>;
  private findReceiverLock: MultiMutex<IDispatchCacheItem>;
  private tracer: Tracer;

  constructor(
    cluster: string,
    id: string,
    address: string,
    gateways: IPeerConfig[],
    logger?: ILogger,
    transport?: ITransport,
  ) {
    this.cluster = cluster;
    this.fullId = id + '@' + cluster;
    this.address = address;
    this.tracer = new Tracer();
    if (logger) {
      this.logger = logger;
    } else {
      const l = new Logger(undefined, undefined, undefined, undefined, this.tracer);
      l.addMask('*', WARNING_AND_UP);
      this.logger = l;
    }
    this.gateways = gateways;
    this.gatewayNodes = Array.from(this.gateways.values()).map((v) => v.id);

    this.dispatchCache = new Map();

    this.outstandingRequests = new Map();
    this.findReceiverLock = new MultiMutex();

    const hub = gateways.some((v) => v.id === this.fullId);
    this.network = new Network(this.logger.getChildLogger('Network'), this.fullId, hub);
    this.network.on('error', (e) => {
      this.logger.warning('Network has error [Error]', () => ({ Error: e }));
    });
    this.transport =
      transport ??
      new WsTransport(
        this.logger.getChildLogger('Transport', 'ws-' + this.fullId),
        this.fullId,
        this.address,
        this.gateways,
      );
    this.network.addTransport(this.transport);

    this.network.on('received', async (c: IContext, message: INetworkMessage) => {
      try {
        await this.handleIncomingMessage(c, message);
      } catch (e) {
        this.logger.error('Unexpected error during receive: [Error]', () => ({ Error: e }));
      }
    });

    this.time = new ActorRunnerTime(this.logger.getChildLogger('Time'));

    this.transportSettleDelay = 2500;

    this.runner = new ActorRunner(
      this.logger.getChildLogger('Runner', this.fullId),
      this.fullId,
      this as IRuntime,
      this.time,
    );
  }

  public async init(): Promise<void> {
    await this.transport.init();
    await sleep(this.transportSettleDelay);
    await this.runner.start();
  }

  public async stop(): Promise<void> {
    this.logger.debug('Stopping application');

    this.logger.deep('Stopping actor runner');
    await this.runner.stop();
    this.logger.deep('Stopped actor runner');

    this.logger.deep('Stopping network');
    await this.network.shutdown();
    this.logger.deep('Stopped network');

    for (const outstanding of this.outstandingRequests.values()) {
      if (outstanding.timeout) {
        clearTimeout(outstanding.timeout);
        outstanding.timeout = undefined;
      }
    }
    this.outstandingRequests.clear();

    this.time.stop();
  }

  public getRunner(): ActorRunner {
    return this.runner;
  }

  public async acquireLock(
    id: string[],
    ttl: number,
    refresh: boolean,
    options: IActionOptions<AnObject>,
  ): Promise<IAcquireLockResult> {
    const response = await this.dispatchAction<IAcquireLockRequest, IAcquireLockResponse>(this.logger, {
      actorType: 'RuntimeService',
      actorId: [],
      actionName: 'AcquireLock',
      data: {
        id,
        requester: this.fullId,
        ttl,
        singleStage: refresh,
      },
      receivers: this.gatewayNodes,
      retryCount: 0,
      ...(options as IActionOptions<IAcquireLockRequest>),
    });
    return response;
  }

  public async releaseLock(
    id: string[],
    waitFor: PerformActionRequestWaitFor,
    options?: IActionOptions<AnObject>,
  ): Promise<void> {
    if (waitFor === 'nothing') {
      this.fireAction<IReleaseLockRequest>(this.logger, {
        actorType: 'RuntimeService',
        actorId: [],
        actionName: 'ReleaseLock',
        data: {
          id,
          requester: this.fullId,
        },
        receivers: this.gatewayNodes,
        retryCount: 0,
        ...(options as IActionOptions<IAcquireLockRequest>),
      });
    } else {
      await this.dispatchAction<IReleaseLockRequest, void>(this.logger, {
        actorType: 'RuntimeService',
        actorId: [],
        actionName: 'ReleaseLock',
        data: {
          id,
          requester: this.fullId,
        },
        receivers: this.gatewayNodes,
        retryCount: 0,
        ...(options as IActionOptions<IAcquireLockRequest>),
      });
    }
  }

  public async getLockInfo(id: string[]): Promise<IGetLockInfoResult> {
    return await this.dispatchAction<IGetLockInfoRequest, IGetLockInfoResponse>(this.logger, {
      actorType: 'RuntimeService',
      actorId: [],
      actionName: 'GetLockInfo',
      data: {
        id,
      },
      receivers: this.gatewayNodes,
      retryCount: 0,
    });
  }

  public async publishInfo(info: IAppInfo): Promise<void> {
    this.logger.debug('Publishing actor info [Info]', () => ({ Info: info }));
    await this.dispatchAction<IAppInfo, void>(this.logger, {
      actorType: 'RuntimeService',
      actorId: [],
      actionName: 'PublishInfo',
      data: info,
      receivers: this.gatewayNodes,
    });
  }

  public async dispatchAction<Input, Output>(c: IContext, action: IActionOptions<Input>): Promise<Output> {
    const result = await this.dispatchActionImpl<Input, Output>(c, action);
    if (result) {
      return result.data;
    }
    throw new Error('DispatchImpl returned unassigned');
  }

  // Dispatches an action for execution, and waits for the result. Throws an error when
  // a network error occurs or when a processing error occurred before the action started
  // to be performed (provided that processErrors === 'throw'). When the perform fails,
  // no error is thrown, but the actorError field in the result is set.
  // When processErrors === 'return', no network/process errors are thrown. They are returned
  // as processError of the returned IActionResponse instead.
  public async dispatchActionRaw<Input, Output>(
    c: IContext,
    action: IActionOptions<Input>,
  ): Promise<IActionResult<Output>> {
    const result = await this.dispatchActionImpl<Input, Output>(c, action);
    if (result) {
      return result;
    }
    throw new Error('DispatchImpl returned unassigned');
  }

  // Fires an action for execution and does not wait for the results. Even when errors occur very early
  // in the process, they are silently absorbed (not thrown to the caller).
  public fireAction<Input>(c: IContext, action: IActionOptions<Input>): void {
    setImmediate(async () => {
      try {
        await this.dispatchActionImpl<Input, unknown>(c, action);
      } catch (e) {
        this.logger.debug('Error while firing action [Action]: [Error]', () => ({ Action: action, Error: e }));
      }
    });
  }

  protected async acquireFindReceiverLock(id: string): Promise<IDispatchCacheItem | undefined> {
    return await this.findReceiverLock.acquire(id);
  }

  protected async findReceivers(
    c: IContext,
    actorType?: string,
    actorId?: string[],
    retry = false,
  ): Promise<{ apps: string[]; multiplicity: 'single' | 'multiple' }> {
    const logger = c.getChildContext('io.darlean.application.FindReceivers', undefined, () => ({
      ActorType: actorType,
      ActorId: actorId,
      Retry: retry,
    }));
    try {
      if (!actorType) {
        throw new Error('ActorType is required');
      }
      if (actorId === undefined) {
        throw new Error('ActorId is required');
      }

      const key = encodeKey([actorType, ...actorId]);

      if (actorType === 'RuntimeService') {
        return { apps: this.gatewayNodes, multiplicity: 'multiple' };
      }

      if (!retry) {
        const logger3 = logger.getChildContext('io.darlean.FindReceivers.CheckCache');
        try {
          const cached = this.dispatchCache.get(key);
          if (cached) {
            // Move to front of LRU
            this.dispatchCache.delete(key);
            this.dispatchCache.set(key, cached);
            // And return
            return { apps: cached.apps, multiplicity: cached.multiplicity };
          }
        } finally {
          logger3.finish();
        }
      }

      const logger4 = logger.getChildContext('io.darlean.FindReceivers.Retrieve');
      try {
        const c8 = logger.getChildContext('io.darlean.FindReceivers.AcquireLock', undefined, () => ({ Key: key }));
        const lockKey = key;
        let cacheItem = await this.acquireFindReceiverLock(lockKey);
        try {
          c8.finish();
          if (cacheItem) {
            return { apps: cacheItem.apps, multiplicity: cacheItem.multiplicity };
          }

          const logger2 = logger.getChildLogger('io.darlean.application.FindReceivers.Retrieve.Dispatch');
          try {
            const results = await this.dispatchAction<IRetrieveDispatchInfoRequest, IRetrieveDispatchInfoResponse>(
              logger2,
              {
                actorType: 'RuntimeService',
                actorId: [],
                actionName: 'RetrieveDispatchInfo',
                receivers: this.gatewayNodes, // Important to avoid endless loops while finding receivers for the 'RuntimeService'.
                data: {
                  actorType,
                  actorId,
                },
              },
            );

            if (results.apps.length > 0) {
              cacheItem = { apps: results.apps, multiplicity: results.multiplicity };
              this.dispatchCache.set(key, cacheItem);
            }
            return results;
          } finally {
            logger2.finish();
          }
        } finally {
          this.findReceiverLock.release(lockKey, cacheItem);
        }
      } finally {
        logger4.finish();
      }
    } finally {
      logger.finish();
    }
  }

  // Dispatches an action by retrying multiple times in case of a network or process failure (and
  // under water retrying multiple app candidates that might hold the actor). Returns an IActionResult
  // instance or throws an error in case of exceptions (including network, process and actor errors).
  protected async dispatchActionImpl<Input, Output>(
    c: IContext,
    action: IActionOptions<Input>,
  ): Promise<IActionResult<Output>> {
    const c2 = c.getChildContext('io.darlean.application.dispatchActionImpl', undefined, () => ({
      ActorType: action.actorType,
      ActorId: action.actorId,
      ActionName: action.actionName,
    }));

    try {
      if ((action.networkHopsRemaining ?? 0) < 0) {
        throw new Error('NO_NETWORK_HOPS_REMAINING');
      }

      if ((action.actionHopsRemaining ?? 0) < 0) {
        throw new Error('NO_ACTION_HOPS_REMAINING');
      }

      const errors: DarleanError[] = [];

      const validUntil = this.time.machineTicks() + (action.ttl ?? DEFAULT_ACTION_TTL);

      const req = struct<IPerformActionReq>((attach) => ({
        actionName: action.actionName,
        actorId: action.actorId,
        actorType: action.actorType,
        actionHopsRemaining: action.actionHopsRemaining ?? DEFAULT_MAX_ACTION_HOP_COUNT,
        hops: action.hops,
        data: attach(action.data),
      }));
      const reqC = req._cont();

      let delay = 5;
      for (let i = 0; i < (action.retryCount ?? 4) + 1; i++) {
        if (i > 0) {
          const sleeptime = Math.round((0.5 + Math.random() * 0.5) * delay);
          const sleepEnd = this.time.machineTicks() + sleeptime;
          if (sleepEnd > validUntil) {
            throw new Error('RETRY_TIMEOUT');
          }
          await sleep(sleeptime);
          delay *= 4;
        }
        let haveActorError = false;
        try {
          const respC = await this.dispatchActionToReceivers<Input, Output>(c2, reqC, action.receivers, validUntil);
          const resp = struct(respC);

          if (resp.actorError) {
            const e = fromRpcError(resp.actorError);
            c2.deep('Dispatch of [Action] resulted in actor error [Error]', () => ({
              Action: action,
              Error: resp.actorError,
            }));
            haveActorError = true;
            throw e;
          }

          if (!resp.data) {
            throw new Error('Encountered response without data');
          }

          const result: IActionResult<Output> = {
            data: resp._att<Output>(resp.data).struct(),
          };

          return result;
        } catch (e) {
          if (haveActorError) {
            throw ensureDarleanError(e);
          }

          errors.push(ensureDarleanError(e));
          c2.deep('Error during attempt [Attempt] of dispatching [Action]: [Error]', () => ({
            Attempt: clearTimeout,
            Action: action,
            Error: e,
          }));
        }
      }

      const e = DispatchError(
        'UNEXPECTED_PERFORM_ERROR',
        'DISPATCH_FAILED: Dispatch of [Action] failed',
        {
          Action: action,
        },
        errors,
      );
      throw e;
    } finally {
      c2.finish();
    }
  }

  // Sends an action request to the predicted receivers, one by one. Returns when one such
  // receiver does not return a network or process error.
  // Returns an action-response-container.
  protected async dispatchActionToReceivers<Input, Output>(
    c: IContext,
    request: IPerformActionReqC<Input>,
    fixedReceivers: string[] | undefined,
    validUntil: number,
  ): Promise<IPerformActionRespC<Output>> {
    const receiversDone: string[] = [];
    let receivers: string[];
    let origReceivers: string[];
    let multiplicity: 'single' | 'multiple' = 'single';
    const action = request.getDataStruct();

    const c2 = c.getChildContext('io.darlean.application.DispatchActionToReceivers', undefined, () => ({
      ActorType: action.actorType,
      ActionName: action.actionName,
    }));
    try {
      if (fixedReceivers) {
        receivers = [...fixedReceivers];
        origReceivers = [...fixedReceivers];
      } else {
        const c2a = c2.getChildLogger('io.darlean.application.DispatchActionToReceivers.FindReceivers');
        try {
          const result = await this.findReceivers(c2a, action.actorType, action.actorId, false);
          // Create a copy because we will remove from the list later on
          receivers = [...result.apps];
          origReceivers = [...receivers];
          multiplicity = result.multiplicity;
        } finally {
          c2a.finish();
        }
      }

      const c9 = c2.getChildLogger(
        'io.darlean.application.ReceivedData:' + receivers.length + ':' + receivers.join('/'),
      );
      c9.finish();

      if (receivers.length === 0) {
        throw new Error('NO_RECEIVERS');
      }

      const errors: DarleanError[] = [];
      for (let attempt = 0; attempt < 5; attempt++) {
        if (receivers.length === 0) {
          if (!fixedReceivers) {
            const additionalReceivers = await this.findReceivers(c2, action.actorType, action.actorId, true);
            origReceivers = [];
            for (const receiver of additionalReceivers.apps) {
              receivers.push(receiver);
              origReceivers.push(receiver);
            }
          }
          if (receivers.length === 0) {
            break;
          }
        }
        let recv: string | undefined;
        for (let i = 0; i < receivers.length; i++) {
          if (receivers[i] === this.fullId) {
            recv = receivers[i];
            receivers.splice(i, 1);
          }
        }

        if (recv === undefined) {
          const idx = Math.floor(Math.random() * receivers.length);
          recv = receivers[idx];
          receivers.splice(idx, 1);
        }

        receiversDone.push(recv);

        try {
          const c3 = c2.getChildContext('io.darlean.application.dispatchActionToSpecificReceiver', undefined, () => ({
            ActorType: action.actorType,
            ActionName: action.actionName,
          }));
          try {
            const result = await this.dispatchActionToSpecificReceiver<Input, Output>(c3, request, recv, validUntil);
            const resp = result.getDataStruct();
            if (resp.processError) {
              this.logger.deep('Process error while dispatching to receiver [Receiver]: [Error]', () => ({
                Receiver: recv,
                Error: resp.processError,
              }));
              errors.push(ensureDarleanError(resp.processError));
              if (resp.appHints !== undefined) {
                receivers = [];
                for (const r of resp.appHints) {
                  if (!receiversDone.includes(r)) {
                    receivers.push(r);
                  }
                }
              }
            } else {
              const cacheItem = { apps: multiplicity === 'single' ? [recv] : origReceivers, multiplicity };
              const key = encodeKey([action.actorType ?? '', ...(action.actorId ?? [])]);
              this.dispatchCache.set(key, cacheItem);

              return result;
            }
          } finally {
            c3.finish();
          }
        } catch (e) {
          this.logger.deep('Error while dispatching to receiver [Receiver]: [Error]', () => ({
            Receiver: recv,
            Error: e,
          }));
          errors.push(ensureDarleanError(e));
        }
      }
      throw DispatchError('UNEXPECTED_PERFORM_ERROR', 'ALL_RECEIVERS_FAILED', undefined, errors);
    } finally {
      c2.finish();
    }
  }

  // Returns an action-response-container, or undefined when options.waitFor is not 'performed'.
  protected async dispatchActionToSpecificReceiver<Input, Output>(
    c: IContext,
    request: IPerformActionReqC<Input>,
    receiver: string,
    validUntil: number,
  ): Promise<IPerformActionRespC<Output>> {
    const reqid = uuid.v4();
    const action = request.getDataStruct();

    const ttl = validUntil - this.time.machineTicks();
    if (ttl < 0) {
      throw new Error('DISPATCH_TIMEOUT');
    }

    const networkWaitFor: NetworkWaitFor = 'undeliverable';
    action.returnEnvelopes = [this.network.getReturnCallback(reqid)];

    const msg = EditableNetworkMessage.from(receiver, 'actreq', uuid.v4(), undefined, request, undefined, c);

    const p = new Promise<IPerformActionRespC<Output>>((resolve, reject) => {
      this.outstandingRequests.set(reqid, {
        reqid,
        resolve,
        reject,
        timeout: setTimeout(() => {
          this.outstandingRequests.delete(reqid);
          this.logger.deep('Timeout occurred for [ReqId], returning.', () => ({ ReqId: reqid }));
          reject('DISPATCH_TIMEOUT');
        }, ttl ?? 5 * 1000),
      });

      const c2 = c.getChildContext('io.darlean.application.networkSendOne', undefined, () => ({
        ActorType: action.actorType,
        ActionName: action.actionName,
      }));
      try {
        this.network.send(c2, msg, new NetworkSendOptions(networkWaitFor, undefined, ttl)).catch((e) => {
          const outstanding = this.outstandingRequests.get(reqid);
          if (outstanding) {
            this.outstandingRequests.delete(reqid);
            if (outstanding.timeout) {
              clearTimeout(outstanding.timeout);
              outstanding.timeout = undefined;
            }
            reject(e);
          }
        });
      } finally {
        c2.finish();
      }
    });

    return p;
  }

  protected async handleIncomingMessage(c: IContext, message: INetworkMessage): Promise<void> {
    c.debug('Application received incoming envelope [Uid] via network!', () => ({
      Uid: message.getEnvelopeContainer().getDataStruct().uid,
    }));

    c.deep('Handling incoming message [Message]', () => ({ Message: message }));
    const receiver = message.getReceiver();
    if (receiver !== this.fullId && receiver !== '') {
      c.error('Envelope for [Receiver] is not for us', () => ({ Receiver: receiver }));
      return;
    }

    const envelope = message.getEnvelopeContainer().getDataStruct();

    if (envelope.type === 'actresp') {
      const reqid = envelope.data;
      if (!reqid) {
        throw new Error('Received ACTRESP envelope without data field');
      }
      const outstanding = this.outstandingRequests.get(reqid);
      if (outstanding) {
        this.outstandingRequests.delete(reqid);
        if (outstanding.timeout) {
          clearTimeout(outstanding.timeout);
          outstanding.timeout = undefined;
        }
        const actionResponse = message.getEnvelopeContainer().getChild0() as IPerformActionRespC<unknown>;
        if (actionResponse) {
          // Regardless of an error being present, here we just return the action response.
          // It is up to the calling party to actually throw them. Rationale is that the action response
          // object also contains other useful information (like app hint) that must be preserved.
          outstanding.resolve(actionResponse);
          return;
        }
        outstanding.reject(new Error('No response (could be an error, todo)'));
      }
      return;
    }

    if (envelope.type === 'actreq') {
      const requestContainer = message.getEnvelopeContainer().getChild<IPerformActionReq>(0);
      await this.handlePerformActionRequest(c, requestContainer);
      return;
    }

    this.logger.error('Unexpected envelope: [Envelope]', () => ({ Envelope: envelope }));
  }

  protected async handlePerformActionRequest(
    c: IContext,
    requestContainer: IPerformActionReqC<AnObject>,
  ): Promise<void> {
    let request: Struct<IPerformActionReq> | undefined;
    try {
      // Note: the performer throws an error for processing errors (like unable to obtain lock), but
      // actor errors (in actor implementation code) are returned as part of result.
      request = struct(requestContainer);
      c.debug('Invoking actionHandler');
      const result = await this.runner.handleAction(c, requestContainer);
      this.logger.debug('Invoked actionHandler');
      if (request.returnEnvelopes) {
        for (const env of request.returnEnvelopes) {
          const msg = EditableNetworkMessage.from(env.to, 'actresp', env.uid, env.data, result, undefined, c);
          await this.network.send(c, msg, new NetworkSendOptions('nothing'));
        }
      }
    } catch (e) {
      this.logger.debug('Invoked actionHandler with [Error]', () => ({ Error: e }));
      const error = toRpcError(ensureDarleanError(e));
      const result = struct<IPerformActionResp>({
        processError: error,
      });

      if (request?.returnEnvelopes) {
        for (const env of request.returnEnvelopes) {
          const msg = EditableNetworkMessage.from(env.to, 'actresp', env.uid, env.data, result);
          await this.network.send(c, msg, new NetworkSendOptions('nothing'));
        }
      }
      if (request?.processErrorEnvelopes) {
        for (const env of request.processErrorEnvelopes) {
          const msg = EditableNetworkMessage.from(env.to, 'actresp', env.uid, env.data, result);
          await this.network.send(c, msg, new NetworkSendOptions('nothing'));
        }
      }
    }
  }

  protected findGateway(): string {
    return ''; // this.gateways[0].id;
  }
}
