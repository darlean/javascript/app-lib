import * as fs from 'fs';

interface ITestMetric {
  moment: string;
  version: string;
  metric: string;
  value?: number;
  unit: string;
  error?: string;
  context?: { [key: string]: unknown };
}

const reportMode = 'md';

const version = process.env.npm_package_version || '';

const fn = 'testmetrics.json.txt';
const reportfileHtml = 'testmetrics-report.html';
const reportfileMd = 'testmetrics-report.md';

function dumpReportToHtml() {
  const lines = fs.readFileSync(fn).toString().split('\n');
  const metrics = new Map<string, ITestMetric[]>();
  for (const line of lines) {
    if (line.trim().length === 0) {
      continue;
    }
    const m = JSON.parse(line) as ITestMetric;
    if (!metrics.has(m.metric)) {
      metrics.set(m.metric, []);
    }
    metrics.get(m.metric)?.push(m);
  }

  const result = [
    '<html><head>',
    '<style>',
    'body { font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Noto Sans", Ubuntu, Cantarell, "Helvetica Neue", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";}',
    'table { border-collapse: collapse; }',
    'td,th { padding-left: 0.5rem; padding-right: 0.5rem; text-align: start; vertical-align: top; }',
    'tr:nth-child(even) {background: #EEE}',
    'tr:nth-child(odd) {background: #FFF}',
    'thead th { position: sticky; top: 0px; background-color: rgba(200,200,200,0.9); }',
    'table table tr:nth-child(even) {background: none}',
    'table table tr:nth-child(odd) {background: none}',
    'table table {font-size: smaller}',
    '</style>',
    '</head><body>',
    '<table>',
    '<thead><tr><th>Metric</th><th>Value</th><th>Context</th><th>Moment</th></tr></thead>',
  ];
  const mSorted = Array.from(metrics.keys()).sort();
  for (const name of mSorted) {
    const m = metrics.get(name);
    if (!m) {
      continue;
    }
    const contexts = new Map<string, ITestMetric>();
    for (const m2 of m) {
      contexts.set(JSON.stringify(m2.context), m2);
    }
    let first = true;
    for (const [cname, c] of contexts.entries()) {
      const value = c.error ?? `${c.value?.toFixed(2)} ${c.unit}`;
      result.push(
        `<tr><td>${first ? name : ''}</td><td style="text-align:right">${value}</td><td>${cname}</td><td>${
          c.moment
        }</td></tr>`,
      );
      first = false;
    }
  }
  result.push('</table></body></html>');
  fs.writeFileSync(reportfileHtml, result.join('\n'));
}

function dumpReportToMd() {
  const lines = fs.readFileSync(fn).toString().split('\n');
  const metrics = new Map<string, ITestMetric[]>();
  for (const line of lines) {
    if (line.trim().length === 0) {
      continue;
    }
    const m = JSON.parse(line) as ITestMetric;
    if (!metrics.has(m.metric)) {
      metrics.set(m.metric, []);
    }
    metrics.get(m.metric)?.push(m);
  }

  const result = ['| Metric | Value | Context | Version | Moment |', '| ------ | ----: | ------- | ------ | ------ |'];
  const mSorted = Array.from(metrics.keys()).sort();
  for (const name of mSorted) {
    const m = metrics.get(name);
    if (!m) {
      continue;
    }
    const contexts = new Map<string, ITestMetric>();
    for (const m2 of m) {
      contexts.set(JSON.stringify(m2.context), m2);
    }
    let first = true;
    for (const [cname, c] of contexts.entries()) {
      const ndecimals = c.unit === 'times' ? 0 : c.unit === 'ops/s' ? 0 : 2;
      const value = c.error ?? `${c.value?.toFixed(ndecimals)} ${c.unit}`;
      result.push(`| ${(first ? name : '').padEnd(40, ' ')} | ${value} | ${cname} | ${c.version} | ${c.moment} |`);
      first = false;
    }
  }
  fs.writeFileSync(reportfileMd, result.join('\n'));
}

export function emitTestMetric(
  metric: string,
  value: number | string,
  unit: string,
  context?: { [key: string]: unknown },
) {
  const m: ITestMetric = {
    moment: new Date().toISOString(),
    version,
    metric,
    context,
    value: typeof value === 'number' ? value : undefined,
    error: typeof value === 'string' ? value : undefined,
    unit,
  };

  fs.appendFileSync(fn, JSON.stringify(m) + '\n');

  if (reportMode === 'md') {
    dumpReportToMd();
  } else {
    dumpReportToHtml();
  }
}
