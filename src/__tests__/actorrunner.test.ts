/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { Logger } from '../logging';
import { sleep } from '../util';
import { action, actor, actorFactory, ActorFactory, ActorRunner, BaseActor, IRuntime, ITime } from '..';
import { ActorRunnerTime } from '../actorrunnertime';
import { IActionResult } from '../services/performing';
import { IPerformActionReq, IPerformActionResp } from '../interfaces/performing';
import { IAcquireLockResult, IGetLockInfoResult } from '../services/locking';
import { IAttachmentRef, Struct, struct } from '../structs';

function obtainRuntime(time: ITime, onAcquire?: (ttl: number) => Promise<IAcquireLockResult>): IRuntime {
  const runtime: IRuntime = {
    time,
    acquireLock: async function (id: string[], ttl: number, _refresh: boolean): Promise<IAcquireLockResult> {
      if (onAcquire) {
        return await onAcquire(ttl);
      }
      return { duration: ttl, holders: ['app1'] };
    },
    releaseLock: async function (): Promise<void> {
      //
    },
    getLockInfo: async function (): Promise<IGetLockInfoResult> {
      throw new Error('Function not implemented.');
    },
    publishInfo: async function (): Promise<void> {
      //
    },
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    dispatchAction: async function <Input, Output>(): Promise<Output> {
      throw new Error('Not implemented');
    },
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    dispatchActionRaw: async function <Input, Output>(): Promise<IActionResult<Output>> {
      throw new Error('Not implemented');
    },
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    fireAction: function <Input>(): void {
      throw new Error('Not implemented');
    },
  };
  return runtime;
}

describe('ActorRunner', () => {
  beforeEach(async () => {
    //
  });

  afterEach(async () => {
    //
  });

  // TODO: Make this test part of application test (app is responsible for lock retries)
  test.skip('ConcurrentActivate', async () => {
    let nActivations = 0;
    let nPerforms = 0;
    const actions: string[] = [];

    class MyActor extends BaseActor {
      public async activate() {
        nActivations++;
      }

      public async DoIt() {
        // console.log('WE ARE IN OUR ACTOR');
        nPerforms++;
        actions.push('BEGIN');
        await sleep(3000);
        actions.push('END');
        return 42;
      }
    }

    const logger = new Logger();
    const time = new ActorRunnerTime(logger.getChildLogger('Time'));
    let idx = -1;
    const runtime = obtainRuntime(time, async (ttl) => {
      idx++;
      await sleep(idx === 0 ? 1000 : 100);
      return idx === 0
        ? {
            duration: -1,
            holders: ['app1'],
          }
        : {
            duration: ttl,
            holders: ['app1'],
          };
    });
    const runner = new ActorRunner(logger.getChildLogger('AppRunner'), 'ar0', runtime, time);
    runner.registerActor(
      new ActorFactory(logger, 'MyActor', 'actor', 1000, ['RuntimeService'], (options) => new MyActor(options)),
    );

    const tasks = [];
    for (let j = 0; j <= 1; j++) {
      tasks.push(
        runner.handleAction(logger, {
          actorType: 'MyActor',
          actorId: ['123'],
          actionName: 'DoIt',
        } as IPerformActionReq),
      );
    }
    await Promise.all(tasks);

    expect(nActivations).toBe(1);
    expect(nPerforms).toBe(2);
    // Test that the actions are non-overlapping:
    expect(actions).toEqual(['BEGIN', 'END', 'BEGIN', 'END']);

    await runner.stop();
  }, 35000);

  test('ConcurrentActions', async () => {
    let nActivations = 0;
    let nPerforms = 0;
    const actions: string[] = [];

    class MyActor extends BaseActor {
      public async activate() {
        nActivations++;
      }

      public async DoIt() {
        // console.log('WE ARE IN OUR ACTOR');
        nPerforms++;
        actions.push('BEGIN');
        await sleep(3000);
        actions.push('END');
        return { value: 42 };
      }
    }

    const logger = new Logger();
    const time = new ActorRunnerTime(logger.getChildLogger('Time'));
    const runtime = obtainRuntime(time);
    const runner = new ActorRunner(logger.getChildLogger('AppRunner'), 'ar0', runtime, time);
    runner.registerActor(
      new ActorFactory(logger, 'MyActor', 'actor', 1000, ['RuntimeService'], (options) => new MyActor(options)),
    );

    // Ensure the running is active
    await runner.handleAction(logger, {
      actorType: 'MyActor',
      actorId: ['123'],
      actionName: 'DoIt',
    } as IPerformActionReq);

    // Invoke 2 actions in parallel
    const tasks = [];
    for (let j = 0; j <= 1; j++) {
      tasks.push(
        runner.handleAction(logger, {
          actorType: 'MyActor',
          actorId: ['123'],
          actionName: 'DoIt',
        } as IPerformActionReq),
      );
    }
    await Promise.all(tasks);

    expect(nActivations).toBe(1);
    expect(nPerforms).toBe(3);
    // Test that the actions are non-overlapping:
    expect(actions).toEqual(['BEGIN', 'END', 'BEGIN', 'END', 'BEGIN', 'END']);

    await runner.stop();
  }, 35000);

  test('InputMode', async () => {
    const actions: string[] = [];
    const actionsDeact: string[] = [];

    @actor({ name: 'MyActor' })
    class MyActor extends BaseActor {
      public async activate() {
        actions.push('+' + this.id[0]);
      }

      public async deactivate() {
        actionsDeact.push('-' + this.id[0]);
      }

      @action()
      public async Regular(value: { Hello: string }): Promise<{ Hello: string }> {
        return {
          Hello: value.Hello,
        };
      }

      @action()
      public async AttachmentInput(
        value: Struct<{ Hello: string; Text: IAttachmentRef; Foo: IAttachmentRef; Bin: IAttachmentRef }>,
      ): Promise<{ Hello: string; Text: string; Bin: string; Foo: string }> {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        return {
          Hello: value.Hello,
          Text: value._att(value.Text).text(),
          Foo: value._att<{ Foo: string }>(value.Foo).struct().Foo,
          Bin: value._att(value.Bin).binary().toString(),
        };
      }

      @action()
      public async AttachmentOutput1(
        value: Struct<{ Hello: string; Text: IAttachmentRef; Foo: IAttachmentRef; Bin: IAttachmentRef }>,
      ): Promise<{ Hello: string; Text: IAttachmentRef; Foo: IAttachmentRef; Bin: IAttachmentRef }> {
        return struct((attach) => ({
          Hello: value.Hello,
          Text: attach(value._att(value.Text).text()),
          Foo: attach(value._att(value.Foo).struct()),
          Bin: attach(value._att(value.Bin).binary()),
        }));
      }

      @action()
      public async AttachmentOutput2(value: {
        Hello: string;
        Text: IAttachmentRef;
        Foo: IAttachmentRef;
        Bin: IAttachmentRef;
      }): Promise<{ Hello: string; Text: IAttachmentRef; Foo: IAttachmentRef; Bin: IAttachmentRef }> {
        return value;
      }

      @action()
      public async AttachmentOutput3(value: {
        Hello: string;
        Text: IAttachmentRef;
        Foo: IAttachmentRef;
        Bin: IAttachmentRef;
      }): Promise<{ sub: IAttachmentRef }> {
        return struct((attach) => ({
          Hello: value.Hello,
          sub: attach(value),
        }));
      }
    }

    const logger = new Logger();
    const time = new ActorRunnerTime(logger.getChildLogger('Time'));
    const runtime = obtainRuntime(time);
    const runner = new ActorRunner(logger.getChildLogger('AppRunner'), 'ar0', runtime, time);

    runner.registerActor(
      actorFactory(logger, {
        capacity: 100,
        clazz: MyActor,
        actorOptions: { actions, actionsDeact },
      }),
      // new ActorFactory(logger, 'MyActor', 'actor', 4, ['RuntimeService'], (options) => new MyActor({...options, actions, actionsDeact})),
    );

    for (const mode of ['Regular', 'AttachmentInput', 'AttachmentOutput1', 'AttachmentOutput2', 'AttachmentOutput3']) {
      type T = { Hello: string; Text: IAttachmentRef; Foo: IAttachmentRef; Bin: IAttachmentRef };
      const reqdata = struct<T>((attach) => ({
        Hello: 'World',
        Text: attach('Text'),
        Bin: attach(Buffer.from('Binary')),
        Foo: attach({ Foo: 'Bar' }),
      }));

      const req = struct<IPerformActionReq>((attach) => ({
        actorType: 'MyActor',
        actorId: ['123'],
        actionName: mode,
        actionHopsRemaining: 4,
        data: attach(reqdata),
      }));

      const respc = await runner.handleAction(logger, req._cont());
      const resp = struct<IPerformActionResp>(respc);
      const data = resp._att(resp.data!).struct();

      // console.log('RESULT', mode, JSON.stringify(result, undefined, '  '));

      //const valueC = result.getChild0();
      //const data = valueC.getDataStruct();
      expect((data as unknown as { Hello: string }).Hello).toBe('World');

      if (mode === 'AttachmentInput') {
        expect((data as unknown as { Foo: string }).Foo).toBe('Bar');
        expect((data as unknown as { Text: string }).Text).toBe('Text');
        expect((data as unknown as { Bin: string }).Bin).toBe('Binary');
      }

      if (mode.startsWith('AttachmentOutput')) {
        const data2 = (mode === 'AttachmentOutput3'
          ? data._att((data as unknown as { sub: IAttachmentRef }).sub).struct()
          : data) as unknown as Struct<T>;

        expect(data2._att(data2.Text).text()).toBe('Text');
        expect(data2._att(data2.Bin).binary().toString()).toBe('Binary');
        expect((data2._att(data2.Foo).struct() as unknown as { Foo: string }).Foo).toBe('Bar');
      }
    }

    await runner.stop();
  }, 35000);

  /*test('Context', async () => {
    const actions: string[] = [];
    
    @actor({ name: 'MyActor' })
    class MyActor extends BaseActor {
      public async activate() {
        actions.push('+' + this.id[0]);
      }

      public async deactivate() {
      }

      @action()
      public async Sub(value: { Hello: string }, context: IActionContext): Promise<{ Hello: string }> {
        const resp = await context.performAction<{Hello: string}, {Byte: string}>({
          actorType: 'MyActor',
          actorId: this.id,
          actionName: 'doSub',
          data: struct(() => ({
            Hello: 'world'
          })
        })
        return {
          Hello: value.Hello,
        };
      }

      @action()
      public async AttachmentInput(
        value: Struct<{ Hello: string; Text: IAttachmentRef; Foo: IAttachmentRef; Bin: IAttachmentRef }>,
      ): Promise<{ Hello: string; Text: string; Bin: string; Foo: string }> {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        return {
          Hello: value.Hello,
          Text: value._att(value.Text).text(),
          Foo: value._att<{ Foo: string }>(value.Foo).struct().Foo,
          Bin: value._att(value.Bin).binary().toString(),
        };
      }

      @action()
      public async AttachmentOutput1(
        value: Struct<{ Hello: string; Text: IAttachmentRef; Foo: IAttachmentRef; Bin: IAttachmentRef }>,
      ): Promise<{ Hello: string; Text: IAttachmentRef; Foo: IAttachmentRef; Bin: IAttachmentRef }> {
        return struct((attach) => ({
          Hello: value.Hello,
          Text: attach(value._att(value.Text).text()),
          Foo: attach(value._att(value.Foo).struct()),
          Bin: attach(value._att(value.Bin).binary()),
        }));
      }

      @action()
      public async AttachmentOutput2(value: {
        Hello: string;
        Text: IAttachmentRef;
        Foo: IAttachmentRef;
        Bin: IAttachmentRef;
      }): Promise<{ Hello: string; Text: IAttachmentRef; Foo: IAttachmentRef; Bin: IAttachmentRef }> {
        return value;
      }

      @action()
      public async AttachmentOutput3(value: {
        Hello: string;
        Text: IAttachmentRef;
        Foo: IAttachmentRef;
        Bin: IAttachmentRef;
      }): Promise<{ sub: IAttachmentRef }> {
        return struct((attach) => ({
          Hello: value.Hello,
          sub: attach(value),
        }));
      }
    }

    const logger = new Logger();
    const time = new ActorRunnerTime(logger.getChildLogger('Time'));
    const runtime = obtainRuntime(time);
    const runner = new ActorRunner(
      'ar0',
      runtime,
      time,
      logger.getChildLogger('AppRunner'),
      logger.getChildLogger('ActorLog'),
    );

    runner.registerActor(
      BaseActorFactory(logger, {
        capacity: 100,
        clazz: MyActor,
        actorOptions: { actions, actionsDeact },
      }),
      // new ActorFactory(logger, 'MyActor', 'actor', 4, ['RuntimeService'], (options) => new MyActor({...options, actions, actionsDeact})),
    );

    for (const mode of ['Regular', 'AttachmentInput', 'AttachmentOutput1', 'AttachmentOutput2', 'AttachmentOutput3']) {
      type T = { Hello: string; Text: IAttachmentRef; Foo: IAttachmentRef; Bin: IAttachmentRef };
      const reqdata = struct<T>((attach) => ({
        Hello: 'World',
        Text: attach('Text'),
        Bin: attach(Buffer.from('Binary')),
        Foo: attach({ Foo: 'Bar' }),
      }));

      const req = struct<IPerformActionReq>((attach) => ({
        actorType: 'MyActor',
        actorId: ['123'],
        actionName: mode,
        actionHopsRemaining: 4,
        data: attach(reqdata),
      }));

      const respc = await runner.handleAction(req._cont());
      const resp = struct<IPerformActionResp>(respc);
      const data = resp._att(resp.data!).struct();

      // console.log('RESULT', mode, JSON.stringify(result, undefined, '  '));

      //const valueC = result.getChild0();
      //const data = valueC.getDataStruct();
      expect((data as unknown as { Hello: string }).Hello).toBe('World');

      if (mode === 'AttachmentInput') {
        expect((data as unknown as { Foo: string }).Foo).toBe('Bar');
        expect((data as unknown as { Text: string }).Text).toBe('Text');
        expect((data as unknown as { Bin: string }).Bin).toBe('Binary');
      }

      if (mode.startsWith('AttachmentOutput')) {
        const data2 = (mode === 'AttachmentOutput3'
          ? data._att((data as unknown as { sub: IAttachmentRef }).sub).struct()
          : data) as unknown as Struct<T>;

        expect(data2._att(data2.Text).text()).toBe('Text');
        expect(data2._att(data2.Bin).binary().toString()).toBe('Binary');
        expect((data2._att(data2.Foo).struct() as unknown as { Foo: string }).Foo).toBe('Bar');
      }
    }

    await runner.stop();
  }, 35000);
*/

  test('AutoRelease', async () => {
    const actions: string[] = [];
    const actionsDeact: string[] = [];

    class MyActor extends BaseActor {
      public async activate() {
        actions.push('+' + this.id[0]);
      }

      public async deactivate() {
        actionsDeact.push('-' + this.id[0]);
      }

      public async DoIt() {
        actions.push(this.id[0]);
        // Gives actur runner the time to actually do a clean up
        await sleep(50);
      }
    }

    const logger = new Logger();
    const time = new ActorRunnerTime(logger.getChildLogger('Time'));
    const runtime = obtainRuntime(time);
    const runner = new ActorRunner(logger.getChildLogger('AppRunner'), 'ar0', runtime, time);
    runner.registerActor(
      new ActorFactory(logger, 'MyActor', 'actor', 4, ['RuntimeService'], (options) => new MyActor(options)),
    );

    // Ensure the running is active
    const order = ['A', 'B', 'C', 'D', 'E', 'C', 'A', 'F'];
    const expected = ['+A', 'A', '+B', 'B', '+C', 'C', '+D', 'D', '+E', 'E', 'C', '+A', 'A', '+F', 'F'];
    const expectedDeact = ['-A', '-B', '-D', '-E'];
    for (const item of order) {
      await runner.handleAction(logger, {
        actorType: 'MyActor',
        actorId: [item],
        actionName: 'DoIt',
      } as IPerformActionReq);
    }

    expect(actions).toEqual(expected);
    expect(actionsDeact).toEqual(expectedDeact);
    await runner.stop();
  }, 35000);

  test('ActorRunnerTime', async () => {
    const logger = new Logger();
    const time = new ActorRunnerTime(logger);
    let intervals: number[] = [];
    let moments: number[] = [];
    let last = time.machineTicks();
    function f() {
      const t = time.machineTicks();
      const now = Math.round((t - last) / 50) * 50;
      intervals.push(now);
      moments.push(moments.length > 0 ? moments[moments.length - 1] + now : now);
      last = t;
    }
    function reset() {
      intervals = [];
      moments = [];
      last = time.machineTicks();
    }

    {
      // Repeatcount = 0 => fire once, no repeats => 1 in total
      reset();
      const timer = time.repeat(f, 'Timer', 100, 50, 0);
      await time.sleep(1000);
      expect(intervals).toEqual([50]);
      timer.cancel();
    }

    {
      // Repeatcount = 1 => Fire one + repeat once = 2 in total
      reset();
      const timer = time.repeat(f, 'Timer', 100, 50, 1);
      await time.sleep(1000);
      expect(intervals).toEqual([50, 100]);
      timer.cancel();
    }

    {
      // Repeatcount > 1
      reset();
      const timer = time.repeat(f, 'Timer', 100, 50, 3);
      await time.sleep(1000);
      expect(intervals).toEqual([50, 100, 100, 100]);
      timer.cancel();
    }

    {
      // Undefined delay = use interval as delay
      reset();
      const timer = time.repeat(f, 'Timer', 100, undefined, 1);
      await time.sleep(1000);
      expect(intervals).toEqual([100, 100]);
      timer.cancel();
    }

    {
      // Undefined repeatcount = indefinately
      reset();
      const timer = time.repeat(f, 'Timer', 100, undefined, undefined);
      await time.sleep(550);
      expect(intervals).toEqual([100, 100, 100, 100, 100]);
      timer.cancel();
    }

    {
      // Pause/Resume without arguments, first tick 'interval' ms after resume moment
      reset();
      const timer = time.repeat(f, 'Timer', 100, undefined, 3);
      await time.sleep(150);
      timer.pause();
      await time.sleep(200);
      timer.resume();
      await time.sleep(1000);
      expect(moments).toEqual([100, 450, 550, 650]);
      timer.cancel();
    }

    {
      // Pause/Resume with resume=0, first tick directly after resume moment
      reset();
      const timer = time.repeat(f, 'Timer', 100, undefined, 3);
      await time.sleep(150);
      timer.pause();
      await time.sleep(200);
      timer.resume(0);
      await time.sleep(1000);
      expect(moments).toEqual([100, 350, 450, 550]);
      timer.cancel();
    }

    {
      // Pause/Resume with resume=50, first tick 50ms after resume moment
      reset();
      const timer = time.repeat(f, 'Timer', 100, undefined, 3);
      await time.sleep(150);
      timer.pause();
      await time.sleep(200);
      timer.resume(50);
      await time.sleep(1000);
      expect(moments).toEqual([100, 400, 500, 600]);
      timer.cancel();
    }

    {
      // Resume without pause, resume should be ignored
      reset();
      const timer = time.repeat(f, 'Timer', 100, undefined, 3);
      await time.sleep(150);
      timer.resume(0);
      await time.sleep(1000);
      expect(moments).toEqual([100, 200, 300, 400]);
      timer.cancel();
    }

    {
      // Pause with auto-resume after 100 ms
      reset();
      const timer = time.repeat(f, 'Timer', 100, undefined, 3);
      await time.sleep(150);
      timer.pause(100);
      await time.sleep(1000);
      expect(moments).toEqual([100, 250, 350, 450]);
      timer.cancel();
    }

    {
      // Pause with auto-resume and resume in between, should resume after interval ms
      // Also tests that the "auto-resume" and "manual resume" do not occur both (only manual resume should be there)
      reset();
      const timer = time.repeat(f, 'Timer', 100, undefined, 3);
      await time.sleep(150);
      timer.pause(200);
      await time.sleep(50);
      timer.resume();
      await time.sleep(1000);
      expect(moments).toEqual([100, 300, 400, 500]);
      timer.cancel();
    }

    {
      // Pause with auto-resume and immediate resume in between, should resume directly after resume call
      reset();
      const timer = time.repeat(f, 'Timer', 100, undefined, 3);
      await time.sleep(150);
      timer.pause(200);
      await time.sleep(50);
      timer.resume(0);
      await time.sleep(1000);
      expect(moments).toEqual([100, 200, 300, 400]);
      timer.cancel();
    }
  }, 50000);
});
