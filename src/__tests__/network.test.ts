import { Logger } from '../logging';
import { EditableNetworkMessage, INetworkMessage, NetworkSendOptions } from '../networktypes';
import { Network } from '../network';
import { sleep } from '../util';
import * as types from '../types';
import { ILogger, parallel } from '..';
import { BaseTransport } from '../basetransport';
import { ActorRunnerTime } from '../actorrunnertime';
import * as uuid from 'uuid';
import { emitTestMetric } from '../testing';

class TransportPool {
  public transports: Map<string, TestTransport>;
  protected logger: ILogger;
  protected peers: types.IPeerConfig[];

  constructor(logger: ILogger, peers: types.IPeerConfig[]) {
    this.transports = new Map();
    this.logger = logger;
    this.peers = peers;
  }

  public async createTransport(id: string): Promise<TestTransport> {
    const t = new TestTransport(this.logger.getChildLogger('Transport', id), id, id, this.peers);
    t.pool = this;
    await t.init();
    this.transports.set(id, t);
    for (const transport of this.transports.values()) {
      transport.connect();
    }
    return t;
  }

  public findTransport(id: string) {
    return this.transports.get(id);
  }
}

class TestTransport extends BaseTransport<TestTransport, TestTransport> {
  public pool?: TransportPool;

  public receive(msg: INetworkMessage) {
    this.logger.debug('Received [Message]', () => ({ Message: msg }));

    if (this.handler) {
      this.handler(msg).catch((e) => {
        this.logger.error('Unexpected error during handling of message: [Error]', () => ({ Error: e }));
      });
    }
  }

  public connect(): void {
    for (const transport of this.pool?.transports.values() || []) {
      let peer = this.peers.get(transport.id);
      if (!peer) {
        peer = {
          id: transport.id,
          kind: 'client',
        };
        this.peers.set(transport.id, peer);
      }
      peer.incoming = transport;
      peer.outgoing = transport;
    }
  }

  protected writeOutgoing(socket: TestTransport, msg: INetworkMessage, cb?: (error: unknown) => void): void {
    this.logger.debug('Sending [Message]to [Remote]', () => ({ Message: msg, Remote: socket.id }));
    socket.receive(msg);
    if (cb) {
      cb(undefined);
    }
  }

  protected writeIncoming(socket: TestTransport, msg: INetworkMessage, cb?: (error: unknown) => void): void {
    this.logger.debug('Sending [Message]to [Remote]', () => ({ Message: msg, Remote: socket.id }));
    socket.receive(msg);
    if (cb) {
      cb(undefined);
    }
  }
}

async function testClientHub(sender: string, receiver: string, description: string, expected?: 'received' | 'error') {
  const logger = new Logger();
  // logger.addMask('*', DEBUG_AND_UP);
  const pool = new TransportPool(logger.getChildLogger('Pool'), [
    {
      id: 'hub',
      address: '',
    },
    {
      id: 'hub3',
      address: '',
    },
  ]);
  const client = new Network(logger.getChildLogger('Network', 'client'), 'client', false);
  client.addTransport(await pool.createTransport('client'));
  const client2 = new Network(logger.getChildLogger('Network', 'client2'), 'client2', false);
  client2.addTransport(await pool.createTransport('client2'));
  const hub = new Network(logger.getChildLogger('Network', 'hub'), 'hub', true);
  hub.addTransport(await pool.createTransport('hub'));
  let received: INetworkMessage | undefined;
  const dest = receiver === 'client' ? client : receiver === 'client2' ? client2 : hub;
  dest.on('received', (c, msg) => {
    logger.debug('Destination received [Msg]', () => ({ Msg: msg }));
    received = msg;
  });
  const source = sender === 'client' ? client : hub;
  const uid = uuid.v4();
  let error: unknown;
  try {
    const msg = EditableNetworkMessage.from(receiver, 'actreq', uid, undefined, description);
    await source.send(logger, msg, new NetworkSendOptions('delivered'));
  } catch (e) {
    error = e;
  }

  if (expected !== 'error') {
    expect(error).toBeUndefined();
    expect(received?.getEnvelopeContainer().getChild(0).getDataText()).toBe(description);
    expect(received?.getEnvelopeContainer().getDataStruct().uid).toBe(uid);
  } else {
    expect(error).toBeDefined();
    expect(received).toBeUndefined();
  }
}

async function testPerformance(sender: string, receiver: string, description: string) {
  const logger = new Logger();
  // logger.addMask('*', DEBUG_AND_UP);
  const pool = new TransportPool(logger.getChildLogger('Pool'), [
    {
      id: 'hub',
      address: '',
    },
    {
      id: 'hub3',
      address: '',
    },
  ]);
  const client = new Network(logger.getChildLogger('Network', 'client'), 'client', false);
  client.addTransport(await pool.createTransport('client'));
  const client2 = new Network(logger.getChildLogger('Network', 'client2'), 'client2', false);
  client2.addTransport(await pool.createTransport('client2'));
  const hub = new Network(logger.getChildLogger('Network', 'hub'), 'hub', true);
  hub.addTransport(await pool.createTransport('hub'));
  let nreceived = 0;
  const dest = receiver === 'client' ? client : receiver === 'client2' ? client2 : hub;
  dest.on('received', (_envelope) => {
    // logger.debug('Destination received [Envelope]', { Envelope: envelope });
    nreceived++;
  });
  const source = sender === 'client' ? client : hub;
  const time = new ActorRunnerTime(logger);
  for (const p of [1, 4, 16, 64, 256]) {
    const a = 100;
    const b = 100;
    const tasks = [];
    for (let i = 0; i < b; i++) {
      tasks.push(async () => {
        const msg = EditableNetworkMessage.from(receiver, 'actreq', uuid.v4(), undefined, description);

        await source.send(logger, msg, new NetworkSendOptions('delivered'));
      });
    }
    const start = time.machineTicks();
    nreceived = 0;
    for (let j = 0; j < a; j++) {
      await parallel(tasks, 10000, p);
    }
    const stop = time.machineTicks();
    const duration = stop - start;
    // console.log('Duration', duration);
    emitTestMetric(
      'network.throughput',
      nreceived === a * b ? Math.round((a * b * 1000) / duration) : `Only ${nreceived} out of ${a * b} items received`,
      'ops/sec',
      { a, b, n: a * b, parallel: p },
    );
    emitTestMetric(
      'network.period',
      nreceived === a * b ? duration / (a * b) : `Only ${nreceived} out of ${a * b} items received`,
      'ms',
      { a, b, n: a * b, parallel: p },
    );
    expect(duration).toBeLessThan(1000);
    expect(nreceived).toBe(a * b);
  }
}

describe('Network', () => {
  test('Send to self (loopback)', async () => {
    const logger = new Logger();
    const network = new Network(logger, 'n0', true);
    let received: INetworkMessage | undefined;
    network.on('received', (c, msg) => {
      received = msg;
    });
    const msg = EditableNetworkMessage.from('n0', 'actreq', uuid.v4(), undefined, 'To self');

    await network.send(logger, msg, new NetworkSendOptions('nothing'));
    await sleep(100);
    expect(received?.getEnvelopeContainer().getChild(0).getDataText()).toBe('To self');
  });

  test('Send from client to self', async () => {
    await testClientHub('client', 'client', 'Client to self');
  });
  test('Send from client to hub', async () => {
    await testClientHub('client', 'hub', 'Client to Hub');
  });

  test('Send from hub to itself', async () => {
    await testClientHub('hub', 'hub', 'Hub to itself');
  });

  test('Send from hub to client', async () => {
    await testClientHub('hub', 'client', 'Hub to client');
  });

  test('Send from client via hub to other client', async () => {
    await testClientHub('client', 'client2', 'Client to other client');
  });

  test('Send from client via hub to disconnected/unexisting client', async () => {
    await testClientHub('client', 'client3', 'Client to disconnected client', 'error');
  });

  test('Send from client via hub to disconnected/unexisting hub', async () => {
    await testClientHub('client', 'hub3', 'Client to disconnected hub', 'error');
  });

  test('Send from hub to client (performance)', async () => {
    await testPerformance('hub', 'client', 'Hub to client');
  });

  /*test.skip('Basic network', async () => {
    const pool = new InProcessPool();
    const n0 = new Network(new Logger(undefined, 'Network', 'N0'), 'n0', true);
    const t0 = pool.addNode('n0');
    n0.addTransport(t0);
    const n1 = new Network(new Logger(undefined, 'Network', 'N1'), 'n1', true);
    const t1 = pool.addNode('n1');
    n1.addTransport(t1);

    let n = 0;
    n1.on('received', (envelope) => {
      console.log('Received', envelope);
      n++;
    });
    await n0.send(
      {
        receiver: 'n1',
        contents: [
          {
            name: 'con',
            contents: 'hello',
          },
        ],
      },
      new NetworkSendOptions('nothing'),
    );

    await sleep(2000);
    expect(n).toBe(1);

    await n0.send(
      {
        receiver: 'n1',
        contents: [
          {
            name: 'con',
            contents: 'hello',
          },
        ],
      },
      new NetworkSendOptions('delivered'),
    );
    expect(n).toBe(2);

    await n0.send(
      {
        receiver: 'n2',
        contents: [
          {
            name: 'con',
            contents: 'hello',
          },
        ],
      },
      new NetworkSendOptions('delivered'),
    );
  }, 15000);
  */

  // TODO: SPlit this test into 3 subtests
  // TODO: Test Timeout (not yet implemented)
  // TODO: Test what if A -> B -> C, but C is unavailable.
  // TODO: Test A -> B -> C (happy flow)
  // TODO: I case of exception, also have the events fired (for both network and options). PErhaps do not output the exception
  // (just emit an error to avoid confusion).
  // TODO: Test hopsremaining works (not < 0)
  // TODO: Test shortcut for sending to 'self'
});
