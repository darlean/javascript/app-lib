import { sleep } from '../util';
import { IAttachmentRef, parallel, ParallelAbort, struct } from '..';
import { performance } from 'perf_hooks';
import * as uuid from 'uuid';
import { ContainerData, EditableContainer } from '../containers';
import { BufferContainer, ContainerToBuffers } from '../binaryencoding';
import { SharedExclusiveLock } from '../selock';
import { emitTestMetric } from '../testing';

describe('Util', () => {
  test('Parallel', async () => {
    const log: number[] = [];
    const tasks = [
      async () => {
        await sleep(50);
        log.push(1);
        return 1;
      },
      async () => {
        await sleep(100);
        log.push(2);
        return 2;
      },
      async () => {
        await sleep(200);
        log.push(3);
        return 3;
      },
      async () => {
        await sleep(300);
        log.push(4);
        return 4;
      },
      async () => {
        await sleep(500);
        log.push(5);
        return 5;
      },
    ];
    const results = await parallel(tasks, 1000);
    expect(log).toEqual([1, 2, 3, 4, 5]);
    expect(results.result).toBeUndefined();
    expect(results.results).toEqual([{ result: 1 }, { result: 2 }, { result: 3 }, { result: 4 }, { result: 5 }]);
  });

  test('ParallelWithTimeout', async () => {
    const log: number[] = [];
    const tasks = [
      async () => {
        await sleep(50);
        log.push(1);
        return 1;
      },
      async () => {
        await sleep(100);
        log.push(2);
        return 2;
      },
      async () => {
        await sleep(200);
        log.push(3);
        return 3;
      },
      async () => {
        await sleep(300);
        log.push(4);
        return 4;
      },
      async () => {
        await sleep(500);
        log.push(5);
        return 5;
      },
    ];
    const results = await parallel(tasks, 250);
    expect(log).toEqual([1, 2, 3]);
    expect(results.result).toBeUndefined();
    expect(results.results).toEqual([{ result: 1 }, { result: 2 }, { result: 3 }, {}, {}]);

    await sleep(600);

    expect(log).toEqual([1, 2, 3, 4, 5]);
    expect(results.result).toBeUndefined();
    expect(results.results).toEqual([{ result: 1 }, { result: 2 }, { result: 3 }, {}, {}]);
  });

  test('ParallelWithAbort', async () => {
    const log: number[] = [];
    const tasks = [
      async () => {
        await sleep(50);
        log.push(1);
        return 1;
      },
      async (abort: ParallelAbort<number>) => {
        await sleep(100);
        log.push(99);
        abort(99);
        log.push(2);
        return 2;
      },
      async () => {
        await sleep(200);
        log.push(3);
        return 3;
      },
      async (abort: ParallelAbort<number>) => {
        await sleep(300);
        log.push(98);
        abort(98);
        log.push(4);
        return 4;
      },
      async () => {
        await sleep(500);
        log.push(5);
        return 5;
      },
    ];
    const results = await parallel(tasks, 400);
    expect(log).toEqual([1, 99, 2]);
    expect(results.result).toBe(99);
    expect(results.results).toEqual([{ result: 1 }, { result: 2 }, {}, {}, {}]);

    await sleep(600);

    expect(log).toEqual([1, 99, 2, 3, 98, 4, 5]);
    expect(results.result).toBe(99);
    expect(results.results).toEqual([{ result: 1 }, { result: 2 }, {}, {}, {}]);
  });

  test('Containers', () => {
    const c = new EditableContainer({ Hello: 'World' });
    c.addChild(new EditableContainer({ Bye: 'Moon' }));
    c.addChild(new EditableContainer(Buffer.from('Bye, Moon!')));

    const bufs: Buffer[] = [];
    new ContainerToBuffers().toBuffers(c, bufs);

    const buf = Buffer.concat(bufs);
    const d = new BufferContainer<{ Hello: string }, ContainerData>(buf, 0);
    expect(d.getDataStruct().Hello).toBe('World');
    expect(d.getChildCount()).toBe(2);
    expect(d.getChild<{ Bye: string }>(0).getDataStruct().Bye).toBe('Moon');
    expect((d.getChild(1).getDataBinary() as Buffer).toString()).toBe('Bye, Moon!');
    expect(d.getChild(0).getKind() === 'structure');
    expect(d.getChild(1).getKind() === 'binary');
  });

  test('Containers Performance Buffers', () => {
    const len = 100;
    const n = 10000;
    const c = new EditableContainer(Buffer.alloc(len, '0'));
    let nreceived = 0;
    c.addChild(new EditableContainer(Buffer.alloc(len, '1')));
    c.addChild(new EditableContainer(Buffer.alloc(len, '2')));

    const start = performance.now();
    for (let idx = 0; idx < n; idx++) {
      const bufs: Buffer[] = [];
      new ContainerToBuffers().toBuffers(c, bufs);
      const buf = Buffer.concat(bufs);
      const d = new BufferContainer(buf, 0);
      nreceived += d.getDataBinary().length;
    }
    const end = performance.now();
    const duration = end - start;
    const rate = (n * 1000) / duration;
    emitTestMetric('util.containers.binary.duration', duration, 'ms', { n });
    emitTestMetric('util.containers.binary.rate', rate, 'ops/s', { n });
    expect(nreceived).toBe(n * len);
    expect(duration).toBeLessThan(1000);
  });

  test('Containers Performance Json', () => {
    for (const len of [1, 4, 16, 64, 256, 1024]) {
      const n = 1000;
      const struct: { [key: string]: number } = {};
      const struct1: { [key: string]: number } = {};
      const struct2: { [key: string]: number } = {};
      for (let i = 0; i < len; i++) {
        struct['Item' + i.toFixed(0)] = i;
        struct1['Item' + i.toFixed(0)] = i;
        struct2['Item' + i.toFixed(0)] = i;
      }

      const c = new EditableContainer(struct);
      c.addChild(new EditableContainer(struct1));
      c.addChild(new EditableContainer(struct2));

      let duration0 = 0;
      let duration1 = 0;
      let duration2 = 0;

      {
        // Run 1: with everything json objects (slowest)
        let nreceived = 0;
        const start = performance.now();
        for (let idx = 0; idx < n; idx++) {
          const bufs: Buffer[] = [];
          new ContainerToBuffers().toBuffers(c, bufs);
          const buf = Buffer.concat(bufs);
          const d = new BufferContainer<{ [key: string]: number }, ContainerData>(buf, 0);
          nreceived += 1 + d.getDataStruct()['Item' + (len - 1).toFixed(0)];
        }
        const end = performance.now();
        duration0 = end - start;
        const rate0 = (n * 1000) / duration0;

        emitTestMetric(
          'util.containers.struct.a.duration',
          nreceived === n * len ? duration0 : `Only received ${nreceived} out of ${n * len} items`,
          'ms',
          { n, len },
        );
        emitTestMetric(
          'util.containers.struct.a.rate',
          nreceived === n * len ? rate0 : `Only received ${nreceived} out of ${n * len} items`,
          'ops/s',
          { n, len },
        );
        expect(nreceived).toBe(n * len);
        expect(duration0).toBeLessThan(2000);
      }

      {
        // Run 2: Top level json, 2 child nodes are copied from 'input'
        // The 2 child nodes chould be copied as buffer (not from/to json)
        // so it should be faster than the first run
        let nreceived = 0;
        const bufs: Buffer[] = [];
        new ContainerToBuffers().toBuffers(c, bufs);
        const buf = Buffer.concat(bufs);
        const z = new BufferContainer(buf, 0);
        const struct2 = JSON.parse(JSON.stringify(struct));
        const c2 = new EditableContainer(struct2);
        c2.addChild(z.getChild(0));
        c2.addChild(z.getChild(1));

        const start = performance.now();
        for (let idx = 0; idx < n; idx++) {
          const bufs: Buffer[] = [];
          new ContainerToBuffers().toBuffers(c2, bufs);
          const buf = Buffer.concat(bufs);
          const d = new BufferContainer<{ [key: string]: number }, ContainerData>(buf, 0);
          nreceived += 1 + d.getDataStruct()['Item' + (len - 1).toFixed(0)];
        }
        const end = performance.now();
        duration1 = end - start;
        const rate1 = (n * 1000) / duration1;
        emitTestMetric(
          'util.containers.struct.b.duration',
          nreceived === n * len ? duration1 : `Only received ${nreceived} out of ${n * len} items`,
          'ms',
          { n, len },
        );
        emitTestMetric(
          'util.containers.struct.b.rate',
          nreceived === n * len ? rate1 : `Only received ${nreceived} out of ${n * len} items`,
          'ops/s',
          { n, len },
        );
        // console.log('Container Duration', duration1, 'ms for ', n, ' times ', len, ' entries with prepacked children');
        expect(nreceived).toBe(n * len);
        expect(duration1).toBeLessThan(2000);
        expect(duration1).toBeLessThan(duration0 * 0.9);
      }

      {
        // Run 3: All 3 containers are copied from 'input'.
        // All 3 nodes should be copied as buffer (not from/to json)
        // so it should be faster than the other 2 runs
        let nreceived = 0;
        const bufs: Buffer[] = [];
        new ContainerToBuffers().toBuffers(c, bufs);
        const buf = Buffer.concat(bufs);
        const c2 = new BufferContainer<ContainerData, ContainerData>(buf, 0);
        const start = performance.now();
        for (let idx = 0; idx < n; idx++) {
          const bufs: Buffer[] = [];
          new ContainerToBuffers().toBuffers(c2, bufs);
          const buf = Buffer.concat(bufs);
          const d = new BufferContainer<{ [key: string]: number }, ContainerData>(buf, 0);
          nreceived += 1 + d.getDataStruct()['Item' + (len - 1).toFixed(0)];
        }
        const end = performance.now();
        duration2 = end - start;
        const rate2 = (n * 1000) / duration1;
        emitTestMetric(
          'util.containers.struct.c.duration',
          nreceived === n * len ? duration2 : `Only received ${nreceived} out of ${n * len} items`,
          'ms',
          { n, len },
        );
        emitTestMetric(
          'util.containers.struct.c.rate',
          nreceived === n * len ? rate2 : `Only received ${nreceived} out of ${n * len} items`,
          'ops/s',
          { n, len },
        );
        // console.log('Container Duration', duration2, 'ms for ', n, ' times ', len, ' entries with all 3 nodes prepacked');
        expect(nreceived).toBe(n * len);
        expect(duration2).toBeLessThan(1000);
        expect(duration2).toBeLessThan(duration1 * 0.9);
      }
    }
  });

  test('JSON Performance', () => {
    const n = 100000;
    const msg = {
      receiver: '12345@cluster',
      hops: 12,
      onDF: [{ to: '12345@cluster', data: '832498745694864964864873743987287584728375' }],
      onF: [{ to: '99999@cluster', data: '32ireqwiqrewuir448784erqwy8yfn8fne8ncu8ucucf' }],
    };
    const start = performance.now();
    for (let idx = 0; idx < n; idx++) {
      //const s = '12345@cluster' + ' ' + idx.toFixed(0);
      //const parts = s.split(' ');

      //JSON.parse(JSON.stringify(msg));

      const msg2 =
        'onDF ' +
        msg.onDF[0].to +
        ' DATA ' +
        msg.onDF[0].data +
        '\n' +
        'onD ' +
        msg.onF[0].to +
        ' DATA ' +
        msg.onF[0].data +
        '\n';
      const a = msg2.indexOf('\nonD');
      const b = msg2.indexOf('\n', a + 1);
      const c = msg2.substring(a, b);
      c.split(' ');
      const a2 = msg2.indexOf('\nonD');
      const b2 = msg2.indexOf('\n', a2 + 1);
      const c2 = msg2.substring(a2, b2);
      c2.split(' ');
    }
    const end = performance.now();
    const duration = end - start;
    const rate = (n * 1000) / duration;
    emitTestMetric('util.json.duration', duration, 'ms', { n });
    emitTestMetric('util.json.rate', rate, 'ops/s', { n });
    // console.log('Container Duration', duration, ' for ', n, ' rate ', rate.toFixed(0), '/s');
  });

  test('UUID Performance', () => {
    const n = 100000;
    /*const msg = {
      receiver: '12345@cluster',
      hops: 12,
      onDF: [{to: '12345@cluster', data: '832498745694864964864873743987287584728375'}],
      onF: [{to: '99999@cluster', data: '32ireqwiqrewuir448784erqwy8yfn8fne8ncu8ucucf'}]
    }*/
    const start = performance.now();
    for (let idx = 0; idx < n; idx++) {
      /*const kv = new EditableKeyValues();
      kv.addMulti('DF', [msg.onDF[0].to], msg.onDF[0].data);
      kv.addMulti('D', [msg.onF[0].to], msg.onF[0].data);
      const buf = kv.toBuffer();*/
      /*const kv2 = new BufferKeyValues(buf);
      kv2.getValues('D');
      kv2.getData('D');*/
      uuid.v4();
    }
    const end = performance.now();
    const duration = end - start;
    const rate = (n * 1000) / duration;
    emitTestMetric('util.uuid.duration', duration, 'ms', { n });
    emitTestMetric('util.uuid.rate', rate, 'ops/s', { n });
    // console.log('Duration UUID ', duration, ' for ', n, ' rate ', rate.toFixed(0), '/s');
  });

  test('struct', () => {
    type T = { Hello: string; Text: IAttachmentRef; Binary: IAttachmentRef; Struct: IAttachmentRef };

    const s = struct({ Hello: 'World' });
    expect(s.Hello).toBe('World');

    const t = struct<T>((attach) => ({
      Hello: 'World',
      Text: attach('text'),
      Binary: attach(Buffer.from('binary')),
      Struct: attach({ Bye: 'Moon' }),
    }));
    expect(t.Hello).toBe('World');
    expect(t._att(t.Text).text()).toBe('text');
    expect(t._att(t.Binary).binary().toString()).toBe('binary');
    expect(t._att<{ Bye: string }>(t.Struct).struct().Bye).toBe('Moon');

    const u = struct<{ Hello: string; Sub: IAttachmentRef }>((attach) => ({
      Hello: 'World',
      Sub: attach(t),
    }));
    const sub = u._att<typeof t>(u.Sub).struct();
    expect(sub.Hello).toBe('World');
    expect(sub._att(sub.Text).text()).toBe('text');
    expect(sub._att(sub.Binary).binary().toString()).toBe('binary');
    expect(sub._att<{ Bye: string }>(sub.Struct).struct().Bye).toBe('Moon');
    // Check that t is not modified although it is included in u
    expect(t.Hello).toBe('World');
    expect(t._att(t.Text).text()).toBe('text');
    expect(t._att(t.Binary).binary().toString()).toBe('binary');
    expect(t._att<{ Bye: string }>(t.Struct).struct().Bye).toBe('Moon');

    const v = struct(u);
    expect(v).toBe(u);

    //const s2 = struct({ hello: 'World' });
    // console.log(s2);
  });

  test('SharedExclusiveLock', async () => {
    const lock = new SharedExclusiveLock('shared');

    {
      // Test multiple shared
      await lock.beginShared('A', []);
      await lock.beginShared('B', []);
      await lock.endShared('A');
      await lock.endShared('B');
    }

    {
      // Test multiple exclusive. Should block.
      let fired = false;
      setTimeout(() => {
        fired = true;
        lock.endExclusive('A');
      }, 1000);
      await lock.beginExclusive('A', []);
      await lock.beginExclusive('B', []);
      expect(fired).toBeTruthy();
      lock.endExclusive('B');
    }

    {
      // Test multiple exclusive with proper reentrancy token.
      let fired = false;
      const t = setTimeout(() => {
        fired = true;
        lock.endExclusive('A');
      }, 1000);
      await lock.beginExclusive('A', []);
      await lock.beginExclusive('B', ['A']);
      expect(fired).toBeFalsy();
      lock.endExclusive('B');
      lock.endExclusive('A');
      clearTimeout(t);
    }

    {
      // Test reentrancy for shared within exclusive
      await lock.beginExclusive('A', []);
      await lock.beginShared('B', ['A']);
      await lock.endShared('B');
      await lock.endExclusive('A');
    }

    {
      // Test no reentrancy for shared within exclusive with other reentrancyToken
      let fired = false;
      setTimeout(() => {
        fired = true;
        lock.endExclusive('A');
      }, 1000);
      await lock.beginExclusive('A', []);

      // Blocks until timeout fires when A is released
      await lock.beginShared('B', ['C']);
      expect(fired).toBeTruthy();
      await lock.endShared('B');
    }

    {
      // Test no reentrancy for exclusive within shared lock (no upgrade from shared to exclusive)
      await lock.beginShared('A', []);

      // Should raise error
      let msg = '';
      try {
        await lock.beginExclusive('B', ['A']);
      } catch (e) {
        msg = e as string;
      }
      expect(msg).toBe('NO_UPGRADE');
      await lock.endShared('A');
    }
  });
});
