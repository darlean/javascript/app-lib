import { Logger } from '../logging';
import { sleep } from '../util';
import { ActorFactory, BaseActor, ILogger, ILogMetric, parallel } from '..';
import { Application } from '../application';
import { RuntimeApplication } from '../runtimeapplication';
import * as types from '../types';
import { BaseTransport } from '../basetransport';
import { INetworkMessage } from '../networktypes';
import { TraceCollector } from '../tracecollector';
import {
  IAcquireLockRequest,
  IAcquireLockResponse,
  IGetLockInfoRequest,
  IGetLockInfoResponse,
} from '../interfaces/locking';
import { Tracer } from '../tracer';
import { emitTestMetric } from '../testing';
import { WsTransport } from '../wstransport2';
import { ITraceInfo } from '../interfaces/tracing';

class TransportPool {
  public transports: Map<string, TestTransport>;
  protected logger: ILogger;
  protected peers: types.IPeerConfig[];

  constructor(logger: ILogger, peers: types.IPeerConfig[]) {
    this.transports = new Map();
    this.logger = logger;
    this.peers = peers;
  }

  public async createTransport(id: string): Promise<TestTransport> {
    const t = new TestTransport(this.logger.getChildLogger('Transport', id), id, id, this.peers);
    t.pool = this;
    await t.init();
    this.transports.set(id, t);
    for (const transport of this.transports.values()) {
      transport.connect();
    }
    return t;
  }

  public findTransport(id: string) {
    return this.transports.get(id);
  }
}

async function createWsTransport(
  logger: ILogger,
  id: string,
  address: string,
  peers: types.IPeerConfig[],
): Promise<WsTransport> {
  const t = new WsTransport(logger, id, address, peers);
  return t;
}

class TestTransport extends BaseTransport<TestTransport, TestTransport> {
  public pool?: TransportPool;

  public receive(msg: INetworkMessage) {
    this.logger.debug('Received [Message]', () => ({ Message: msg }));

    if (this.handler) {
      this.handler(msg).catch((e) => {
        this.logger.error('Unexpected error during handling of message: [Error]', () => ({ Error: e }));
      });
    }
  }

  public connect(): void {
    for (const transport of this.pool?.transports.values() || []) {
      let peer = this.peers.get(transport.id);
      if (!peer) {
        peer = {
          id: transport.id,
          kind: 'client',
        };
        this.peers.set(transport.id, peer);
      }
      peer.incoming = transport;
      peer.outgoing = transport;
    }
  }

  protected writeOutgoing(socket: TestTransport, msg: INetworkMessage, cb?: (error: unknown) => void): void {
    this.logger.debug('Sending [Message]to [Remote]', () => ({ Message: msg, Remote: socket.id }));
    socket.receive(msg);
    if (cb) {
      cb(undefined);
    }
  }

  protected writeIncoming(socket: TestTransport, msg: INetworkMessage, cb?: (error: unknown) => void): void {
    this.logger.debug('Sending [Message]to [Remote]', () => ({ Message: msg, Remote: socket.id }));
    socket.receive(msg);
    if (cb) {
      cb(undefined);
    }
  }
}

describe('RuntimeApplication', () => {
  beforeEach(async () => {
    //
  });

  afterEach(async () => {
    //
  });

  class MyActor extends BaseActor {
    public async DoIt(data: { value?: number }) {
      // console.log('WE ARE IN OUR ACTOR');

      // await sleep(10);

      if (data.value === undefined) {
        return { value: 42 };
      }
      return data;
    }
  }

  test('Lock', async () => {
    const c = new Logger();
    const newApp = async (id: string, port: string) => {
      const logger = new Logger(undefined, 'App', id);
      // logger.addMask('*LockImpl*', DEEP_AND_UP);
      // logger.addMask('*Runner*', DEEP_AND_UP);
      // logger.addMask('*Network*', DEEP_AND_UP);
      // logger.addMask('*App*', DEEP_AND_UP);

      const app = new Application(
        'cluster0',
        id,
        'localhost:' + port,
        [
          {
            id: 'app0@cluster0',
            address: 'localhost:6100',
          },
          {
            id: 'app1@cluster0',
            address: 'localhost:6101',
          },
          {
            id: 'app2@cluster0',
            address: 'localhost:6102',
          },
        ],
        logger,
      );
      const rapp = new RuntimeApplication(logger, app, ['app0@cluster0', 'app1@cluster0', 'app2@cluster0']);
      await rapp.init();
      app
        .getRunner()
        .registerActor(
          new ActorFactory(logger, 'MyActor', 'actor', 1000, ['RuntimeService'], (options) => new MyActor(options)),
        );
      return { app, rapp };
    };

    const app0 = await newApp('app0', '6100');
    const app1 = await newApp('app1', '6101');
    const app2 = await newApp('app2', '6102');

    try {
      await sleep(5000);

      // console.log('========= RUNNING ========');

      // Run the same series of actions twice. On second run, one node is stopped. It all should keep working.
      for (let idx = 0; idx <= 1; idx++) {
        if (idx === 1) {
          await app2.app.stop();
          await sleep(3000);
        }
        // console.log('-> A', idx);
        const lock0 = await app0.app.dispatchAction(c, {
          actorType: 'RuntimeService',
          actorId: [],
          actionName: 'AcquireLock',
          data: {
            id: ['123'],
            requester: 'app0',
            ttl: 60 * 1000,
          } as IAcquireLockRequest,
        });

        // console.log('LOCK 0 RESPONSE', lock0.data);
        expect((lock0 as IAcquireLockResponse)?.duration).toBeGreaterThan(0);

        // console.log('-> B', idx);
        const info0 = await app0.app.dispatchAction(c, {
          actorType: 'RuntimeService',
          actorId: [],
          actionName: 'GetLockInfo',
          data: {
            id: ['123'],
          } as IGetLockInfoRequest,
        });
        expect((info0 as IGetLockInfoResponse)?.holders).toEqual(['app0']);

        // console.log('-> C', idx);
        const lock1 = await app1.app.dispatchAction(c, {
          actorType: 'RuntimeService',
          actorId: [],
          actionName: 'AcquireLock',
          data: {
            id: ['123'],
            requester: 'app1',
            ttl: 60 * 1000,
          } as IAcquireLockRequest,
        });

        // console.log('-> D', idx);
        // console.log('LOCK 1 RESPONSE', lock1.data);
        expect((lock1 as IAcquireLockResponse)?.duration).toBeLessThanOrEqual(0);

        const info1 = await app1.app.dispatchAction(c, {
          actorType: 'RuntimeService',
          actorId: [],
          actionName: 'GetLockInfo',
          data: {
            id: ['123'],
          } as IGetLockInfoRequest,
        });
        expect((info1 as IGetLockInfoResponse)?.holders).toEqual(['app0']);

        // console.log('-> E', idx);

        // Release by app1 that does not hold the lock -- lock should still be present afterwards
        await app1.app.dispatchAction(c, {
          actorType: 'RuntimeService',
          actorId: [],
          actionName: 'ReleaseLock',
          data: {
            id: ['123'],
            requester: 'app1',
            ttl: 60 * 1000,
          },
        });

        // console.log('-> F', idx);

        const info0a = await app0.app.dispatchAction(c, {
          actorType: 'RuntimeService',
          actorId: [],
          actionName: 'GetLockInfo',
          data: {
            id: ['123'],
          } as IGetLockInfoRequest,
        });
        expect((info0a as IGetLockInfoResponse)?.holders).toEqual(['app0']);

        // console.log('-> G', idx);

        // Properly release by app0 that holds the lock -- lock should not be present anymore afterwards
        await app0.app.dispatchAction(c, {
          actorType: 'RuntimeService',
          actorId: [],
          actionName: 'ReleaseLock',
          data: {
            id: ['123'],
            requester: 'app0',
            ttl: 60 * 1000,
          },
        });

        // console.log('-> H', idx);
        const info1a = await app1.app.dispatchAction(c, {
          actorType: 'RuntimeService',
          actorId: [],
          actionName: 'GetLockInfo',
          data: {
            id: ['123'],
          } as IGetLockInfoRequest,
        });
        expect((info1a as IGetLockInfoResponse)?.holders).toEqual([]);
      }

      // console.log('-> I');

      // Also stop the 2nd app. Only 1 left. Acquire should fail (no majority).
      await app1.app.stop();
      await sleep(4000);

      // console.log('-> J');

      const lock00 = await app0.app.dispatchAction(c, {
        actorType: 'RuntimeService',
        actorId: [],
        actionName: 'AcquireLock',
        data: {
          id: ['123'],
          requester: 'app0',
          ttl: 60 * 1000,
        } as IAcquireLockRequest,
      });
      // console.log('-> K');
      expect((lock00 as IAcquireLockResponse)?.duration).toBeLessThanOrEqual(0);
    } finally {
      await sleep(1000);
      try {
        // console.log('==== STOPPING =====');
        await app0.app.stop();
        // console.log('====== STOPPED ==========');
      } catch (e) {
        console.log('Error during shutdown:', e);
      }
      await sleep(1000);
    }
  }, 35000);

  test('Tracing', async () => {
    const tracers: Tracer[] = [];

    const t = new Tracer();
    tracers.push(t);
    const c = new Logger(undefined, undefined, undefined, undefined, t);
    const c1 = c.getChildContext('Test', undefined, undefined, {
      correlationIds: ['CORR01'],
    });
    const newApp = async (id: string, port: string) => {
      const t = new Tracer();
      const logger = new Logger(undefined, 'App', id, undefined, t);
      tracers.push(t);

      const app = new Application(
        'cluster0',
        id,
        'localhost:' + port,
        [
          {
            id: 'app0@cluster0',
            address: 'localhost:6150',
          },
          {
            id: 'app1@cluster0',
            address: 'localhost:6151',
          },
          {
            id: 'app2@cluster0',
            address: 'localhost:6152',
          },
        ],
        logger,
      );
      const rapp = new RuntimeApplication(logger, app, ['app0@cluster0', 'app1@cluster0', 'app2@cluster0']);
      await rapp.init();
      app
        .getRunner()
        .registerActor(
          new ActorFactory(logger, 'MyActor', 'actor', 1000, ['RuntimeService'], (options) => new MyActor(options)),
        );
      return { app, rapp };
    };

    const app0 = await newApp('app0', '6150');
    // console.log('========0');
    const app1 = await newApp('app1', '6151');
    // console.log('========1');
    const app2 = await newApp('app2', '6152');
    // console.log('========2');

    try {
      await sleep(10000);

      // console.log('============== START =================');

      // console.log('DISPATCHING');

      /*const result = await app0.app.dispatchAction<void, { value: number }>(c1, {
        actorType: 'MyActor',
        actorId: ['0'],
        actionName: 'DoIt',
        receivers: ['app0']
      });
      // console.log('Result', result);
      expect(result.value).toBe(42);
      */

      try {
        await app0.app.dispatchAction<void, { value: number }>(c1, {
          actorType: 'RuntimeService',
          actorId: [],
          actionName: '',
          receivers: ['app1@cluster0'],
        });
        // console.log('Result', result);
        //expect(result1.value).toBe(42);
      } catch (e) {
        console.log('E', e, JSON.stringify(e));
      }

      let count = 0;
      for (const tracer of tracers) {
        const items = tracer.extract();
        count += items.length;
        /*console.log('====== TRACER =======');
        for (const item of items) {
          console.log('ITEM', JSON.stringify(item.options));
        }*/
      }
      expect(count).toBeGreaterThan(0);
    } finally {
      // console.log('STOPPING THE TEST!!!!!!!!!!!!!!!!!!!!');
      await sleep(1000);
      try {
        await app0.rapp.shutdownCluster();
        await sleep(2000);
        // console.log('============== STOPPING 0 ===============');
        await app0.app.stop();
        // console.log('============== STOPPED 0 ===============');
        // await sleep(5000);
        await app1.app.stop();
        // console.log('============== STOPPED 1 ===============');
        await app2.app.stop();
        // console.log('============== STOPPED 2 ===============');
      } catch (e) {
        console.log('Error during shutdown:', JSON.stringify(e, undefined, '  '));
      }
      await sleep(1000);
      // console.log('============ STOPPED ============');
    }
  }, 65000);

  test('Performance', async () => {
    const tracer = new TraceCollector(['ActorType', 'ActionName']);
    const logger = new Logger(undefined, undefined, undefined, undefined, tracer);
    const peers = [
      {
        id: 'app0@cluster0',
        address: 'localhost:6100',
      },
      {
        id: 'app1@cluster0',
        address: 'localhost:6101',
      },
      {
        id: 'app2@cluster0',
        address: 'localhost:6102',
      },
    ];
    const pool = new TransportPool(logger, peers);

    for (const t of ['base', 'ws']) {
      const newApp = async (id: string, port: string) => {
        // logger.addMask('*', DEEP_AND_UP);
        const transport =
          t === 'base'
            ? await pool.createTransport(id + '@cluster0')
            : await createWsTransport(logger, id + '@cluster0', 'localhost:' + port, peers);
        const app = new Application(
          'cluster0',
          id,
          'localhost:' + port,
          peers,
          logger.getChildLogger('App', id),
          transport,
        );
        const rapp = new RuntimeApplication(logger, app, ['app0@cluster0', 'app1@cluster0', 'app2@cluster0']);
        await rapp.init();
        app
          .getRunner()
          .registerActor(
            new ActorFactory(
              logger,
              'MyActor',
              'actor',
              10000,
              ['RuntimeService'],
              (options) => new MyActor(options),
              undefined,
              undefined,
              undefined,
              'shared',
            ),
          );
        return { app, rapp };
      };

      const app0 = await newApp('app0', '6100');
      const app1 = await newApp('app1', '6101');
      const app2 = await newApp('app2', '6102');

      let metrics: Array<ILogMetric & { avgTime?: number; atts?: { [key: string]: unknown } }> | undefined;

      try {
        await sleep(5000);

        // console.log('DISPATCHING');

        let n = 0;

        const A = 1;
        const B = 1000;
        for (const P of [1, 4, 16, 64, 256]) {
          const tasks = [];
          for (let i = 0; i < B; i++) {
            tasks.push(async () => {
              try {
                const traceinfo: ITraceInfo | undefined = i === 100 ? { correlationIds: [i.toString()] } : undefined;
                const c2 = logger.getChildContext('', [], undefined, traceinfo);
                const result = await app0.app.dispatchAction<{ value: number }, { value: number }>(c2, {
                  actorType: 'MyActor',
                  actorId: [P.toString(), i.toString()],
                  actionName: 'DoIt',
                  data: { value: i },
                });
                // console.log('RESULT', result);

                if (result.value === i) {
                  n++;
                } else {
                  console.log('RESULT WRONG', result, i);
                }
              } catch (e) {
                console.log('Error', JSON.stringify(e));
              }
            });
          }

          for (const turn of ['cold', 'warm']) {
            tracer.reset();
            n = 0;
            ///logger.error('============ START ============');
            const start = app1.app.time.machineTicks();
            await parallel(tasks, 80000, P);
            const stop = app1.app.time.machineTicks();
            ///logger.error('============ STOP ============');
            const duration = stop - start;
            emitTestMetric(
              `rta.${t}.${turn}.throughput`,
              n === A * B ? Math.round((n * 1000) / duration) : `Only ${n} out of ${A * B} items received`,
              'ops/s',
              { a: A, b: B, n: A * B, parallel: P, transport: t },
            );
            emitTestMetric(
              `rta.${t}.${turn}.period`,
              n === A * B ? duration / n : `Only ${n} out of ${A * B} items received`,
              'ms',
              { a: A, b: B, n: A * B, parallel: P, transport: t },
            );
            /// console.log('Duration:', duration);
            expect(n).toBe(A * B);

            metrics = tracer
              .extract()
              .sort((a, b) => a.name.localeCompare(b.name))
              .map((x) => ({
                scope: x.name,
                count: x.count,
                totalTime: x.totalTime,
                avgTime: x.count > 0 ? x.totalTime / x.count : undefined,
                atts: x.atts,
              }));

            for (const metric of metrics || []) {
              emitTestMetric(`rta.profile.${metric.scope}.count`, metric.count, 'times', {
                parallel: P,
                state: turn,
                transport: t,
                ...metric.atts,
              });
              emitTestMetric(`rta.profile.${metric.scope}.avg`, metric.avgTime ?? -1, 'ms', {
                parallel: P,
                state: turn,
                transport: t,
                ...metric.atts,
              });
            }
          }
        }
      } finally {
        // console.log('STOPPING THE TEST!!!!!!!!!!!!!!!!!!!!');
        await sleep(1000);
        try {
          await app0.rapp.shutdownCluster();
          await app0.app.stop();
          // console.log('============== STOPPED 0 ===============');
          await sleep(5000);
          await app1.app.stop();
          // console.log('============== STOPPED 1 ===============');
          await app2.app.stop();
          // console.log('============== STOPPED 2 ===============');
        } catch (e) {
          console.log('Error during shutdown:', e);
        }
        await sleep(1000);
      }
    }
    // expect(false).toBeTruthy();
  }, 165000);
});
