/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { ERROR_AND_UP, Logger, WARNING_AND_UP } from '../logging';
import { sleep } from '../util';
import { performance } from 'perf_hooks';
import { WsTransport } from '../wstransport2';
import { container, parallel } from '..';
import * as uuid from 'uuid';
import { EditableNetworkMessage } from '../networktypes';
import * as testing from '../testing';

describe('WSTransport', () => {
  type TransportT = WsTransport;
  const Transport = WsTransport;

  let t0: TransportT | undefined;
  let t1: TransportT | undefined;
  let t2: TransportT | undefined;
  let t3: TransportT | undefined;
  let logger: Logger | undefined;

  beforeEach(async () => {
    jest.setTimeout(20 * 1000);

    logger = new Logger();
    logger.addMask('*', WARNING_AND_UP);

    const hubconfig = [
      {
        id: 'trans0',
        address: 'localhost:6000',
      },
      {
        id: 'trans1',
        address: 'localhost:6001',
      },
    ];

    // The two hubs t0 and t1:
    t0 = new Transport(logger.getChildLogger('Transport', 'trans0'), 'trans0', 'localhost:6000', hubconfig);
    await t0.init();

    t1 = new Transport(logger.getChildLogger('Transport', 'trans1'), 'trans1', 'localhost:6001', hubconfig);
    await t1.init();

    // The two clients t2 and t3:
    t2 = new Transport(logger.getChildLogger('Transport', 'trans2'), 'trans2', 'localhost:6002', hubconfig);
    await t2.init();

    t3 = new Transport(logger.getChildLogger('Transport', 'trans3'), 'trans3', 'localhost:6003', hubconfig);
    await t3.init();

    await sleep(2000);
  });

  afterEach(async () => {
    jest.setTimeout(20 * 1000);
    await t0?.close();
    await t1?.close();
    await t2?.close();
    await t3?.close();
    t0 = undefined;
    t1 = undefined;
    t2 = undefined;
    t3 = undefined;
    await sleep(4000);
  });

  test('Binary', async () => {
    let msg = '';
    let bin: Uint8Array | undefined;
    t0?.setHandler(async (message) => {
      bin = message.getEnvelopeContainer().getChild(0).getDataBinary();
      msg = bin.toString();
    });

    const msg2 = EditableNetworkMessage.from('trans0', 'actreq', uuid.v4(), undefined, Buffer.from('Binary'));

    await t0?.send(logger!, msg2);

    await sleep(200);

    expect(Buffer.isBuffer(bin)).toBeTruthy();
    expect(msg).toBe('Binary');
  });

  test('From self to empty', async () => {
    let msg = '';
    let err: unknown;
    t0?.setHandler(async (message) => {
      msg = message.getEnvelopeContainer().getChild(0).getDataText();
    });
    const msg2 = EditableNetworkMessage.from('', 'actreq', uuid.v4(), undefined, 'From 0 to empty');
    try {
      await t0?.send(logger!, msg2);
    } catch (e) {
      err = e;
    }
    await sleep(100);

    expect(msg).toBe('');
    expect(err).toBeDefined();
  });

  test('From runtime to runtime', async () => {
    let msg = '';
    t1?.setHandler(async (message) => {
      msg = message.getEnvelopeContainer().getChild(0).getDataText();
    });

    const msg2 = EditableNetworkMessage.from('trans1', 'actreq', uuid.v4(), undefined, 'From 0 to 1');
    await t0?.send(logger!, msg2);
    await sleep(100);

    expect(msg).toBe('From 0 to 1');
  });

  test('From runtime to client', async () => {
    let msg = '';
    t2?.setHandler(async (message) => {
      // console.log('HANDLER RECEIVED MSG', message.envelope, message.envelope.contents);
      msg = message.getEnvelopeContainer().getChild(0).getDataText();
    });

    const msg2 = EditableNetworkMessage.from('trans2', 'actreq', uuid.v4(), undefined, 'From 0 to 2');
    await t0?.send(logger!, msg2);

    await sleep(200);

    expect(msg).toBe('From 0 to 2');
  });

  test('From client to runtime', async () => {
    let msg = '';
    t1?.setHandler(async (message) => {
      // console.log('MSG', message);
      msg = message.getEnvelopeContainer().getChild(0).getDataText();
    });

    const msg2 = EditableNetworkMessage.from('trans1', 'actreq', uuid.v4(), undefined, 'From 2 to 1');

    await t2?.send(logger!, msg2);

    await sleep(200);

    expect(msg).toBe('From 2 to 1');
  });

  // Transport is not resposible for this; network is.
  /*test.skip('From client to other client via runtime', async () => {
    let msg = '';
    t3?.setHandler(async (envelope) => {
      msg = envelope.contents?.[0]?.contents as string;
    });

    await t2?.send({
      receiver: 'trans3',
      contents: [
        {
          contents: 'From 2 to 3',
        },
      ],
      hopsRemaining: 4,
    });

    await sleep(200);
    expect(msg).toBe('From 2 to 3');
  });*/

  test('From client to unreachable hub should give error', async () => {
    await t0?.close();
    t0 = undefined;
    await t1?.close();
    t1 = undefined;
    await sleep(4000);

    const msg2 = EditableNetworkMessage.from('trans0', 'actreq', uuid.v4(), undefined, 'From 2 to nothing');

    await expect(t2?.send(logger!, msg2)).rejects.toThrow();

    await sleep(200);
  });

  test('From hub to unreachable client should give error', async () => {
    await t2?.close();
    t2 = undefined;
    await t3?.close();
    t3 = undefined;
    await sleep(4000);

    const msg2 = EditableNetworkMessage.from('trans2', 'actreq', uuid.v4(), undefined, 'From 0 to nothing');

    await expect(async () => {
      await t0?.send(logger!, msg2);
    }).rejects.toThrow();

    await sleep(200);
  });

  test('Performance', async () => {
    jest.setTimeout(20 * 1000);

    let msg = '';
    let n = 0;
    let stop = 0;
    t0?.setHandler(async (message) => {
      msg = message.getEnvelopeContainer().getChild(0).getDataText();
      if (msg === 'Test message') {
        // console.log('MSG0', msg, JSON.stringify(msg), typeof msg, n);
        n++;
        // console.log(n);
        if (n === nr) {
          stop = performance.now();
        }
      }
    });
    t1?.setHandler(async (message) => {
      msg = message.getEnvelopeContainer().getChild(0).getDataText();
      if (msg === 'Test message') {
        // console.log('MSG1', msg, JSON.stringify(msg), typeof msg, n);
        n++;
        // console.log(n);
        if (n === nr) {
          stop = performance.now();
        }
        //await sleep(2);
      }
    });

    // Reduce log level to make sure that logging does not influence performance
    logger?.replaceMasks([
      {
        mask: '*',
        levels: ERROR_AND_UP,
      },
    ]);

    const nr = 10000;
    // const missing = new Map<string, boolean>();
    const tasks = [];
    for (let i = 0; i < nr; i++) {
      tasks.push(async () => {
        // missing.set(i.toFixed(), true);
        const msg2 = EditableNetworkMessage.from(
          'trans1',
          'actreq',
          uuid.v4(),
          undefined,
          container('Test message', [Buffer.alloc(1000)]),
        );

        await t0?.send(logger!, msg2);
        //await sleep(10);
      });
    }

    for (const p of [1, 4, 16, 64, 256]) {
      n = 0;

      const start = performance.now();
      await parallel(tasks, 60000, p);
      await sleep(5000);
      const duration = stop - start;
      testing.emitTestMetric(
        'wstransport.throughput',
        n === nr ? Math.round((1000 * n) / duration) : `Only ${n} out of ${nr} items received`,
        'ops/sec',
        {
          n: nr,
          parallel: p,
        },
      );
      testing.emitTestMetric(
        'wstransport.period',
        n === nr ? duration / n : `Only ${n} out of ${nr} items received`,
        'ms',
        {
          n: nr,
          parallel: p,
        },
      );

      expect(n).toBe(nr);
      // TODO: Make this check computer invariant?
      expect(duration).toBeLessThan(2500);
      await sleep(100);
    }
  }, 120000);
});
