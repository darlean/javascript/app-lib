import { actorFactory, ILogger } from '.';
import { ActorFactory } from './actorfactory';
import { Application } from './application';
import { DispatchActor } from './helpers/dispatching/dispatchactor';
import { LockActor } from './helpers/locks/lockactor';
import { RuntimeService } from './helpers/runtime/runtimeservice';
import { ShutdownActor } from './helpers/runtime/shutdownservice';

export class RuntimeApplication {
  private application: Application;
  private runtimeNodes: string[];
  private logger: ILogger;

  constructor(logger: ILogger, application: Application, runtimeNodes: string[]) {
    this.logger = logger;
    this.application = application;
    this.runtimeNodes = runtimeNodes;
  }

  public async init(): Promise<void> {
    this.application.getRunner().registerActor(
      new ActorFactory(
        this.logger.getChildLogger('ActorFactory', 'LockActor'),
        'LockActor',
        'actor',
        100,
        ['ShutdownService'],
        (options) => {
          return new LockActor(options);
        },
        undefined,
        undefined,
        -1,
        'shared',
      ),
    );

    this.application.getRunner().registerActor(
      new ActorFactory(
        this.logger.getChildLogger('ActorFactory', 'ShutdownActor'),
        'ShutdownActor',
        'actor',
        100,
        [],
        (options) => {
          return new ShutdownActor({
            ...options,
            fullAppId: this.application.fullId,
          });
        },
      ),
    );

    this.application.getRunner().registerActor(
      actorFactory(this.logger.getChildLogger('ActorFactory', 'RuntimeService'), {
        clazz: RuntimeService,
        actorOptions: {
          fullAppId: this.application.fullId,
          lockNodes: this.runtimeNodes,
          dispatchNodes: this.runtimeNodes,
        },
        capacity: 100,
      }),
    );

    this.application.getRunner().registerActor(
      actorFactory(this.logger.getChildLogger('ActorFactory', 'DispatchActor'), {
        clazz: DispatchActor,
        actorOptions: {
          runtime: this.application,
          fullAppId: this.application.fullId,
          actors: this.application.getRunner().getSupportedActors(),
        },
        capacity: 100,
      }),
    );
    await this.application.init();
  }

  public async shutdownCluster(): Promise<void> {
    await this.application.dispatchAction(this.logger, {
      actorType: 'RuntimeService',
      actorId: [],
      actionName: 'Shutdown',
    });
  }
}
