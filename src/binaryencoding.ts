/**
 * This module provides classes and methods for encoding and decoding containers from and to an
 * efficient human-readable binary format. See [[containers]] for more info.
 *
 * The *encoding* (serialization) of [[IContainer]] instances is performed by the [[encodeToBinaryBuffers]]
 * function. It converts a container (and its child containers) into an array of buffers. When it detects
 * that (child) containers already are encoded (serialized), their serialized contents is simply returned.
 * Otherwise, their contents is encoded and added to the buffer array.
 *
 * The *decooding* (deserialization) is performed by means of the [[decodeFromBinaryBuffer]] function that creates a new
 * [[BufferContainer]] instance around a `Buffer`. The returned [[BufferContainer]] implements [[IContainer]], so it
 * can be used directly everywhere [[IContainer]] is used. The BufferContainer performs
 * *lazy deserialization*. It only deserializes the parts of the buffer that are
 * needed by application code.
 *
 * The binary data format of a container is:
 *
 * | Field | Length | Description |
 * | --- | --- | --- |
 * | VERSION_MAJOR | 1 byte | Currently '0'. Implementations must reject a VERSION_MAJOR that is higher that their own supported VERSION_MAJOR. Update this field for backwards-*incompatible* changes to the data format. |
 * | VERSION_MINOR | 1 byte | Currently '0'. Update this field for backwards-*compatible* changes to the data format. |
 * | DATA_TYPE | 1 byte | 'e', 'b', 't' or 's' for empty, binary, text and structured data, respectively. |
 * | RESERVED | 1 byte | Must be ' ' (space, ascii 32). Reservered for future use. |
 * | TOTAL_SIZE | 10 bytes | The total size of this entire container blob, including all header fields, the data and all subcontainers, represented as ascii number (example: '0000000123') |
 * | DATA_SIZE | 10 bytes | The size of the data for this container, respresented as ascii number (example: '0000000042'). |
 * | DATA | \<DATA_SIZE\> | The actual container data. Text data must be utf-8 encoded, struct data must be utf-8 encoded JSON.=, binary data is just copied in literally, and for 'empty' data, this field is not present (0 bytes long). |
 *
 * @module
 */
import { ContainerData, ContainerLike0, IContainerizeable } from '.';
import { ContainerKind, IContainer, BufferEncoding, AnObject, asContainer } from './containers';

const KIND_BUFS: { [encoding: string]: Buffer } = {
  binary: Buffer.from('b'),
  text: Buffer.from('t'),
  structure: Buffer.from('s'),
  empty: Buffer.from('e'),
};

const VERSION_MAJOR = '0';
const VERSION_MINOR = '0';

const FILLER_BUF = Buffer.from(' ');

const EMPTY_BUF = Buffer.alloc(0);
const VERSION_BUF = Buffer.from(VERSION_MAJOR + VERSION_MINOR);
const ZERO_BUF = Buffer.from('0000000000');

const BUFFER_ENCODING_TLV = 'tlv';
const HEADER_SIZE = 24;
const SIZE_FIELD_LENGTH = 10;

const PREAMBLE_SIZE = 4;
const TOTAL_SIZE_OFFSET = PREAMBLE_SIZE;
const OWN_SIZE_OFFSET = TOTAL_SIZE_OFFSET + SIZE_FIELD_LENGTH;

/**
 * Decodes buffer, that should previously be encoded using [[encodeToBinaryBuffer]], into a container.
 * @param buffer The buffer that needs to be decoded.
 * @param byteOffset The offset in bytes from the beginning of buffer where the decoding should start
 * @returns A container.
 */
export function decodeFromBinaryBuffer<Type extends ContainerData, Child0Type extends ContainerData>(
  buffer: Buffer,
  byteOffset: number,
): IContainer<Type, Child0Type> {
  return new BufferContainer<Type, Child0Type>(buffer, byteOffset);
}

/**
 * Encodes a container into a series of binary buffers and adds them to buffers.
 *
 * The reason for 'returning' multiple buffers (instead of just one buffer) is performance. When encoding
 * multiple (sub)containers, it is more efficient to first collect all the individual small buffers in one
 * large array (and then concatenating all of these small buffers at once) than concatenating all small
 * buffers time and time again for every (sub)container.
 * @param value The value to be encoded.
 * @param buffers The buffers array to which the resulting buffers are added.
 */
export function encodeToBinaryBuffers(value: ContainerLike0, buffers: Buffer[]): void {
  const encoder = new ContainerToBuffers();
  encoder.toBuffers(asContainer(value), buffers);
}

/**
 * Encodes a container into one combined binary buffer. Just a wrapper around [[encodeToBinaryBuffers]].
 * @param value The value to be encoded.
 * @returns A Buffer with the binary data.
 */
export function encodeToBinaryBuffer(value: ContainerLike0): Buffer {
  const encoder = new ContainerToBuffers();
  const buffers: Buffer[] = [];
  encoder.toBuffers(asContainer(value), buffers);
  return Buffer.concat(buffers);
}

/**
 * For internal use only. Use the [[encodeToBinaryBuffers]] function instead.
 *
 * Converts a container into a series of buffers.
 * @internal
 */
export class ContainerToBuffers {
  public toBuffers(container: IContainer<ContainerData, ContainerData>, buffers: Buffer[]): void {
    this.toBuffersImpl(container, buffers);
  }

  protected toBuffersImpl(container: IContainer<ContainerData, ContainerData>, nodes: Buffer[]): void {
    if (container.getBufferEncoding() === BUFFER_ENCODING_TLV) {
      const buffer = container.getBuffer();
      if (buffer) {
        nodes.push(buffer);
        return;
      }
    }

    const nodesOffset = nodes.length;

    let data: Buffer;
    const kind = container.getKind();
    if (kind === 'structure') {
      data = Buffer.from(JSON.stringify(container.getDataStruct()));
    } else if (kind === 'binary') {
      data = container.getDataBinary() as Buffer;
    } else if (kind === 'text') {
      data = Buffer.from(container.getDataText() as string);
    } else {
      data = EMPTY_BUF;
    }

    nodes.push(VERSION_BUF);
    nodes.push(KIND_BUFS[container.getKind()]);
    nodes.push(FILLER_BUF);
    const totalIdx = nodes.length;
    nodes.push(ZERO_BUF); // Is replaced with actual value later on
    nodes.push(Buffer.from(data.length.toString().padStart(SIZE_FIELD_LENGTH), 'latin1'));
    nodes.push(data);

    for (let idx = 0; idx < container.getChildCount(); idx++) {
      const sub = container.getChild<ContainerData>(idx);
      this.toBuffersImpl(sub, nodes);
    }

    let total = 0;
    for (let idx = nodesOffset; idx < nodes.length; idx++) {
      total += nodes[idx].length;
    }

    nodes[totalIdx] = Buffer.from(total.toString().padStart(SIZE_FIELD_LENGTH), 'latin1');
  }
}

function parse(data: string): unknown {
  return JSON.parse(data);
}

/**
 * For internal use only. Use the [[decodeFromBinaryBuffer]] function instead.
 *
 * Creates a container instance out of a binary buffer.
 * @internal
 */
export class BufferContainer<Type extends ContainerData, Child0Type extends ContainerData>
  implements IContainer<Type, Child0Type>
{
  protected buffer: Buffer;
  protected byteOffset: number;
  protected totalSize: number;
  protected dataSize: number;
  protected dataKind: ContainerKind;
  protected hasData = false;
  protected data?: ContainerData;
  protected subs?: IContainer<ContainerData, ContainerData>[];

  public __container: () => boolean = () => true;

  public constructor(buffer: Buffer, byteOffset: number) {
    this.buffer = buffer;
    this.byteOffset = byteOffset;
    const major = buffer.toString(undefined, 0, 1);
    if (major !== VERSION_MAJOR) {
      throw new Error(`Unsupported major container version ${major} (we support up to ${VERSION_MAJOR})`);
    }
    const kind = buffer[byteOffset + 2];
    this.dataKind = kind === 115 ? 'structure' : kind === 98 ? 'binary' : kind === 116 ? 'text' : 'empty';
    this.totalSize = parseInt(
      buffer.toString('latin1', byteOffset + TOTAL_SIZE_OFFSET, byteOffset + TOTAL_SIZE_OFFSET + SIZE_FIELD_LENGTH),
    );
    this.dataSize = parseInt(
      buffer.toString('latin1', byteOffset + OWN_SIZE_OFFSET, byteOffset + OWN_SIZE_OFFSET + SIZE_FIELD_LENGTH),
    );
  }

  public getDataStruct(): Type {
    return this.getData() as Type;
  }

  public getDataBinary(): Buffer {
    if (this.dataKind === 'binary') {
      return this.getData() as unknown as Buffer;
    }
    throw new Error('Data is not binary');
  }

  public getDataText(): string {
    if (this.dataKind === 'text') {
      return this.getData() as unknown as string;
    }
    throw new Error('Data is not text');
  }

  public getKind(): ContainerKind {
    return this.dataKind;
  }

  public getChild0(): IContainer<Child0Type, ContainerData> {
    return this.ensureSubs()[0] as IContainer<Child0Type, ContainerData>;
  }

  public tryGetChild0(): IContainer<Child0Type, ContainerData> | undefined {
    return this.ensureSubs()[0] as IContainer<Child0Type, ContainerData>;
  }

  public getChild<Type>(idx: number): IContainer<Type, ContainerData> {
    return this.ensureSubs()[idx] as IContainer<Type, ContainerData>;
  }

  public getChildCount(): number {
    return this.ensureSubs().length;
  }

  public getBufferEncoding(): BufferEncoding {
    return BUFFER_ENCODING_TLV;
  }

  public getBuffer(): Buffer | undefined {
    return this.buffer.slice(this.byteOffset, this.byteOffset + this.totalSize);
  }

  public getData(): Type | undefined {
    if (this.hasData) {
      return this.data as Type;
    }
    if (this.dataKind === 'binary') {
      this.data = this.buffer.slice(this.byteOffset + HEADER_SIZE, this.byteOffset + HEADER_SIZE + this.dataSize);
      this.hasData = true;
      return this.data as unknown as Type;
    } else if (this.dataKind === 'text') {
      this.data = this.buffer.toString(
        undefined,
        this.byteOffset + HEADER_SIZE,
        this.byteOffset + HEADER_SIZE + this.dataSize,
      );
      this.hasData = true;
      return this.data as unknown as Type;
    } else if (this.dataKind === 'structure') {
      this.data = parse(
        this.buffer.toString(undefined, this.byteOffset + HEADER_SIZE, this.byteOffset + HEADER_SIZE + this.dataSize),
      ) as AnObject;
      (this.data as IContainerizeable<Type, Child0Type>)._cont = () => this;
      this.hasData = true;
      return this.data as Type;
    } else if (this.dataKind === 'empty') {
      this.data = undefined;
      this.hasData = true;
      return this.data;
    }
    throw new Error('Invalid data');
  }

  protected ensureSubs(): IContainer<ContainerData, ContainerData>[] {
    if (this.subs !== undefined) {
      return this.subs;
    }
    this.subs = [];

    let offset = this.byteOffset + 24 + this.dataSize;
    while (offset < this.byteOffset + this.totalSize) {
      const c = new BufferContainer<ContainerData, ContainerData>(this.buffer, offset);
      this.subs.push(c);
      offset += c.totalSize;
    }
    return this.subs;
  }
}
