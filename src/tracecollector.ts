import { ISegment, ISegmentOptions, ITracer } from './services/tracing';
import * as uuid from 'uuid';
import { ITraceInfo } from './interfaces/tracing';
import * as perf from 'perf_hooks';

const base = perf.performance.timeOrigin;

function now() {
  return base + perf.performance.now();
}

export class Segment implements ISegment {
  public options: ISegmentOptions;
  protected tracer: TraceCollector;

  constructor(tracer: TraceCollector, parent: Segment | undefined, options: ISegmentOptions) {
    this.tracer = tracer;
    this.options = {
      ...options,
      correlationIds: options.correlationIds ?? parent?.options.correlationIds ?? [uuid.v4()],
      id: options.id ?? uuid.v4(),
      parentSegmentId: parent?.options.id,
      startMoment: options.startMoment ?? now(),
    };
  }

  public sub(options: ISegmentOptions): ISegment {
    //console.log('OPTIONS', options);
    const seg = new Segment(this.tracer, this, options);
    return seg;
  }

  public finish(): void {
    this.options.endMoment = now();
    this.tracer._finish(this);
  }

  public getCorrelationIds(): string[] {
    if (!this.options.correlationIds) {
      throw new Error('No correlationids');
    }
    return this.options.correlationIds;
  }
}

interface IAggregation {
  name: string;
  count: number;
  totalTime: number;
  atts?: { [key: string]: unknown };
}

export class TraceCollector implements ITracer {
  public _aggregates: Map<string, IAggregation>;
  protected attributes: string[];

  constructor(attributes: string[]) {
    this._aggregates = new Map();
    this.attributes = attributes;
  }

  public reset() {
    this._aggregates = new Map();
  }

  public trace(options: ITraceInfo): ISegment {
    return new Segment(this, undefined, {
      correlationIds: options.correlationIds,
      id: options.parentSegmentId,
    });
  }

  public _finish(segment: Segment): void {
    if (!(segment.options.startMoment && segment.options.endMoment)) {
      return;
    }

    const duration = (segment.options.endMoment ?? 0) - (segment.options.startMoment ?? 0);

    const name = this.makeName(segment);
    //console.log('NAME', name, segment.options);
    if (name) {
      const atts = this.makeAtts(segment);
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const fullName = atts ? [name, ...Object.values(atts || {}).map((v) => (v as any).toString())].join(':') : name;
      const a = this._aggregates.get(fullName);
      if (!a) {
        this._aggregates.set(fullName, {
          count: 1,
          name,
          totalTime: duration,
          atts,
        });
      } else {
        a.count++;
        a.totalTime += duration;
      }
    }
  }

  public extract(): IAggregation[] {
    const results = Array.from(this._aggregates.values());
    //console.log('EXTRACTED', results);
    return results;
  }

  protected makeName(segment: ISegment): string {
    const atts = segment.options.attributes as { scope: string } | undefined;
    if (atts) {
      return atts.scope;
    }
    return '';
  }

  protected makeAtts(segment: ISegment): { [key: string]: unknown } | undefined {
    const atts = segment.options.attributes;
    if (atts) {
      const result: { [key: string]: unknown } = {};
      for (const attName of this.attributes) {
        const v = atts[attName];
        //console.log('ATT', attName, v, atts2);
        if (v !== undefined) {
          result[attName] = v;
        }
      }
      return result;
    }
  }
}
