import * as ev from 'events';
import * as uuid from 'uuid';
import { IContext } from '.';
import { asContainer, container, ContainerData, ContainerLike, ContainerLike0, IContainer } from './containers';
import { ITraceInfo } from './interfaces/tracing';

export declare interface NetworkSendOptions {
  // Emitted when the message was sent successfully, or when an error occurred before or during sending.
  on(event: 'sent', listener: (error?: unknown) => void): this;

  // Emitted when the message was delivered successfully to the final recipient, or when an error occurred
  // after sending and before delivery.
  on(event: 'delivered', listener: (error?: unknown) => void): this;
}

export type NetworkWaitFor = 'nothing' | 'sent' | 'undeliverable' | 'delivered';

export class NetworkSendOptions extends ev.EventEmitter {
  waitFor: NetworkWaitFor;
  // maxHopCount?: number;
  direct: boolean;
  ttl?: number;

  constructor(waitFor: 'nothing' | 'sent' | 'delivered' | 'undeliverable', direct?: boolean, ttl?: number) {
    super();
    this.waitFor = waitFor;
    this.direct = direct ?? false;
    this.ttl = ttl;
  }
}

export type TransportMessageHandler = (message: INetworkMessage) => Promise<void>;

export interface INetwork {
  send(c: IContext, message: EditableNetworkMessage, options: NetworkSendOptions): Promise<void>;
  getReturnCallback(data: string): IEnvelopeCallback;
}

export interface ITransport {
  estimateDistance(receiver: string, direct: boolean): number;
  // Sends a wire message to a receiver within the transport.
  send(c: IContext, message: EditableNetworkMessage): Promise<void>;
  setHandler(handler: TransportMessageHandler): void;
  close(): Promise<void>;
  init(): Promise<void>;
}

export interface IEnvelopeCallback {
  to: string;
  uid: string;
  data: string;
}

export interface INetworkHeader {
  receiver: string;
  hops: number;
  tracing?: ITraceInfo;
}
export type INetworkHeaderC<Child0> = IContainer<INetworkHeader, Child0>;
export type INetworkHeaderCL<Child0> = ContainerLike<INetworkHeader, Child0>;

export type EnvelopeType = 'actreq' | 'actresp' | 'dlv' | 'fail';

export interface INetworkEnvelope {
  uid: string;
  // Receivers when a message is delivered (successfully) or failed to deliver
  onDF?: IEnvelopeCallback[];
  // Receivers when a message is delivered successfully
  onD?: IEnvelopeCallback[];
  // Receivers when a message failed to deliver
  onF?: IEnvelopeCallback[];
  // Type of envelope: action request, action response, delivered receipt, failed receipt
  type: EnvelopeType;
  // Optional, copied from envelope we are replying to
  data?: string;
  // Optional description of an error that occurred in the network
  error?: string;
}
export type INetworkEnvelopeC<Child0> = IContainer<INetworkEnvelope, Child0>;
export type INetworkEnvelopeCL<Child0> = ContainerLike<INetworkEnvelope, Child0>;

export class ContainerNetworkMessage implements INetworkMessage {
  public header: INetworkHeader;
  public container: IContainer<INetworkHeader, INetworkEnvelope>;
  public envelope?: IContainer<INetworkEnvelope, ContainerData>;

  constructor(container: IContainer<INetworkHeader, INetworkEnvelope>) {
    this.container = container;
    this.header = container.getDataStruct();
  }

  public getEnvelopeContainer(): IContainer<INetworkEnvelope, ContainerData> {
    if (!this.envelope) {
      this.envelope = this.container.getChild<INetworkEnvelope>(0);
    }
    return this.envelope;
  }

  public getReceiver(): string {
    return this.header.receiver;
  }

  public getHopsRemaining(): number {
    return this.header.hops;
  }

  public getTracing(): ITraceInfo | undefined {
    return this.header.tracing;
  }
}

const DEFAULT_NETWORK_HOP_COUNT = 2;

export interface INetworkMessage {
  getReceiver(): string;
  getHopsRemaining(): number;
  getEnvelopeContainer(): IContainer<INetworkEnvelope, ContainerData>;
  getTracing(): ITraceInfo | undefined;
}

export class EditableNetworkMessage implements INetworkMessage {
  protected receiver: string;
  protected hopsRemaining: number;
  protected envelope: INetworkEnvelopeCL<unknown>;
  protected traceContext?: IContext;

  constructor(
    receiver: string,
    envelope: INetworkEnvelopeCL<unknown>,
    hopsRemaining?: number,
    traceContext?: IContext,
  ) {
    this.receiver = receiver;
    this.hopsRemaining = hopsRemaining || DEFAULT_NETWORK_HOP_COUNT;
    this.envelope = envelope;
    this.traceContext = traceContext;
  }

  // Covenience function for creating an Editable Network Message.
  public static from(
    receiver: string,
    type: EnvelopeType,
    uid?: string,
    data?: string,
    contents?: ContainerLike0,
    hopsRemaining?: number,
    traceContext?: IContext,
  ): EditableNetworkMessage {
    const env: INetworkEnvelope = {
      type,
      uid: uid ?? uuid.v4(),
      data,
    };
    const envC = contents !== undefined ? container(env, [contents]) : env;
    return new EditableNetworkMessage(receiver, envC, hopsRemaining, traceContext);
  }

  public getReceiver(): string {
    return this.receiver;
  }

  public getHopsRemaining(): number {
    return this.hopsRemaining;
  }

  public getTracing(): ITraceInfo | undefined {
    const seg = this.traceContext?.getSegment();
    if (seg) {
      return { parentSegmentId: seg.options.id, correlationIds: seg.getCorrelationIds() };
    }
  }

  public decreaseHopsRemaining(): void {
    if (this.hopsRemaining <= 0) {
      throw new Error('Hops remaining must remaing positive or 0');
    }
    this.hopsRemaining--;
  }

  public getEnvelopeContainer(): IContainer<INetworkEnvelope, ContainerData> {
    return asContainer(this.envelope);
  }

  public toContainer(): IContainer<ContainerData, ContainerData> {
    const value: INetworkHeader = {
      hops: this.hopsRemaining,
      receiver: this.receiver,
      tracing: this.getTracing(),
    };
    return container(value, [this.envelope]);
  }
}
