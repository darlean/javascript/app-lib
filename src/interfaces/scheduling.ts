// Schedule a callback at delay milliseconds after moment, and repeat for
// at most repeatCount times with the specified interval. When moment is
// not specified, the current moment ('now') is used instead. When delay
// is not specified, the value of interval is used as delay. When interval

import { IPerformActionReq } from './performing';

// is not set, the callback is only invoked once.
export interface IScheduleReminderRequest {
  id: string[];
  delay?: number;
  interval?: number;
  repeatCount?: number;
  moment?: number;
  callback: IPerformActionReq;
}

export interface IScheduleReminderResponse {
  handle: string;
}
