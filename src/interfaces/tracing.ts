export interface ISegment {
  correlationIds: string[];
  id: string;
  parentId?: string;
  name: string;
  startMoment: number;
  endMoment?: number;
  attributes?: { [key: string]: unknown };
}

export interface IPushSegmentsRequest {
  segments: ISegment[];
}

export interface ITraceInfo {
  correlationIds: string[];
  parentSegmentId?: string;
}

export interface IGetTracesResponse {
  correlationIds: string[];
}

export interface IGetTraceRequest {
  correlationId: string;
}

export interface IGetTraceResponse {
  segments: ISegment[];
}
