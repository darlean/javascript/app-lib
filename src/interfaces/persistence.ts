import { IAttachmentRef } from '..';

export interface IStoreRequest {
  compartment?: string;
  lookupKeys: string[];
  sortKeys?: string[];
  value: IAttachmentRef;
}

export interface IDeleteRequest {
  compartment?: string;
  lookupKeys: string[];
  sortKeys?: string[];
}

export interface ILoadRequest {
  compartment?: string;
  lookupKeys: string[];
  sortKeys?: string[];
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ILoadResponse {
  value?: IAttachmentRef;
}

export interface IQueryRangePart {
  sortKeys: string[];
  compare: 'inclusive' | 'exclusive';
}

export interface IQueryRequest {
  compartment?: string;
  lookupKeys: string[];
  start?: IQueryRangePart;
  end?: IQueryRangePart;
  limit: number;
  order?: 'ascending' | 'descending';
  includeValues?: boolean;
}

export interface IQueryResponseItem {
  lookupKeys: string[];
  sortKeys: string[];
  value: IAttachmentRef;
}

export interface IQueryResponse {
  items: IQueryResponseItem[];
}

export interface IInspectRequest {
  compartment?: string;
  shardLimit: number;
  includeValues?: boolean;
}

export interface IInspectResponseItem {
  lookupKeys: string[];
  sortKeys: string[];
  shard: number;
  value?: IAttachmentRef;
}

export interface IInspectResponse {
  items: IInspectResponseItem[];
}
