/*
The files in this folder define "over-the-wire" interfaces for use between applications. To make life for a typical
app developer a bit easier, a bit more user friendly types are included in the "../services" folder.

Usually, the 2 structure are very similar. The major differences are on how included data is represented.
Services typically use a data field for this, whereas interfaces typically use child-containers.

The input/output interface objects are typically postfixed "Request" and "Response" (to differentiate them from their
service counterparts that end with "Options" and "Result")
*/
