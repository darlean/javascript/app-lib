import { IAttachmentRef, IRpcError } from '..';
import { AnObject, IContainer, ContainerLike } from '../containers';
import { IEnvelopeCallback } from '../networktypes';

export interface IActionHop {
  id: string;
}

export interface IPerformActionReq {
  actorType?: string;
  actorId?: string[];
  actionName?: string;
  data: IAttachmentRef;
  actionHopsRemaining: number;
  returnEnvelopes?: IEnvelopeCallback[];
  // Receivers for process errors.
  processErrorEnvelopes?: IEnvelopeCallback[];
  hops?: IActionHop[];
}

export type IPerformActionReqC<Data extends AnObject> = IContainer<IPerformActionReq, Data>;
export type IPerformActionReqCL<Data extends AnObject> = ContainerLike<IPerformActionReq, Data>;

export interface IPerformActionResp {
  // Array of appnames that might host the requested actor
  appHints?: string[];
  processError?: IRpcError;
  actorError?: IRpcError;
  data?: IAttachmentRef;
}

export type IPerformActionRespC<Data> = IContainer<IPerformActionResp, Data>;
export type IPerformActionRespCL<Data> = ContainerLike<IPerformActionResp, Data>;
