export interface IAggregatedSIngleLockHolder {
  holder?: string;
  ttl: number;
  source: string;
}

export interface IAggregatedSingleLock {
  id: string[];
  holders: IAggregatedSIngleLockHolder[];
}

export interface IAcquireLockRequest {
  id: string[];
  requester: string;
  ttl: number;
  retryTime?: number;
  singleStage?: boolean;
}

export interface IAcquireLockResponse {
  duration: number;
  holders: string[];
}

export interface IReleaseLockRequest {
  id: string[];
  requester: string;
}

export interface IGetLockInfoRequest {
  id: string[];
}

export interface IGetLockInfoResponse {
  holders: string[];
}
