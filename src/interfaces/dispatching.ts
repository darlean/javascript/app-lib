export interface IRetrieveDispatchInfoRequest {
  actorType: string;
  actorId: string[];
}

//export type IDispatchRequestC = IContainer<IDispatchRequest, IActionRequest>;
//export type IDispatchRequestCL = IDispatchRequestC | IDispatchRequest;

export interface IRetrieveDispatchInfoResponse {
  apps: string[];
  multiplicity: 'single' | 'multiple';
}
