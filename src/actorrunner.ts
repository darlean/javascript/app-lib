import * as selock from './selock';
import * as uuid from 'uuid';
import { IContext } from './logging';
import {
  IRegisterActorOptions,
  IInstanceInfo,
  IRuntime,
  ITime,
  ITimer,
  IBaseActionContext,
  ProcessError,
  ISupportedActor,
  IScheduleTimerOptions,
} from './types';
import { actionToString, actorToString, encodeKey } from './util';
import { DEFAULT_MAX_HOP_COUNT, ensureDarleanError, IActorRegistration, IShutdownInfo, sleep, Struct, struct } from '.';
import { AnObject } from './containers';
import { IActionOptions, IActionResult } from './services/performing';
import { IPerformActionReq, IPerformActionReqCL, IPerformActionRespC } from './interfaces/performing';
import { IAcquireLockResult } from './services/locking';

export class ActorRunner {
  protected id: string;
  protected actors: Map<string, IRegisterActorOptions>;
  protected instances: Map<string, IInstanceInfo>;
  protected instancesByKind: Map<string, Map<string, boolean>>;
  protected runtime: IRuntime;
  protected time: ITime;
  protected pushTimer?: ITimer;
  protected services: Map<string, (action: IActionOptions<AnObject>, context: IBaseActionContext) => unknown>;
  protected c: IContext;
  protected defaultTtl = 60000;
  protected activationLock: Map<string, boolean>;
  protected stopping = false;
  protected cleaningup: Map<string, boolean>;

  constructor(c: IContext, id: string, runtime: IRuntime, time: ITime) {
    this.c = c;
    this.id = id;
    this.actors = new Map();
    this.instances = new Map();
    this.instancesByKind = new Map();
    this.services = new Map();
    this.cleaningup = new Map();
    this.runtime = runtime;
    this.time = time;
    this.activationLock = new Map();
  }

  public registerService(
    name: string,
    impl: (action: IActionOptions<AnObject>, context: IBaseActionContext) => unknown,
  ): void {
    this.services.set(name, impl);
  }

  public async start(): Promise<void> {
    await this.startPushingStatus();
    await this.startActors();
  }

  public async stop(): Promise<void> {
    if (this.stopping) {
      return;
    }

    this.c.info('Stopping actor runner');
    this.stopping = true;

    this.cancelPushingStatus();

    await this.release();

    await this.stopActors();

    // Send our status for the very last time
    try {
      this.c.info('Sending [Status]', () => ({ Status: this.getSupportedActors() }));
      await this.pushStatus();
    } catch (e) {
      this.c.error('Unable to push last status before stopping: [Error]', () => ({ Error: e }));
    }

    this.c.info('Stopped actor runner');
  }

  public registerActor(options: IRegisterActorOptions | IActorRegistration): void {
    const opts = (options as IActorRegistration).getRegisterOptions
      ? (options as IActorRegistration).getRegisterOptions()
      : (options as IRegisterActorOptions);
    this.actors.set(opts.name, opts);
    this.instancesByKind.set(opts.name, new Map());
  }

  public async handleAction<Input, Output>(
    c: IContext,
    actionC: IPerformActionReqCL<Input>,
  ): Promise<IPerformActionRespC<Output>> {
    const action = struct(actionC);
    const c1 = c.getChildContext('io.darlean.actorrunner.HandleAction', undefined, () => ({
      ActorType: action.actorType,
      ActionName: action.actionName,
    }));
    try {
      if (!action.actorType) {
        throw ProcessError('INVALID_REQUEST', 'Field actorType is not set in request for [Action]', {
          Action: actionToString(action),
        });
      }
      const actorType = action.actorType;

      if (action.actorId === undefined) {
        throw ProcessError('INVALID_REQUEST', 'Field actorId is not set in request for [Action]', {
          Action: actionToString(action),
        });
      }

      const actor = this.actors.get(action.actorType);
      if (!actor) {
        throw ProcessError('ACTOR_TYPE_NOT_DEFINED', 'Actor type [ActorType] is not defined in this app', {
          ActorType: action.actorType,
        });
      }

      const actorId = action.actorId ?? '';
      const instanceKey = action.actorType + '#' + encodeKey(actorId);
      const sessionId = uuid.v4();

      const instanceInfo = await this.ensureInstanceActive(
        c,
        instanceKey,
        actor,
        action,
        actorType,
        actorId,
        sessionId,
      );

      // Perform the actual action
      c1.debug('Runner started performing of [Action]', () => ({ Action: actionToString(action) }));
      const c3 = c1.getChildContext('io.darlean.actorrunner.PerformAction');
      const context = this.obtainActionContext(c3, action, sessionId);
      try {
        // It is up to the actor wrapper to acquire a shared/exclusive lock
        return (await actor.perform(c3, instanceInfo, actionC, context)) as IPerformActionRespC<Output>;
      } finally {
        c3.debug('Runner done performing of [Action]', () => ({ Action: actionToString(action) }));
        c3.finish();
      }
    } finally {
      c1.finish();
    }
  }

  public async release(): Promise<void> {
    let graceful = true;
    while (this.instances.size > 0) {
      let shutdownInfo: IShutdownInfo | undefined;
      if (graceful) {
        try {
          shutdownInfo = await this.runtime.dispatchAction<void, IShutdownInfo>(this.c, {
            actorType: 'RuntimeService',
            actionName: 'GetShutdownInfo',
            actorId: [],
          });
        } catch (e) {
          this.c.info('Unable to get shutdown info during shutdown, switching to ungraceful shutdown: [Error]', () => ({
            Error: e,
          }));
          graceful = false;
        }
      }

      if (graceful && shutdownInfo?.status !== 'shuttingdown') {
        this.c.info('Shutdown information not present, switching to ungraceful shutdown');
        graceful = false;
      }

      if (!graceful || (shutdownInfo && shutdownInfo.status === 'shuttingdown')) {
        for (const actorInfo of this.instances.values()) {
          const actorType = actorInfo.actorInfo.name;
          const info = actorInfo.actorInfo;
          if (!graceful || !shutdownInfo?.actorsBeingUsed.includes(actorType)) {
            info.status = 'disabled';

            this.c.verbose('Releasing actor [Actor] because of shutting down', () => ({
              Actor: { actorType, actorId: actorInfo.id },
            }));

            await this.releaseActor(actorInfo);

            this.c.verbose('Released actor [Actor] because of shutting down', () => ({
              Actor: { actorType, actorId: actorInfo.id },
            }));
          } else {
            this.c.verbose('Not yet releasing actor [Actor] during shutting down: still in use', () => ({
              Actor: { actorType, actorId: actorInfo.id },
            }));
          }
        }
      }

      try {
        if (graceful) {
          await this.pushStatus();
        }
      } catch (e) {
        this.c.debug('Unable to push state during shutdown: [Error]', () => ({ Error: e }));
        graceful = false;
      }
      await sleep(500);
    }
  }

  public async releaseActor(info: IInstanceInfo): Promise<void> {
    const instanceKey = info.actorInfo.name + '#' + encodeKey(info.id);
    if (info.state === 'active') {
      await info.localLock.takeOver(uuid.v4());
      try {
        info.state = 'deactivating';

        if (info.actorInfo.deactivator) {
          try {
            const action = struct<IPerformActionReq>((attach) => ({
              actionName: '',
              actorId: info.id,
              actorType: info.actorInfo.name,
              actionHopsRemaining: DEFAULT_MAX_HOP_COUNT,
              data: attach({}),
            }));
            const sessionId = uuid.v4();
            const context = this.obtainActionContext(this.c, action, sessionId);
            await info.actorInfo.deactivator(info, context);
          } catch (e) {
            this.c.warning('Unable to deactivate actor: [Error]', () => ({ Error: e }));
          }
        }

        info.state = 'deactive';
        this.c.debug('Actor [Actor] deactivated during release', () => ({
          Actor: actorToString(info.actorInfo.name, info.id),
        }));
        this.instances.delete(instanceKey);
        this.instancesByKind.get(info.actorInfo.name)?.delete(instanceKey);
        if (info.globalLockRefreshTimer) {
          this.c.deep('Stopping global lock refresh timer');
          info.globalLockRefreshTimer.cancel();
        }

        for (const timer of info.timers) {
          timer.cancel();
        }

        try {
          this.c.info('Trying to release lock [Until] [A] [B]', () => ({
            Until: info.globalLockUntil,
            A: info.actorInfo.name,
            B: info.id,
          }));
          if (info.globalLockUntil > 0) {
            const globalLockId = [info.actorInfo.name].concat(info.id);
            await this.runtime.releaseLock(globalLockId, 'performed');
          }
        } catch (e) {
          this.c.warning('Unable to release actor lock during shutdown');
        }
      } finally {
        info.localLock.finalize();
      }
    } else {
      this.c.error(
        'Removed actor [Actor] with state [State] from administration (which should already be performed)',
        () => ({
          Actor: actorToString(info.actorInfo.name, info.id),
          State: info.state,
        }),
      );
      this.instances.delete(instanceKey);
      this.instancesByKind.get(info.actorInfo.name)?.delete(instanceKey);
    }
  }

  public getSupportedActors(): ISupportedActor[] {
    const supportedActors: ISupportedActor[] = [];
    for (const [name, info] of this.actors.entries()) {
      const count = this.instancesByKind.get(name)?.size || 0;
      supportedActors.push({
        name,
        multiplicity: info.multiplicity,
        availability: info.capacity <= 0 ? 1 : (info.capacity - count) / info.capacity,
        appBindIndex: info.appBindIndex,
        placement: info.placement,
        dependencies: info.dependencies,
        status: info.status,
      });
    }
    return supportedActors;
  }

  protected async startPushingStatus(): Promise<void> {
    await this.pushStatus();
    this.pushTimer = this.time.repeat(async () => await this.pushStatus(), 'PushStatus', 30 * 1000, 2 * 1000);
  }

  protected cancelPushingStatus(): void {
    if (this.pushTimer) {
      this.pushTimer.cancel();
      this.pushTimer = undefined;
    }
  }

  protected async startActors(): Promise<void> {
    for (const [, actor] of this.actors.entries()) {
      if (actor.onAppStart) {
        try {
          await actor.onAppStart(this.runtime);
        } catch (e) {
          this.c.warning('Actor [Actor] failed to start: [Error]', () => ({
            Actor: actor.name,
            Error: e,
          }));
        }
      }
    }
  }

  protected async stopActors(): Promise<void> {
    for (const [, actor] of Array.from(this.actors.entries()).reverse()) {
      actor.status = 'disabled';

      if (actor.onAppStop) {
        try {
          await actor.onAppStop(this.runtime);
        } catch (e) {
          this.c.warning('Actor [Actor] failed to stop: [Error]', () => ({
            Actor: actor.name,
            Error: e,
          }));
        }
      }
    }
  }

  protected async ensureInstanceActive(
    c: IContext,
    instanceKey: string,
    actor: IRegisterActorOptions,
    action: Struct<IPerformActionReq>,
    actorType: string,
    actorId: string[],
    sessionId: string,
  ): Promise<IInstanceInfo> {
    let instanceInfo = this.instances.get(instanceKey);
    if (!instanceInfo) {
      if (actor.status === 'disabled') {
        throw ProcessError(
          'ACTOR_TYPE_DISABLED',
          'Actor type [ActorType] is disabled (most likely because of shutting down)',
          {
            ActorType: actorType,
          },
        );
      }

      const appBindNodes = [];
      if (actor.appBindIndex !== undefined) {
        const node =
          actor.appBindIndex >= 0 ? actorId[actor.appBindIndex] : actorId[actorId.length + actor.appBindIndex];
        if (node) {
          appBindNodes.push(node);
        }
      }

      if (appBindNodes.length > 0) {
        if (!appBindNodes.includes(this.id)) {
          throw ProcessError(
            'ACTOR_BIND_CONFLICT',
            'Actor [ActorId] of type [ActorType] is not bound to [CurrentNode]',
            { ActorId: actorId, ActorType: actorType, CurrentNode: this.id },
          );
        }
      }

      if (actor.capacity > 0 && (this.instancesByKind.get(actorType)?.size || 0) >= actor.capacity) {
        this.triggerCleanup(actorType);
      }

      const globalLockId = [actor.name].concat(actorId);
      let timer: ITimer | undefined;
      const initializationSessionId = uuid.v4();

      // Create the new actor object instance
      let instance;
      const c2 = c.getChildContext('io.darlean.actorrunner.DoCreate');
      try {
        try {
          c2.debug('Creating new instance with id [Id] of actor [ActorType]', () => ({
            Id: actorId,
            ActorType: actorType,
          }));
          instance = actor.creator({
            id: actorId,
            c: c2.getChildLogger('Actor', actorToString(actor.name, actorId)),
          });
        } catch (e: unknown) {
          const e2 = ensureDarleanError(e);
          throw ProcessError(
            'ACTOR_CREATE_FAILED',
            'The actor instance for [Actor] could not be created',
            { Actore: actorToString(actor.name, actorId) },
            [e2],
          );
        }

        // Register the instance info
        instanceInfo = {
          actorInfo: actor,
          id: actorId,
          instance,
          state: 'created',
          globalLockUntil: 0,
          localLock: new selock.SharedExclusiveLock('exclusive'),
          globalLockRefreshTimer: timer ?? undefined,
          timers: [],
        };
        this.instances.set(instanceKey, instanceInfo);
        c2.debug('Instance was set for [Actor] [Present]', () => ({
          Actor: actor.name,
          Present: !!this.instances.get(instanceKey),
        }));
        this.instancesByKind.get(actorType)?.set(instanceKey, true);

        // Acquire an exclusive local lock so that we can safely do some async processing below.
        instanceInfo.localLock.tryBeginExclusive(initializationSessionId, []);
        try {
          await this.activateActor(
            c2,
            actor,
            appBindNodes,
            instanceInfo,
            globalLockId,
            action,
            instanceKey,
            actorType,
            actorId,
            sessionId,
          );
        } finally {
          instanceInfo.localLock.endExclusive(initializationSessionId);
        }
      } finally {
        c2.finish();
      }
    } else {
      // Put the current actor at the end of LRU list
      this.instancesByKind.get(actorType)?.delete(instanceKey);
      this.instancesByKind.get(actorType)?.set(instanceKey, true);
    }
    return instanceInfo;
  }

  protected async activateActor(
    c: IContext,
    actor: IRegisterActorOptions,
    appBindNodes: string[],
    instanceInfo: IInstanceInfo,
    globalLockId: string[],
    action: Struct<IPerformActionReq>,
    instanceKey: string,
    actorType: string,
    actorId: string[],
    sessionId: string,
  ): Promise<void> {
    // In future, we may bind an actor to a set of nodes (framework can choose one of
    // these nodes). We do need the global lock in that case.
    // We do not do this when actor is bound to exactly one node, because it makes no sense,
    // and because the LockActor actor itself then could not obtain a lock for itself
    // (circular dependency).
    if (actor.multiplicity === 'single' && appBindNodes.length !== 1) {
      const now = this.time.machineTime();
      c.debug('Obtain initial actor lock [HasInstanceInfo] [GlobalLockUntil] [Now]', () => ({
        HasInstanceInfo: !!instanceInfo,
        GlobalLockUntil: instanceInfo?.globalLockUntil,
        Now: now,
      }));

      await this.tryObtainGlobalLock(c, globalLockId, action, instanceKey, actorType, actorId, instanceInfo, now);
    }

    // Activate
    try {
      const context = this.obtainActionContext(c, action, sessionId);

      c.verbose('Activating actor [Id] of type [ActorType]', () => ({
        Id: actorId,
        ActorType: actorType,
        Instance: !!instanceInfo?.instance,
      }));

      const c1 = c.getChildContext('io.darlean.actorrunner.Activate');
      try {
        await actor.activator(instanceInfo, context);
      } finally {
        c1.finish();
      }

      c.deep('Activated actor [Id] of type [ActorType] with [Instance]', () => ({
        Id: actorId,
        ActorType: actorType,
        Instance: !!instanceInfo?.instance,
      }));

      instanceInfo.state = 'active';
    } catch (e) {
      c.warning('Actor [Actor] deleted after activation error: [Error]', () => ({
        Actor: actorToString(actor.name, actorId),
        Error: e,
      }));
      this.instances.delete(instanceKey);
      this.instancesByKind.get(actorType)?.delete(instanceKey);
      try {
        if (instanceInfo.globalLockUntil > 0) {
          await this.runtime.releaseLock(globalLockId, 'nothing', {
            actionHopsRemaining: action.actionHopsRemaining - 1,
          });
        }
      } catch (e) {
        c.error('Unable to release lock');
      }
      const e2 = ensureDarleanError(e);
      throw ProcessError(
        'ACTOR_ACTIVATION_FAILED',
        'The actor instance for [Actor] could not be activated',
        { Actor: actorToString(actorType, actorId) },
        [e2],
      );
    }
  }

  protected async tryObtainGlobalLock(
    c: IContext,
    globalLockId: string[],
    action: Struct<IPerformActionReq>,
    instanceKey: string,
    actorType: string,
    actorId: string[],
    instanceInfo: IInstanceInfo,
    now: number,
  ): Promise<IAcquireLockResult | undefined> {
    let lockInfo: IAcquireLockResult | undefined;

    const c2 = c.getChildContext('io.darlean.actorrunner.AcquireGlobalLock');
    try {
      lockInfo = await this.runtime.acquireLock(globalLockId, this.defaultTtl, false, {
        actionHopsRemaining: action.actionHopsRemaining - 1,
      });
    } catch (e) {
      this.instances.delete(instanceKey);
      c2.debug('Deleting instance for [Actor] because acquire lock failed', () => ({
        Actor: actorType,
      }));
      this.instancesByKind.get(actorType)?.delete(instanceKey);
      throw ProcessError('ACTOR_LOCK_REFUSED', 'Failed to acquire lock for actor [Actor]: [Error]', {
        Actor: { actorType: actorType, actorId },
        Error: e,
      });
    } finally {
      c2.finish();
    }

    c2.deep('Resulting global lock for [GlobalLockId]: [Info]', () => ({
      GlobalLockId: globalLockId,
      Info: lockInfo,
    }));
    if (lockInfo.duration <= 0) {
      this.instances.delete(instanceKey);
      c2.debug('Deleting instance for [Actor]', () => ({ Actor: actorType }));
      this.instancesByKind.get(actorType)?.delete(instanceKey);
      throw ProcessError('ACTOR_LOCK_REFUSED', 'Failed to acquire lock for actor [Actor]', {
        Actor: { actorType: actorType, actorId: actorId },
        Holders: lockInfo.holders,
      });
    }

    instanceInfo.globalLockUntil = now + lockInfo.duration;

    const timer2 = this.time.repeat(
      async () => {
        const c3 = c2.getChildContext('io.darlean.actorrunner.RefreshLockTimer');
        try {
          const renewalInfo = await this.runtime.acquireLock(globalLockId, this.defaultTtl, true, {
            actionHopsRemaining: action.actionHopsRemaining - 1,
          });
          if (renewalInfo.duration <= 0) {
            c3.warning('Actor [Actor] suddenly locked by other party', () => ({
              Actor: actorToString(actorType, actorId),
            }));
            // TODO: gracefully stop the actor. Should we? We are out of control anyways??
            timer2.cancel();
            this.instances.delete(instanceKey);
            this.instancesByKind.get(actorType)?.delete(instanceKey);
          } else {
            if (instanceInfo) {
              instanceInfo.globalLockUntil = this.time.machineTime() + renewalInfo.duration;
            }
          }
        } finally {
          c3.finish();
        }
      },
      'ActorRenewal-' + actorType + ':' + JSON.stringify(actorId),
      Math.round(lockInfo.duration * 0.5),
    );
    instanceInfo.globalLockRefreshTimer = timer2;

    return lockInfo;
  }

  protected triggerCleanup(actorType: string): void {
    if (this.cleaningup.has(actorType)) {
      return;
    }
    this.cleaningup.set(actorType, true);
    setImmediate(async () => {
      try {
        await this.cleanupActors(actorType);
      } catch (e) {
        this.c.warning('Error during cleaning up of actors [ActorType]: [Error]', () => ({
          ActorType: actorType,
          Error: e,
        }));
      } finally {
        this.cleaningup.delete(actorType);
      }
    });
  }

  protected async cleanupActors(actorType: string): Promise<void> {
    const info = this.actors.get(actorType);
    if (!info) {
      this.c.error('Unable to clean up actors [ActorType]: type not known', () => ({
        ActorType: actorType,
      }));
      return;
    }
    const threshold = 0.8 * info.capacity;
    const instances = this.instancesByKind.get(actorType);
    for (const instanceKey of instances?.keys() || []) {
      // Note: list of instances may change while we are async releasing actors, so let's
      // use the actual size here (instead of relying on a local counter or so)
      if ((this.instances?.size || 0) <= threshold) {
        break;
      }
      const instance = this.instances.get(instanceKey);
      if (instance) {
        await this.releaseActor(instance);
      }
    }
  }

  protected obtainActionContext(c: IContext, baseaction: IPerformActionReq, sessionId: string): IBaseActionContext {
    const c1 = c.getChildContext('io.darlean.actorrunner.ObtainActionContext');
    try {
      const runtime = this.runtime;

      // eslint-disable-next-line no-inner-declarations
      async function performActionRaw<Input, Output>(action: IActionOptions<Input>): Promise<IActionResult<Output>> {
        if (!action.hops) {
          action.hops = [];
        }
        action.hops.push({ id: sessionId });
        if (!action.actorType) {
          action.actorType = baseaction.actorType;
        }
        if (action.actorId === undefined) {
          action.actorId = baseaction.actorId;
        }
        if (action.actionHopsRemaining === undefined && baseaction.actionHopsRemaining !== undefined) {
          action.actionHopsRemaining = baseaction.actionHopsRemaining - 1;
        }
        const response = await runtime.dispatchActionRaw<Input, Output>(c, action);
        return response;
      }

      const result: IBaseActionContext = {
        sessionId,
        fullAppId: this.id,
        clusterId: 'TODO:CLUSTERID',
        hopsRemaining: baseaction.actionHopsRemaining ?? DEFAULT_MAX_HOP_COUNT,
        performAction: async <Input, Output>(
          action: IActionOptions<Input>,
        ): Promise<Output extends AnObject ? Struct<Output> : void> => {
          const resp = await performActionRaw<Input, Output>(struct(action));
          return resp.data as Output extends AnObject ? Struct<Output> : void;
        },
        fireAction: <Input>(action: IActionOptions<Input>): void => {
          setImmediate(async () => {
            try {
              await performActionRaw<Input, void>(action);
            } catch (e) {
              c1.deep('Error while firing action [Action]: [Error]', () => ({
                Action: action,
                Error: e,
              }));
            }
          });
        },

        scheduleTimer: <Input>(options: IScheduleTimerOptions<Input>): ITimer => {
          if (!baseaction.actorId) {
            throw new Error(`nable to schedule ${options.name}: ActorId unknown`);
          }
          const instanceKey = baseaction.actorType + '#' + encodeKey(baseaction.actorId);
          const instanceInfo = this.instances.get(instanceKey);
          if (!instanceInfo || instanceInfo.state === 'deactive') {
            throw new Error(`nable to schedule ${options.name}: Actor not active`);
          }
          const timer = this.time.repeat(
            async () => {
              try {
                const req = struct<IPerformActionReq>((attach) => ({
                  actorType: baseaction.actorType,
                  actorId: baseaction.actorId,
                  actionName: options.actionName,
                  actionHopsRemaining: DEFAULT_MAX_HOP_COUNT,
                  data: attach(options.data),
                }));

                await this.handleAction(c, req);
              } catch (e) {
                this.c.error('Error during performing scheduled action [Action]: [Error]', () => ({
                  Action: options.actionName,
                  Error: e,
                }));
              }
            },
            options.name,
            options.interval,
            options.delay,
            options.maxCount,
          );

          instanceInfo.timers.push(timer);

          return timer;
        },

        getService: (name) => {
          const serviceimpl = this.services.get(name);
          if (!serviceimpl) {
            throw new Error(`No such service: ${name}`);
          }
          return serviceimpl(baseaction, result);
        },
      };
      return result;
    } finally {
      c1.finish();
    }
  }

  protected async pushStatus(): Promise<void> {
    const supportedActors = this.getSupportedActors();

    try {
      await this.runtime.publishInfo({
        id: this.id,
        supportedActors,
      });
    } catch (e) {
      this.c.warning('Unable to push app info: [Error]', () => ({ Error: e }));
    }

    try {
      const shutdownInfo = await this.runtime.dispatchAction<void, IShutdownInfo>(this.c, {
        actorType: 'RuntimeService',
        actionName: 'GetShutdownInfo',
        actorId: [],
      });
      if (shutdownInfo?.status === 'shuttingdown') {
        this.c.info('The cluster requested our shutdown.');
        setImmediate(async () => {
          try {
            await this.stop();
          } catch (e) {
            this.c.warning('Error during runtime stop: [Error]', () => ({ Error: e }));
          }
        });
      }
    } catch (e) {
      this.c.debug('Unable to get shutdown info: [Error]', () => ({ Error: e }));
    }
  }
}
