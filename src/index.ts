/**
 * @module everything
 */
export * from './logging';
export * from './types';
export * from './actorfactory';
export * from './actorrunner';
export * from './util';
export * from './application';
export * from './runtimeapplication';
export * from './containers';
export * from './structs';
export * from './binaryencoding';
export * from './tracer';

export * from './testing';

export * as interface_persistence from './interfaces/persistence';
export * as interface_performing from './interfaces/performing';
export * as interface_scheduling from './interfaces/scheduling';
export * as interface_locking from './interfaces/locking';
export * as interface_tracing from './interfaces/tracing';
// export * as interface_time from './interfaces/time';

export * from './services/persistence';
export * from './services/performing';
export * from './services/scheduling';
export * from './services/time';
export * from './services/tracing';

if (require.main === module) {
  /*main()
    .then()
    .catch((e) => console.log(e));*/
}
