export interface ITimeService {
  /**
   * Returns the current time as reported by the machine on which the current node is running.
   */
  nodeTime(): Promise<number>;

  /**
   * Returns the millisecond-ticks on the current node.
   */
  nodeTicks(): Promise<number>;

  /**
   * Returns the current time that the cluster agrees upon.
   */
  clusterTime(): Promise<number>;

  /**
   * Returns the millisecond-ticks for the cluster. The value may jump forward, but it is guaranteed
   * to never decrease.
   */
  clusterTicks(): Promise<number>;

  /**
   * Sleeps for a certain amount of milliseconds. While sleeping, other events continue to be processed
   * by the NodeJS event loop. However, when an action lock is in place, that lock will continue to be
   * respected, so while sleeping from within an action handler method with exclusive locking, no other
   * actions can and will be performed by the same actor instance.
   */
  sleep(ms: number): Promise<void>;

  /**
   * Performs a 'noop', which means that the NodeJS event loop gets the chance to process pending events.
   * The noop function should be called at regular moments (preferably every few milliseconds) during
   * long-running synchronous operations to keep the application responsive and to avoid actor locks from
   * expiration (which could undermine the guarantee that no more than 1 instance of a certain actor is
   * active at any given time within the system).
   */
  noop(): Promise<void>;
}
