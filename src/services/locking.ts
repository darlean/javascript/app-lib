import { PerformActionRequestWaitFor } from '..';

export interface IAcquireLockOptions {
  id: string[];
  requester: string;
  ttl: number;
  retryTime?: number;
  singleStage?: boolean;
}

export interface IAcquireLockResult {
  duration: number;
  holders: string[];
}

export interface IReleaseLockOptions {
  id: string[];
  requester: string;
  waitFor: PerformActionRequestWaitFor;
}

export interface IGetLockInfoOptions {
  id: string[];
}

export interface IGetLockInfoResult {
  holders: string[];
}

export interface ILockingService {
  acquireLock(options: IAcquireLockOptions): Promise<IAcquireLockResult>;
  releaseLock(options: IReleaseLockOptions): Promise<void>;
  getLockInfo(options: IGetLockInfoOptions): Promise<IGetLockInfoResult>;
}
