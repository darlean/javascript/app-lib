// Schedule a callback at delay milliseconds after moment, and repeat for
// at most repeatCount times with the specified interval. When moment is
// not specified, the current moment ('now') is used instead. When delay
// is not specified, the value of interval is used as delay. When interval
// is not set, the callback is only invoked once.

import { IActionOptions } from './performing';

export interface IScheduleReminderOptions<Data> {
  id: string[];
  delay?: number;
  interval?: number;
  repeatCount?: number;
  moment?: number;
  callback: IActionOptions<Data>;
}

export interface IScheduleReminderResult {
  handle: string;
}

export interface ISchedulingService {
  scheduleReminder<Data>(request: IScheduleReminderOptions<Data>): Promise<IScheduleReminderResult>;
}
