import { AttachmentLike0, IAttachment } from '..';

export interface IStoreOptions {
  compartment?: string;
  lookupKeys: string[];
  sortKeys?: string[];
  value: AttachmentLike0;
}

export interface IDeleteOptions {
  compartment?: string;
  lookupKeys: string[];
  sortKeys?: string[];
}

export interface ILoadOptions {
  compartment?: string;
  lookupKeys: string[];
  sortKeys?: string[];
}

export interface ILoadResult<ObjectType> {
  value?: IAttachment<ObjectType>;
}

export interface IQueryRangePart {
  sortKeys: string[];
  compare: 'inclusive' | 'exclusive';
}

export interface IQueryOptions {
  compartment?: string;
  lookupKeys: string[];
  start?: IQueryRangePart;
  end?: IQueryRangePart;
  limit: number;
  order?: 'ascending' | 'descending';
  includeValues?: boolean;
}

export interface IQueryResultItem<ObjectType> {
  lookupKeys: string[];
  sortKeys: string[];
  value: IAttachment<ObjectType>;
}

export interface IQueryResult<ObjectType> {
  items: IQueryResultItem<ObjectType>[];
}

export interface IInspectRequest {
  compartment?: string;
  shardLimit: number;
  includeValues?: boolean;
}

/**
 * Provides access to methods to [[load]], [[store]], [[delete]] or [[query]] data from
 * configured state stores.
 */
export interface IPersistenceService {
  store(request: IStoreOptions): Promise<void>;
  delete(request: IDeleteOptions): Promise<void>;
  load<ObjectType>(request: ILoadOptions): Promise<ILoadResult<ObjectType>>;
  query<ObjectType>(request: IQueryOptions): Promise<IQueryResult<ObjectType>>;
}
