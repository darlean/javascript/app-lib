/*
The files in this folder define interfaces for use within custom NodeJS apps. Internally, they are
converted into the data structures that are actually transmitted over the wire (those are defined
in "../interfaces")

Usually, the 2 structure are very similar. The major differences are on how included data is represented.
Services typically use a data field for this, whereas interfaces typically use child-containers.

The input/output service objects are typically postfixed "Options" and "Result" (to differentiate them from their
interface counterparts that end with "Request and "Response")
*/
