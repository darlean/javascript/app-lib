import { AnObject } from '../containers';
import { Struct, StructLike } from '../structs';

export interface IActionHop {
  id: string;
}

/**
 * Contains the options when performing a sub-action from the implementation code of a parent-action via
 * [[IBaseActionContext.performAction]] or [[IBaseActionContext.fireAction]].
 * @template Data The (object) type of the action input data, or `void` when the caller does not want to pass any data.
 */
export interface IActionOptions<Data extends AnObject | void> {
  /**
   * The actor type that should perform the action. When not provided or empty, defaults to the
   * actor type of the parent action.
   */
  actorType?: string;

  /**
   * The actor id that should perform the action. When not provided (`undefined` or not present), defaults
   * to the actorId of the parent action.
   */
  actorId?: string[];

  /**
   * The name of the action to be performed. When not provided or empty, no actual action is invoked. This can be useful
   * to ensure that the actor is active, without actually invoking an action on the actor.
   */
  actionName?: string;

  /**
   * The data to be passed to the actor. This should either be an object (or a [[Struct]], which also is an object) or
   * `undefined` or not present.
   */
  data?: Data extends AnObject ? StructLike<Data> : undefined;

  // returnData?: ContainerData;

  /**
   * An optional list of nodenames by which the action could be performed. When present, the framework skips the
   * dispatching logic that normally determines on which nodes a certain actor is active or could become active.
   * This is a performance optimization for specialized use cases (like Darlean helper actors themselves), there
   * should be no need to use this field for regular actors.
   */
  receivers?: string[];

  /**
   * The number of network hops this action is allowed to visit on its way to its final destination. When not present,
   * the framework fills this in with a reasonable value. Usually, there is no need to specify this explicitly.
   */
  networkHopsRemaining?: number;

  /**
   * The maximum length of the action chain that any sub-actions (and their suqsequent sub-actions, et cetera) may span.
   * When not provided, the framework sets this to a reasonable value. Usually, there is no need to specify this explicitly.
   * This option is used to prevent infinite action loops of A -> B -> C -> A -> B -> C, et cetera.
   */
  actionHopsRemaining?: number;

  /**
   * The number of retries when a network or process error occurs. Darlean retries actions that result in a
   * network or processing error several times with exponentially increasing time intervals. When, after the retries,
   * still an error occurs, the error is finally thrown.
   *
   * The retry mechanism only works for network and process errors. The 3rd category of errors, actor errors, are not
   * retried, but actor errors are immediately thrown to the calling code. Actor errors are errors that occur within
   * the actual actor implementation ([[BaseActor.activate|activate]], [[BaseActor.deactivate|deactivate]] and action
   * handler methods). Actor errors are normally used to indicate to the caller that something went wrong during the
   * performing of the action. It is up to the caller to interpret this error, and to decide whether to retry or not.
   * It is not the responsibility of the framework to automatically perform retries on actor errors.
   *
   * The framework already sets a decent default value, that corresponds to about 5 retries with a total span time of
   * several seconds.
   */
  retryCount?: number;

  /**
   * The maximum time to live for this request. After the ttl expires, no more network and/or process retries are performed
   * anymore. Actors are expected to read and respect the provided ttl value, but the framework makes no guarantees on that.
   * What the framework *does* guarantee is that after the ttl expires, the call to performAction is immediately aborted and
   * a `TIMEOUT` error is thrown. However, on the background, network and process retries as well as the execution of the
   * action by the actions actor may continue.
   */
  ttl?: number;

  /**
   * List of action hops up to this action. Every sub-action that is being performed adds one entry to the hops of the
   * parent action. This list of hops is used by the framework to detect reentrancy.
   */
  hops?: IActionHop[];

  /**
   * When present, indicates that a new trace (unrelated to any previous trace) should be started for this action.
   */
  // tracing?: ITraceInfo;
}

/**
 * Represents the result of an action that is performed via
 * [[IBaseActionContext.performAction]] or [[IBaseActionContext.fireAction]].
 * @template Data The (object) type of the result data.
 */
export interface IRawActionResult<Data extends AnObject> {
  /**
   * The data as returned by the action.
   *
   * When the action returns attachments, they can be
   * obtained by means of the [[Struct._att]] method.
   */
  data: Struct<Data>;
  // returnedData?: ContainerData;
}

// When data should not be parsed (according to the parse field in the corresponding IActionOptions),
// use IActionResult<void> as type definition to please TypeScript.
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IActionResult<Data extends AnObject> extends IRawActionResult<Data> {}
