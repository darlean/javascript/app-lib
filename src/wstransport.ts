import { ILogger } from './logging';
import {
  ITransport,
  INetworkMessage,
  EditableNetworkMessage,
  ContainerNetworkMessage,
  INetworkHeader,
  INetworkEnvelope,
} from './networktypes';
import { WebSocket } from 'ws';
import * as uws from 'uWebSockets.js';
import { BaseTransport, IPeerInfo } from './basetransport';
import { IPeerConfig } from './types';
import { decodeFromBinaryBuffer, encodeToBinaryBuffers } from './binaryencoding';

const PROTO_VERSION = '1.0';
const MAX_BUFFERED_AMOUNT = 50000; //200000;
const MAX_MESSAGE_SIZE = 10 * 1000 * 1000;

type WsMessage = Buffer;

interface IWsMessage {
  version: string;
  sender: string;
  blobs: { id: string; size: number }[];
}

export class WsTransport extends BaseTransport<uws.WebSocket, WebSocket> implements ITransport {
  private listenSocket?: uws.us_listen_socket;
  private incomingSockets: Map<unknown, string>;
  private incomingWaiters: Map<unknown, Array<{ callback?: (error: unknown) => void; message?: WsMessage }>>;
  private peerReconnectTimers: Map<string, NodeJS.Timeout>;

  constructor(logger: ILogger, id: string, address: string, peers: IPeerConfig[]) {
    super(logger, id, address, peers);
    this.incomingSockets = new Map();
    this.incomingWaiters = new Map();
    this.peerReconnectTimers = new Map();
  }

  public async initIn(): Promise<void> {
    if (this.address) {
      await this.launchIncoming(this.address);
    }
  }

  public async initOut(): Promise<void> {
    for (const peer of this.peers.values()) {
      this.launchOutgoing(peer);
    }
  }

  protected async closeImpl(): Promise<void> {
    this.active = false;

    for (const timer of this.peerReconnectTimers.values()) {
      clearTimeout(timer);
    }
    this.peerReconnectTimers.clear();

    if (this.listenSocket) {
      this.logger.deep('Stopping WS server');
      uws.us_listen_socket_close(this.listenSocket);
    }

    this.logger.deep('Closing open connections');
    for (const peer of this.peers.values()) {
      if (peer.incoming) {
        this.logger.deep('Terminating incoming socket from [Peer]', () => ({ Peer: peer.id }));
        peer.incoming.end(1012, 'Server is stopping');
        peer.incoming = undefined;
      }
      if (peer.outgoing) {
        this.logger.deep('Terminating outgoing socket to [Peer]', () => ({ Peer: peer.id }));
        peer.outgoing.close(1012, 'Server is stopping');
        peer.outgoing = undefined;
      }
    }
    this.logger.debug('Closed network');
  }

  protected async launchIncoming(address: string): Promise<number> {
    return new Promise((resolve, reject) => {
      const server2 = uws.App();
      server2.ws('/*', {
        maxPayloadLength: MAX_MESSAGE_SIZE,
        open: (ws) => {
          this.logger.info('Incoming connection to [B]', () => ({ B: this.id }));
          if (!this.active) {
            ws.end(1012, 'SHUTTING_DOWN');
          }
          // TODO: Close the connection when no initial message is received within certain time
        },

        close: (ws) => {
          console.log('SERVER SIDE CLOSED');
          const waiting = this.incomingWaiters.get(ws);
          if (waiting) {
            for (const waiter of waiting) {
              const e = new Error('Stream closed');
              if (waiter.callback) {
                waiter.callback(e);
              }
            }
            this.incomingWaiters.delete(ws);
          }

          const peerid = this.incomingSockets.get(ws) ?? '';
          this.logger.info('Incoming websocket from [Sender] closed', () => ({ Sender: peerid }));
          if (peerid) {
            this.incomingSockets.delete(ws);
            const peer = this.peers.get(peerid);
            if (peer) {
              peer.incoming = undefined;
            }
          }
        },

        message: (ws, bufferarray) => {
          try {
            // This is just a view over the same data
            const buffer2 = Buffer.from(bufferarray);
            const buffer = Buffer.from(buffer2);
            // Now, buffer is a copy of bufferarray. That is required because we will later
            // asynchronously process the contents, and the original bufferarray is then
            // already reused for the next request.

            let sender = this.incomingSockets.get(ws);
            if (sender === undefined) {
              const info = JSON.parse(buffer.toString()) as IWsMessage;
              sender = info.sender;
              this.logger.deep('Received sender [Sender]', () => ({ Sender: sender }));
              this.incomingSockets.set(ws, sender);
              const peer = this.peers.get(sender);
              if (!peer) {
                const peer2: IPeerInfo<uws.WebSocket, WebSocket> = {
                  id: sender,
                  incoming: ws,
                  kind: 'client',
                };
                this.peers.set(sender, peer2);
                this.logger.debug('Added new incoming node [Id]', () => ({ Id: sender }));
              } else {
                peer.incoming = ws;
                this.logger.deep('Received incoming stream from known node [Id]', () => ({ Id: sender }));
              }
              return;
            }

            const message = this.messageToTransportMessage(buffer);
            this.logger.deep('Incoming socket received message [Message] from [Sender]', () => ({
              Message: message,
              Sender: sender ?? '',
            }));

            if (this.handler) {
              const handler = this.handler;
              process.nextTick(async () => {
                try {
                  await handler(message);
                } catch (e) {
                  this.logger.error('Unexpected error during handling of message: [Error]', () => ({ Error: e }));
                }
              });
            }
          } catch (e) {
            this.logger.error('Error in handling socket data: [Error]', () => ({ Error: e }));
          }
        },

        drain: (ws) => {
          const waiters = this.incomingWaiters.get(ws);
          if (waiters) {
            this.incomingWaiters.set(ws, []);

            /*
            ws.cork(() => {
              for (const waiter of waiters) {
                if (waiter.message) {
                  this.writeIncomingRaw(ws, waiter.message, waiter.callback);
                } else if (waiter.callback) {
                  waiter.callback(undefined);
                }
              }
            });*/
            for (const waiter of waiters) {
              if (waiter.message) {
                this.writeIncomingRaw(ws, waiter.message, waiter.callback);
              } else if (waiter.callback) {
                waiter.callback(undefined);
              }
            }
          }
        },
      });

      try {
        server2.listen(parseInt(address.split(':')[1], 10), (socket) => {
          // console.log('SOCKET', socket, JSON.stringify(socket), uws.us_socket_local_port(socket));
          if (socket) {
            this.listenSocket = socket;
            resolve(0);
          } else {
            reject('Failed to listen');
          }
        });
      } catch (e) {
        this.logger.warning('Failed to listen on [Address]: [Error]', () => ({ Address: address, Error: e }));
        reject('Failed to listen');
      }
    });
  }

  protected writeOutgoing(socket: WebSocket, message: EditableNetworkMessage, cb?: (error: unknown) => void): void {
    // console.log('WRITE OUTGOING');
    const msg = this.messageToProtoMessage(message);
    if (msg.length > MAX_MESSAGE_SIZE) {
      throw new Error(
        `Message size ${msg.length} is larger than max allowed message size of ${MAX_MESSAGE_SIZE} bytes. The message is not sent.`,
      );
    }
    socket.send(msg, { binary: true }, cb);
  }

  protected writeIncoming(socket: uws.WebSocket, message: EditableNetworkMessage, cb?: (error: unknown) => void): void {
    // console.log('WRITE INCOMING');
    const msg = this.messageToProtoMessage(message);
    this.writeIncomingRaw(socket, msg, cb);
  }

  protected writeIncomingRaw(socket: uws.WebSocket, msg: WsMessage, cb?: (error: unknown) => void): void {
    if (msg.length > MAX_MESSAGE_SIZE) {
      throw new Error(
        `Message size ${msg.length} is larger than max allowed message size of ${MAX_MESSAGE_SIZE} bytes. The message is not sent.`,
      );
    }
    const flat = msg;
    try {
      // console.log(socket.getBufferedAmount());
      // If buffer is full, do not even try but put cb + msg in queue
      if (socket.getBufferedAmount() > MAX_BUFFERED_AMOUNT) {
        // console.log('A');
        const callbacks = this.incomingWaiters.get(socket);
        if (!callbacks) {
          this.incomingWaiters.set(socket, [{ callback: cb, message: msg }]);
        } else {
          callbacks.push({ callback: cb, message: msg });
        }
        return;
      }
      // Try to send. When we receive pressure, the message is not sent out yet, so
      // we add just the cb to be queued.
      const pressure = !socket.send(flat, true);
      if (pressure && socket.getBufferedAmount() > MAX_BUFFERED_AMOUNT) {
        // console.log('B');
        if (cb) {
          const callbacks = this.incomingWaiters.get(socket);
          if (!callbacks) {
            this.incomingWaiters.set(socket, [{ callback: cb }]);
          } else {
            callbacks.push({ callback: cb });
          }
          return;
        }
      }
      if (cb) {
        cb(undefined);
      }
    } catch (e) {
      if (cb) {
        cb(e);
      }
    }
  }

  protected launchOutgoing(peer: IPeerInfo<uws.WebSocket, WebSocket>): void {
    this.peerReconnectTimers.delete(peer.id);
    if (!this.active) {
      return;
    }

    if (!peer.address) {
      return;
    }

    if (peer.outgoing) {
      this.logger.error('PEER [Peer] ALREADY HAS OUTGOING', () => ({ Peer: peer.id }));
      return;
    }

    this.logger.debug('Launching outgoing from [Self] to [Peer]', () => ({ Self: this.id, Peer: peer.id }));

    try {
      const socket = new WebSocket('ws://' + peer.address, {
        maxPayload: MAX_MESSAGE_SIZE,
      });
      socket.on('open', () => {
        this.logger.info('Connected from [A] to [B]', () => ({ A: this.id, B: peer.id }));
        if (peer.outgoing) {
          this.logger.error('Peer already has outgoing');
          peer.outgoing.close();
        }
        peer.outgoing = socket;

        const raw = Buffer.from(
          JSON.stringify({
            sender: this.id,
            version: PROTO_VERSION,
          } as IWsMessage),
        );
        socket.send(raw, { binary: true });
      });

      socket.on('message', (data: Buffer) => {
        try {
          // console.log('INCOMING MSG');
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          //(socket as any).pause();
          const message = data;
          this.logger.deep('Outgoing socket received message [Message]', () => ({ Message: message }));

          if (this.handler) {
            const msg2 = this.messageToTransportMessage(message);
            this.handler(msg2)
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              .then(() => (socket as any).resume())
              .catch((e) => {
                this.logger.error('Unexpected error during handling of message: [Error]', () => ({ Error: e }));
              });
          } else {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            (socket as any).resume();
          }
        } catch (e) {
          this.logger.debug('Error in logger data: [Error]', () => ({ Error: e }));
        }
      });

      socket.on('error', (e) => {
        // TODO: Emit
        this.logger.debug('Socket encountered error: [Error]', () => ({ Error: e }));
        console.log('ERROR', e);
      });

      socket.on('close', () => {
        try {
          console.log('CLOSED');
          socket.close();
          if (peer.outgoing) {
            peer.outgoing = undefined;
          }
          if (this.active) {
            const timer = this.peerReconnectTimers.get(peer.id);
            if (!timer) {
              this.logger.debug('Outgoing socket disconnected, trying to reconnect in 1 second');
              this.peerReconnectTimers.set(
                peer.id,
                setTimeout(() => this.launchOutgoing(peer), 1000),
              );
            }
          } else {
            this.logger.info('Outgoing socket disconnected (will not retry, we are inactive)');
          }
        } catch (e) {
          this.logger.debug('Error in socket.end: [Error]', () => ({ Error: e }));
        }
      });
    } catch (e) {
      // TODO: emit
      this.logger.debug('Socket connect error: [Error]', () => ({ Error: e }));
    }
  }

  protected messageToProtoMessage(msg: EditableNetworkMessage): WsMessage {
    const container = msg.toContainer();
    const buffers: Buffer[] = [];
    encodeToBinaryBuffers(container, buffers);
    const buffer = Buffer.concat(buffers);
    return buffer;
  }

  protected messageToTransportMessage(msg: WsMessage): INetworkMessage {
    const container = decodeFromBinaryBuffer<INetworkHeader, INetworkEnvelope>(msg, 0);
    return new ContainerNetworkMessage(container);
  }
}
