/**
 * Provides an actor factory and other types to base custom actors upon.
 * * Custom actors can be created by extending the [[BaseActor]] class.
 * * Custom actors can be registered by means of the [[actorFactory]] function that returns an [[ActorFactory]] instance
 * * Decorators are provided for for regular actors ([[actor|@actor]]) and service actors ([[service|@service]])
 * * A decorator is provided for actor action implementation methods ([[action|@action]])
 * * An [[IActionContext]] (that extends [[IBaseActionContext]]) is defined that allows
 *   action implementation code to interact with other actors and the Darlean platform.
 * @module
 */
import {
  actionToString,
  ensureDarleanError,
  IPersistenceService,
  ISchedulingService,
  ITimeService,
  struct,
  toRpcError,
} from '.';
import { AnObject } from './containers';
import {
  IPerformActionReq,
  IPerformActionReqCL,
  IPerformActionResp,
  IPerformActionRespC,
} from './interfaces/performing';
import { IContext } from './logging';
import { SharedExclusiveLock } from './selock';
import {
  IBaseActionContext,
  IActorCreateOptions,
  IActorRegistration,
  IInstanceInfo,
  IRegisterActorOptions,
  IRuntime,
  ProcessError,
} from './types';

const DEFAULT_DEPENDENCIES = ['PersistenceService', 'RuntimeService'];

/**
 * Context object that can be used during handling of an action for performing (sub)actions, scheduling
 * timers and reminders, persisting state, and more.
 *
 * A new instance of [[IActionContext]] is automatically created by the framework for every action request,
 * and provided to [[BaseActor.activate]], [[BaseActor.deactivate]] and action implementation methods as
 * `context` parameter (see the documentation of [[BaseActor]] for more info).
 */
export interface IActionContext extends IBaseActionContext {
  /**
   * Shortcut for `getService('state')`. Provides easy access to the [[IPersistenceService|state persistence service]]
   * that can be used to load or store actor state. The [[structFromBuf]] and [[structToBuf]] functions can be used to
   * serialize structured data to a (JSON) Buffer that is understood by the state service.
   *
   * Example for loading and storing state:
   * ```
   * await context.state().store({
   *   lookupKeys: 'state',
   *   value: structToBuf({a: 12, b: 14})
   * });
   * ...
   * const result = await context.state.load({
   *   lookupKeys: 'state',
   * });
   * const state = result.value ? structFromBuf(result._att(result.value)) : undefined;
   * console.log(state);    // Prints: {a: 12, b: 14}
   * ```
   */
  state(): IPersistenceService;

  /**
   * Shortcut for `getService('time')`. Provides easy access to the [[ITimeService|time service]]
   * that can be used to access node and cluster time and to sleep.
   */
  time(): ITimeService;

  /**
   * Shortcut for `getService('scheduling')`. Provides easy access to the [[ISchedulingService|scheduling service]]
   * that can be used to schedule persistent reminders.
   */
  scheduling(): ISchedulingService;
}

/**
 * Interface that should be implemented by actors.
 */
export interface IActor {
  activate?(context: IActionContext): Promise<void>;
  deactivate?(context: IActionContext): Promise<void>;
}

interface IActionDefinition {
  name: string;
  locking?: 'shared' | 'exclusive' | 'none';
  // eslint-disable-next-line @typescript-eslint/ban-types
  method?: Function;
}

interface IActorDefinition {
  name: string;
  kind?: 'actor' | 'service';
  locking?: 'shared' | 'exclusive';
  dependencies?: string[];
  actions: Map<string, IActionDefinition>;
  appBindIndex?: number;
  onAppStart?: (runtime: IRuntime) => Promise<void>;
  onAppStop?: (runtime: IRuntime) => Promise<void>;
}

/**
 * Options that can be used to decorate an action method.
 */
export interface IActionDecoration {
  /**
   * The name of the action via which other actors can invoke the action. When omitted, the name of the method is used as action name.
   */
  name?: string;
  /**
   * The locking method for this action. When omitted, the locking method for the actor is used. When that actor decoration also does
   * not explicitly define the locking, a locking of `'exclusive'` is used for regular actors, and a locking of `'shared'` is used for
   * service actors.
   *
   * * For *exclusive locking*, the framework ensures that only one action method is executed at a time per actor instance (with the exception
   *   of reentrant calls for the same call tree).
   * * For *shared* locking*, multiple actions that are also 'shared' can be active at the same time per actor instance, but not at the same time
   *   as an action that is 'exclusive'.
   * * When locking is set to `'none'` for an action, it can always be invoked, regardless of other shared or exclusive actions currently
   *   being performed or not. This option should only be used in very special use cases where the other locking modes are not sufficient.
   */
  locking?: 'shared' | 'exclusive' | 'none';
  /**
   * An optional description for the action that can, for example, be displayed in the Darlean control panel for informative purposes.
   */
  description?: string;
}

/**
 * Decoration options for regular and service actors.
 *
 */
export interface IActorDecoration {
  /**
   * Name of the actor (aka the actorType) by which other actors can reference the actor type.
   */
  name?: string;

  /**
   * Default locking mode for the actions of this actor. When not specified, regular actors
   * (when `@actor` is used as decorator) have default locking mode `'exclusive'`, whereas
   * service actors (with `@service` as decorator) have `'shared'` locking by default.
   */
  locking?: 'shared' | 'exclusive';

  /**
   * An optional list of actor type names that this actor needs to be available for proper
   * operation (incuding deactivation). When the cluster is being shut down, the framework
   * tries to repect these dependencies so that all actors can be disabled properly.
   */
  dependencies?: string[];

  /**
   * When True, a preconfigured list of very basic dependencies is automatically added to the
   * actors list of dependencies. This includes the Darlean runtime and persistence services
   * so that actors can always properly store their state upon deactivation.
   */
  inheritDefaultDependencies?: boolean;

  /**
   * Optional index that indocates which field of the actor id contains the name of the node
   * on which it should be running.
   *
   * When present, the actor is forced to run exactly on the node specified by the corresponding
   * id field. The actor will never run on another node, even not when the specified node is not available.
   *
   * When the appBindIndex is negative, it is counted from the end of the actor id parts (so, a
   * value of -1 means the rightmost id part, and so on).
   *
   * By default, appBindIndex is not set, which allows dynamic actor placement (which usually is a good
   * thing considering it is a conscious choice to use a *virtual* actor framework instead of a regular
   * actor framework).
   */
  appBindIndex?: number;

  /**
   * Optional callback that is invoked when the Darlean application is started. See
   * [[IBaseActorFactoryOptions.onAppStart]] for more info and caveats.
   */
  onAppStart?: (runtime: IRuntime) => Promise<void>;

  /**
   * Optional callback that is invoked when the Darlean application is stopping. See
   * [[IBaseActorFactoryOptions.onAppStop]] for more info and caveats.
   */
  onAppStop?: (runtime: IRuntime) => Promise<void>;
}

// eslint-disable-next-line @typescript-eslint/ban-types
const definitions = new Map<Object, IActorDefinition>();

/**
 * Decorator for a regular actor class.
 *
 * Standard use:
 * ```
 * @actor({name: 'MyActor'})
 * export class MyActor extends BaseActor {...}
 * ```
 * For the list of options, see [[IActorDecoration]]. To decorate a *service actor*, use [[service|@service]] instead.
 */
export function actor(config: IActorDecoration) {
  // eslint-disable-next-line @typescript-eslint/ban-types
  return function (constructor: Function): void {
    const target = constructor.prototype;
    let def = definitions.get(target);
    if (!def) {
      def = {
        name: '',
        actions: new Map(),
      };
      definitions.set(target, def);
    }
    // We overwrite the name, because the @action decorators are invoked before us. The @action
    // decorator does not know the config.name.
    def.name = config.name || constructor.name;
    def.kind = 'actor';
    def.locking = config.locking;
    def.dependencies = config.dependencies;
    if (config.inheritDefaultDependencies !== false) {
      def.dependencies = (def.dependencies ?? []).concat(DEFAULT_DEPENDENCIES);
    }
    def.appBindIndex = config.appBindIndex;
    def.onAppStart = config.onAppStart;
    def.onAppStop = config.onAppStop;
  };
}

/**
 * Decorator for a service actor class.
 *
 * Standard use:
 * ```
 * @service({name: 'MyService'})
 * export class MyService extends BaseActor {...}
 * ```
 * For the list of options, see [[IActorDecoration]]. To decorate a *regular (non-service) actor*, use [[actor|@actor]] instead.
 */
export function service(config: IActorDecoration) {
  // eslint-disable-next-line @typescript-eslint/ban-types
  return function (constructor: Function): void {
    const target = constructor.prototype;
    let def = definitions.get(target);
    if (!def) {
      def = {
        name: '',
        actions: new Map(),
      };
      definitions.set(target, def);
    }
    // We overwrite the name, because the @action decorators are invoked before us. The @action
    // decorator does not know the config.name.
    def.name = config.name || constructor.name;
    def.kind = 'service';
    def.locking = config.locking;
    def.dependencies = config.dependencies;
    if (config.inheritDefaultDependencies !== false) {
      def.dependencies = (def.dependencies ?? []).concat(DEFAULT_DEPENDENCIES);
    }
    def.appBindIndex = config.appBindIndex;
    def.onAppStart = config.onAppStart;
    def.onAppStop = config.onAppStop;
  };
}

function capitalize(value: string) {
  return value[0].toUpperCase() + value.slice(1);
}

/**
 * Decorator for an action method.
 *
 * When the method name already matches with the action name, and no additional opions are required:
 * ```
 * @action()
 * public myActor(...) {}
 * ```
 *
 * When the method name does not match with the action name, and/or when additional options are required:
 * ```
 * @action({name: 'myAction', locking: 'shared'})
 * public myActorFunction(...) {}
 * ```
 *
 * For a list of options, see [[IActionDecoration]].
 */
export function action(config?: IActionDecoration) {
  // eslint-disable-next-line @typescript-eslint/ban-types
  return function (target: Object, propertyKey: string, descriptor: PropertyDescriptor): void {
    let def = definitions.get(target);
    if (!def) {
      def = {
        name: target.constructor.name,
        actions: new Map(),
      };
      definitions.set(target, def);
    }
    def.actions.set(propertyKey, {
      name: config?.name || capitalize(propertyKey),
      locking: config?.locking,
      method: descriptor.value,
    });
  };
}

/**
 * Base class for creating custom actors. BaseActor defines empty [[activate]] and [[deactivate]] placeholder methods that can
 * be overriden in subclasses. These methods are invoked by the framework when a new actor instance becomes active and just
 * before it becomes inactive, respectively.
 *
 * ```
 * @actor({name: 'MyActor})
 * export class MyActor extends BaseActor {
 *   public async activate (...)
 *   public async deactivate (...)
 *
 *   @action({...})
 *   public async myAction1 (...)
 *
 *   @action({...})
 *   public async myAction2 (...)
 * }
 * ```
 *
 * Actions are defined by means of decorated methods of the following signature:
 * ```
 *   @action({...})
 *   public async myAction(data: Input, context: IActionContext): Promise<Output> {
 *     // Implementation goes here
 *   }
 * ```
 * Where `Input` is the type of input data to the action, and `Output` the type of output data. Both
 * Input and Output must be object types (primitive values like numers and strings are not allowed).
 *
 * When an action needs access to the attachments of input data, or when it outputs attachments, it should
 * use the [[Struct]] type:
 * ```
 *   interface IMyType { a: IBinaryAttachmentRef, b: string };
 *   ...
 *   @action()
 *   public async myAction(data: Struct<IMyType>, context: IActionContext): Promise<Struct<IMyType>> {
 *     const a = data._att(data.a).buffer();
 *     const b = data.b;
 *     // Do something with a (a Buffer) and b (a string)
 *     return struct( (attach) => {
 *       a: attach(Buffer.from('Moon'))
 *       b: 'World',
 *     });
 *   }
 * ```
 *
 * Actions, [[activate]] and [[deactivate]] can use the provided [[IActionContext | context object]] to invoke
 * other actions (on the same or a different actor), to load or store state, to set timers or reminders,
 * et cetera. See [[IActionContext]] for more information.
 *
 * Actors must be decorated with [[actor | @actor]] or [[service | @service]], actions must be decorated
 * with [[action | @action]]. The `@actor` and `@service` decorators can be customized with
 * [[IActorDecoration | actor decorator options]], the `@service` decorator can be customized with the
 * [[IActionDecoration | action decorator options]].
 */
export class BaseActor implements IActor {
  /**
   * The id of the actor.
   */
  public id: string[];

  /**
   * The context of the actor. Can be used to perform logging or tracing.
   */
  public c: IContext;

  /**
   * Constructor of the actor. It is best practice to inject any dependencies (like configuration settings)
   * into the constructor by passing a custom options object:
   * ```
   * interface MyActorCreateOptions {
   *    configValueA: string;
   * }
   * ...
   * constructor(options: IActorCreateOptions & IMyActorCreateOptions) {
   *   super(options);
   *   this.configValueA = options.configValueA;
   * }
   * ```
   * @param options
   */
  constructor(options: IActorCreateOptions) {
    this.id = options.id;
    this.c = options.c;
  }

  /**
   * Placeholder for code that is to be executed when the actor instance becomes active.
   * @param _context Context that can be used to load state, set timers and reminders, invoke
   * other actions, et cetera.
   */
  async activate(_context: IActionContext): Promise<void> {
    // Placeholder for any future generic activation code.
  }

  /**
   * Placeholder for code that is to be executed when the actor instace becomes inactive. A typical
   * implementation would persist the actor state during deactivate. During deactivate, timers are still received, but
   * reminders are blocked. Incoming actions that result from actions that originate from within the
   * deactivate are properly received (reentrancy is allowed), but other incoming actions are ignored during
   * (and after) deactivate.
   * @param _context Context that can be used to store state, invoke other actions, et cetera.
   */
  async deactivate(_context: IActionContext): Promise<void> {
    // Placeholder for any future generic deactivation code.
  }
}

/**
 * Instructs the actor factory on how to generate actor instances.
 */
export interface IBaseActorFactoryOptions<Options extends AnObject> {
  /**
   * The class (a subclass of [[BaseActor]]) that holds the actor implementation.
   */
  clazz: typeof BaseActor;

  /**
   * A callback function that is invoked every time a new actor instance is created. The callback
   * function receives the actor id as input. A typical implementation would read the settings for
   * the specific actor from a settings file (see [[ISettings]]) and return the relevant settings
   * as an options object.
   *
   * Using this actorOptions mechanism is recommended over the alternative mechanism of having actors
   * fetch their own settings from a configuration file. The actorOptions mechanism provides a nice
   * [[https://en.wikipedia.org/wiki/Inversion_of_control|inversion of control]] mechanism that removes the need for actor implementation to be dependent on
   * the format and contents of settings files.
   */
  actorOptions?: Options | ((id: string[]) => Options);

  /**
   * The maximum number of active actor instances before the oldest instance are automatically deactivated
   * to make room for new instances. Set to 0 to disable actor instance recycling (but be aware for out of
   * memory issues when actions on actors for a lot of different id's are being performed).
   */
  capacity: number;

  /**
   * Callback function that is invoked when the Darlean application is running and all local actor types
   * are registered. Typically used to launch 'persistent' actors (like those that open a listening TCP socket)
   * or to set periodic timers or reminders.
   *
   * Note that although local actor types (actor types for the current application) are registered, it is not
   * guaranteed that also other actor types (from other applications) are already available. Implementation code
   * must be robust against that.
   *
   * The `runtime` parameter can be used to invoke actions on actors or to schedule timers and/or reminders.
   */
  onAppStart?: (runtime: IRuntime) => Promise<void>;
  /**
   * Callback function that is invoked just before the Darlean application is stopping.
   */
  onAppStop?: (runtime: IRuntime) => Promise<void>;
}

function resolveOptions<Options>(idparts: string[], options?: Options | ((id: string[]) => Options)) {
  const result: { [key: string]: unknown } = {};
  if (options) {
    if (typeof options === 'function') {
      const opts = (options as (id: string[]) => Options)(idparts);
      return opts;
    } else {
      return options;
    }
  }
  return result;
}

/**
 * Creates a factory for a customer actor that extends [[BaseActor]].
 * @param context The context that is used to log factory events to
 * @param options The options that defines the actor for which a factory is to be created
 * @returns A newly created actor factory
 */
export function actorFactory<Options>(context: IContext, options: IBaseActorFactoryOptions<Options>): ActorFactory {
  const def = definitions.get(options.clazz.prototype);
  if (def) {
    const actions: { [name: string]: IActionDefinition } = {};
    for (const action of def.actions.values()) {
      actions[action.name] = action;
    }
    return new ActorFactory(
      context,
      def.name,
      def.kind || 'actor',
      options.capacity,
      def.dependencies || [],
      (opts) => new options.clazz({ ...opts, ...resolveOptions(opts.id, options.actorOptions) }),
      options.onAppStart ?? def.onAppStart,
      options.onAppStop ?? def.onAppStop,
      def.appBindIndex,
      def.locking,
      actions,
    );
  } else {
    throw new Error(`No definitions found for [${options.clazz.name}]. Did you add the @actor decorator?`);
  }
}

export class ActorFactory implements IActorRegistration {
  protected creator: (options: IActorCreateOptions) => unknown;
  protected options: IRegisterActorOptions;
  protected c: IContext;
  protected actions: Map<string, IActionDefinition>;

  constructor(
    c: IContext,
    name: string,
    type: 'actor' | 'service',
    capacity: number,
    dependencies: string[],
    creator: (options: IActorCreateOptions) => unknown,
    onAppStart?: (runtime: IRuntime) => Promise<void>,
    onAppStop?: (runtime: IRuntime) => Promise<void>,
    appBindIndex?: number,
    locking?: 'exclusive' | 'shared',
    actions?: { [name: string]: IActionDefinition },
  ) {
    this.actions = new Map();
    this.creator = creator;
    this.c = c;
    this.actions = new Map();
    if (actions) {
      for (const [name, def] of Object.entries(actions)) {
        def.name = name;
        this.actions.set(name, def);
      }
    }
    this.options = {
      activator: async (instanceInfo, context) => {
        return await this.activate(instanceInfo, context);
      },
      deactivator: async (instanceInfo, context) => {
        return await this.deactivate(instanceInfo, context);
      },
      creator: (options: IActorCreateOptions) => {
        return this.creator(options);
      },
      multiplicity: type === 'actor' ? 'single' : 'multiple',
      placement: type === 'actor' ? 'availability' : 'local',
      locking: locking ?? (type === 'actor' ? 'exclusive' : 'shared'),
      capacity,
      name,
      appBindIndex,
      status: 'enabled',
      dependencies,
      perform: async (c, instanceInfo, actionContainer, context) => {
        return await this.perform(c, instanceInfo, actionContainer /*asContainer(actionContainer)*/, context);
      },
      onAppStart: async (runtime) => {
        if (onAppStart) {
          await onAppStart(runtime);
        }
      },
      onAppStop: async (runtime) => {
        if (onAppStop) {
          await onAppStop(runtime);
        }
      },
    };
  }

  public getRegisterOptions(): IRegisterActorOptions {
    return this.options;
  }

  protected async activate(instanceInfo: IInstanceInfo, context: IBaseActionContext): Promise<void> {
    const func = (instanceInfo.instance as IActor).activate;
    if (func) {
      const ctx = this.makeContext(context);
      await func.call(instanceInfo.instance, ctx);
    }
  }

  protected async deactivate(instanceInfo: IInstanceInfo, context: IBaseActionContext): Promise<void> {
    const func = (instanceInfo.instance as IActor).deactivate;
    if (func) {
      const ctx = this.makeContext(context);
      await func.call(instanceInfo.instance, ctx);
    }
  }

  protected async perform(
    c: IContext,
    instanceInfo: IInstanceInfo,
    actionContainer: IPerformActionReqCL<AnObject>,
    context: IBaseActionContext,
  ): Promise<IPerformActionRespC<AnObject>> {
    const action = struct<IPerformActionReq>(actionContainer);
    const logger = c.getChildContext('io.darlean.actorfactory.Perform', undefined, () => ({
      ActorType: instanceInfo.actorInfo.name,
      ActionName: action.actionName,
    }));
    try {
      if (!action.actionName) {
        const resp = struct<IPerformActionResp>((attach) => ({
          data: attach({}),
        }));
        return resp._cont();
      }

      if (!context.sessionId) {
        throw new Error('SessionId must be present');
      }

      let locking: 'shared' | 'exclusive' | 'none' = instanceInfo.actorInfo.locking;
      const def = this.actions.get(action.actionName);
      if (def?.locking) {
        locking = def.locking;
      }

      // eslint-disable-next-line
      const func: Function | undefined = def?.method ?? (instanceInfo.instance as any)[action.actionName];
      if (func === undefined) {
        throw ProcessError('UNKNOWN_ACTION', 'Action [Action] is not supported by [ActorType]', {
          Action: action.actionName,
          ActorType: action.actorType,
        });
      }

      const lock = instanceInfo.localLock;
      const hops = (action.hops ?? []).map((v) => v.id);
      await this.acquireLocalLock(logger, lock, locking, context.sessionId, hops);
      try {
        if (instanceInfo.state === 'deactive') {
          throw ProcessError('ACTOR_NOT_ACTIVE', 'The actor is not active (anymore).');
        }

        this.c.verbose('Performing action [Action]', () => ({
          Action: actionToString(action),
        }));

        try {
          const req = action;
          const data = req.data ? req._att(req.data).struct() : {};
          let value;
          const logger2 = logger.getChildContext('io.darlean.actorfactory.MethodCall', undefined, () => ({
            ActorType: action.actorType,
            ActionName: action.actionName,
          }));

          const ctx = this.makeContext(context);

          try {
            value = await func.call(instanceInfo.instance, data, ctx);
          } finally {
            logger2.finish();
          }

          this.c.verbose('Performed action [Action]', () => ({
            Action: actionToString(action),
          }));

          if (value !== undefined && typeof value !== 'object') {
            throw new Error(
              `Returned value for action ${actionToString(
                action,
              )} of type [${typeof value}] is not an object. Actor actions must return objects (not scalars or other types).`,
            );
          }

          const result = struct(value || {});
          const resp = struct<IPerformActionResp>((attach) => ({
            data: attach(result),
          }));
          return resp._cont();
        } catch (e) {
          this.c.info('Performed action [Action] resulted in error: [Error]', () => ({
            Action: actionToString(action),
            Error: e,
          }));
          return struct<IPerformActionResp>({
            actorError: toRpcError(ensureDarleanError(e)),
          })._cont();
        }
      } finally {
        this.releaseLocalLock(lock, locking, context.sessionId);
      }
    } finally {
      logger.finish();
    }
  }

  protected async acquireLocalLock(
    context: IContext,
    lock: SharedExclusiveLock,
    locking: 'shared' | 'exclusive' | 'none',
    sessionId: string,
    hops: string[],
  ): Promise<void> {
    const context2 = context.getChildLogger('io.darlean.actorfactory.locallock');
    try {
      if (locking === 'shared') {
        await lock.beginShared(sessionId, hops);
      } else if (locking === 'exclusive') {
        await lock.beginExclusive(sessionId, hops);
      }
    } finally {
      context2.finish();
    }
  }

  protected releaseLocalLock(
    lock: SharedExclusiveLock,
    locking: 'shared' | 'exclusive' | 'none',
    sessionId: string,
  ): void {
    if (locking === 'shared') {
      lock.endShared(sessionId);
    } else if (locking === 'exclusive') {
      lock.endExclusive(sessionId);
    }
  }
  protected makeContext(base: IBaseActionContext): IActionContext {
    return {
      ...base,

      state: () => {
        return base.getService('state') as IPersistenceService;
      },

      time: () => {
        return base.getService('time') as ITimeService;
      },

      scheduling: () => {
        return base.getService('scheduling') as ISchedulingService;
      },
    };
  }
}
