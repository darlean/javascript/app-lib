import * as selock from './selock';
import { IContext } from './logging';
import { formatAllAttributes, replaceArguments } from './util';
import { PerformActionRequestWaitFor, Struct } from '.';
import { AnObject } from './containers';
import { IActionOptions, IActionResult } from './services/performing';
import { IPerformActionReqCL, IPerformActionRespC } from './interfaces/performing';
import { IAcquireLockResult, IGetLockInfoResult } from './services/locking';

export interface IActorCreateOptions {
  id: string[];
  c: IContext;
}

export interface IActorRegistration {
  getRegisterOptions(): IRegisterActorOptions;
}

export interface IRegisterActorOptions {
  name: string;
  creator: (options: IActorCreateOptions) => unknown;
  activator: (instanceInfo: IInstanceInfo, context: IBaseActionContext) => Promise<void>;
  deactivator: (instanceInfo: IInstanceInfo, context: IBaseActionContext) => Promise<void>;
  // Performs an action. The perform is responsible for obtaining the locak
  // read/write lock and for revalidating the status of the instance
  perform: (
    c: IContext,
    instanceInfo: IInstanceInfo,
    action: IPerformActionReqCL<AnObject>,
    context: IBaseActionContext,
  ) => Promise<IPerformActionRespC<AnObject>>;
  onAppStart: (runtime: IRuntime) => Promise<void>;
  onAppStop: (runtime: IRuntime) => Promise<void>;
  multiplicity: 'single' | 'multiple';
  placement: 'local' | 'availability';
  locking: 'exclusive' | 'shared';
  capacity: number;
  // Index within app id that contains the full app id to which an actor instance
  // with a certain id is bound. Can be a negative number to lookup from the right
  // to the left.
  appBindIndex?: number;
  status: 'enabled' | 'disabled';
  dependencies: string[];
}

export type ProcessErrorCode =
  | 'ACTOR_NOT_ACTIVE'
  | 'UNKNOWN_ACTION'
  | 'ACTOR_ACTIVATION_FAILED'
  | 'ACTOR_CREATE_FAILED'
  | 'ACTOR_TYPE_NOT_DEFINED'
  | 'ACTOR_TYPE_DISABLED'
  | 'ACTOR_LOCK_REFUSED'
  | 'ACTOR_BIND_CONFLICT'
  | 'UNEXPECTED_PERFORM_ACTION_ERROR'
  | 'INVALID_REQUEST';

export interface IAttributes {
  [key: string]: unknown;
}

// Note: this class deliberately is not an Error because it can also have nested
// errors, and everytime you create such object, NodeJS will assign the current
// stack trace, which is not what we want.
export class DarleanError {
  public codes: string[];
  public code: string;
  public message: string;
  public attributes?: IAttributes;
  public nested?: DarleanError[];
  public stack?: string;

  constructor(codes: string[], message: string, attributes?: IAttributes, stack?: string, nested?: DarleanError[]) {
    this.message = message ? replaceArguments(message, attributes) : codes.join('__');
    this.codes = codes;
    this.code = codes[codes.length - 1];
    this.attributes = formatAllAttributes(attributes);
    this.stack = stack;
    this.nested = nested;
  }
}

export function ProcessError(
  code: ProcessErrorCode,
  message: string,
  attributes?: IAttributes,
  nested?: DarleanError[],
): DarleanError {
  return new DarleanError(['PROCESS'].concat([code]), message, attributes, undefined, nested);
}

export function isProcessError(error: DarleanError): boolean {
  return error.codes[0] === 'PROCESS';
}

export function ensureDarleanError(e: unknown): DarleanError {
  if (e instanceof DarleanError) {
    return e;
  }

  if (e === undefined) {
    return new DarleanError(['ERROR'], 'Undefined error');
  }

  if (typeof e === 'string') {
    if (e.includes(' ')) {
      return new DarleanError(['ERROR'], e);
    }
    return new DarleanError([e], e);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const ex = e as any;
  const message = ex.message ?? ('no error message' as string);
  const codes = ex.codes ?? ex.code ? [ex.code] : ex.name ? [ex.name] : ['ERROR'];
  const attributes = ex.attributes;
  const stack = ex.stack ?? '';
  const nested = ex.nested;

  return new DarleanError(codes, message, attributes, stack, nested);
}

export interface IRpcError {
  ids: string[];
  message: string;
  stack?: string;
  nested?: IRpcError[];
}

export function toRpcError(error: DarleanError): IRpcError {
  const result: IRpcError = {
    ids: error.codes,
    message: error.message,
    stack: error.stack,
    nested: error.nested ? error.nested.map((v) => toRpcError(v)) : undefined,
  };
  return result;
}

export function fromRpcError(error: IRpcError): DarleanError {
  const result = new DarleanError(
    error.ids,
    error.message,
    undefined,
    error.stack,
    error.nested ? error.nested.map((v) => fromRpcError(v)) : undefined,
  );
  return result;
}

export interface IInstanceInfo {
  actorInfo: IRegisterActorOptions;
  id: string[];
  instance: unknown;
  state: 'created' | 'active' | 'deactivating' | 'deactive';
  globalLockUntil: number;
  globalLockRefreshTimer?: ITimer;
  localLock: selock.SharedExclusiveLock;
  timers: ITimer[];
}

export interface IAppBinding {
  actorId: string[];
  matching: 'exact' | 'prefix';
  appIds: string[];
  break: boolean;
}

export interface ISupportedActor {
  name: string;
  multiplicity: 'single' | 'multiple';
  availability: number;
  appBindIndex?: number;
  placement: 'local' | 'availability';
  status: 'enabled' | 'disabled';
  dependencies: string[];
}

export interface IAppInfo {
  id: string;
  supportedActors: ISupportedActor[];
}

export interface IRuntime {
  time: ITime;
  acquireLock(
    id: string[],
    ttl: number,
    refresh: boolean,
    options: IActionOptions<AnObject>,
  ): Promise<IAcquireLockResult>;
  releaseLock(id: string[], waitFor: PerformActionRequestWaitFor, options?: IActionOptions<AnObject>): Promise<void>;
  getLockInfo(id: string[], options?: IActionOptions<AnObject>): Promise<IGetLockInfoResult>;
  publishInfo(info: IAppInfo): Promise<void>;
  dispatchAction<Input, Output>(c: IContext, action: IActionOptions<Input>): Promise<Output>;
  dispatchActionRaw<Input, Output>(c: IContext, action: IActionOptions<Input>): Promise<IActionResult<Output>>;
  fireAction<Input>(c: IContext, action: IActionOptions<Input>): void;
}

export interface IScheduleTimerOptions<Input> {
  name: string;
  actionName: string;
  data?: Input;
  interval: number;
  delay?: number;
  maxCount?: number;
}

/**
 * Provides an action executor with information about the current action request and
 * with methods to perform additional functionality like performing other actions,
 * scheduling timers and storing state.
 *
 * Typically, one action context instance is created per action request. The action context instance
 * contains information that is specific to that action request, like the 'call chain' that is used to
 * properly handle reentrancy. As a result, *action contexts should only be used within the code that
 * receives the context. They should not be stored and used again at a later moment.*
 */
export interface IBaseActionContext {
  sessionId: string;
  clusterId: string;
  fullAppId: string;
  hopsRemaining: number;

  /**
   * Performs a certain sub-action on the same or a different actor.
   * @param action The sub-action to be invoked. When `actorType` is not set, the sub-action is performed on the actor type
   * of this context's action. When `actorId` is not set, the id is copied from this context's action.
   *
   * When performing a sub-action via `performAction`, reentrancy information is added to the action request so that the
   * subaction that is being performed can perform actions on the original actor without causing a deadlock.
   *
   * Example of invoking a sub-action:
   * ```
   * const result = await context.performAction<{a: number, b: number}, {sum: number}>({
   *   actorType: 'MyCalculator',
   *   actorId: [],
   *   actionName: 'add',
   *   data: {
   *     a: 3,
   *     b: 4
   *   }
   * });
   * console.log(result.sum);  // Prints 7 when MyCalculator.add is implemented properly
   * ```
   *
   * Performed sub-actions can also receive and return attachments:
   * ```
   * const imageBuffer = await fs.readFile('image.jpg', "binary");
   * const result = await context.performAction<{factor: number, image: IBinaryAttachmentRef}, {upsampled: IBinaryAttachmentRef}>({
   *   actorType: 'MyImageFilter',
   *   actorId: [],
   *   actionName: 'upsample',
   *   data: struct( (attach) => ({
   *     factor: 2.5,
   *     image: attach(imageBuffer);
   *   })
   * });
   * const upsampledBuffer = result._att(result.upsampled).binary();
   * await fs.writeFile('upsampled.jpg', upsampledBuffer);
   * ```
   *
   * It is recommended to type performAction with the `Input` and `Output` types. When an action does not require input
   * and/or return output, use `void`:
   * ```
   * await context.performAction<void, void>({
   *    ...
   * });
   * ```
   *
   * @returns A [[Struct]] with the returned data (or throws an error when something goes wrong).
   */
  performAction<Input extends AnObject | void, Output extends AnObject | void>(
    action: IActionOptions<Input>,
  ): Promise<Output extends AnObject ? Struct<Output> : void>;

  /**
   * Fires a sub-action without waiting for the result.
   *
   * Warning: Because fireAction returns immediately, errors that occur during performing of the sub-action are absorbed
   * and not reported back to the calling code.
   *
   * In other aspects, this method behaves similar to [[performAction]].
   * @param action The action to be fired.
   * @returns Nothing. Even does not throw errors when network, processing or actor errors occur.
   */
  fireAction<Input>(action: IActionOptions<Input>): void;

  /**
   * Schedules a timer that fires once or repeatedly until the timer is explicitly cancelled or the actor
   * is deactivated.
   *
   * @param options The timer options.
   * @returns A [[ITimer]] instance that can be used to explicitly cancel the timer.
   */
  scheduleTimer<Input>(options: IScheduleTimerOptions<Input>): ITimer;

  /**
   * Requests a certain service that can be used to perform useful functionality. Currently supported services are:
   * * `state`: Returns an [[IPersistenceService]] instance that can be used to load or store actor state
   * * `scheduling`: Returns an [[ISchedulingService]] instance that can be used to schedule reminders
   * * `time`: Returns an [[ITimeService]] instance that gives access to the current node time (and in the future
   *   to the cluster time as well) and allows an actor to sleep and to pause very briefly to keep the application
   *   responsive during long-running synchronous operations.
   *
   * See also the convenience methods [[IActionContext.state]], [[IActionContext.time]] and [[IActionContext.scheduling]]
   * that allow easy access to the available services.
   *
   * @param name The name of the service. Currently, `state`, `scheduling` and `time` are supported values.
   * @returns An instance of a IXyzService interface that is specific to this contexts action.
   */
  getService(name: string): unknown;
}

export interface ITimer {
  /**
   * Cancels the timer. The timer will never fire again.
   */
  cancel(): void;

  /**
   * Pauses the firing of the timer. When duration is specified, the timer is automatically resumed after
   * the duration. Invoking pause while another pause is already active, or a resume is scheduled, replaces
   * those actions.
   * @param duration When specified, indicates after how many milliseconds the timer will resume. When not specified,
   * the pause lasts indefinately until [[resume]] is explicitly invoked.
   */
  pause(duration?: number): void;

  /**
   * Resumes a paused timer after delay milliseconds. When delay is not present, the timer is resumed after the
   * configured interval of the timer when set, otherwise after the configured initial delay when set, otherwise
   * immediately. When the timer is not paused, the next timer event will be executed after the delay.
   * @param delay The amount of milliseconds after which the timer is resumed.
   */
  resume(delay?: number): void;
}

export interface ITime {
  // Returns the number of milliseconds since some arbitrary moment. Unlike the machineTime method, the machineTicks
  // method is continuous (increments every millisecond by exactly 1), regardless of changes in system time.
  machineTicks(): number;

  // Returns the current time for this machine (computer, virtual machine, container), expressed in milliseconds
  // since January 1, 1970 00:00:00 UTC. (This behaviour is identical to the builtin Date.now function). The
  // machine time may not be continuous (depending on operating system level changes in system time, machine time
  // may jump or move faster or slower than normal).
  machineTime(): number;

  // Invoke callback once after delay milliseconds, and then repeat it for repeatCount times with the provided
  // interval. When interval is negative, or repeatCount is 0, no repetitions are done. When interval is 0, the
  // setImmediate function is used instead of setTimeout. When delay is not provided, the provided interval is
  // used as initial delay. When repeatCount is not present, the callback is repeated indefinately. The initial
  // callback call as well as the subsequent repetitions can be cancelled by means of the 'cancel' method of
  // the returned ITimer instance.
  repeat(callback: () => unknown, name: string, interval: number, delay?: number, repeatCount?: number): ITimer;

  // Sleeps for the specified amount of ms.
  sleep(ms: number): Promise<void>;

  // Sleeps just enough to give timers and asynchronous operations the chance to proceed. This method should be
  // invoked at regular moments during long-running operations to keep the application responsive.
  noop(): Promise<void>;
}

export const DEFAULT_MAX_HOP_COUNT = 128;

export interface IShutdownInfo {
  status: 'running' | 'shuttingdown';
  actorsBeingUsed: string[];
}

export interface IPeerConfig {
  id: string;
  address: string;
}

/**
 * Interface for accessing settings. Settings can be provided as "."-delimited string,
 * or as an array of strings.
 */
export interface ISettings {
  /**
   * Get a setting. Trows an exception when the setting is not
   * present, unless defaultValue is not unassigned, which is then returned.
   */
  get<T>(setting: string | string[], defaultValue?: T): T;

  /**
   * Returns whether the setting exists.
   */
  has(setting: string | string[]): boolean;
}
