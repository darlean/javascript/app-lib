# Darlean JS App

Library for creating JavaScript/TypeScript Darlean apps

## Modules

> Note: Technically, this library consists of only one module, but it is better for the structure of the 
automatically generated documentation to artificially treat separate files as modules. 
When using the types and function of this library from within custom code, they are all simply part of the toplevel 
`@darlean/app-lib` module entry point.

## Functionality

The Darlean app-lib provides the following key functionality:
* [[actorFactory|Creating]] regular and service actors that derive from [[BaseActor]] in module [[actorfactory]].

In addition to that it provides a lot of supportive functionality:
* Shared/exclusive locks with reentrancy and take-over functionality in module [[selock]]
* Serializeable data objects with support for text, binary and struct attachments in module [[structs]]
* Containers that contain and transfer text, binary and struct data over the network in module [[containers]]
* Binary encoding of containers in module [[binaryencoding]]
* Various types in module [[types]]
* Various utility functions (like [[parallel]], [[replaceAll]] and [[wildcardMatch]]) in module [[util]]. 
