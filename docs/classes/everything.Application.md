[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / Application

# Class: Application

[everything](../modules/everything.md).Application

## Implements

- [`IRuntime`](../interfaces/types.IRuntime.md)

## Table of contents

### Constructors

- [constructor](everything.Application.md#constructor)

### Properties

- [address](everything.Application.md#address)
- [cluster](everything.Application.md#cluster)
- [dispatchCache](everything.Application.md#dispatchcache)
- [fullId](everything.Application.md#fullid)
- [gateways](everything.Application.md#gateways)
- [logger](everything.Application.md#logger)
- [network](everything.Application.md#network)
- [outstandingRequests](everything.Application.md#outstandingrequests)
- [runner](everything.Application.md#runner)
- [time](everything.Application.md#time)
- [transport](everything.Application.md#transport)
- [transportSettleDelay](everything.Application.md#transportsettledelay)

### Methods

- [acquireLock](everything.Application.md#acquirelock)
- [dispatchAction](everything.Application.md#dispatchaction)
- [dispatchActionImpl](everything.Application.md#dispatchactionimpl)
- [dispatchActionOnce](everything.Application.md#dispatchactiononce)
- [dispatchActionRaw](everything.Application.md#dispatchactionraw)
- [dispatchActionToReceivers](everything.Application.md#dispatchactiontoreceivers)
- [findGateway](everything.Application.md#findgateway)
- [findReceivers](everything.Application.md#findreceivers)
- [fireAction](everything.Application.md#fireaction)
- [getLockInfo](everything.Application.md#getlockinfo)
- [getRunner](everything.Application.md#getrunner)
- [handleIncomingMessage](everything.Application.md#handleincomingmessage)
- [handlePerformActionRequest](everything.Application.md#handleperformactionrequest)
- [init](everything.Application.md#init)
- [publishInfo](everything.Application.md#publishinfo)
- [releaseLock](everything.Application.md#releaselock)
- [stop](everything.Application.md#stop)

## Constructors

### constructor

• **new Application**(`cluster`, `id`, `address`, `gateways`, `logger?`, `transport?`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `cluster` | `string` |
| `id` | `string` |
| `address` | `string` |
| `gateways` | [`IPeerConfig`](../interfaces/types.IPeerConfig.md)[] |
| `logger?` | [`ILogger`](../interfaces/everything.ILogger.md) |
| `transport?` | `ITransport` |

#### Defined in

[src/application.ts:74](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L74)

## Properties

### address

• `Private` **address**: `string`

#### Defined in

[src/application.ts:67](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L67)

___

### cluster

• **cluster**: `string`

#### Defined in

[src/application.ts:60](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L60)

___

### dispatchCache

• `Private` **dispatchCache**: `Map`<`string`, `string`\>

#### Defined in

[src/application.ts:72](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L72)

___

### fullId

• **fullId**: `string`

#### Defined in

[src/application.ts:61](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L61)

___

### gateways

• `Private` **gateways**: [`IPeerConfig`](../interfaces/types.IPeerConfig.md)[]

#### Defined in

[src/application.ts:68](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L68)

___

### logger

• `Private` **logger**: [`ILogger`](../interfaces/everything.ILogger.md)

#### Defined in

[src/application.ts:64](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L64)

___

### network

• `Private` **network**: `Network`

#### Defined in

[src/application.ts:65](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L65)

___

### outstandingRequests

• `Private` **outstandingRequests**: `Map`<`string`, `IOutstandingRequest`\>

#### Defined in

[src/application.ts:69](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L69)

___

### runner

• `Private` **runner**: [`ActorRunner`](everything.ActorRunner.md)

#### Defined in

[src/application.ts:70](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L70)

___

### time

• **time**: `ActorRunnerTime`

#### Implementation of

[IRuntime](../interfaces/types.IRuntime.md).[time](../interfaces/types.IRuntime.md#time)

#### Defined in

[src/application.ts:62](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L62)

___

### transport

• `Private` **transport**: `ITransport`

#### Defined in

[src/application.ts:66](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L66)

___

### transportSettleDelay

• `Private` **transportSettleDelay**: `number`

#### Defined in

[src/application.ts:71](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L71)

## Methods

### acquireLock

▸ **acquireLock**(`id`, `ttl`, `refresh`, `options`): `Promise`<`IAcquireLockResult`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `id` | `string`[] |
| `ttl` | `number` |
| `refresh` | `boolean` |
| `options` | [`IActionOptions`](../interfaces/everything.IActionOptions.md)<`Object`\> |

#### Returns

`Promise`<`IAcquireLockResult`\>

#### Implementation of

[IRuntime](../interfaces/types.IRuntime.md).[acquireLock](../interfaces/types.IRuntime.md#acquirelock)

#### Defined in

[src/application.ts:166](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L166)

___

### dispatchAction

▸ **dispatchAction**<`Input`, `Output`\>(`action`): `Promise`<`Output`\>

#### Type parameters

| Name |
| :------ |
| `Input` |
| `Output` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `action` | [`IActionOptions`](../interfaces/everything.IActionOptions.md)<`Input`\> |

#### Returns

`Promise`<`Output`\>

#### Implementation of

[IRuntime](../interfaces/types.IRuntime.md).[dispatchAction](../interfaces/types.IRuntime.md#dispatchaction)

#### Defined in

[src/application.ts:247](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L247)

___

### dispatchActionImpl

▸ `Protected` **dispatchActionImpl**<`Input`, `Output`\>(`action`): `Promise`<[`IActionResult`](../interfaces/everything.IActionResult.md)<`Output`\>\>

#### Type parameters

| Name |
| :------ |
| `Input` |
| `Output` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `action` | [`IActionOptions`](../interfaces/everything.IActionOptions.md)<`Input`\> |

#### Returns

`Promise`<[`IActionResult`](../interfaces/everything.IActionResult.md)<`Output`\>\>

#### Defined in

[src/application.ts:346](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L346)

___

### dispatchActionOnce

▸ `Protected` **dispatchActionOnce**<`Input`, `Output`\>(`request`, `receiver`, `validUntil`): `Promise`<[`IPerformActionRespC`](../modules/everything.interface_performing.md#iperformactionrespc)<`Output`\>\>

#### Type parameters

| Name |
| :------ |
| `Input` |
| `Output` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `request` | [`IPerformActionReqC`](../modules/everything.interface_performing.md#iperformactionreqc)<`Input`\> |
| `receiver` | `string` |
| `validUntil` | `number` |

#### Returns

`Promise`<[`IPerformActionRespC`](../modules/everything.interface_performing.md#iperformactionrespc)<`Output`\>\>

#### Defined in

[src/application.ts:509](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L509)

___

### dispatchActionRaw

▸ **dispatchActionRaw**<`Input`, `Output`\>(`action`): `Promise`<[`IActionResult`](../interfaces/everything.IActionResult.md)<`Output`\>\>

#### Type parameters

| Name |
| :------ |
| `Input` |
| `Output` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `action` | [`IActionOptions`](../interfaces/everything.IActionOptions.md)<`Input`\> |

#### Returns

`Promise`<[`IActionResult`](../interfaces/everything.IActionResult.md)<`Output`\>\>

#### Implementation of

[IRuntime](../interfaces/types.IRuntime.md).[dispatchActionRaw](../interfaces/types.IRuntime.md#dispatchactionraw)

#### Defined in

[src/application.ts:261](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L261)

___

### dispatchActionToReceivers

▸ `Protected` **dispatchActionToReceivers**<`Input`, `Output`\>(`request`, `fixedReceivers`, `validUntil`): `Promise`<[`IPerformActionRespC`](../modules/everything.interface_performing.md#iperformactionrespc)<`Output`\>\>

#### Type parameters

| Name |
| :------ |
| `Input` |
| `Output` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `request` | [`IPerformActionReqC`](../modules/everything.interface_performing.md#iperformactionreqc)<`Input`\> |
| `fixedReceivers` | `undefined` \| `string`[] |
| `validUntil` | `number` |

#### Returns

`Promise`<[`IPerformActionRespC`](../modules/everything.interface_performing.md#iperformactionrespc)<`Output`\>\>

#### Defined in

[src/application.ts:439](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L439)

___

### findGateway

▸ `Protected` **findGateway**(): `string`

#### Returns

`string`

#### Defined in

[src/application.ts:647](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L647)

___

### findReceivers

▸ `Protected` **findReceivers**(`actorType?`, `actorId?`, `forbiddenApps?`): `Promise`<`string`[]\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `actorType?` | `string` |
| `actorId?` | `string`[] |
| `forbiddenApps?` | `string`[] |

#### Returns

`Promise`<`string`[]\>

#### Defined in

[src/application.ts:281](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L281)

___

### fireAction

▸ **fireAction**<`Input`\>(`action`): `void`

#### Type parameters

| Name |
| :------ |
| `Input` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `action` | [`IActionOptions`](../interfaces/everything.IActionOptions.md)<`Input`\> |

#### Returns

`void`

#### Implementation of

[IRuntime](../interfaces/types.IRuntime.md).[fireAction](../interfaces/types.IRuntime.md#fireaction)

#### Defined in

[src/application.ts:271](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L271)

___

### getLockInfo

▸ **getLockInfo**(`id`): `Promise`<`IGetLockInfoResult`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `id` | `string`[] |

#### Returns

`Promise`<`IGetLockInfoResult`\>

#### Implementation of

[IRuntime](../interfaces/types.IRuntime.md).[getLockInfo](../interfaces/types.IRuntime.md#getlockinfo)

#### Defined in

[src/application.ts:223](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L223)

___

### getRunner

▸ **getRunner**(): [`ActorRunner`](everything.ActorRunner.md)

#### Returns

[`ActorRunner`](everything.ActorRunner.md)

#### Defined in

[src/application.ts:162](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L162)

___

### handleIncomingMessage

▸ `Protected` **handleIncomingMessage**(`message`): `Promise`<`void`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `message` | `INetworkMessage` |

#### Returns

`Promise`<`void`\>

#### Defined in

[src/application.ts:562](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L562)

___

### handlePerformActionRequest

▸ `Protected` **handlePerformActionRequest**(`requestContainer`): `Promise`<`void`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `requestContainer` | [`IPerformActionReqC`](../modules/everything.interface_performing.md#iperformactionreqc)<`Object`\> |

#### Returns

`Promise`<`void`\>

#### Defined in

[src/application.ts:610](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L610)

___

### init

▸ **init**(): `Promise`<`void`\>

#### Returns

`Promise`<`void`\>

#### Defined in

[src/application.ts:134](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L134)

___

### publishInfo

▸ **publishInfo**(`info`): `Promise`<`void`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `info` | [`IAppInfo`](../interfaces/types.IAppInfo.md) |

#### Returns

`Promise`<`void`\>

#### Implementation of

[IRuntime](../interfaces/types.IRuntime.md).[publishInfo](../interfaces/types.IRuntime.md#publishinfo)

#### Defined in

[src/application.ts:236](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L236)

___

### releaseLock

▸ **releaseLock**(`id`, `waitFor`, `options?`): `Promise`<`void`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `id` | `string`[] |
| `waitFor` | [`PerformActionRequestWaitFor`](../modules/everything.md#performactionrequestwaitfor) |
| `options?` | [`IActionOptions`](../interfaces/everything.IActionOptions.md)<`Object`\> |

#### Returns

`Promise`<`void`\>

#### Implementation of

[IRuntime](../interfaces/types.IRuntime.md).[releaseLock](../interfaces/types.IRuntime.md#releaselock)

#### Defined in

[src/application.ts:189](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L189)

___

### stop

▸ **stop**(): `Promise`<`void`\>

#### Returns

`Promise`<`void`\>

#### Defined in

[src/application.ts:140](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L140)
