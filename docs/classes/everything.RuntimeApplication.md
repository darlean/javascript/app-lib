[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / RuntimeApplication

# Class: RuntimeApplication

[everything](../modules/everything.md).RuntimeApplication

## Table of contents

### Constructors

- [constructor](everything.RuntimeApplication.md#constructor)

### Properties

- [application](everything.RuntimeApplication.md#application)
- [logger](everything.RuntimeApplication.md#logger)
- [runtimeNodes](everything.RuntimeApplication.md#runtimenodes)

### Methods

- [init](everything.RuntimeApplication.md#init)
- [shutdownCluster](everything.RuntimeApplication.md#shutdowncluster)

## Constructors

### constructor

• **new RuntimeApplication**(`logger`, `application`, `runtimeNodes`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `logger` | [`ILogger`](../interfaces/everything.ILogger.md) |
| `application` | [`Application`](everything.Application.md) |
| `runtimeNodes` | `string`[] |

#### Defined in

[src/runtimeapplication.ts:14](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/runtimeapplication.ts#L14)

## Properties

### application

• `Private` **application**: [`Application`](everything.Application.md)

#### Defined in

[src/runtimeapplication.ts:10](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/runtimeapplication.ts#L10)

___

### logger

• `Private` **logger**: [`ILogger`](../interfaces/everything.ILogger.md)

#### Defined in

[src/runtimeapplication.ts:12](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/runtimeapplication.ts#L12)

___

### runtimeNodes

• `Private` **runtimeNodes**: `string`[]

#### Defined in

[src/runtimeapplication.ts:11](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/runtimeapplication.ts#L11)

## Methods

### init

▸ **init**(): `Promise`<`void`\>

#### Returns

`Promise`<`void`\>

#### Defined in

[src/runtimeapplication.ts:20](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/runtimeapplication.ts#L20)

___

### shutdownCluster

▸ **shutdownCluster**(): `Promise`<`void`\>

#### Returns

`Promise`<`void`\>

#### Defined in

[src/runtimeapplication.ts:80](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/runtimeapplication.ts#L80)
