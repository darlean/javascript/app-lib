[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [types](../modules/types.md) / DarleanError

# Class: DarleanError

[types](../modules/types.md).DarleanError

## Table of contents

### Constructors

- [constructor](types.DarleanError.md#constructor)

### Properties

- [attributes](types.DarleanError.md#attributes)
- [code](types.DarleanError.md#code)
- [codes](types.DarleanError.md#codes)
- [message](types.DarleanError.md#message)
- [nested](types.DarleanError.md#nested)
- [stack](types.DarleanError.md#stack)

## Constructors

### constructor

• **new DarleanError**(`codes`, `message`, `attributes?`, `stack?`, `nested?`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `codes` | `string`[] |
| `message` | `string` |
| `attributes?` | [`IAttributes`](../interfaces/types.IAttributes.md) |
| `stack?` | `string` |
| `nested?` | [`DarleanError`](types.DarleanError.md)[] |

#### Defined in

[src/types.ts:71](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L71)

## Properties

### attributes

• `Optional` **attributes**: [`IAttributes`](../interfaces/types.IAttributes.md)

#### Defined in

[src/types.ts:67](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L67)

___

### code

• **code**: `string`

#### Defined in

[src/types.ts:65](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L65)

___

### codes

• **codes**: `string`[]

#### Defined in

[src/types.ts:64](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L64)

___

### message

• **message**: `string`

#### Defined in

[src/types.ts:66](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L66)

___

### nested

• `Optional` **nested**: [`DarleanError`](types.DarleanError.md)[]

#### Defined in

[src/types.ts:68](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L68)

___

### stack

• `Optional` **stack**: `string`

#### Defined in

[src/types.ts:69](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L69)
