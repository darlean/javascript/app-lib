[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [selock](../modules/selock.md) / SharedExclusiveLock

# Class: SharedExclusiveLock

[selock](../modules/selock.md).SharedExclusiveLock

Provides a shared-exclusive lock (aka multi-read-exclusive-write lock) with support for reentrancy.
It provides support for shared locks (multiple callers can have a shared lock at the same time) and
exclusive locks (exclusive locks can only be obtained when there are no shared locks active, and at
most one exclusive lock can be active at any moment, with the exception of reentrant exclusive locks
of which multiple can be active at the same time, but all for the same reentrancy-tree).

In addition to that, it provides and the ability to take over the lock. Once taken over, no more
shared and/or exclusive locks are ever granted, except for reentrant lock requests that originate
from the take-over lock.

## Table of contents

### Constructors

- [constructor](selock.SharedExclusiveLock.md#constructor)

### Properties

- [disabled](selock.SharedExclusiveLock.md#disabled)
- [exclusiveTokens](selock.SharedExclusiveLock.md#exclusivetokens)
- [pendingExclusives](selock.SharedExclusiveLock.md#pendingexclusives)
- [pendingShareds](selock.SharedExclusiveLock.md#pendingshareds)
- [pendingTakeOver](selock.SharedExclusiveLock.md#pendingtakeover)
- [priority](selock.SharedExclusiveLock.md#priority)
- [sharedTokens](selock.SharedExclusiveLock.md#sharedtokens)

### Methods

- [beginExclusive](selock.SharedExclusiveLock.md#beginexclusive)
- [beginShared](selock.SharedExclusiveLock.md#beginshared)
- [endExclusive](selock.SharedExclusiveLock.md#endexclusive)
- [endShared](selock.SharedExclusiveLock.md#endshared)
- [finalize](selock.SharedExclusiveLock.md#finalize)
- [takeOver](selock.SharedExclusiveLock.md#takeover)
- [tryBeginExclusive](selock.SharedExclusiveLock.md#trybeginexclusive)
- [tryProcessStep](selock.SharedExclusiveLock.md#tryprocessstep)

## Constructors

### constructor

• **new SharedExclusiveLock**(`priority`)

Creates a new shared-exclusive lock instance.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `priority` | ``"exclusive"`` \| ``"shared"`` | Indicates whether shared lock requests or exclusive lock requests are handled first. |

#### Defined in

[src/selock.ts:44](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/selock.ts#L44)

## Properties

### disabled

• `Protected` **disabled**: `boolean`

#### Defined in

[src/selock.ts:37](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/selock.ts#L37)

___

### exclusiveTokens

• `Protected` **exclusiveTokens**: `string`[]

#### Defined in

[src/selock.ts:33](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/selock.ts#L33)

___

### pendingExclusives

• `Protected` **pendingExclusives**: `IPendingRequest`[]

#### Defined in

[src/selock.ts:31](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/selock.ts#L31)

___

### pendingShareds

• `Protected` **pendingShareds**: `IPendingRequest`[]

#### Defined in

[src/selock.ts:30](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/selock.ts#L30)

___

### pendingTakeOver

• `Protected` `Optional` **pendingTakeOver**: () => `void`

#### Type declaration

▸ (): `void`

##### Returns

`void`

#### Defined in

[src/selock.ts:32](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/selock.ts#L32)

___

### priority

• `Protected` **priority**: ``"exclusive"`` \| ``"shared"``

#### Defined in

[src/selock.ts:36](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/selock.ts#L36)

___

### sharedTokens

• `Protected` **sharedTokens**: `string`[]

#### Defined in

[src/selock.ts:35](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/selock.ts#L35)

## Methods

### beginExclusive

▸ **beginExclusive**(`token`, `reentrancyTokens?`): `Promise`<`void`\>

Begin an exclusive lock, and wait until it is granted. An exclusive lock is granted
when no shared locks are active and when the lock is not taken over. It is
also granted when an other exclusive lock is active, or when the lock is taken over,
and when at least one of the provided reentrancyTokens matches with the token
of the active exclusive or takeover lock.

Trying to upgrade an existing shareded lock to an exclusive lock is considered an error
as it can lead to deadlock situations when two callers try to to the same trick at the
same moment. So, when an exclusive lock is tried to be obtained while another shared lock
that matches one of the exclusive locks reentrancyTokens is already active, an error is thrown.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `token` | `string` | The token that uniquely identifies this lock request |
| `reentrancyTokens?` | `string`[] | - |

#### Returns

`Promise`<`void`\>

Void when the lock is granted, or throws `'TAKEN_OVER'` when the lock is taken over
while waiting or `'NO_UPGRADE'` when the caller tries to upgrade a shared lock to an exclusive lock.

#### Defined in

[src/selock.ts:103](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/selock.ts#L103)

___

### beginShared

▸ **beginShared**(`token`, `reentrancyTokens?`): `Promise`<`void`\>

Begin a shared lock, and wait until it is granted. A shared lock is granted
when no exclusive lock is active and when the lock is not taken over. It is
also granted when an exclusive lock is active, or when the lock is taken over,
but when at least one of the provided reentrancyTokens matches with the token
of the active exclusive or takeover lock.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `token` | `string` | The token that uniquely identifies this lock request |
| `reentrancyTokens?` | `string`[] | - |

#### Returns

`Promise`<`void`\>

Void when the lock is granted, or throws `'TAKEN_OVER'` when the lock is taken over
while waiting.

#### Defined in

[src/selock.ts:64](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/selock.ts#L64)

___

### endExclusive

▸ **endExclusive**(`token`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `token` | `string` |

#### Returns

`void`

#### Defined in

[src/selock.ts:128](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/selock.ts#L128)

___

### endShared

▸ **endShared**(`token`): `void`

Ends a shared lock. This method must be invoked exactly once for every corresponding call
to [beginShared](selock.SharedExclusiveLock.md#beginshared).

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `token` | `string` | The token as passed before to the corresponding call to [beginShared](selock.SharedExclusiveLock.md#beginshared). |

#### Returns

`void`

#### Defined in

[src/selock.ts:77](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/selock.ts#L77)

___

### finalize

▸ **finalize**(): `Promise`<`void`\>

Rejects all pending locks.

#### Returns

`Promise`<`void`\>

#### Defined in

[src/selock.ts:159](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/selock.ts#L159)

___

### takeOver

▸ **takeOver**(`token`): `Promise`<`void`\>

Takes over the lock. Effecively, it adds a new exclusive lock request to the top of the
internal lock request queue, and changes the lock priority to 'exclusive'. Once taken
over, it cannot be undone. During take over, reentrant locks are still allowed. After the
take over logic is finished, the caller should call [finalize](selock.SharedExclusiveLock.md#finalize) to reject all pending
locks.

#### Parameters

| Name | Type |
| :------ | :------ |
| `token` | `string` |

#### Returns

`Promise`<`void`\>

#### Defined in

[src/selock.ts:145](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/selock.ts#L145)

___

### tryBeginExclusive

▸ **tryBeginExclusive**(`token`, `reentrancyTokens?`): `boolean`

Tries to obtain an exclusive lock, and return immediately when this is not immediately
possible. See [beginExclusive](selock.SharedExclusiveLock.md#beginexclusive).

#### Parameters

| Name | Type |
| :------ | :------ |
| `token` | `string` |
| `reentrancyTokens?` | `string`[] |

#### Returns

`boolean`

True when the exclusive lock was granted, False otherwise. Throws `'TAKEN_OVER'` when
the lock is taken over or `'NO_UPGRADE'` when the caller tries to upgrade a shared lock to an exclusive lock.

#### Defined in

[src/selock.ts:117](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/selock.ts#L117)

___

### tryProcessStep

▸ `Protected` **tryProcessStep**(): `boolean`

#### Returns

`boolean`

#### Defined in

[src/selock.ts:175](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/selock.ts#L175)
