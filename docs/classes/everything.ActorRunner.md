[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / ActorRunner

# Class: ActorRunner

[everything](../modules/everything.md).ActorRunner

## Table of contents

### Constructors

- [constructor](everything.ActorRunner.md#constructor)

### Properties

- [activationLock](everything.ActorRunner.md#activationlock)
- [actorLogger](everything.ActorRunner.md#actorlogger)
- [actors](everything.ActorRunner.md#actors)
- [cleaningup](everything.ActorRunner.md#cleaningup)
- [defaultTtl](everything.ActorRunner.md#defaultttl)
- [id](everything.ActorRunner.md#id)
- [instances](everything.ActorRunner.md#instances)
- [instancesByKind](everything.ActorRunner.md#instancesbykind)
- [pushTimer](everything.ActorRunner.md#pushtimer)
- [runtime](everything.ActorRunner.md#runtime)
- [runtimeLogger](everything.ActorRunner.md#runtimelogger)
- [services](everything.ActorRunner.md#services)
- [stopping](everything.ActorRunner.md#stopping)
- [time](everything.ActorRunner.md#time)

### Methods

- [cleanupActors](everything.ActorRunner.md#cleanupactors)
- [getSupportedActors](everything.ActorRunner.md#getsupportedactors)
- [handleAction](everything.ActorRunner.md#handleaction)
- [obtainActionContext](everything.ActorRunner.md#obtainactioncontext)
- [pushStatus](everything.ActorRunner.md#pushstatus)
- [registerActor](everything.ActorRunner.md#registeractor)
- [registerService](everything.ActorRunner.md#registerservice)
- [release](everything.ActorRunner.md#release)
- [releaseActor](everything.ActorRunner.md#releaseactor)
- [start](everything.ActorRunner.md#start)
- [stop](everything.ActorRunner.md#stop)
- [triggerCleanup](everything.ActorRunner.md#triggercleanup)

## Constructors

### constructor

• **new ActorRunner**(`id`, `runtime`, `time`, `runtimeLogger`, `actorLogger`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `id` | `string` |
| `runtime` | [`IRuntime`](../interfaces/types.IRuntime.md) |
| `time` | [`ITime`](../interfaces/types.ITime.md) |
| `runtimeLogger` | [`ILogger`](../interfaces/everything.ILogger.md) |
| `actorLogger` | [`ILogger`](../interfaces/everything.ILogger.md) |

#### Defined in

[src/actorrunner.ts:38](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L38)

## Properties

### activationLock

• `Protected` **activationLock**: `Map`<`string`, `boolean`\>

#### Defined in

[src/actorrunner.ts:34](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L34)

___

### actorLogger

• `Protected` **actorLogger**: [`ILogger`](../interfaces/everything.ILogger.md)

#### Defined in

[src/actorrunner.ts:32](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L32)

___

### actors

• `Protected` **actors**: `Map`<`string`, [`IRegisterActorOptions`](../interfaces/types.IRegisterActorOptions.md)\>

#### Defined in

[src/actorrunner.ts:24](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L24)

___

### cleaningup

• `Protected` **cleaningup**: `Map`<`string`, `boolean`\>

#### Defined in

[src/actorrunner.ts:36](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L36)

___

### defaultTtl

• `Protected` **defaultTtl**: `number` = `60000`

#### Defined in

[src/actorrunner.ts:33](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L33)

___

### id

• `Protected` **id**: `string`

#### Defined in

[src/actorrunner.ts:23](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L23)

___

### instances

• `Protected` **instances**: `Map`<`string`, [`IInstanceInfo`](../interfaces/types.IInstanceInfo.md)\>

#### Defined in

[src/actorrunner.ts:25](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L25)

___

### instancesByKind

• `Protected` **instancesByKind**: `Map`<`string`, `Map`<`string`, `boolean`\>\>

#### Defined in

[src/actorrunner.ts:26](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L26)

___

### pushTimer

• `Protected` `Optional` **pushTimer**: [`ITimer`](../interfaces/types.ITimer.md)

#### Defined in

[src/actorrunner.ts:29](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L29)

___

### runtime

• `Protected` **runtime**: [`IRuntime`](../interfaces/types.IRuntime.md)

#### Defined in

[src/actorrunner.ts:27](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L27)

___

### runtimeLogger

• `Protected` **runtimeLogger**: [`ILogger`](../interfaces/everything.ILogger.md)

#### Defined in

[src/actorrunner.ts:31](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L31)

___

### services

• `Protected` **services**: `Map`<`string`, (`action`: [`IActionOptions`](../interfaces/everything.IActionOptions.md)<`Object`\>, `context`: [`IBaseActionContext`](../interfaces/types.IBaseActionContext.md)) => `unknown`\>

#### Defined in

[src/actorrunner.ts:30](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L30)

___

### stopping

• `Protected` **stopping**: `boolean` = `false`

#### Defined in

[src/actorrunner.ts:35](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L35)

___

### time

• `Protected` **time**: [`ITime`](../interfaces/types.ITime.md)

#### Defined in

[src/actorrunner.ts:28](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L28)

## Methods

### cleanupActors

▸ `Protected` **cleanupActors**(`actorType`): `Promise`<`void`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `actorType` | `string` |

#### Returns

`Promise`<`void`\>

#### Defined in

[src/actorrunner.ts:531](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L531)

___

### getSupportedActors

▸ **getSupportedActors**(): [`ISupportedActor`](../interfaces/types.ISupportedActor.md)[]

#### Returns

[`ISupportedActor`](../interfaces/types.ISupportedActor.md)[]

#### Defined in

[src/actorrunner.ts:495](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L495)

___

### handleAction

▸ **handleAction**<`Input`, `Output`\>(`actionC`): `Promise`<[`IPerformActionRespC`](../modules/everything.interface_performing.md#iperformactionrespc)<`Output`\>\>

#### Type parameters

| Name |
| :------ |
| `Input` |
| `Output` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `actionC` | [`IPerformActionReqCL`](../modules/everything.interface_performing.md#iperformactionreqcl)<`Input`\> |

#### Returns

`Promise`<[`IPerformActionRespC`](../modules/everything.interface_performing.md#iperformactionrespc)<`Output`\>\>

#### Defined in

[src/actorrunner.ts:124](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L124)

___

### obtainActionContext

▸ `Protected` **obtainActionContext**(`baseaction`, `sessionId`): [`IBaseActionContext`](../interfaces/types.IBaseActionContext.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `baseaction` | [`IPerformActionReq`](../interfaces/everything.interface_performing.IPerformActionReq.md) |
| `sessionId` | `string` |

#### Returns

[`IBaseActionContext`](../interfaces/types.IBaseActionContext.md)

#### Defined in

[src/actorrunner.ts:554](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L554)

___

### pushStatus

▸ `Protected` **pushStatus**(): `Promise`<`void`\>

#### Returns

`Promise`<`void`\>

#### Defined in

[src/actorrunner.ts:648](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L648)

___

### registerActor

▸ **registerActor**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`IActorRegistration`](../interfaces/types.IActorRegistration.md) \| [`IRegisterActorOptions`](../interfaces/types.IRegisterActorOptions.md) |

#### Returns

`void`

#### Defined in

[src/actorrunner.ts:116](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L116)

___

### registerService

▸ **registerService**(`name`, `impl`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `name` | `string` |
| `impl` | (`action`: [`IActionOptions`](../interfaces/everything.IActionOptions.md)<`Object`\>, `context`: [`IBaseActionContext`](../interfaces/types.IBaseActionContext.md)) => `unknown` |

#### Returns

`void`

#### Defined in

[src/actorrunner.ts:52](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L52)

___

### release

▸ **release**(): `Promise`<`void`\>

#### Returns

`Promise`<`void`\>

#### Defined in

[src/actorrunner.ts:359](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L359)

___

### releaseActor

▸ **releaseActor**(`info`): `Promise`<`void`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `info` | [`IInstanceInfo`](../interfaces/types.IInstanceInfo.md) |

#### Returns

`Promise`<`void`\>

#### Defined in

[src/actorrunner.ts:429](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L429)

___

### start

▸ **start**(): `Promise`<`void`\>

#### Returns

`Promise`<`void`\>

#### Defined in

[src/actorrunner.ts:59](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L59)

___

### stop

▸ **stop**(): `Promise`<`void`\>

#### Returns

`Promise`<`void`\>

#### Defined in

[src/actorrunner.ts:76](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L76)

___

### triggerCleanup

▸ `Protected` **triggerCleanup**(`actorType`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `actorType` | `string` |

#### Returns

`void`

#### Defined in

[src/actorrunner.ts:512](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorrunner.ts#L512)
