[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [containers](../modules/containers.md) / EditableContainer

# Class: EditableContainer<Type, Child0Type\>

[containers](../modules/containers.md).EditableContainer

## Type parameters

| Name | Type |
| :------ | :------ |
| `Type` | extends [`ContainerData`](../modules/containers.md#containerdata) |
| `Child0Type` | extends [`ContainerData`](../modules/containers.md#containerdata) |

## Implements

- [`IContainer`](../interfaces/containers.IContainer.md)<`Type`, `Child0Type`\>

## Table of contents

### Constructors

- [constructor](containers.EditableContainer.md#constructor)

### Properties

- [\_\_container](containers.EditableContainer.md#__container)
- [children](containers.EditableContainer.md#children)
- [data](containers.EditableContainer.md#data)
- [kind](containers.EditableContainer.md#kind)

### Methods

- [addChild](containers.EditableContainer.md#addchild)
- [getBuffer](containers.EditableContainer.md#getbuffer)
- [getBufferEncoding](containers.EditableContainer.md#getbufferencoding)
- [getChild](containers.EditableContainer.md#getchild)
- [getChild0](containers.EditableContainer.md#getchild0)
- [getChildCount](containers.EditableContainer.md#getchildcount)
- [getData](containers.EditableContainer.md#getdata)
- [getDataBinary](containers.EditableContainer.md#getdatabinary)
- [getDataStruct](containers.EditableContainer.md#getdatastruct)
- [getDataText](containers.EditableContainer.md#getdatatext)
- [getKind](containers.EditableContainer.md#getkind)
- [tryGetChild0](containers.EditableContainer.md#trygetchild0)

## Constructors

### constructor

• **new EditableContainer**<`Type`, `Child0Type`\>(`data`, `kind?`, `children?`)

#### Type parameters

| Name | Type |
| :------ | :------ |
| `Type` | extends [`ContainerData`](../modules/containers.md#containerdata) |
| `Child0Type` | extends [`ContainerData`](../modules/containers.md#containerdata) |

#### Parameters

| Name | Type |
| :------ | :------ |
| `data` | `undefined` \| `Type` |
| `kind?` | [`ContainerKind`](../modules/containers.md#containerkind) |
| `children?` | [`ContainerLike0`](../modules/containers.md#containerlike0)[] |

#### Defined in

[src/containers.ts:123](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L123)

## Properties

### \_\_container

• **\_\_container**: () => `boolean`

#### Type declaration

▸ (): `boolean`

##### Returns

`boolean`

#### Implementation of

[IContainer](../interfaces/containers.IContainer.md).[__container](../interfaces/containers.IContainer.md#__container)

#### Defined in

[src/containers.ts:121](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L121)

___

### children

• `Protected` `Optional` **children**: [`ContainerLike`](../modules/containers.md#containerlike)<[`ContainerData`](../modules/containers.md#containerdata), [`ContainerData`](../modules/containers.md#containerdata)\>[]

#### Defined in

[src/containers.ts:119](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L119)

___

### data

• `Protected` `Optional` **data**: `Type`

#### Defined in

[src/containers.ts:117](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L117)

___

### kind

• `Protected` **kind**: [`ContainerKind`](../modules/containers.md#containerkind)

#### Defined in

[src/containers.ts:118](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L118)

## Methods

### addChild

▸ **addChild**(`value`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | [`ContainerLike0`](../modules/containers.md#containerlike0) |

#### Returns

`void`

#### Defined in

[src/containers.ts:151](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L151)

___

### getBuffer

▸ **getBuffer**(): `undefined` \| `Buffer`

#### Returns

`undefined` \| `Buffer`

#### Implementation of

[IContainer](../interfaces/containers.IContainer.md).[getBuffer](../interfaces/containers.IContainer.md#getbuffer)

#### Defined in

[src/containers.ts:206](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L206)

___

### getBufferEncoding

▸ **getBufferEncoding**(): [`BufferEncoding`](../modules/containers.md#bufferencoding)

#### Returns

[`BufferEncoding`](../modules/containers.md#bufferencoding)

#### Implementation of

[IContainer](../interfaces/containers.IContainer.md).[getBufferEncoding](../interfaces/containers.IContainer.md#getbufferencoding)

#### Defined in

[src/containers.ts:174](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L174)

___

### getChild

▸ **getChild**<`Type`\>(`idx`): [`IContainer`](../interfaces/containers.IContainer.md)<`Type`, [`ContainerData`](../modules/containers.md#containerdata)\>

#### Type parameters

| Name | Type |
| :------ | :------ |
| `Type` | extends [`ContainerData`](../modules/containers.md#containerdata) |

#### Parameters

| Name | Type |
| :------ | :------ |
| `idx` | `number` |

#### Returns

[`IContainer`](../interfaces/containers.IContainer.md)<`Type`, [`ContainerData`](../modules/containers.md#containerdata)\>

#### Implementation of

[IContainer](../interfaces/containers.IContainer.md).[getChild](../interfaces/containers.IContainer.md#getchild)

#### Defined in

[src/containers.ts:182](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L182)

___

### getChild0

▸ **getChild0**(): [`IContainer`](../interfaces/containers.IContainer.md)<`Child0Type`, [`ContainerData`](../modules/containers.md#containerdata)\>

#### Returns

[`IContainer`](../interfaces/containers.IContainer.md)<`Child0Type`, [`ContainerData`](../modules/containers.md#containerdata)\>

#### Implementation of

[IContainer](../interfaces/containers.IContainer.md).[getChild0](../interfaces/containers.IContainer.md#getchild0)

#### Defined in

[src/containers.ts:189](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L189)

___

### getChildCount

▸ **getChildCount**(): `number`

#### Returns

`number`

#### Implementation of

[IContainer](../interfaces/containers.IContainer.md).[getChildCount](../interfaces/containers.IContainer.md#getchildcount)

#### Defined in

[src/containers.ts:202](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L202)

___

### getData

▸ **getData**(): `undefined` \| `Type`

#### Returns

`undefined` \| `Type`

#### Implementation of

[IContainer](../interfaces/containers.IContainer.md).[getData](../interfaces/containers.IContainer.md#getdata)

#### Defined in

[src/containers.ts:158](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L158)

___

### getDataBinary

▸ **getDataBinary**(): `Buffer`

#### Returns

`Buffer`

#### Implementation of

[IContainer](../interfaces/containers.IContainer.md).[getDataBinary](../interfaces/containers.IContainer.md#getdatabinary)

#### Defined in

[src/containers.ts:166](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L166)

___

### getDataStruct

▸ **getDataStruct**(): `Type`

#### Returns

`Type`

#### Implementation of

[IContainer](../interfaces/containers.IContainer.md).[getDataStruct](../interfaces/containers.IContainer.md#getdatastruct)

#### Defined in

[src/containers.ts:162](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L162)

___

### getDataText

▸ **getDataText**(): `string`

#### Returns

`string`

#### Implementation of

[IContainer](../interfaces/containers.IContainer.md).[getDataText](../interfaces/containers.IContainer.md#getdatatext)

#### Defined in

[src/containers.ts:170](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L170)

___

### getKind

▸ **getKind**(): [`ContainerKind`](../modules/containers.md#containerkind)

#### Returns

[`ContainerKind`](../modules/containers.md#containerkind)

#### Implementation of

[IContainer](../interfaces/containers.IContainer.md).[getKind](../interfaces/containers.IContainer.md#getkind)

#### Defined in

[src/containers.ts:178](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L178)

___

### tryGetChild0

▸ **tryGetChild0**(): `undefined` \| [`IContainer`](../interfaces/containers.IContainer.md)<`Child0Type`, [`ContainerData`](../modules/containers.md#containerdata)\>

#### Returns

`undefined` \| [`IContainer`](../interfaces/containers.IContainer.md)<`Child0Type`, [`ContainerData`](../modules/containers.md#containerdata)\>

#### Implementation of

[IContainer](../interfaces/containers.IContainer.md).[tryGetChild0](../interfaces/containers.IContainer.md#trygetchild0)

#### Defined in

[src/containers.ts:196](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L196)
