[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [binaryencoding](../modules/binaryencoding.md) / ContainerToBuffers

# Class: ContainerToBuffers

[binaryencoding](../modules/binaryencoding.md).ContainerToBuffers

For internal use only. Use the [encodeToBinaryBuffers](../modules/binaryencoding.md#encodetobinarybuffers) function instead.

Converts a container into a series of buffers.

**`internal`**

## Table of contents

### Constructors

- [constructor](binaryencoding.ContainerToBuffers.md#constructor)

### Methods

- [toBuffers](binaryencoding.ContainerToBuffers.md#tobuffers)
- [toBuffersImpl](binaryencoding.ContainerToBuffers.md#tobuffersimpl)

## Constructors

### constructor

• **new ContainerToBuffers**()

## Methods

### toBuffers

▸ **toBuffers**(`container`, `buffers`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `container` | [`IContainer`](../interfaces/containers.IContainer.md)<[`ContainerData`](../modules/containers.md#containerdata), [`ContainerData`](../modules/containers.md#containerdata)\> |
| `buffers` | `Buffer`[] |

#### Returns

`void`

#### Defined in

[src/binaryencoding.ts:102](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L102)

___

### toBuffersImpl

▸ `Protected` **toBuffersImpl**(`container`, `nodes`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `container` | [`IContainer`](../interfaces/containers.IContainer.md)<[`ContainerData`](../modules/containers.md#containerdata), [`ContainerData`](../modules/containers.md#containerdata)\> |
| `nodes` | `Buffer`[] |

#### Returns

`void`

#### Defined in

[src/binaryencoding.ts:106](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L106)
