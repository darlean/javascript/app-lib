[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [structs](../modules/structs.md) / ContainerAttachment

# Class: ContainerAttachment<StructType\>

[structs](../modules/structs.md).ContainerAttachment

Internal class that represents an attachment that is backed by an [IContainer](../interfaces/containers.IContainer.md) instance. Should not be
used directly.

**`internal`**

## Type parameters

| Name | Type |
| :------ | :------ |
| `StructType` | extends [`AnObject`](../modules/containers.md#anobject) |

## Implements

- [`IAttachment`](../interfaces/structs.IAttachment.md)<`StructType`\>

## Table of contents

### Constructors

- [constructor](structs.ContainerAttachment.md#constructor)

### Properties

- [\_\_attachment](structs.ContainerAttachment.md#__attachment)
- [container](structs.ContainerAttachment.md#container)
- [structValue](structs.ContainerAttachment.md#structvalue)

### Methods

- [\_cont](structs.ContainerAttachment.md#_cont)
- [binary](structs.ContainerAttachment.md#binary)
- [getAttachment](structs.ContainerAttachment.md#getattachment)
- [getType](structs.ContainerAttachment.md#gettype)
- [raw](structs.ContainerAttachment.md#raw)
- [struct](structs.ContainerAttachment.md#struct)
- [text](structs.ContainerAttachment.md#text)

## Constructors

### constructor

• **new ContainerAttachment**<`StructType`\>(`container`)

#### Type parameters

| Name | Type |
| :------ | :------ |
| `StructType` | extends `Object` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `container` | [`IContainer`](../interfaces/containers.IContainer.md)<[`ContainerData`](../modules/containers.md#containerdata), [`ContainerData`](../modules/containers.md#containerdata)\> |

#### Defined in

[src/structs.ts:235](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L235)

## Properties

### \_\_attachment

• **\_\_attachment**: () => `boolean`

#### Type declaration

▸ (): `boolean`

##### Returns

`boolean`

#### Implementation of

[IAttachment](../interfaces/structs.IAttachment.md).[__attachment](../interfaces/structs.IAttachment.md#__attachment)

#### Defined in

[src/structs.ts:233](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L233)

___

### container

• `Protected` **container**: [`IContainer0`](../modules/containers.md#icontainer0)

#### Defined in

[src/structs.ts:231](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L231)

___

### structValue

• `Protected` `Optional` **structValue**: [`Struct`](../modules/structs.md#struct)<`StructType`\>

#### Defined in

[src/structs.ts:232](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L232)

## Methods

### \_cont

▸ **_cont**(): [`IContainer0`](../modules/containers.md#icontainer0)

Returns the underlying container that holds this attachments data.

#### Returns

[`IContainer0`](../modules/containers.md#icontainer0)

#### Implementation of

[IAttachment](../interfaces/structs.IAttachment.md).[_cont](../interfaces/structs.IAttachment.md#_cont)

#### Defined in

[src/structs.ts:266](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L266)

___

### binary

▸ **binary**(): `Buffer`

Returns the contents of this attachment as a Buffer. Throws an exception when the attachment
is not a binary attachment.

#### Returns

`Buffer`

#### Implementation of

[IAttachment](../interfaces/structs.IAttachment.md).[binary](../interfaces/structs.IAttachment.md#binary)

#### Defined in

[src/structs.ts:243](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L243)

___

### getAttachment

▸ **getAttachment**<`A`\>(`ref`): [`IAttachment`](../interfaces/structs.IAttachment.md)<`A`\>

#### Type parameters

| Name |
| :------ |
| `A` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `ref` | [`IAttachmentRef`](../interfaces/structs.IAttachmentRef.md) |

#### Returns

[`IAttachment`](../interfaces/structs.IAttachment.md)<`A`\>

#### Defined in

[src/structs.ts:270](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L270)

___

### getType

▸ **getType**(): [`AttachmentType`](../modules/structs.md#attachmenttype)

Returns the type of this attachment.

#### Returns

[`AttachmentType`](../modules/structs.md#attachmenttype)

#### Implementation of

[IAttachment](../interfaces/structs.IAttachment.md).[getType](../interfaces/structs.IAttachment.md#gettype)

#### Defined in

[src/structs.ts:239](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L239)

___

### raw

▸ **raw**(): [`ContainerData`](../modules/containers.md#containerdata)

Returns the contents of this attachment as either a Buffer, a string, an object or undefined.

#### Returns

[`ContainerData`](../modules/containers.md#containerdata)

#### Implementation of

[IAttachment](../interfaces/structs.IAttachment.md).[raw](../interfaces/structs.IAttachment.md#raw)

#### Defined in

[src/structs.ts:262](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L262)

___

### struct

▸ **struct**(): [`Struct`](../modules/structs.md#struct)<`StructType`\>

Returns the contents of this attachment as an object. Throws an exception when the attachment
is not a struct attachment.

#### Returns

[`Struct`](../modules/structs.md#struct)<`StructType`\>

#### Implementation of

[IAttachment](../interfaces/structs.IAttachment.md).[struct](../interfaces/structs.IAttachment.md#struct)

#### Defined in

[src/structs.ts:251](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L251)

___

### text

▸ **text**(): `string`

Returns the contents of this attachment as a string. Throws an exception when the attachment
is not a text attachment.

#### Returns

`string`

#### Implementation of

[IAttachment](../interfaces/structs.IAttachment.md).[text](../interfaces/structs.IAttachment.md#text)

#### Defined in

[src/structs.ts:247](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L247)
