[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [actorfactory](../modules/actorfactory.md) / BaseActor

# Class: BaseActor

[actorfactory](../modules/actorfactory.md).BaseActor

Base class for creating custom actors. BaseActor defines empty [activate](actorfactory.BaseActor.md#activate) and [deactivate](actorfactory.BaseActor.md#deactivate) placeholder methods that can
be overriden in subclasses. These methods are invoked by the framework when a new actor instance becomes active and just
before it becomes inactive, respectively.

```
@actor({name: 'MyActor})
export class MyActor extends BaseActor {
  public async activate (...)
  public async deactivate (...)

  @action({...})
  public async myAction1 (...)

  @action({...})
  public async myAction2 (...)
}
```

Actions are defined by means of decorated methods of the following signature:
```
  @action({...})
  public async myAction(data: Input, context: IActionContext): Promise<Output> {
    // Implementation goes here
  }
```
Where `Input` is the type of input data to the action, and `Output` the type of output data. Both
Input and Output must be object types (primitive values like numers and strings are not allowed).

When an action needs access to the attachments of input data, or when it outputs attachments, it should
use the [Struct](../modules/structs.md#struct) type:
```
  interface IMyType { a: IBinaryAttachmentRef, b: string };
  ...
  @action()
  public async myAction(data: Struct<IMyType>, context: IActionContext): Promise<Struct<IMyType>> {
    const a = data._att(data.a).buffer();
    const b = data.b;
    // Do something with a (a Buffer) and b (a string)
    return struct( (attach) => {
      a: attach(Buffer.from('Moon'))
      b: 'World',
    });
  }
```

Actions, [activate](actorfactory.BaseActor.md#activate) and [deactivate](actorfactory.BaseActor.md#deactivate) can use the provided [context object](../interfaces/actorfactory.IActionContext.md) to invoke
other actions (on the same or a different actor), to load or store state, to set timers or reminders,
et cetera. See [IActionContext](../interfaces/actorfactory.IActionContext.md) for more information.

Actors must be decorated with [@actor](../modules/actorfactory.md#actor) or [@service](../modules/actorfactory.md#service), actions must be decorated
with [@action](../modules/actorfactory.md#action). The `@actor` and `@service` decorators can be customized with
[actor decorator options](../interfaces/actorfactory.IActorDecoration.md), the `@service` decorator can be customized with the
[action decorator options](../interfaces/actorfactory.IActionDecoration.md).

## Implements

- [`IActor`](../interfaces/actorfactory.IActor.md)

## Table of contents

### Constructors

- [constructor](actorfactory.BaseActor.md#constructor)

### Properties

- [id](actorfactory.BaseActor.md#id)
- [logger](actorfactory.BaseActor.md#logger)

### Methods

- [activate](actorfactory.BaseActor.md#activate)
- [deactivate](actorfactory.BaseActor.md#deactivate)

## Constructors

### constructor

• **new BaseActor**(`options`)

Constructor of the actor. It is best practice to inject any dependencies (like configuration settings)
into the constructor by passing a custom options object:
```
interface MyActorCreateOptions {
   configValueA: string;
}
...
constructor(options: IActorCreateOptions & IMyActorCreateOptions) {
  super(options);
  this.configValueA = options.configValueA;
}
```

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`IActorCreateOptions`](../interfaces/types.IActorCreateOptions.md) |

#### Defined in

[src/actorfactory.ts:385](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L385)

## Properties

### id

• **id**: `string`[]

The id of the actor.

#### Defined in

[src/actorfactory.ts:363](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L363)

___

### logger

• **logger**: [`ILogger`](../interfaces/everything.ILogger.md)

The logger of the actor. Can be used to perform logging.

#### Defined in

[src/actorfactory.ts:368](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L368)

## Methods

### activate

▸ **activate**(`_context`): `Promise`<`void`\>

Placeholder for code that is to be executed when the actor instance becomes active.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `_context` | [`IActionContext`](../interfaces/actorfactory.IActionContext.md) | Context that can be used to load state, set timers and reminders, invoke other actions, et cetera. |

#### Returns

`Promise`<`void`\>

#### Implementation of

[IActor](../interfaces/actorfactory.IActor.md).[activate](../interfaces/actorfactory.IActor.md#activate)

#### Defined in

[src/actorfactory.ts:395](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L395)

___

### deactivate

▸ **deactivate**(`_context`): `Promise`<`void`\>

Placeholder for code that is to be executed when the actor instace becomes inactive. A typical
implementation would persist the actor state during deactivate. During deactivate, timers are still received, but
reminders are blocked. Incoming actions that result from actions that originate from within the
deactivate are properly received (reentrancy is allowed), but other incoming actions are ignored during
(and after) deactivate.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `_context` | [`IActionContext`](../interfaces/actorfactory.IActionContext.md) | Context that can be used to store state, invoke other actions, et cetera. |

#### Returns

`Promise`<`void`\>

#### Implementation of

[IActor](../interfaces/actorfactory.IActor.md).[deactivate](../interfaces/actorfactory.IActor.md#deactivate)

#### Defined in

[src/actorfactory.ts:407](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L407)
