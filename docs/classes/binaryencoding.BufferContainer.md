[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [binaryencoding](../modules/binaryencoding.md) / BufferContainer

# Class: BufferContainer<Type, Child0Type\>

[binaryencoding](../modules/binaryencoding.md).BufferContainer

For internal use only. Use the [decodeFromBinaryBuffer](../modules/binaryencoding.md#decodefrombinarybuffer) function instead.

Creates a container instance out of a binary buffer.

**`internal`**

## Type parameters

| Name | Type |
| :------ | :------ |
| `Type` | extends [`ContainerData`](../modules/containers.md#containerdata) |
| `Child0Type` | extends [`ContainerData`](../modules/containers.md#containerdata) |

## Implements

- [`IContainer`](../interfaces/containers.IContainer.md)<`Type`, `Child0Type`\>

## Table of contents

### Constructors

- [constructor](binaryencoding.BufferContainer.md#constructor)

### Properties

- [\_\_container](binaryencoding.BufferContainer.md#__container)
- [buffer](binaryencoding.BufferContainer.md#buffer)
- [byteOffset](binaryencoding.BufferContainer.md#byteoffset)
- [data](binaryencoding.BufferContainer.md#data)
- [dataKind](binaryencoding.BufferContainer.md#datakind)
- [dataSize](binaryencoding.BufferContainer.md#datasize)
- [hasData](binaryencoding.BufferContainer.md#hasdata)
- [subs](binaryencoding.BufferContainer.md#subs)
- [totalSize](binaryencoding.BufferContainer.md#totalsize)

### Methods

- [ensureSubs](binaryencoding.BufferContainer.md#ensuresubs)
- [getBuffer](binaryencoding.BufferContainer.md#getbuffer)
- [getBufferEncoding](binaryencoding.BufferContainer.md#getbufferencoding)
- [getChild](binaryencoding.BufferContainer.md#getchild)
- [getChild0](binaryencoding.BufferContainer.md#getchild0)
- [getChildCount](binaryencoding.BufferContainer.md#getchildcount)
- [getData](binaryencoding.BufferContainer.md#getdata)
- [getDataBinary](binaryencoding.BufferContainer.md#getdatabinary)
- [getDataStruct](binaryencoding.BufferContainer.md#getdatastruct)
- [getDataText](binaryencoding.BufferContainer.md#getdatatext)
- [getKind](binaryencoding.BufferContainer.md#getkind)
- [tryGetChild0](binaryencoding.BufferContainer.md#trygetchild0)

## Constructors

### constructor

• **new BufferContainer**<`Type`, `Child0Type`\>(`buffer`, `byteOffset`)

#### Type parameters

| Name | Type |
| :------ | :------ |
| `Type` | extends [`ContainerData`](../modules/containers.md#containerdata) |
| `Child0Type` | extends [`ContainerData`](../modules/containers.md#containerdata) |

#### Parameters

| Name | Type |
| :------ | :------ |
| `buffer` | `Buffer` |
| `byteOffset` | `number` |

#### Defined in

[src/binaryencoding.ts:172](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L172)

## Properties

### \_\_container

• **\_\_container**: () => `boolean`

#### Type declaration

▸ (): `boolean`

##### Returns

`boolean`

#### Implementation of

[IContainer](../interfaces/containers.IContainer.md).[__container](../interfaces/containers.IContainer.md#__container)

#### Defined in

[src/binaryencoding.ts:170](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L170)

___

### buffer

• `Protected` **buffer**: `Buffer`

#### Defined in

[src/binaryencoding.ts:161](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L161)

___

### byteOffset

• `Protected` **byteOffset**: `number`

#### Defined in

[src/binaryencoding.ts:162](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L162)

___

### data

• `Protected` `Optional` **data**: [`ContainerData`](../modules/containers.md#containerdata)

#### Defined in

[src/binaryencoding.ts:167](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L167)

___

### dataKind

• `Protected` **dataKind**: [`ContainerKind`](../modules/containers.md#containerkind)

#### Defined in

[src/binaryencoding.ts:165](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L165)

___

### dataSize

• `Protected` **dataSize**: `number`

#### Defined in

[src/binaryencoding.ts:164](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L164)

___

### hasData

• `Protected` **hasData**: `boolean` = `false`

#### Defined in

[src/binaryencoding.ts:166](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L166)

___

### subs

• `Protected` `Optional` **subs**: [`IContainer`](../interfaces/containers.IContainer.md)<[`ContainerData`](../modules/containers.md#containerdata), [`ContainerData`](../modules/containers.md#containerdata)\>[]

#### Defined in

[src/binaryencoding.ts:168](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L168)

___

### totalSize

• `Protected` **totalSize**: `number`

#### Defined in

[src/binaryencoding.ts:163](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L163)

## Methods

### ensureSubs

▸ `Protected` **ensureSubs**(): [`IContainer`](../interfaces/containers.IContainer.md)<[`ContainerData`](../modules/containers.md#containerdata), [`ContainerData`](../modules/containers.md#containerdata)\>[]

#### Returns

[`IContainer`](../interfaces/containers.IContainer.md)<[`ContainerData`](../modules/containers.md#containerdata), [`ContainerData`](../modules/containers.md#containerdata)\>[]

#### Defined in

[src/binaryencoding.ts:266](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L266)

___

### getBuffer

▸ **getBuffer**(): `undefined` \| `Buffer`

#### Returns

`undefined` \| `Buffer`

#### Implementation of

[IContainer](../interfaces/containers.IContainer.md).[getBuffer](../interfaces/containers.IContainer.md#getbuffer)

#### Defined in

[src/binaryencoding.ts:231](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L231)

___

### getBufferEncoding

▸ **getBufferEncoding**(): [`BufferEncoding`](../modules/containers.md#bufferencoding)

#### Returns

[`BufferEncoding`](../modules/containers.md#bufferencoding)

#### Implementation of

[IContainer](../interfaces/containers.IContainer.md).[getBufferEncoding](../interfaces/containers.IContainer.md#getbufferencoding)

#### Defined in

[src/binaryencoding.ts:227](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L227)

___

### getChild

▸ **getChild**<`Type`\>(`idx`): [`IContainer`](../interfaces/containers.IContainer.md)<`Type`, [`ContainerData`](../modules/containers.md#containerdata)\>

#### Type parameters

| Name |
| :------ |
| `Type` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `idx` | `number` |

#### Returns

[`IContainer`](../interfaces/containers.IContainer.md)<`Type`, [`ContainerData`](../modules/containers.md#containerdata)\>

#### Implementation of

[IContainer](../interfaces/containers.IContainer.md).[getChild](../interfaces/containers.IContainer.md#getchild)

#### Defined in

[src/binaryencoding.ts:219](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L219)

___

### getChild0

▸ **getChild0**(): [`IContainer`](../interfaces/containers.IContainer.md)<`Child0Type`, [`ContainerData`](../modules/containers.md#containerdata)\>

#### Returns

[`IContainer`](../interfaces/containers.IContainer.md)<`Child0Type`, [`ContainerData`](../modules/containers.md#containerdata)\>

#### Implementation of

[IContainer](../interfaces/containers.IContainer.md).[getChild0](../interfaces/containers.IContainer.md#getchild0)

#### Defined in

[src/binaryencoding.ts:211](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L211)

___

### getChildCount

▸ **getChildCount**(): `number`

#### Returns

`number`

#### Implementation of

[IContainer](../interfaces/containers.IContainer.md).[getChildCount](../interfaces/containers.IContainer.md#getchildcount)

#### Defined in

[src/binaryencoding.ts:223](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L223)

___

### getData

▸ **getData**(): `undefined` \| `Type`

#### Returns

`undefined` \| `Type`

#### Implementation of

[IContainer](../interfaces/containers.IContainer.md).[getData](../interfaces/containers.IContainer.md#getdata)

#### Defined in

[src/binaryencoding.ts:235](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L235)

___

### getDataBinary

▸ **getDataBinary**(): `Buffer`

#### Returns

`Buffer`

#### Implementation of

[IContainer](../interfaces/containers.IContainer.md).[getDataBinary](../interfaces/containers.IContainer.md#getdatabinary)

#### Defined in

[src/binaryencoding.ts:193](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L193)

___

### getDataStruct

▸ **getDataStruct**(): `Type`

#### Returns

`Type`

#### Implementation of

[IContainer](../interfaces/containers.IContainer.md).[getDataStruct](../interfaces/containers.IContainer.md#getdatastruct)

#### Defined in

[src/binaryencoding.ts:189](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L189)

___

### getDataText

▸ **getDataText**(): `string`

#### Returns

`string`

#### Implementation of

[IContainer](../interfaces/containers.IContainer.md).[getDataText](../interfaces/containers.IContainer.md#getdatatext)

#### Defined in

[src/binaryencoding.ts:200](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L200)

___

### getKind

▸ **getKind**(): [`ContainerKind`](../modules/containers.md#containerkind)

#### Returns

[`ContainerKind`](../modules/containers.md#containerkind)

#### Implementation of

[IContainer](../interfaces/containers.IContainer.md).[getKind](../interfaces/containers.IContainer.md#getkind)

#### Defined in

[src/binaryencoding.ts:207](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L207)

___

### tryGetChild0

▸ **tryGetChild0**(): `undefined` \| [`IContainer`](../interfaces/containers.IContainer.md)<`Child0Type`, [`ContainerData`](../modules/containers.md#containerdata)\>

#### Returns

`undefined` \| [`IContainer`](../interfaces/containers.IContainer.md)<`Child0Type`, [`ContainerData`](../modules/containers.md#containerdata)\>

#### Implementation of

[IContainer](../interfaces/containers.IContainer.md).[tryGetChild0](../interfaces/containers.IContainer.md#trygetchild0)

#### Defined in

[src/binaryencoding.ts:215](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L215)
