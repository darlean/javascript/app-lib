[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [actorfactory](../modules/actorfactory.md) / ActorFactory

# Class: ActorFactory

[actorfactory](../modules/actorfactory.md).ActorFactory

## Implements

- [`IActorRegistration`](../interfaces/types.IActorRegistration.md)

## Table of contents

### Constructors

- [constructor](actorfactory.ActorFactory.md#constructor)

### Properties

- [actions](actorfactory.ActorFactory.md#actions)
- [creator](actorfactory.ActorFactory.md#creator)
- [logger](actorfactory.ActorFactory.md#logger)
- [options](actorfactory.ActorFactory.md#options)

### Methods

- [activate](actorfactory.ActorFactory.md#activate)
- [deactivate](actorfactory.ActorFactory.md#deactivate)
- [getRegisterOptions](actorfactory.ActorFactory.md#getregisteroptions)
- [makeContext](actorfactory.ActorFactory.md#makecontext)
- [perform](actorfactory.ActorFactory.md#perform)

## Constructors

### constructor

• **new ActorFactory**(`logger`, `name`, `type`, `capacity`, `dependencies`, `creator`, `onAppStart?`, `onAppStop?`, `appBindIndex?`, `locking?`, `actions?`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `logger` | [`ILogger`](../interfaces/everything.ILogger.md) |
| `name` | `string` |
| `type` | ``"actor"`` \| ``"service"`` |
| `capacity` | `number` |
| `dependencies` | `string`[] |
| `creator` | (`options`: [`IActorCreateOptions`](../interfaces/types.IActorCreateOptions.md)) => `unknown` |
| `onAppStart?` | (`runtime`: [`IRuntime`](../interfaces/types.IRuntime.md)) => `Promise`<`void`\> |
| `onAppStop?` | (`runtime`: [`IRuntime`](../interfaces/types.IRuntime.md)) => `Promise`<`void`\> |
| `appBindIndex?` | `number` |
| `locking?` | ``"exclusive"`` \| ``"shared"`` |
| `actions?` | `Object` |

#### Defined in

[src/actorfactory.ts:509](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L509)

## Properties

### actions

• `Protected` **actions**: `Map`<`string`, `IActionDefinition`\>

#### Defined in

[src/actorfactory.ts:507](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L507)

___

### creator

• `Protected` **creator**: (`options`: [`IActorCreateOptions`](../interfaces/types.IActorCreateOptions.md)) => `unknown`

#### Type declaration

▸ (`options`): `unknown`

##### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`IActorCreateOptions`](../interfaces/types.IActorCreateOptions.md) |

##### Returns

`unknown`

#### Defined in

[src/actorfactory.ts:504](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L504)

___

### logger

• `Protected` **logger**: [`ILogger`](../interfaces/everything.ILogger.md)

#### Defined in

[src/actorfactory.ts:506](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L506)

___

### options

• `Protected` **options**: [`IRegisterActorOptions`](../interfaces/types.IRegisterActorOptions.md)

#### Defined in

[src/actorfactory.ts:505](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L505)

## Methods

### activate

▸ `Protected` **activate**(`instanceInfo`, `context`): `Promise`<`void`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `instanceInfo` | [`IInstanceInfo`](../interfaces/types.IInstanceInfo.md) |
| `context` | [`IBaseActionContext`](../interfaces/types.IBaseActionContext.md) |

#### Returns

`Promise`<`void`\>

#### Defined in

[src/actorfactory.ts:570](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L570)

___

### deactivate

▸ `Protected` **deactivate**(`instanceInfo`, `context`): `Promise`<`void`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `instanceInfo` | [`IInstanceInfo`](../interfaces/types.IInstanceInfo.md) |
| `context` | [`IBaseActionContext`](../interfaces/types.IBaseActionContext.md) |

#### Returns

`Promise`<`void`\>

#### Defined in

[src/actorfactory.ts:578](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L578)

___

### getRegisterOptions

▸ **getRegisterOptions**(): [`IRegisterActorOptions`](../interfaces/types.IRegisterActorOptions.md)

#### Returns

[`IRegisterActorOptions`](../interfaces/types.IRegisterActorOptions.md)

#### Implementation of

[IActorRegistration](../interfaces/types.IActorRegistration.md).[getRegisterOptions](../interfaces/types.IActorRegistration.md#getregisteroptions)

#### Defined in

[src/actorfactory.ts:566](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L566)

___

### makeContext

▸ `Protected` **makeContext**(`base`): [`IActionContext`](../interfaces/actorfactory.IActionContext.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `base` | [`IBaseActionContext`](../interfaces/types.IBaseActionContext.md) |

#### Returns

[`IActionContext`](../interfaces/actorfactory.IActionContext.md)

#### Defined in

[src/actorfactory.ts:697](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L697)

___

### perform

▸ `Protected` **perform**(`instanceInfo`, `actionContainer`, `context`): `Promise`<[`IPerformActionRespC`](../modules/everything.interface_performing.md#iperformactionrespc)<`Object`\>\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `instanceInfo` | [`IInstanceInfo`](../interfaces/types.IInstanceInfo.md) |
| `actionContainer` | [`IPerformActionReqCL`](../modules/everything.interface_performing.md#iperformactionreqcl)<`Object`\> |
| `context` | [`IBaseActionContext`](../interfaces/types.IBaseActionContext.md) |

#### Returns

`Promise`<[`IPerformActionRespC`](../modules/everything.interface_performing.md#iperformactionrespc)<`Object`\>\>

#### Defined in

[src/actorfactory.ts:586](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L586)
