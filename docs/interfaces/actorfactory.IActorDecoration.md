[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [actorfactory](../modules/actorfactory.md) / IActorDecoration

# Interface: IActorDecoration

[actorfactory](../modules/actorfactory.md).IActorDecoration

Decoration options for regular and service actors.

## Table of contents

### Properties

- [appBindIndex](actorfactory.IActorDecoration.md#appbindindex)
- [dependencies](actorfactory.IActorDecoration.md#dependencies)
- [inheritDefaultDependencies](actorfactory.IActorDecoration.md#inheritdefaultdependencies)
- [locking](actorfactory.IActorDecoration.md#locking)
- [name](actorfactory.IActorDecoration.md#name)

### Methods

- [onAppStart](actorfactory.IActorDecoration.md#onappstart)
- [onAppStop](actorfactory.IActorDecoration.md#onappstop)

## Properties

### appBindIndex

• `Optional` **appBindIndex**: `number`

Optional index that indocates which field of the actor id contains the name of the node
on which it should be running.

When present, the actor is forced to run exactly on the node specified by the corresponding
id field. The actor will never run on another node, even not when the specified node is not available.

When the appBindIndex is negative, it is counted from the end of the actor id parts (so, a
value of -1 means the rightmost id part, and so on).

By default, appBindIndex is not set, which allows dynamic actor placement (which usually is a good
thing considering it is a conscious choice to use a *virtual* actor framework instead of a regular
actor framework).

#### Defined in

[src/actorfactory.ts:178](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L178)

___

### dependencies

• `Optional` **dependencies**: `string`[]

An optional list of actor type names that this actor needs to be available for proper
operation (incuding deactivation). When the cluster is being shut down, the framework
tries to repect these dependencies so that all actors can be disabled properly.

#### Defined in

[src/actorfactory.ts:155](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L155)

___

### inheritDefaultDependencies

• `Optional` **inheritDefaultDependencies**: `boolean`

When True, a preconfigured list of very basic dependencies is automatically added to the
actors list of dependencies. This includes the Darlean runtime and persistence services
so that actors can always properly store their state upon deactivation.

#### Defined in

[src/actorfactory.ts:162](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L162)

___

### locking

• `Optional` **locking**: ``"exclusive"`` \| ``"shared"``

Default locking mode for the actions of this actor. When not specified, regular actors
(when `@actor` is used as decorator) have default locking mode `'exclusive'`, whereas
service actors (with `@service` as decorator) have `'shared'` locking by default.

#### Defined in

[src/actorfactory.ts:148](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L148)

___

### name

• `Optional` **name**: `string`

Name of the actor (aka the actorType) by which other actors can reference the actor type.

#### Defined in

[src/actorfactory.ts:141](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L141)

## Methods

### onAppStart

▸ `Optional` **onAppStart**(`runtime`): `Promise`<`void`\>

Optional callback that is invoked when the Darlean application is started. See
[IBaseActorFactoryOptions.onAppStart](actorfactory.IBaseActorFactoryOptions.md#onappstart) for more info and caveats.

#### Parameters

| Name | Type |
| :------ | :------ |
| `runtime` | [`IRuntime`](types.IRuntime.md) |

#### Returns

`Promise`<`void`\>

#### Defined in

[src/actorfactory.ts:184](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L184)

___

### onAppStop

▸ `Optional` **onAppStop**(`runtime`): `Promise`<`void`\>

Optional callback that is invoked when the Darlean application is stopping. See
[IBaseActorFactoryOptions.onAppStop](actorfactory.IBaseActorFactoryOptions.md#onappstop) for more info and caveats.

#### Parameters

| Name | Type |
| :------ | :------ |
| `runtime` | [`IRuntime`](types.IRuntime.md) |

#### Returns

`Promise`<`void`\>

#### Defined in

[src/actorfactory.ts:190](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L190)
