[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / [interface\_locking](../modules/everything.interface_locking.md) / IGetLockInfoRequest

# Interface: IGetLockInfoRequest

[everything](../modules/everything.md).[interface_locking](../modules/everything.interface_locking.md).IGetLockInfoRequest

## Table of contents

### Properties

- [id](everything.interface_locking.IGetLockInfoRequest.md#id)

## Properties

### id

• **id**: `string`[]

#### Defined in

[src/interfaces/locking.ts:31](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/locking.ts#L31)
