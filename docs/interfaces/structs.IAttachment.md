[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [structs](../modules/structs.md) / IAttachment

# Interface: IAttachment<StructType\>

[structs](../modules/structs.md).IAttachment

Represents an attachment to a struct. Attachments can be binary, text, structured or empty. Struct attachments
can have sub-attachments themselves, thus forming a tree-like hierarchy of attachments.

Attachments are [IContainerizeable](containers.IContainerizeable.md), which means that their underlying container can be obtained by means
of the [_cont](structs.IAttachment.md#_cont) methods, and that they can be passed to functions that accept an [IContainerizeable](containers.IContainerizeable.md), like
the [asContainer](../modules/containers.md#ascontainer) function.

Attachment instances can be created by means of [IStruct._att](structs.IStruct.md#_att).

## Type parameters

| Name |
| :------ |
| `StructType` |

## Hierarchy

- [`IContainerizeable0`](../modules/containers.md#icontainerizeable0)

  ↳ **`IAttachment`**

## Implemented by

- [`ContainerAttachment`](../classes/structs.ContainerAttachment.md)

## Table of contents

### Methods

- [\_\_attachment](structs.IAttachment.md#__attachment)
- [\_cont](structs.IAttachment.md#_cont)
- [binary](structs.IAttachment.md#binary)
- [getType](structs.IAttachment.md#gettype)
- [raw](structs.IAttachment.md#raw)
- [struct](structs.IAttachment.md#struct)
- [text](structs.IAttachment.md#text)

## Methods

### \_\_attachment

▸ **__attachment**(): `boolean`

Function that indicates that an object that implements this interface is an IAttachment.

This indicator is modeled as a function so that it will not be included in generated JSON (functions are
ignored by JSON.stringify).

#### Returns

`boolean`

#### Defined in

[src/structs.ts:172](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L172)

___

### \_cont

▸ **_cont**(): [`IContainer0`](../modules/containers.md#icontainer0)

Returns the underlying container that holds this attachments data.

#### Returns

[`IContainer0`](../modules/containers.md#icontainer0)

#### Overrides

IContainerizeable0.\_cont

#### Defined in

[src/structs.ts:177](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L177)

___

### binary

▸ **binary**(): `Buffer`

Returns the contents of this attachment as a Buffer. Throws an exception when the attachment
is not a binary attachment.

#### Returns

`Buffer`

#### Defined in

[src/structs.ts:188](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L188)

___

### getType

▸ **getType**(): [`AttachmentType`](../modules/structs.md#attachmenttype)

Returns the type of this attachment.

#### Returns

[`AttachmentType`](../modules/structs.md#attachmenttype)

#### Defined in

[src/structs.ts:182](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L182)

___

### raw

▸ **raw**(): [`ContainerData`](../modules/containers.md#containerdata)

Returns the contents of this attachment as either a Buffer, a string, an object or undefined.

#### Returns

[`ContainerData`](../modules/containers.md#containerdata)

#### Defined in

[src/structs.ts:205](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L205)

___

### struct

▸ **struct**(): [`Struct`](../modules/structs.md#struct)<`StructType`\>

Returns the contents of this attachment as an object. Throws an exception when the attachment
is not a struct attachment.

#### Returns

[`Struct`](../modules/structs.md#struct)<`StructType`\>

#### Defined in

[src/structs.ts:200](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L200)

___

### text

▸ **text**(): `string`

Returns the contents of this attachment as a string. Throws an exception when the attachment
is not a text attachment.

#### Returns

`string`

#### Defined in

[src/structs.ts:194](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L194)
