[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [types](../modules/types.md) / IRegisterActorOptions

# Interface: IRegisterActorOptions

[types](../modules/types.md).IRegisterActorOptions

## Table of contents

### Properties

- [appBindIndex](types.IRegisterActorOptions.md#appbindindex)
- [capacity](types.IRegisterActorOptions.md#capacity)
- [dependencies](types.IRegisterActorOptions.md#dependencies)
- [locking](types.IRegisterActorOptions.md#locking)
- [multiplicity](types.IRegisterActorOptions.md#multiplicity)
- [name](types.IRegisterActorOptions.md#name)
- [placement](types.IRegisterActorOptions.md#placement)
- [status](types.IRegisterActorOptions.md#status)

### Methods

- [activator](types.IRegisterActorOptions.md#activator)
- [creator](types.IRegisterActorOptions.md#creator)
- [deactivator](types.IRegisterActorOptions.md#deactivator)
- [onAppStart](types.IRegisterActorOptions.md#onappstart)
- [onAppStop](types.IRegisterActorOptions.md#onappstop)
- [perform](types.IRegisterActorOptions.md#perform)

## Properties

### appBindIndex

• `Optional` **appBindIndex**: `number`

#### Defined in

[src/types.ts:40](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L40)

___

### capacity

• **capacity**: `number`

#### Defined in

[src/types.ts:36](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L36)

___

### dependencies

• **dependencies**: `string`[]

#### Defined in

[src/types.ts:42](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L42)

___

### locking

• **locking**: ``"exclusive"`` \| ``"shared"``

#### Defined in

[src/types.ts:35](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L35)

___

### multiplicity

• **multiplicity**: ``"single"`` \| ``"multiple"``

#### Defined in

[src/types.ts:33](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L33)

___

### name

• **name**: `string`

#### Defined in

[src/types.ts:20](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L20)

___

### placement

• **placement**: ``"local"`` \| ``"availability"``

#### Defined in

[src/types.ts:34](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L34)

___

### status

• **status**: ``"enabled"`` \| ``"disabled"``

#### Defined in

[src/types.ts:41](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L41)

## Methods

### activator

▸ **activator**(`instanceInfo`, `context`): `Promise`<`void`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `instanceInfo` | [`IInstanceInfo`](types.IInstanceInfo.md) |
| `context` | [`IBaseActionContext`](types.IBaseActionContext.md) |

#### Returns

`Promise`<`void`\>

#### Defined in

[src/types.ts:22](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L22)

___

### creator

▸ **creator**(`options`): `unknown`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`IActorCreateOptions`](types.IActorCreateOptions.md) |

#### Returns

`unknown`

#### Defined in

[src/types.ts:21](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L21)

___

### deactivator

▸ **deactivator**(`instanceInfo`, `context`): `Promise`<`void`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `instanceInfo` | [`IInstanceInfo`](types.IInstanceInfo.md) |
| `context` | [`IBaseActionContext`](types.IBaseActionContext.md) |

#### Returns

`Promise`<`void`\>

#### Defined in

[src/types.ts:23](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L23)

___

### onAppStart

▸ **onAppStart**(`runtime`): `Promise`<`void`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `runtime` | [`IRuntime`](types.IRuntime.md) |

#### Returns

`Promise`<`void`\>

#### Defined in

[src/types.ts:31](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L31)

___

### onAppStop

▸ **onAppStop**(`runtime`): `Promise`<`void`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `runtime` | [`IRuntime`](types.IRuntime.md) |

#### Returns

`Promise`<`void`\>

#### Defined in

[src/types.ts:32](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L32)

___

### perform

▸ **perform**(`instanceInfo`, `action`, `context`): `Promise`<[`IPerformActionRespC`](../modules/everything.interface_performing.md#iperformactionrespc)<`Object`\>\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `instanceInfo` | [`IInstanceInfo`](types.IInstanceInfo.md) |
| `action` | [`IPerformActionReqCL`](../modules/everything.interface_performing.md#iperformactionreqcl)<`Object`\> |
| `context` | [`IBaseActionContext`](types.IBaseActionContext.md) |

#### Returns

`Promise`<[`IPerformActionRespC`](../modules/everything.interface_performing.md#iperformactionrespc)<`Object`\>\>

#### Defined in

[src/types.ts:26](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L26)
