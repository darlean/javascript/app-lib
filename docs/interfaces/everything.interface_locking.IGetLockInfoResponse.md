[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / [interface\_locking](../modules/everything.interface_locking.md) / IGetLockInfoResponse

# Interface: IGetLockInfoResponse

[everything](../modules/everything.md).[interface_locking](../modules/everything.interface_locking.md).IGetLockInfoResponse

## Table of contents

### Properties

- [holders](everything.interface_locking.IGetLockInfoResponse.md#holders)

## Properties

### holders

• **holders**: `string`[]

#### Defined in

[src/interfaces/locking.ts:35](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/locking.ts#L35)
