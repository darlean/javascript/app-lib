[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / IScheduleReminderResult

# Interface: IScheduleReminderResult

[everything](../modules/everything.md).IScheduleReminderResult

## Table of contents

### Properties

- [handle](everything.IScheduleReminderResult.md#handle)

## Properties

### handle

• **handle**: `string`

#### Defined in

[src/services/scheduling.ts:19](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/scheduling.ts#L19)
