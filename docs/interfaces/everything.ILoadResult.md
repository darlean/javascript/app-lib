[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / ILoadResult

# Interface: ILoadResult<ObjectType\>

[everything](../modules/everything.md).ILoadResult

## Type parameters

| Name |
| :------ |
| `ObjectType` |

## Table of contents

### Properties

- [value](everything.ILoadResult.md#value)

## Properties

### value

• `Optional` **value**: [`IAttachment`](structs.IAttachment.md)<`ObjectType`\>

#### Defined in

[src/services/persistence.ts:23](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L23)
