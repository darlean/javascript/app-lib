[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / IInspectRequest

# Interface: IInspectRequest

[everything](../modules/everything.md).IInspectRequest

## Table of contents

### Properties

- [compartment](everything.IInspectRequest.md#compartment)
- [includeValues](everything.IInspectRequest.md#includevalues)
- [shardLimit](everything.IInspectRequest.md#shardlimit)

## Properties

### compartment

• `Optional` **compartment**: `string`

#### Defined in

[src/services/persistence.ts:52](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L52)

___

### includeValues

• `Optional` **includeValues**: `boolean`

#### Defined in

[src/services/persistence.ts:54](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L54)

___

### shardLimit

• **shardLimit**: `number`

#### Defined in

[src/services/persistence.ts:53](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L53)
