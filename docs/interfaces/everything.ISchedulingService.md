[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / ISchedulingService

# Interface: ISchedulingService

[everything](../modules/everything.md).ISchedulingService

## Table of contents

### Methods

- [scheduleReminder](everything.ISchedulingService.md#schedulereminder)

## Methods

### scheduleReminder

▸ **scheduleReminder**<`Data`\>(`request`): `Promise`<[`IScheduleReminderResult`](everything.IScheduleReminderResult.md)\>

#### Type parameters

| Name |
| :------ |
| `Data` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `request` | [`IScheduleReminderOptions`](everything.IScheduleReminderOptions.md)<`Data`\> |

#### Returns

`Promise`<[`IScheduleReminderResult`](everything.IScheduleReminderResult.md)\>

#### Defined in

[src/services/scheduling.ts:23](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/scheduling.ts#L23)
