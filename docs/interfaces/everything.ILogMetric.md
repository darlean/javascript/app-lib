[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / ILogMetric

# Interface: ILogMetric

[everything](../modules/everything.md).ILogMetric

## Table of contents

### Properties

- [count](everything.ILogMetric.md#count)
- [scope](everything.ILogMetric.md#scope)
- [totalTime](everything.ILogMetric.md#totaltime)

## Properties

### count

• **count**: `number`

#### Defined in

[src/logging.ts:56](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/logging.ts#L56)

___

### scope

• **scope**: `string`

#### Defined in

[src/logging.ts:55](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/logging.ts#L55)

___

### totalTime

• **totalTime**: `number`

#### Defined in

[src/logging.ts:57](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/logging.ts#L57)
