[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [types](../modules/types.md) / ITime

# Interface: ITime

[types](../modules/types.md).ITime

## Table of contents

### Methods

- [machineTicks](types.ITime.md#machineticks)
- [machineTime](types.ITime.md#machinetime)
- [noop](types.ITime.md#noop)
- [repeat](types.ITime.md#repeat)
- [sleep](types.ITime.md#sleep)

## Methods

### machineTicks

▸ **machineTicks**(): `number`

#### Returns

`number`

#### Defined in

[src/types.ts:340](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L340)

___

### machineTime

▸ **machineTime**(): `number`

#### Returns

`number`

#### Defined in

[src/types.ts:346](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L346)

___

### noop

▸ **noop**(): `Promise`<`void`\>

#### Returns

`Promise`<`void`\>

#### Defined in

[src/types.ts:361](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L361)

___

### repeat

▸ **repeat**(`callback`, `name`, `interval`, `delay?`, `repeatCount?`): [`ITimer`](types.ITimer.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `callback` | () => `unknown` |
| `name` | `string` |
| `interval` | `number` |
| `delay?` | `number` |
| `repeatCount?` | `number` |

#### Returns

[`ITimer`](types.ITimer.md)

#### Defined in

[src/types.ts:354](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L354)

___

### sleep

▸ **sleep**(`ms`): `Promise`<`void`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `ms` | `number` |

#### Returns

`Promise`<`void`\>

#### Defined in

[src/types.ts:357](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L357)
