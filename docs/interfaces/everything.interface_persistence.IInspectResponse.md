[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / [interface\_persistence](../modules/everything.interface_persistence.md) / IInspectResponse

# Interface: IInspectResponse

[everything](../modules/everything.md).[interface_persistence](../modules/everything.interface_persistence.md).IInspectResponse

## Table of contents

### Properties

- [items](everything.interface_persistence.IInspectResponse.md#items)

## Properties

### items

• **items**: [`IInspectResponseItem`](everything.interface_persistence.IInspectResponseItem.md)[]

#### Defined in

[src/interfaces/persistence.ts:66](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L66)
