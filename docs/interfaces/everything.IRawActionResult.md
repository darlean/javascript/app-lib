[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / IRawActionResult

# Interface: IRawActionResult<Data\>

[everything](../modules/everything.md).IRawActionResult

Represents the result of an action that is performed via
[IBaseActionContext.performAction](types.IBaseActionContext.md#performaction) or [IBaseActionContext.fireAction](types.IBaseActionContext.md#fireaction).

## Type parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `Data` | extends [`AnObject`](../modules/containers.md#anobject) | The (object) type of the result data. |

## Hierarchy

- **`IRawActionResult`**

  ↳ [`IActionResult`](everything.IActionResult.md)

## Table of contents

### Properties

- [data](everything.IRawActionResult.md#data)

## Properties

### data

• **data**: [`Struct`](../modules/structs.md#struct)<`Data`\>

The data as returned by the action.

When the action returns attachments, they can be
obtained by means of the [[Struct._att]] method.

#### Defined in

[src/services/performing.ts:106](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/performing.ts#L106)
