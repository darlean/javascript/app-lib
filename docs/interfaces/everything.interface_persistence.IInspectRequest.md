[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / [interface\_persistence](../modules/everything.interface_persistence.md) / IInspectRequest

# Interface: IInspectRequest

[everything](../modules/everything.md).[interface_persistence](../modules/everything.interface_persistence.md).IInspectRequest

## Table of contents

### Properties

- [compartment](everything.interface_persistence.IInspectRequest.md#compartment)
- [includeValues](everything.interface_persistence.IInspectRequest.md#includevalues)
- [shardLimit](everything.interface_persistence.IInspectRequest.md#shardlimit)

## Properties

### compartment

• `Optional` **compartment**: `string`

#### Defined in

[src/interfaces/persistence.ts:53](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L53)

___

### includeValues

• `Optional` **includeValues**: `boolean`

#### Defined in

[src/interfaces/persistence.ts:55](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L55)

___

### shardLimit

• **shardLimit**: `number`

#### Defined in

[src/interfaces/persistence.ts:54](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L54)
