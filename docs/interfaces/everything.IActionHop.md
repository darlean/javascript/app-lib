[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / IActionHop

# Interface: IActionHop

[everything](../modules/everything.md).IActionHop

## Table of contents

### Properties

- [id](everything.IActionHop.md#id)

## Properties

### id

• **id**: `string`

#### Defined in

[src/services/performing.ts:5](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/performing.ts#L5)
