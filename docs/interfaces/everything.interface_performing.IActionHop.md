[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / [interface\_performing](../modules/everything.interface_performing.md) / IActionHop

# Interface: IActionHop

[everything](../modules/everything.md).[interface_performing](../modules/everything.interface_performing.md).IActionHop

## Table of contents

### Properties

- [id](everything.interface_performing.IActionHop.md#id)

## Properties

### id

• **id**: `string`

#### Defined in

[src/interfaces/performing.ts:6](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/performing.ts#L6)
