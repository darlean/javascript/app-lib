[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / [interface\_persistence](../modules/everything.interface_persistence.md) / ILoadResponse

# Interface: ILoadResponse

[everything](../modules/everything.md).[interface_persistence](../modules/everything.interface_persistence.md).ILoadResponse

## Table of contents

### Properties

- [value](everything.interface_persistence.ILoadResponse.md#value)

## Properties

### value

• `Optional` **value**: [`IAttachmentRef`](structs.IAttachmentRef.md)

#### Defined in

[src/interfaces/persistence.ts:24](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L24)
