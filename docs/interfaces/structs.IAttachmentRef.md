[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [structs](../modules/structs.md) / IAttachmentRef

# Interface: IAttachmentRef

[structs](../modules/structs.md).IAttachmentRef

Represents a reference to an attachment. Instances can be generated by means of the [struct](../modules/structs.md#struct) function.

## Table of contents

### Properties

- [\_\_child\_idx](structs.IAttachmentRef.md#__child_idx)

## Properties

### \_\_child\_idx

• **\_\_child\_idx**: `number`

The index of the child within the struct's container that contains the attachment information.
This field is prefixed with double underscores, so it should not be used directly.

#### Defined in

[src/structs.ts:77](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L77)
