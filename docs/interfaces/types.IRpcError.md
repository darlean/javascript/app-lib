[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [types](../modules/types.md) / IRpcError

# Interface: IRpcError

[types](../modules/types.md).IRpcError

## Table of contents

### Properties

- [ids](types.IRpcError.md#ids)
- [message](types.IRpcError.md#message)
- [nested](types.IRpcError.md#nested)
- [stack](types.IRpcError.md#stack)

## Properties

### ids

• **ids**: `string`[]

#### Defined in

[src/types.ts:122](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L122)

___

### message

• **message**: `string`

#### Defined in

[src/types.ts:123](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L123)

___

### nested

• `Optional` **nested**: [`IRpcError`](types.IRpcError.md)[]

#### Defined in

[src/types.ts:125](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L125)

___

### stack

• `Optional` **stack**: `string`

#### Defined in

[src/types.ts:124](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L124)
