[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / [interface\_persistence](../modules/everything.interface_persistence.md) / IQueryRangePart

# Interface: IQueryRangePart

[everything](../modules/everything.md).[interface_persistence](../modules/everything.interface_persistence.md).IQueryRangePart

## Table of contents

### Properties

- [compare](everything.interface_persistence.IQueryRangePart.md#compare)
- [sortKeys](everything.interface_persistence.IQueryRangePart.md#sortkeys)

## Properties

### compare

• **compare**: ``"exclusive"`` \| ``"inclusive"``

#### Defined in

[src/interfaces/persistence.ts:29](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L29)

___

### sortKeys

• **sortKeys**: `string`[]

#### Defined in

[src/interfaces/persistence.ts:28](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L28)
