[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / [interface\_persistence](../modules/everything.interface_persistence.md) / IStoreRequest

# Interface: IStoreRequest

[everything](../modules/everything.md).[interface_persistence](../modules/everything.interface_persistence.md).IStoreRequest

## Table of contents

### Properties

- [compartment](everything.interface_persistence.IStoreRequest.md#compartment)
- [lookupKeys](everything.interface_persistence.IStoreRequest.md#lookupkeys)
- [sortKeys](everything.interface_persistence.IStoreRequest.md#sortkeys)
- [value](everything.interface_persistence.IStoreRequest.md#value)

## Properties

### compartment

• `Optional` **compartment**: `string`

#### Defined in

[src/interfaces/persistence.ts:4](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L4)

___

### lookupKeys

• **lookupKeys**: `string`[]

#### Defined in

[src/interfaces/persistence.ts:5](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L5)

___

### sortKeys

• `Optional` **sortKeys**: `string`[]

#### Defined in

[src/interfaces/persistence.ts:6](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L6)

___

### value

• **value**: [`IAttachmentRef`](structs.IAttachmentRef.md)

#### Defined in

[src/interfaces/persistence.ts:7](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L7)
