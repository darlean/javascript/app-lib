[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / [interface\_scheduling](../modules/everything.interface_scheduling.md) / IScheduleReminderResponse

# Interface: IScheduleReminderResponse

[everything](../modules/everything.md).[interface_scheduling](../modules/everything.interface_scheduling.md).IScheduleReminderResponse

## Table of contents

### Properties

- [handle](everything.interface_scheduling.IScheduleReminderResponse.md#handle)

## Properties

### handle

• **handle**: `string`

#### Defined in

[src/interfaces/scheduling.ts:19](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/scheduling.ts#L19)
