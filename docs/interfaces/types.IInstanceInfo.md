[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [types](../modules/types.md) / IInstanceInfo

# Interface: IInstanceInfo

[types](../modules/types.md).IInstanceInfo

## Table of contents

### Properties

- [actorInfo](types.IInstanceInfo.md#actorinfo)
- [globalLockRefreshTimer](types.IInstanceInfo.md#globallockrefreshtimer)
- [globalLockUntil](types.IInstanceInfo.md#globallockuntil)
- [id](types.IInstanceInfo.md#id)
- [instance](types.IInstanceInfo.md#instance)
- [localLock](types.IInstanceInfo.md#locallock)
- [state](types.IInstanceInfo.md#state)
- [timers](types.IInstanceInfo.md#timers)

## Properties

### actorInfo

• **actorInfo**: [`IRegisterActorOptions`](types.IRegisterActorOptions.md)

#### Defined in

[src/types.ts:150](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L150)

___

### globalLockRefreshTimer

• `Optional` **globalLockRefreshTimer**: [`ITimer`](types.ITimer.md)

#### Defined in

[src/types.ts:155](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L155)

___

### globalLockUntil

• **globalLockUntil**: `number`

#### Defined in

[src/types.ts:154](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L154)

___

### id

• **id**: `string`[]

#### Defined in

[src/types.ts:151](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L151)

___

### instance

• **instance**: `unknown`

#### Defined in

[src/types.ts:152](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L152)

___

### localLock

• **localLock**: [`SharedExclusiveLock`](../classes/selock.SharedExclusiveLock.md)

#### Defined in

[src/types.ts:156](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L156)

___

### state

• **state**: ``"created"`` \| ``"active"`` \| ``"deactivating"`` \| ``"deactive"``

#### Defined in

[src/types.ts:153](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L153)

___

### timers

• **timers**: [`ITimer`](types.ITimer.md)[]

#### Defined in

[src/types.ts:157](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L157)
