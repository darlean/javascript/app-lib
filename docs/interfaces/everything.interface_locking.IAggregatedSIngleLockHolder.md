[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / [interface\_locking](../modules/everything.interface_locking.md) / IAggregatedSIngleLockHolder

# Interface: IAggregatedSIngleLockHolder

[everything](../modules/everything.md).[interface_locking](../modules/everything.interface_locking.md).IAggregatedSIngleLockHolder

## Table of contents

### Properties

- [holder](everything.interface_locking.IAggregatedSIngleLockHolder.md#holder)
- [source](everything.interface_locking.IAggregatedSIngleLockHolder.md#source)
- [ttl](everything.interface_locking.IAggregatedSIngleLockHolder.md#ttl)

## Properties

### holder

• `Optional` **holder**: `string`

#### Defined in

[src/interfaces/locking.ts:2](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/locking.ts#L2)

___

### source

• **source**: `string`

#### Defined in

[src/interfaces/locking.ts:4](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/locking.ts#L4)

___

### ttl

• **ttl**: `number`

#### Defined in

[src/interfaces/locking.ts:3](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/locking.ts#L3)
