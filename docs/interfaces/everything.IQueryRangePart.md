[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / IQueryRangePart

# Interface: IQueryRangePart

[everything](../modules/everything.md).IQueryRangePart

## Table of contents

### Properties

- [compare](everything.IQueryRangePart.md#compare)
- [sortKeys](everything.IQueryRangePart.md#sortkeys)

## Properties

### compare

• **compare**: ``"exclusive"`` \| ``"inclusive"``

#### Defined in

[src/services/persistence.ts:28](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L28)

___

### sortKeys

• **sortKeys**: `string`[]

#### Defined in

[src/services/persistence.ts:27](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L27)
