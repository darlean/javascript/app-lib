[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [types](../modules/types.md) / IRuntime

# Interface: IRuntime

[types](../modules/types.md).IRuntime

## Implemented by

- [`Application`](../classes/everything.Application.md)

## Table of contents

### Properties

- [time](types.IRuntime.md#time)

### Methods

- [acquireLock](types.IRuntime.md#acquirelock)
- [dispatchAction](types.IRuntime.md#dispatchaction)
- [dispatchActionRaw](types.IRuntime.md#dispatchactionraw)
- [fireAction](types.IRuntime.md#fireaction)
- [getLockInfo](types.IRuntime.md#getlockinfo)
- [publishInfo](types.IRuntime.md#publishinfo)
- [releaseLock](types.IRuntime.md#releaselock)

## Properties

### time

• **time**: [`ITime`](types.ITime.md)

#### Defined in

[src/types.ts:183](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L183)

## Methods

### acquireLock

▸ **acquireLock**(`id`, `ttl`, `refresh`, `options`): `Promise`<`IAcquireLockResult`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `id` | `string`[] |
| `ttl` | `number` |
| `refresh` | `boolean` |
| `options` | [`IActionOptions`](everything.IActionOptions.md)<`Object`\> |

#### Returns

`Promise`<`IAcquireLockResult`\>

#### Defined in

[src/types.ts:184](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L184)

___

### dispatchAction

▸ **dispatchAction**<`Input`, `Output`\>(`action`): `Promise`<`Output`\>

#### Type parameters

| Name |
| :------ |
| `Input` |
| `Output` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `action` | [`IActionOptions`](everything.IActionOptions.md)<`Input`\> |

#### Returns

`Promise`<`Output`\>

#### Defined in

[src/types.ts:193](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L193)

___

### dispatchActionRaw

▸ **dispatchActionRaw**<`Input`, `Output`\>(`action`): `Promise`<[`IActionResult`](everything.IActionResult.md)<`Output`\>\>

#### Type parameters

| Name |
| :------ |
| `Input` |
| `Output` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `action` | [`IActionOptions`](everything.IActionOptions.md)<`Input`\> |

#### Returns

`Promise`<[`IActionResult`](everything.IActionResult.md)<`Output`\>\>

#### Defined in

[src/types.ts:194](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L194)

___

### fireAction

▸ **fireAction**<`Input`\>(`action`): `void`

#### Type parameters

| Name |
| :------ |
| `Input` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `action` | [`IActionOptions`](everything.IActionOptions.md)<`Input`\> |

#### Returns

`void`

#### Defined in

[src/types.ts:195](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L195)

___

### getLockInfo

▸ **getLockInfo**(`id`, `options?`): `Promise`<`IGetLockInfoResult`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `id` | `string`[] |
| `options?` | [`IActionOptions`](everything.IActionOptions.md)<`Object`\> |

#### Returns

`Promise`<`IGetLockInfoResult`\>

#### Defined in

[src/types.ts:191](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L191)

___

### publishInfo

▸ **publishInfo**(`info`): `Promise`<`void`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `info` | [`IAppInfo`](types.IAppInfo.md) |

#### Returns

`Promise`<`void`\>

#### Defined in

[src/types.ts:192](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L192)

___

### releaseLock

▸ **releaseLock**(`id`, `waitFor`, `options?`): `Promise`<`void`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `id` | `string`[] |
| `waitFor` | [`PerformActionRequestWaitFor`](../modules/everything.md#performactionrequestwaitfor) |
| `options?` | [`IActionOptions`](everything.IActionOptions.md)<`Object`\> |

#### Returns

`Promise`<`void`\>

#### Defined in

[src/types.ts:190](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L190)
