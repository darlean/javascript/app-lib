[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / IActionResult

# Interface: IActionResult<Data\>

[everything](../modules/everything.md).IActionResult

## Type parameters

| Name | Type |
| :------ | :------ |
| `Data` | extends [`AnObject`](../modules/containers.md#anobject) |

## Hierarchy

- [`IRawActionResult`](everything.IRawActionResult.md)<`Data`\>

  ↳ **`IActionResult`**

## Table of contents

### Properties

- [data](everything.IActionResult.md#data)

## Properties

### data

• **data**: [`Struct`](../modules/structs.md#struct)<`Data`\>

The data as returned by the action.

When the action returns attachments, they can be
obtained by means of the [[Struct._att]] method.

#### Inherited from

[IRawActionResult](everything.IRawActionResult.md).[data](everything.IRawActionResult.md#data)

#### Defined in

[src/services/performing.ts:106](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/performing.ts#L106)
