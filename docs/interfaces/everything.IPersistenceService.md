[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / IPersistenceService

# Interface: IPersistenceService

[everything](../modules/everything.md).IPersistenceService

Provides access to methods to [load](everything.IPersistenceService.md#load), [store](everything.IPersistenceService.md#store), [delete](everything.IPersistenceService.md#delete) or [query](everything.IPersistenceService.md#query) data from
configured state stores.

## Table of contents

### Methods

- [delete](everything.IPersistenceService.md#delete)
- [load](everything.IPersistenceService.md#load)
- [query](everything.IPersistenceService.md#query)
- [store](everything.IPersistenceService.md#store)

## Methods

### delete

▸ **delete**(`request`): `Promise`<`void`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `request` | [`IDeleteOptions`](everything.IDeleteOptions.md) |

#### Returns

`Promise`<`void`\>

#### Defined in

[src/services/persistence.ts:63](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L63)

___

### load

▸ **load**<`ObjectType`\>(`request`): `Promise`<[`ILoadResult`](everything.ILoadResult.md)<`ObjectType`\>\>

#### Type parameters

| Name |
| :------ |
| `ObjectType` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `request` | [`ILoadOptions`](everything.ILoadOptions.md) |

#### Returns

`Promise`<[`ILoadResult`](everything.ILoadResult.md)<`ObjectType`\>\>

#### Defined in

[src/services/persistence.ts:64](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L64)

___

### query

▸ **query**<`ObjectType`\>(`request`): `Promise`<[`IQueryResult`](everything.IQueryResult.md)<`ObjectType`\>\>

#### Type parameters

| Name |
| :------ |
| `ObjectType` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `request` | [`IQueryOptions`](everything.IQueryOptions.md) |

#### Returns

`Promise`<[`IQueryResult`](everything.IQueryResult.md)<`ObjectType`\>\>

#### Defined in

[src/services/persistence.ts:65](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L65)

___

### store

▸ **store**(`request`): `Promise`<`void`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `request` | [`IStoreOptions`](everything.IStoreOptions.md) |

#### Returns

`Promise`<`void`\>

#### Defined in

[src/services/persistence.ts:62](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L62)
