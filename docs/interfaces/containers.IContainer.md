[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [containers](../modules/containers.md) / IContainer

# Interface: IContainer<Type, Child0Type\>

[containers](../modules/containers.md).IContainer

## Type parameters

| Name | Type |
| :------ | :------ |
| `Type` | extends [`ContainerData`](../modules/containers.md#containerdata) |
| `Child0Type` | extends [`ContainerData`](../modules/containers.md#containerdata) |

## Implemented by

- [`BufferContainer`](../classes/binaryencoding.BufferContainer.md)
- [`EditableContainer`](../classes/containers.EditableContainer.md)

## Table of contents

### Methods

- [\_\_container](containers.IContainer.md#__container)
- [getBuffer](containers.IContainer.md#getbuffer)
- [getBufferEncoding](containers.IContainer.md#getbufferencoding)
- [getChild](containers.IContainer.md#getchild)
- [getChild0](containers.IContainer.md#getchild0)
- [getChildCount](containers.IContainer.md#getchildcount)
- [getData](containers.IContainer.md#getdata)
- [getDataBinary](containers.IContainer.md#getdatabinary)
- [getDataStruct](containers.IContainer.md#getdatastruct)
- [getDataText](containers.IContainer.md#getdatatext)
- [getKind](containers.IContainer.md#getkind)
- [tryGetChild0](containers.IContainer.md#trygetchild0)

## Methods

### \_\_container

▸ **__container**(): `boolean`

#### Returns

`boolean`

#### Defined in

[src/containers.ts:73](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L73)

___

### getBuffer

▸ **getBuffer**(): `undefined` \| `Buffer`

#### Returns

`undefined` \| `Buffer`

#### Defined in

[src/containers.ts:84](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L84)

___

### getBufferEncoding

▸ **getBufferEncoding**(): [`BufferEncoding`](../modules/containers.md#bufferencoding)

#### Returns

[`BufferEncoding`](../modules/containers.md#bufferencoding)

#### Defined in

[src/containers.ts:83](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L83)

___

### getChild

▸ **getChild**<`Type`\>(`idx`): [`IContainer`](containers.IContainer.md)<`Type`, [`ContainerData`](../modules/containers.md#containerdata)\>

#### Type parameters

| Name |
| :------ |
| `Type` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `idx` | `number` |

#### Returns

[`IContainer`](containers.IContainer.md)<`Type`, [`ContainerData`](../modules/containers.md#containerdata)\>

#### Defined in

[src/containers.ts:81](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L81)

___

### getChild0

▸ **getChild0**(): [`IContainer`](containers.IContainer.md)<`Child0Type`, [`ContainerData`](../modules/containers.md#containerdata)\>

#### Returns

[`IContainer`](containers.IContainer.md)<`Child0Type`, [`ContainerData`](../modules/containers.md#containerdata)\>

#### Defined in

[src/containers.ts:79](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L79)

___

### getChildCount

▸ **getChildCount**(): `number`

#### Returns

`number`

#### Defined in

[src/containers.ts:82](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L82)

___

### getData

▸ **getData**(): `undefined` \| `Type`

#### Returns

`undefined` \| `Type`

#### Defined in

[src/containers.ts:74](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L74)

___

### getDataBinary

▸ **getDataBinary**(): `Buffer`

#### Returns

`Buffer`

#### Defined in

[src/containers.ts:76](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L76)

___

### getDataStruct

▸ **getDataStruct**(): `Type`

#### Returns

`Type`

#### Defined in

[src/containers.ts:75](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L75)

___

### getDataText

▸ **getDataText**(): `string`

#### Returns

`string`

#### Defined in

[src/containers.ts:77](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L77)

___

### getKind

▸ **getKind**(): [`ContainerKind`](../modules/containers.md#containerkind)

#### Returns

[`ContainerKind`](../modules/containers.md#containerkind)

#### Defined in

[src/containers.ts:78](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L78)

___

### tryGetChild0

▸ **tryGetChild0**(): `undefined` \| [`IContainer`](containers.IContainer.md)<`Child0Type`, [`ContainerData`](../modules/containers.md#containerdata)\>

#### Returns

`undefined` \| [`IContainer`](containers.IContainer.md)<`Child0Type`, [`ContainerData`](../modules/containers.md#containerdata)\>

#### Defined in

[src/containers.ts:80](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L80)
