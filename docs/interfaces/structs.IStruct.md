[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [structs](../modules/structs.md) / IStruct

# Interface: IStruct<T\>

[structs](../modules/structs.md).IStruct

Within the scope of Darlean, a *struct* is an object that can be converted from and to
a [container](containers.IContainer.md), and that can contain [attachments](structs.IAttachment.md)) that
can be binary, text or structs themselves (that then can again contain attachments, et
cetera).

An object can be turned into a struct by calling the [struct](../modules/structs.md#struct) function. An object that has become
a struct, implements [IStruct](structs.IStruct.md), which means that the properties [__struct](structs.IStruct.md#__struct), [_att](structs.IStruct.md#_att) and [_cont](structs.IStruct.md#_cont)
are added to the object.

## Type parameters

| Name | Type |
| :------ | :------ |
| `T` | extends [`AnObject`](../modules/containers.md#anobject) |

## Hierarchy

- [`IContainerizeable1`](../modules/containers.md#icontainerizeable1)<`T`\>

  ↳ **`IStruct`**

## Table of contents

### Methods

- [\_\_struct](structs.IStruct.md#__struct)
- [\_att](structs.IStruct.md#_att)
- [\_cont](structs.IStruct.md#_cont)

## Methods

### \_\_struct

▸ **__struct**(): `boolean`

Indicator that this object is a struct. Used internally by [isStruct](../modules/structs.md#isstruct). This field is
implemented as a function so that it will not be included in generated json (function are ignored
when generating json via JSON.stringify).

This field is prefixed with a double underscore because it is for internal use only.

#### Returns

`boolean`

#### Defined in

[src/structs.ts:135](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L135)

___

### \_att

▸ **_att**<`A`\>(`ref`): [`IAttachment`](structs.IAttachment.md)<`A`\>

Function that returns a sub-attachment for this struct object. It receives an [IAttachmentRef](structs.IAttachmentRef.md) that
can be obtained by invoking [struct](../modules/structs.md#struct) with an [Attacher](../modules/structs.md#attacher) function is parameter.

This field is prefixed with one underscore to make reduce the likelyhood of interfering with the
regular fields of the struct object.

#### Type parameters

| Name |
| :------ |
| `A` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `ref` | [`IAttachmentRef`](structs.IAttachmentRef.md) |

#### Returns

[`IAttachment`](structs.IAttachment.md)<`A`\>

#### Defined in

[src/structs.ts:144](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L144)

___

### \_cont

▸ **_cont**(): [`IContainer`](containers.IContainer.md)<`T`, [`ContainerData`](../modules/containers.md#containerdata)\>

Function that returns a the underlying container that stores the struct object and any attachments.

This field is prefixed with one underscore to make reduce the likelyhood of interfering with the
regular fields of the struct object.

#### Returns

[`IContainer`](containers.IContainer.md)<`T`, [`ContainerData`](../modules/containers.md#containerdata)\>

#### Overrides

IContainerizeable1.\_cont

#### Defined in

[src/structs.ts:152](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L152)
