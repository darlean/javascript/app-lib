[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / ILoadOptions

# Interface: ILoadOptions

[everything](../modules/everything.md).ILoadOptions

## Table of contents

### Properties

- [compartment](everything.ILoadOptions.md#compartment)
- [lookupKeys](everything.ILoadOptions.md#lookupkeys)
- [sortKeys](everything.ILoadOptions.md#sortkeys)

## Properties

### compartment

• `Optional` **compartment**: `string`

#### Defined in

[src/services/persistence.ts:17](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L17)

___

### lookupKeys

• **lookupKeys**: `string`[]

#### Defined in

[src/services/persistence.ts:18](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L18)

___

### sortKeys

• `Optional` **sortKeys**: `string`[]

#### Defined in

[src/services/persistence.ts:19](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L19)
