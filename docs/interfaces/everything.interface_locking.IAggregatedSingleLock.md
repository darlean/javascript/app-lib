[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / [interface\_locking](../modules/everything.interface_locking.md) / IAggregatedSingleLock

# Interface: IAggregatedSingleLock

[everything](../modules/everything.md).[interface_locking](../modules/everything.interface_locking.md).IAggregatedSingleLock

## Table of contents

### Properties

- [holders](everything.interface_locking.IAggregatedSingleLock.md#holders)
- [id](everything.interface_locking.IAggregatedSingleLock.md#id)

## Properties

### holders

• **holders**: [`IAggregatedSIngleLockHolder`](everything.interface_locking.IAggregatedSIngleLockHolder.md)[]

#### Defined in

[src/interfaces/locking.ts:9](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/locking.ts#L9)

___

### id

• **id**: `string`[]

#### Defined in

[src/interfaces/locking.ts:8](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/locking.ts#L8)
