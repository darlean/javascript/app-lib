[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [types](../modules/types.md) / IShutdownInfo

# Interface: IShutdownInfo

[types](../modules/types.md).IShutdownInfo

## Table of contents

### Properties

- [actorsBeingUsed](types.IShutdownInfo.md#actorsbeingused)
- [status](types.IShutdownInfo.md#status)

## Properties

### actorsBeingUsed

• **actorsBeingUsed**: `string`[]

#### Defined in

[src/types.ts:368](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L368)

___

### status

• **status**: ``"running"`` \| ``"shuttingdown"``

#### Defined in

[src/types.ts:367](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L367)
