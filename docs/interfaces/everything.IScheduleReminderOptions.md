[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / IScheduleReminderOptions

# Interface: IScheduleReminderOptions<Data\>

[everything](../modules/everything.md).IScheduleReminderOptions

## Type parameters

| Name |
| :------ |
| `Data` |

## Table of contents

### Properties

- [callback](everything.IScheduleReminderOptions.md#callback)
- [delay](everything.IScheduleReminderOptions.md#delay)
- [id](everything.IScheduleReminderOptions.md#id)
- [interval](everything.IScheduleReminderOptions.md#interval)
- [moment](everything.IScheduleReminderOptions.md#moment)
- [repeatCount](everything.IScheduleReminderOptions.md#repeatcount)

## Properties

### callback

• **callback**: [`IActionOptions`](everything.IActionOptions.md)<`Data`\>

#### Defined in

[src/services/scheduling.ts:15](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/scheduling.ts#L15)

___

### delay

• `Optional` **delay**: `number`

#### Defined in

[src/services/scheduling.ts:11](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/scheduling.ts#L11)

___

### id

• **id**: `string`[]

#### Defined in

[src/services/scheduling.ts:10](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/scheduling.ts#L10)

___

### interval

• `Optional` **interval**: `number`

#### Defined in

[src/services/scheduling.ts:12](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/scheduling.ts#L12)

___

### moment

• `Optional` **moment**: `number`

#### Defined in

[src/services/scheduling.ts:14](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/scheduling.ts#L14)

___

### repeatCount

• `Optional` **repeatCount**: `number`

#### Defined in

[src/services/scheduling.ts:13](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/scheduling.ts#L13)
