[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [types](../modules/types.md) / ISupportedActor

# Interface: ISupportedActor

[types](../modules/types.md).ISupportedActor

## Table of contents

### Properties

- [appBindIndex](types.ISupportedActor.md#appbindindex)
- [availability](types.ISupportedActor.md#availability)
- [dependencies](types.ISupportedActor.md#dependencies)
- [multiplicity](types.ISupportedActor.md#multiplicity)
- [name](types.ISupportedActor.md#name)
- [placement](types.ISupportedActor.md#placement)
- [status](types.ISupportedActor.md#status)

## Properties

### appBindIndex

• `Optional` **appBindIndex**: `number`

#### Defined in

[src/types.ts:171](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L171)

___

### availability

• **availability**: `number`

#### Defined in

[src/types.ts:170](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L170)

___

### dependencies

• **dependencies**: `string`[]

#### Defined in

[src/types.ts:174](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L174)

___

### multiplicity

• **multiplicity**: ``"single"`` \| ``"multiple"``

#### Defined in

[src/types.ts:169](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L169)

___

### name

• **name**: `string`

#### Defined in

[src/types.ts:168](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L168)

___

### placement

• **placement**: ``"local"`` \| ``"availability"``

#### Defined in

[src/types.ts:172](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L172)

___

### status

• **status**: ``"enabled"`` \| ``"disabled"``

#### Defined in

[src/types.ts:173](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L173)
