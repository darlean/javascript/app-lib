[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [types](../modules/types.md) / IAppInfo

# Interface: IAppInfo

[types](../modules/types.md).IAppInfo

## Table of contents

### Properties

- [id](types.IAppInfo.md#id)
- [supportedActors](types.IAppInfo.md#supportedactors)

## Properties

### id

• **id**: `string`

#### Defined in

[src/types.ts:178](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L178)

___

### supportedActors

• **supportedActors**: [`ISupportedActor`](types.ISupportedActor.md)[]

#### Defined in

[src/types.ts:179](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L179)
