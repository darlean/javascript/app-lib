[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [actorfactory](../modules/actorfactory.md) / IActionDecoration

# Interface: IActionDecoration

[actorfactory](../modules/actorfactory.md).IActionDecoration

Options that can be used to decorate an action method.

## Table of contents

### Properties

- [description](actorfactory.IActionDecoration.md#description)
- [locking](actorfactory.IActionDecoration.md#locking)
- [name](actorfactory.IActionDecoration.md#name)

## Properties

### description

• `Optional` **description**: `string`

An optional description for the action that can, for example, be displayed in the Darlean control panel for informative purposes.

#### Defined in

[src/actorfactory.ts:130](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L130)

___

### locking

• `Optional` **locking**: ``"none"`` \| ``"exclusive"`` \| ``"shared"``

The locking method for this action. When omitted, the locking method for the actor is used. When that actor decoration also does
not explicitly define the locking, a locking of `'exclusive'` is used for regular actors, and a locking of `'shared'` is used for
service actors.

* For *exclusive locking*, the framework ensures that only one action method is executed at a time per actor instance (with the exception
  of reentrant calls for the same call tree).
* For *shared* locking*, multiple actions that are also 'shared' can be active at the same time per actor instance, but not at the same time
  as an action that is 'exclusive'.
* When locking is set to `'none'` for an action, it can always be invoked, regardless of other shared or exclusive actions currently
  being performed or not. This option should only be used in very special use cases where the other locking modes are not sufficient.

#### Defined in

[src/actorfactory.ts:126](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L126)

___

### name

• `Optional` **name**: `string`

The name of the action via which other actors can invoke the action. When omitted, the name of the method is used as action name.

#### Defined in

[src/actorfactory.ts:113](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L113)
