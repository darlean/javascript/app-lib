[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [types](../modules/types.md) / ITimer

# Interface: ITimer

[types](../modules/types.md).ITimer

## Table of contents

### Methods

- [cancel](types.ITimer.md#cancel)
- [pause](types.ITimer.md#pause)
- [resume](types.ITimer.md#resume)

## Methods

### cancel

▸ **cancel**(): `void`

Cancels the timer. The timer will never fire again.

#### Returns

`void`

#### Defined in

[src/types.ts:317](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L317)

___

### pause

▸ **pause**(`duration?`): `void`

Pauses the firing of the timer. When duration is specified, the timer is automatically resumed after
the duration. Invoking pause while another pause is already active, or a resume is scheduled, replaces
those actions.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `duration?` | `number` | When specified, indicates after how many milliseconds the timer will resume. When not specified, the pause lasts indefinately until [resume](types.ITimer.md#resume) is explicitly invoked. |

#### Returns

`void`

#### Defined in

[src/types.ts:326](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L326)

___

### resume

▸ **resume**(`delay?`): `void`

Resumes a paused timer after delay milliseconds. When delay is not present, the timer is resumed after the
configured interval of the timer when set, otherwise after the configured initial delay when set, otherwise
immediately. When the timer is not paused, the next timer event will be executed after the delay.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `delay?` | `number` | The amount of milliseconds after which the timer is resumed. |

#### Returns

`void`

#### Defined in

[src/types.ts:334](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L334)
