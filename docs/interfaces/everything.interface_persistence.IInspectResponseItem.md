[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / [interface\_persistence](../modules/everything.interface_persistence.md) / IInspectResponseItem

# Interface: IInspectResponseItem

[everything](../modules/everything.md).[interface_persistence](../modules/everything.interface_persistence.md).IInspectResponseItem

## Table of contents

### Properties

- [lookupKeys](everything.interface_persistence.IInspectResponseItem.md#lookupkeys)
- [shard](everything.interface_persistence.IInspectResponseItem.md#shard)
- [sortKeys](everything.interface_persistence.IInspectResponseItem.md#sortkeys)
- [value](everything.interface_persistence.IInspectResponseItem.md#value)

## Properties

### lookupKeys

• **lookupKeys**: `string`[]

#### Defined in

[src/interfaces/persistence.ts:59](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L59)

___

### shard

• **shard**: `number`

#### Defined in

[src/interfaces/persistence.ts:61](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L61)

___

### sortKeys

• **sortKeys**: `string`[]

#### Defined in

[src/interfaces/persistence.ts:60](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L60)

___

### value

• `Optional` **value**: [`IAttachmentRef`](structs.IAttachmentRef.md)

#### Defined in

[src/interfaces/persistence.ts:62](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L62)
