[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / [interface\_persistence](../modules/everything.interface_persistence.md) / IDeleteRequest

# Interface: IDeleteRequest

[everything](../modules/everything.md).[interface_persistence](../modules/everything.interface_persistence.md).IDeleteRequest

## Table of contents

### Properties

- [compartment](everything.interface_persistence.IDeleteRequest.md#compartment)
- [lookupKeys](everything.interface_persistence.IDeleteRequest.md#lookupkeys)
- [sortKeys](everything.interface_persistence.IDeleteRequest.md#sortkeys)

## Properties

### compartment

• `Optional` **compartment**: `string`

#### Defined in

[src/interfaces/persistence.ts:11](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L11)

___

### lookupKeys

• **lookupKeys**: `string`[]

#### Defined in

[src/interfaces/persistence.ts:12](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L12)

___

### sortKeys

• `Optional` **sortKeys**: `string`[]

#### Defined in

[src/interfaces/persistence.ts:13](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L13)
