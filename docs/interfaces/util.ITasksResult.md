[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [util](../modules/util.md) / ITasksResult

# Interface: ITasksResult<TaskResult, FinalResult\>

[util](../modules/util.md).ITasksResult

## Type parameters

| Name |
| :------ |
| `TaskResult` |
| `FinalResult` |

## Table of contents

### Properties

- [result](util.ITasksResult.md#result)
- [results](util.ITasksResult.md#results)
- [status](util.ITasksResult.md#status)

## Properties

### result

• `Optional` **result**: `FinalResult`

#### Defined in

[src/util.ts:105](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/util.ts#L105)

___

### results

• **results**: [`ITaskResult`](util.ITaskResult.md)<`TaskResult`\>[]

#### Defined in

[src/util.ts:104](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/util.ts#L104)

___

### status

• **status**: ``"completed"`` \| ``"timeout"`` \| ``"aborted"``

#### Defined in

[src/util.ts:106](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/util.ts#L106)
