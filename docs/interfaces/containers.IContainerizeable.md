[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [containers](../modules/containers.md) / IContainerizeable

# Interface: IContainerizeable<A, B\>

[containers](../modules/containers.md).IContainerizeable

## Type parameters

| Name |
| :------ |
| `A` |
| `B` |

## Table of contents

### Methods

- [\_cont](containers.IContainerizeable.md#_cont)

## Methods

### \_cont

▸ **_cont**(): [`IContainer`](containers.IContainer.md)<`A`, `B`\>

#### Returns

[`IContainer`](containers.IContainer.md)<`A`, `B`\>

#### Defined in

[src/containers.ts:60](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L60)
