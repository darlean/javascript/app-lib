[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [types](../modules/types.md) / ISettings

# Interface: ISettings

[types](../modules/types.md).ISettings

Interface for accessing settings. Settings can be provided as "."-delimited string,
or as an array of strings.

## Table of contents

### Methods

- [get](types.ISettings.md#get)
- [has](types.ISettings.md#has)

## Methods

### get

▸ **get**<`T`\>(`setting`, `defaultValue?`): `T`

Get a setting. Trows an exception when the setting is not
present, unless defaultValue is not unassigned, which is then returned.

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `setting` | `string` \| `string`[] |
| `defaultValue?` | `T` |

#### Returns

`T`

#### Defined in

[src/types.ts:385](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L385)

___

### has

▸ **has**(`setting`): `boolean`

Returns whether the setting exists.

#### Parameters

| Name | Type |
| :------ | :------ |
| `setting` | `string` \| `string`[] |

#### Returns

`boolean`

#### Defined in

[src/types.ts:390](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L390)
