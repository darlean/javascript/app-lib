[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [types](../modules/types.md) / IAppBinding

# Interface: IAppBinding

[types](../modules/types.md).IAppBinding

## Table of contents

### Properties

- [actorId](types.IAppBinding.md#actorid)
- [appIds](types.IAppBinding.md#appids)
- [break](types.IAppBinding.md#break)
- [matching](types.IAppBinding.md#matching)

## Properties

### actorId

• **actorId**: `string`[]

#### Defined in

[src/types.ts:161](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L161)

___

### appIds

• **appIds**: `string`[]

#### Defined in

[src/types.ts:163](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L163)

___

### break

• **break**: `boolean`

#### Defined in

[src/types.ts:164](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L164)

___

### matching

• **matching**: ``"exact"`` \| ``"prefix"``

#### Defined in

[src/types.ts:162](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L162)
