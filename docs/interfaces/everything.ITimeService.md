[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / ITimeService

# Interface: ITimeService

[everything](../modules/everything.md).ITimeService

## Table of contents

### Methods

- [clusterTicks](everything.ITimeService.md#clusterticks)
- [clusterTime](everything.ITimeService.md#clustertime)
- [nodeTicks](everything.ITimeService.md#nodeticks)
- [nodeTime](everything.ITimeService.md#nodetime)
- [noop](everything.ITimeService.md#noop)
- [sleep](everything.ITimeService.md#sleep)

## Methods

### clusterTicks

▸ **clusterTicks**(): `Promise`<`number`\>

Returns the millisecond-ticks for the cluster. The value may jump forward, but it is guaranteed
to never decrease.

#### Returns

`Promise`<`number`\>

#### Defined in

[src/services/time.ts:21](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/time.ts#L21)

___

### clusterTime

▸ **clusterTime**(): `Promise`<`number`\>

Returns the current time that the cluster agrees upon.

#### Returns

`Promise`<`number`\>

#### Defined in

[src/services/time.ts:15](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/time.ts#L15)

___

### nodeTicks

▸ **nodeTicks**(): `Promise`<`number`\>

Returns the millisecond-ticks on the current node.

#### Returns

`Promise`<`number`\>

#### Defined in

[src/services/time.ts:10](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/time.ts#L10)

___

### nodeTime

▸ **nodeTime**(): `Promise`<`number`\>

Returns the current time as reported by the machine on which the current node is running.

#### Returns

`Promise`<`number`\>

#### Defined in

[src/services/time.ts:5](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/time.ts#L5)

___

### noop

▸ **noop**(): `Promise`<`void`\>

Performs a 'noop', which means that the NodeJS event loop gets the chance to process pending events.
The noop function should be called at regular moments (preferably every few milliseconds) during
long-running synchronous operations to keep the application responsive and to avoid actor locks from
expiration (which could undermine the guarantee that no more than 1 instance of a certain actor is
active at any given time within the system).

#### Returns

`Promise`<`void`\>

#### Defined in

[src/services/time.ts:38](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/time.ts#L38)

___

### sleep

▸ **sleep**(`ms`): `Promise`<`void`\>

Sleeps for a certain amount of milliseconds. While sleeping, other events continue to be processed
by the NodeJS event loop. However, when an action lock is in place, that lock will continue to be
respected, so while sleeping from within an action handler method with exclusive locking, no other
actions can and will be performed by the same actor instance.

#### Parameters

| Name | Type |
| :------ | :------ |
| `ms` | `number` |

#### Returns

`Promise`<`void`\>

#### Defined in

[src/services/time.ts:29](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/time.ts#L29)
