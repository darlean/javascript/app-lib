[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / IQueryResultItem

# Interface: IQueryResultItem<ObjectType\>

[everything](../modules/everything.md).IQueryResultItem

## Type parameters

| Name |
| :------ |
| `ObjectType` |

## Table of contents

### Properties

- [lookupKeys](everything.IQueryResultItem.md#lookupkeys)
- [sortKeys](everything.IQueryResultItem.md#sortkeys)
- [value](everything.IQueryResultItem.md#value)

## Properties

### lookupKeys

• **lookupKeys**: `string`[]

#### Defined in

[src/services/persistence.ts:42](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L42)

___

### sortKeys

• **sortKeys**: `string`[]

#### Defined in

[src/services/persistence.ts:43](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L43)

___

### value

• **value**: [`IAttachment`](structs.IAttachment.md)<`ObjectType`\>

#### Defined in

[src/services/persistence.ts:44](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L44)
