[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / IActionOptions

# Interface: IActionOptions<Data\>

[everything](../modules/everything.md).IActionOptions

Contains the options when performing a sub-action from the implementation code of a parent-action via
[IBaseActionContext.performAction](types.IBaseActionContext.md#performaction) or [IBaseActionContext.fireAction](types.IBaseActionContext.md#fireaction).

## Type parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `Data` | extends [`AnObject`](../modules/containers.md#anobject) \| `void` | The (object) type of the action input data, or `void` when the caller does not want to pass any data. |

## Table of contents

### Properties

- [actionHopsRemaining](everything.IActionOptions.md#actionhopsremaining)
- [actionName](everything.IActionOptions.md#actionname)
- [actorId](everything.IActionOptions.md#actorid)
- [actorType](everything.IActionOptions.md#actortype)
- [data](everything.IActionOptions.md#data)
- [hops](everything.IActionOptions.md#hops)
- [networkHopsRemaining](everything.IActionOptions.md#networkhopsremaining)
- [receivers](everything.IActionOptions.md#receivers)
- [retryCount](everything.IActionOptions.md#retrycount)
- [ttl](everything.IActionOptions.md#ttl)

## Properties

### actionHopsRemaining

• `Optional` **actionHopsRemaining**: `number`

The maximum length of the action chain that any sub-actions (and their suqsequent sub-actions, et cetera) may span.
When not provided, the framework sets this to a reasonable value. Usually, there is no need to specify this explicitly.
This option is used to prevent infinite action loops of A -> B -> C -> A -> B -> C, et cetera.

#### Defined in

[src/services/performing.ts:59](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/performing.ts#L59)

___

### actionName

• `Optional` **actionName**: `string`

The name of the action to be performed. When not provided or empty, no actual action is invoked. This can be useful
to ensure that the actor is active, without actually invoking an action on the actor.

#### Defined in

[src/services/performing.ts:30](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/performing.ts#L30)

___

### actorId

• `Optional` **actorId**: `string`[]

The actor id that should perform the action. When not provided (`undefined` or not present), defaults
to the actorId of the parent action.

#### Defined in

[src/services/performing.ts:24](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/performing.ts#L24)

___

### actorType

• `Optional` **actorType**: `string`

The actor type that should perform the action. When not provided or empty, defaults to the
actor type of the parent action.

#### Defined in

[src/services/performing.ts:18](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/performing.ts#L18)

___

### data

• `Optional` **data**: `Data` extends `Object` ? [`StructLike`](../modules/structs.md#structlike)<`Data`\> : `undefined`

The data to be passed to the actor. This should either be an object (or a [Struct](../modules/everything.md#struct), which also is an object) or
`undefined` or not present.

#### Defined in

[src/services/performing.ts:36](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/performing.ts#L36)

___

### hops

• `Optional` **hops**: [`IActionHop`](everything.IActionHop.md)[]

List of action hops up to this action. Every sub-action that is being performed adds one entry to the hops of the
parent action. This list of hops is used by the framework to detect reentrancy.

#### Defined in

[src/services/performing.ts:91](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/performing.ts#L91)

___

### networkHopsRemaining

• `Optional` **networkHopsRemaining**: `number`

The number of network hops this action is allowed to visit on its way to its final destination. When not present,
the framework fills this in with a reasonable value. Usually, there is no need to specify this explicitly.

#### Defined in

[src/services/performing.ts:52](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/performing.ts#L52)

___

### receivers

• `Optional` **receivers**: `string`[]

An optional list of nodenames by which the action could be performed. When present, the framework skips the
dispatching logic that normally determines on which nodes a certain actor is active or could become active.
This is a performance optimization for specialized use cases (like Darlean helper actors themselves), there
should be no need to use this field for regular actors.

#### Defined in

[src/services/performing.ts:46](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/performing.ts#L46)

___

### retryCount

• `Optional` **retryCount**: `number`

The number of retries when a network or process error occurs. Darlean retries actions that result in a
network or processing error several times with exponentially increasing time intervals. When, after the retries,
still an error occurs, the error is finally thrown.

The retry mechanism only works for network and process errors. The 3rd category of errors, actor errors, are not
retried, but actor errors are immediately thrown to the calling code. Actor errors are errors that occur within
the actual actor implementation ([activate](../classes/actorfactory.BaseActor.md#activate), [deactivate](../classes/actorfactory.BaseActor.md#deactivate) and action
handler methods). Actor errors are normally used to indicate to the caller that something went wrong during the
performing of the action. It is up to the caller to interpret this error, and to decide whether to retry or not.
It is not the responsibility of the framework to automatically perform retries on actor errors.

The framework already sets a decent default value, that corresponds to about 5 retries with a total span time of
several seconds.

#### Defined in

[src/services/performing.ts:76](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/performing.ts#L76)

___

### ttl

• `Optional` **ttl**: `number`

The maximum time to live for this request. After the ttl expires, no more network and/or process retries are performed
anymore. Actors are expected to read and respect the provided ttl value, but the framework makes no guarantees on that.
What the framework *does* guarantee is that after the ttl expires, the call to performAction is immediately aborted and
a `TIMEOUT` error is thrown. However, on the background, network and process retries as well as the execution of the
action by the actions actor may continue.

#### Defined in

[src/services/performing.ts:85](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/performing.ts#L85)
