[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / [interface\_persistence](../modules/everything.interface_persistence.md) / IQueryResponseItem

# Interface: IQueryResponseItem

[everything](../modules/everything.md).[interface_persistence](../modules/everything.interface_persistence.md).IQueryResponseItem

## Table of contents

### Properties

- [lookupKeys](everything.interface_persistence.IQueryResponseItem.md#lookupkeys)
- [sortKeys](everything.interface_persistence.IQueryResponseItem.md#sortkeys)
- [value](everything.interface_persistence.IQueryResponseItem.md#value)

## Properties

### lookupKeys

• **lookupKeys**: `string`[]

#### Defined in

[src/interfaces/persistence.ts:43](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L43)

___

### sortKeys

• **sortKeys**: `string`[]

#### Defined in

[src/interfaces/persistence.ts:44](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L44)

___

### value

• **value**: [`IAttachmentRef`](structs.IAttachmentRef.md)

#### Defined in

[src/interfaces/persistence.ts:45](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L45)
