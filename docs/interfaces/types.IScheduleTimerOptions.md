[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [types](../modules/types.md) / IScheduleTimerOptions

# Interface: IScheduleTimerOptions<Input\>

[types](../modules/types.md).IScheduleTimerOptions

## Type parameters

| Name |
| :------ |
| `Input` |

## Table of contents

### Properties

- [actionName](types.IScheduleTimerOptions.md#actionname)
- [data](types.IScheduleTimerOptions.md#data)
- [delay](types.IScheduleTimerOptions.md#delay)
- [interval](types.IScheduleTimerOptions.md#interval)
- [maxCount](types.IScheduleTimerOptions.md#maxcount)
- [name](types.IScheduleTimerOptions.md#name)

## Properties

### actionName

• **actionName**: `string`

#### Defined in

[src/types.ts:200](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L200)

___

### data

• `Optional` **data**: `Input`

#### Defined in

[src/types.ts:201](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L201)

___

### delay

• `Optional` **delay**: `number`

#### Defined in

[src/types.ts:203](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L203)

___

### interval

• **interval**: `number`

#### Defined in

[src/types.ts:202](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L202)

___

### maxCount

• `Optional` **maxCount**: `number`

#### Defined in

[src/types.ts:204](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L204)

___

### name

• **name**: `string`

#### Defined in

[src/types.ts:199](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L199)
