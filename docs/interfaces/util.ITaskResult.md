[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [util](../modules/util.md) / ITaskResult

# Interface: ITaskResult<Result\>

[util](../modules/util.md).ITaskResult

## Type parameters

| Name |
| :------ |
| `Result` |

## Table of contents

### Properties

- [error](util.ITaskResult.md#error)
- [result](util.ITaskResult.md#result)

## Properties

### error

• `Optional` **error**: `unknown`

#### Defined in

[src/util.ts:111](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/util.ts#L111)

___

### result

• `Optional` **result**: `Result`

#### Defined in

[src/util.ts:110](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/util.ts#L110)
