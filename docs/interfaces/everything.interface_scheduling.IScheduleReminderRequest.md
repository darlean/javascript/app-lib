[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / [interface\_scheduling](../modules/everything.interface_scheduling.md) / IScheduleReminderRequest

# Interface: IScheduleReminderRequest

[everything](../modules/everything.md).[interface_scheduling](../modules/everything.interface_scheduling.md).IScheduleReminderRequest

## Table of contents

### Properties

- [callback](everything.interface_scheduling.IScheduleReminderRequest.md#callback)
- [delay](everything.interface_scheduling.IScheduleReminderRequest.md#delay)
- [id](everything.interface_scheduling.IScheduleReminderRequest.md#id)
- [interval](everything.interface_scheduling.IScheduleReminderRequest.md#interval)
- [moment](everything.interface_scheduling.IScheduleReminderRequest.md#moment)
- [repeatCount](everything.interface_scheduling.IScheduleReminderRequest.md#repeatcount)

## Properties

### callback

• **callback**: [`IPerformActionReq`](everything.interface_performing.IPerformActionReq.md)

#### Defined in

[src/interfaces/scheduling.ts:15](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/scheduling.ts#L15)

___

### delay

• `Optional` **delay**: `number`

#### Defined in

[src/interfaces/scheduling.ts:11](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/scheduling.ts#L11)

___

### id

• **id**: `string`[]

#### Defined in

[src/interfaces/scheduling.ts:10](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/scheduling.ts#L10)

___

### interval

• `Optional` **interval**: `number`

#### Defined in

[src/interfaces/scheduling.ts:12](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/scheduling.ts#L12)

___

### moment

• `Optional` **moment**: `number`

#### Defined in

[src/interfaces/scheduling.ts:14](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/scheduling.ts#L14)

___

### repeatCount

• `Optional` **repeatCount**: `number`

#### Defined in

[src/interfaces/scheduling.ts:13](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/scheduling.ts#L13)
