[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / [interface\_persistence](../modules/everything.interface_persistence.md) / IQueryRequest

# Interface: IQueryRequest

[everything](../modules/everything.md).[interface_persistence](../modules/everything.interface_persistence.md).IQueryRequest

## Table of contents

### Properties

- [compartment](everything.interface_persistence.IQueryRequest.md#compartment)
- [end](everything.interface_persistence.IQueryRequest.md#end)
- [includeValues](everything.interface_persistence.IQueryRequest.md#includevalues)
- [limit](everything.interface_persistence.IQueryRequest.md#limit)
- [lookupKeys](everything.interface_persistence.IQueryRequest.md#lookupkeys)
- [order](everything.interface_persistence.IQueryRequest.md#order)
- [start](everything.interface_persistence.IQueryRequest.md#start)

## Properties

### compartment

• `Optional` **compartment**: `string`

#### Defined in

[src/interfaces/persistence.ts:33](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L33)

___

### end

• `Optional` **end**: [`IQueryRangePart`](everything.interface_persistence.IQueryRangePart.md)

#### Defined in

[src/interfaces/persistence.ts:36](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L36)

___

### includeValues

• `Optional` **includeValues**: `boolean`

#### Defined in

[src/interfaces/persistence.ts:39](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L39)

___

### limit

• **limit**: `number`

#### Defined in

[src/interfaces/persistence.ts:37](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L37)

___

### lookupKeys

• **lookupKeys**: `string`[]

#### Defined in

[src/interfaces/persistence.ts:34](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L34)

___

### order

• `Optional` **order**: ``"ascending"`` \| ``"descending"``

#### Defined in

[src/interfaces/persistence.ts:38](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L38)

___

### start

• `Optional` **start**: [`IQueryRangePart`](everything.interface_persistence.IQueryRangePart.md)

#### Defined in

[src/interfaces/persistence.ts:35](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L35)
