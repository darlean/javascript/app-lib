[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / ILogEvent

# Interface: ILogEvent

[everything](../modules/everything.md).ILogEvent

## Table of contents

### Properties

- [level](everything.ILogEvent.md#level)
- [msg](everything.ILogEvent.md#msg)

### Methods

- [args](everything.ILogEvent.md#args)
- [tags](everything.ILogEvent.md#tags)

## Properties

### level

• **level**: `string`

#### Defined in

[src/logging.ts:44](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/logging.ts#L44)

___

### msg

• **msg**: `string`

#### Defined in

[src/logging.ts:45](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/logging.ts#L45)

## Methods

### args

▸ `Optional` **args**(): `Object`

#### Returns

`Object`

#### Defined in

[src/logging.ts:46](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/logging.ts#L46)

___

### tags

▸ `Optional` **tags**(): `Object`

#### Returns

`Object`

#### Defined in

[src/logging.ts:47](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/logging.ts#L47)
