[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / ILogger

# Interface: ILogger

[everything](../modules/everything.md).ILogger

## Implemented by

- [`Logger`](../classes/everything.Logger.md)

## Table of contents

### Methods

- [debug](everything.ILogger.md#debug)
- [deep](everything.ILogger.md#deep)
- [error](everything.ILogger.md#error)
- [finish](everything.ILogger.md#finish)
- [getChildLogger](everything.ILogger.md#getchildlogger)
- [info](everything.ILogger.md#info)
- [log](everything.ILogger.md#log)
- [verbose](everything.ILogger.md#verbose)
- [warning](everything.ILogger.md#warning)

## Methods

### debug

▸ **debug**(`msg`, `args?`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `msg` | `string` |
| `args?` | () => { [key: string]: `unknown`;  } |

#### Returns

`void`

#### Defined in

[src/logging.ts:29](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/logging.ts#L29)

___

### deep

▸ **deep**(`msg`, `args?`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `msg` | `string` |
| `args?` | () => { [key: string]: `unknown`;  } |

#### Returns

`void`

#### Defined in

[src/logging.ts:30](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/logging.ts#L30)

___

### error

▸ **error**(`msg`, `args?`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `msg` | `string` |
| `args?` | () => { [key: string]: `unknown`;  } |

#### Returns

`void`

#### Defined in

[src/logging.ts:25](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/logging.ts#L25)

___

### finish

▸ **finish**(): `void`

#### Returns

`void`

#### Defined in

[src/logging.ts:33](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/logging.ts#L33)

___

### getChildLogger

▸ **getChildLogger**(`scope`, `id?`): [`ILogger`](everything.ILogger.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `scope` | `string` |
| `id?` | `string` \| `string`[] |

#### Returns

[`ILogger`](everything.ILogger.md)

#### Defined in

[src/logging.ts:32](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/logging.ts#L32)

___

### info

▸ **info**(`msg`, `args?`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `msg` | `string` |
| `args?` | () => { [key: string]: `unknown`;  } |

#### Returns

`void`

#### Defined in

[src/logging.ts:27](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/logging.ts#L27)

___

### log

▸ **log**(`level`, `msg`, `args?`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `level` | `string` |
| `msg` | `string` |
| `args?` | () => { [key: string]: `unknown`;  } |

#### Returns

`void`

#### Defined in

[src/logging.ts:24](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/logging.ts#L24)

___

### verbose

▸ **verbose**(`msg`, `args?`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `msg` | `string` |
| `args?` | () => { [key: string]: `unknown`;  } |

#### Returns

`void`

#### Defined in

[src/logging.ts:28](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/logging.ts#L28)

___

### warning

▸ **warning**(`msg`, `args?`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `msg` | `string` |
| `args?` | () => { [key: string]: `unknown`;  } |

#### Returns

`void`

#### Defined in

[src/logging.ts:26](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/logging.ts#L26)
