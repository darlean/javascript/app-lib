[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [actorfactory](../modules/actorfactory.md) / IBaseActorFactoryOptions

# Interface: IBaseActorFactoryOptions<Options\>

[actorfactory](../modules/actorfactory.md).IBaseActorFactoryOptions

Instructs the actor factory on how to generate actor instances.

## Type parameters

| Name | Type |
| :------ | :------ |
| `Options` | extends [`AnObject`](../modules/containers.md#anobject) |

## Table of contents

### Properties

- [actorOptions](actorfactory.IBaseActorFactoryOptions.md#actoroptions)
- [capacity](actorfactory.IBaseActorFactoryOptions.md#capacity)
- [clazz](actorfactory.IBaseActorFactoryOptions.md#clazz)

### Methods

- [onAppStart](actorfactory.IBaseActorFactoryOptions.md#onappstart)
- [onAppStop](actorfactory.IBaseActorFactoryOptions.md#onappstop)

## Properties

### actorOptions

• `Optional` **actorOptions**: `Options` \| (`id`: `string`[]) => `Options`

A callback function that is invoked every time a new actor instance is created. The callback
function receives the actor id as input. A typical implementation would read the settings for
the specific actor from a settings file (see [ISettings](types.ISettings.md)) and return the relevant settings
as an options object.

Using this actorOptions mechanism is recommended over the alternative mechanism of having actors
fetch their own settings from a configuration file. The actorOptions mechanism provides a nice
[inversion of control](https://en.wikipedia.org/wiki/Inversion_of_control) mechanism that removes the need for actor implementation to be dependent on
the format and contents of settings files.

#### Defined in

[src/actorfactory.ts:432](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L432)

___

### capacity

• **capacity**: `number`

The maximum number of active actor instances before the oldest instance are automatically deactivated
to make room for new instances. Set to 0 to disable actor instance recycling (but be aware for out of
memory issues when actions on actors for a lot of different id's are being performed).

#### Defined in

[src/actorfactory.ts:439](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L439)

___

### clazz

• **clazz**: typeof [`BaseActor`](../classes/actorfactory.BaseActor.md)

The class (a subclass of [BaseActor](../classes/actorfactory.BaseActor.md)) that holds the actor implementation.

#### Defined in

[src/actorfactory.ts:419](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L419)

## Methods

### onAppStart

▸ `Optional` **onAppStart**(`runtime`): `Promise`<`void`\>

Callback function that is invoked when the Darlean application is running and all local actor types
are registered. Typically used to launch 'persistent' actors (like those that open a listening TCP socket)
or to set periodic timers or reminders.

Note that although local actor types (actor types for the current application) are registered, it is not
guaranteed that also other actor types (from other applications) are already available. Implementation code
must be robust against that.

The `runtime` parameter can be used to invoke actions on actors or to schedule timers and/or reminders.

#### Parameters

| Name | Type |
| :------ | :------ |
| `runtime` | [`IRuntime`](types.IRuntime.md) |

#### Returns

`Promise`<`void`\>

#### Defined in

[src/actorfactory.ts:452](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L452)

___

### onAppStop

▸ `Optional` **onAppStop**(`runtime`): `Promise`<`void`\>

Callback function that is invoked just before the Darlean application is stopping.

#### Parameters

| Name | Type |
| :------ | :------ |
| `runtime` | [`IRuntime`](types.IRuntime.md) |

#### Returns

`Promise`<`void`\>

#### Defined in

[src/actorfactory.ts:456](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L456)
