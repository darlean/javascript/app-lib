[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / [interface\_locking](../modules/everything.interface_locking.md) / IAcquireLockRequest

# Interface: IAcquireLockRequest

[everything](../modules/everything.md).[interface_locking](../modules/everything.interface_locking.md).IAcquireLockRequest

## Table of contents

### Properties

- [id](everything.interface_locking.IAcquireLockRequest.md#id)
- [requester](everything.interface_locking.IAcquireLockRequest.md#requester)
- [retryTime](everything.interface_locking.IAcquireLockRequest.md#retrytime)
- [singleStage](everything.interface_locking.IAcquireLockRequest.md#singlestage)
- [ttl](everything.interface_locking.IAcquireLockRequest.md#ttl)

## Properties

### id

• **id**: `string`[]

#### Defined in

[src/interfaces/locking.ts:13](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/locking.ts#L13)

___

### requester

• **requester**: `string`

#### Defined in

[src/interfaces/locking.ts:14](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/locking.ts#L14)

___

### retryTime

• `Optional` **retryTime**: `number`

#### Defined in

[src/interfaces/locking.ts:16](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/locking.ts#L16)

___

### singleStage

• `Optional` **singleStage**: `boolean`

#### Defined in

[src/interfaces/locking.ts:17](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/locking.ts#L17)

___

### ttl

• **ttl**: `number`

#### Defined in

[src/interfaces/locking.ts:15](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/locking.ts#L15)
