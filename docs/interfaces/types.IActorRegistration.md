[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [types](../modules/types.md) / IActorRegistration

# Interface: IActorRegistration

[types](../modules/types.md).IActorRegistration

## Implemented by

- [`ActorFactory`](../classes/actorfactory.ActorFactory.md)

## Table of contents

### Methods

- [getRegisterOptions](types.IActorRegistration.md#getregisteroptions)

## Methods

### getRegisterOptions

▸ **getRegisterOptions**(): [`IRegisterActorOptions`](types.IRegisterActorOptions.md)

#### Returns

[`IRegisterActorOptions`](types.IRegisterActorOptions.md)

#### Defined in

[src/types.ts:16](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L16)
