[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / [interface\_performing](../modules/everything.interface_performing.md) / IPerformActionResp

# Interface: IPerformActionResp

[everything](../modules/everything.md).[interface_performing](../modules/everything.interface_performing.md).IPerformActionResp

## Table of contents

### Properties

- [actorError](everything.interface_performing.IPerformActionResp.md#actorerror)
- [appHints](everything.interface_performing.IPerformActionResp.md#apphints)
- [data](everything.interface_performing.IPerformActionResp.md#data)
- [processError](everything.interface_performing.IPerformActionResp.md#processerror)

## Properties

### actorError

• `Optional` **actorError**: [`IRpcError`](types.IRpcError.md)

#### Defined in

[src/interfaces/performing.ts:28](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/performing.ts#L28)

___

### appHints

• `Optional` **appHints**: `string`[]

#### Defined in

[src/interfaces/performing.ts:26](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/performing.ts#L26)

___

### data

• `Optional` **data**: [`IAttachmentRef`](structs.IAttachmentRef.md)

#### Defined in

[src/interfaces/performing.ts:29](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/performing.ts#L29)

___

### processError

• `Optional` **processError**: [`IRpcError`](types.IRpcError.md)

#### Defined in

[src/interfaces/performing.ts:27](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/performing.ts#L27)
