[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / [interface\_performing](../modules/everything.interface_performing.md) / IPerformActionReq

# Interface: IPerformActionReq

[everything](../modules/everything.md).[interface_performing](../modules/everything.interface_performing.md).IPerformActionReq

## Table of contents

### Properties

- [actionHopsRemaining](everything.interface_performing.IPerformActionReq.md#actionhopsremaining)
- [actionName](everything.interface_performing.IPerformActionReq.md#actionname)
- [actorId](everything.interface_performing.IPerformActionReq.md#actorid)
- [actorType](everything.interface_performing.IPerformActionReq.md#actortype)
- [data](everything.interface_performing.IPerformActionReq.md#data)
- [hops](everything.interface_performing.IPerformActionReq.md#hops)
- [processErrorEnvelopes](everything.interface_performing.IPerformActionReq.md#processerrorenvelopes)
- [returnEnvelopes](everything.interface_performing.IPerformActionReq.md#returnenvelopes)

## Properties

### actionHopsRemaining

• **actionHopsRemaining**: `number`

#### Defined in

[src/interfaces/performing.ts:14](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/performing.ts#L14)

___

### actionName

• `Optional` **actionName**: `string`

#### Defined in

[src/interfaces/performing.ts:12](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/performing.ts#L12)

___

### actorId

• `Optional` **actorId**: `string`[]

#### Defined in

[src/interfaces/performing.ts:11](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/performing.ts#L11)

___

### actorType

• `Optional` **actorType**: `string`

#### Defined in

[src/interfaces/performing.ts:10](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/performing.ts#L10)

___

### data

• **data**: [`IAttachmentRef`](structs.IAttachmentRef.md)

#### Defined in

[src/interfaces/performing.ts:13](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/performing.ts#L13)

___

### hops

• `Optional` **hops**: [`IActionHop`](everything.interface_performing.IActionHop.md)[]

#### Defined in

[src/interfaces/performing.ts:18](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/performing.ts#L18)

___

### processErrorEnvelopes

• `Optional` **processErrorEnvelopes**: `IEnvelopeCallback`[]

#### Defined in

[src/interfaces/performing.ts:17](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/performing.ts#L17)

___

### returnEnvelopes

• `Optional` **returnEnvelopes**: `IEnvelopeCallback`[]

#### Defined in

[src/interfaces/performing.ts:15](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/performing.ts#L15)
