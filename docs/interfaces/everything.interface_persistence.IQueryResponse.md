[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / [interface\_persistence](../modules/everything.interface_persistence.md) / IQueryResponse

# Interface: IQueryResponse

[everything](../modules/everything.md).[interface_persistence](../modules/everything.interface_persistence.md).IQueryResponse

## Table of contents

### Properties

- [items](everything.interface_persistence.IQueryResponse.md#items)

## Properties

### items

• **items**: [`IQueryResponseItem`](everything.interface_persistence.IQueryResponseItem.md)[]

#### Defined in

[src/interfaces/persistence.ts:49](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L49)
