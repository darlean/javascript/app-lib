[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / IQueryResult

# Interface: IQueryResult<ObjectType\>

[everything](../modules/everything.md).IQueryResult

## Type parameters

| Name |
| :------ |
| `ObjectType` |

## Table of contents

### Properties

- [items](everything.IQueryResult.md#items)

## Properties

### items

• **items**: [`IQueryResultItem`](everything.IQueryResultItem.md)<`ObjectType`\>[]

#### Defined in

[src/services/persistence.ts:48](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L48)
