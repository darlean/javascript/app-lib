[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / [interface\_persistence](../modules/everything.interface_persistence.md) / ILoadRequest

# Interface: ILoadRequest

[everything](../modules/everything.md).[interface_persistence](../modules/everything.interface_persistence.md).ILoadRequest

## Table of contents

### Properties

- [compartment](everything.interface_persistence.ILoadRequest.md#compartment)
- [lookupKeys](everything.interface_persistence.ILoadRequest.md#lookupkeys)
- [sortKeys](everything.interface_persistence.ILoadRequest.md#sortkeys)

## Properties

### compartment

• `Optional` **compartment**: `string`

#### Defined in

[src/interfaces/persistence.ts:17](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L17)

___

### lookupKeys

• **lookupKeys**: `string`[]

#### Defined in

[src/interfaces/persistence.ts:18](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L18)

___

### sortKeys

• `Optional` **sortKeys**: `string`[]

#### Defined in

[src/interfaces/persistence.ts:19](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/persistence.ts#L19)
