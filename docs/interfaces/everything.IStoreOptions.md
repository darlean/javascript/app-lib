[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / IStoreOptions

# Interface: IStoreOptions

[everything](../modules/everything.md).IStoreOptions

## Table of contents

### Properties

- [compartment](everything.IStoreOptions.md#compartment)
- [lookupKeys](everything.IStoreOptions.md#lookupkeys)
- [sortKeys](everything.IStoreOptions.md#sortkeys)
- [value](everything.IStoreOptions.md#value)

## Properties

### compartment

• `Optional` **compartment**: `string`

#### Defined in

[src/services/persistence.ts:4](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L4)

___

### lookupKeys

• **lookupKeys**: `string`[]

#### Defined in

[src/services/persistence.ts:5](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L5)

___

### sortKeys

• `Optional` **sortKeys**: `string`[]

#### Defined in

[src/services/persistence.ts:6](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L6)

___

### value

• **value**: [`AttachmentLike0`](../modules/structs.md#attachmentlike0)

#### Defined in

[src/services/persistence.ts:7](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L7)
