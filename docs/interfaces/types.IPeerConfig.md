[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [types](../modules/types.md) / IPeerConfig

# Interface: IPeerConfig

[types](../modules/types.md).IPeerConfig

## Table of contents

### Properties

- [address](types.IPeerConfig.md#address)
- [id](types.IPeerConfig.md#id)

## Properties

### address

• **address**: `string`

#### Defined in

[src/types.ts:373](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L373)

___

### id

• **id**: `string`

#### Defined in

[src/types.ts:372](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L372)
