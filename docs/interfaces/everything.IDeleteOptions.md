[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / IDeleteOptions

# Interface: IDeleteOptions

[everything](../modules/everything.md).IDeleteOptions

## Table of contents

### Properties

- [compartment](everything.IDeleteOptions.md#compartment)
- [lookupKeys](everything.IDeleteOptions.md#lookupkeys)
- [sortKeys](everything.IDeleteOptions.md#sortkeys)

## Properties

### compartment

• `Optional` **compartment**: `string`

#### Defined in

[src/services/persistence.ts:11](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L11)

___

### lookupKeys

• **lookupKeys**: `string`[]

#### Defined in

[src/services/persistence.ts:12](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L12)

___

### sortKeys

• `Optional` **sortKeys**: `string`[]

#### Defined in

[src/services/persistence.ts:13](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L13)
