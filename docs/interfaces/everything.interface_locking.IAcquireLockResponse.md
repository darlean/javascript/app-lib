[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / [interface\_locking](../modules/everything.interface_locking.md) / IAcquireLockResponse

# Interface: IAcquireLockResponse

[everything](../modules/everything.md).[interface_locking](../modules/everything.interface_locking.md).IAcquireLockResponse

## Table of contents

### Properties

- [duration](everything.interface_locking.IAcquireLockResponse.md#duration)
- [holders](everything.interface_locking.IAcquireLockResponse.md#holders)

## Properties

### duration

• **duration**: `number`

#### Defined in

[src/interfaces/locking.ts:21](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/locking.ts#L21)

___

### holders

• **holders**: `string`[]

#### Defined in

[src/interfaces/locking.ts:22](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/locking.ts#L22)
