[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [types](../modules/types.md) / IAttributes

# Interface: IAttributes

[types](../modules/types.md).IAttributes

## Indexable

▪ [key: `string`]: `unknown`
