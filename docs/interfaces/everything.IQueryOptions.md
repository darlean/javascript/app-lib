[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / IQueryOptions

# Interface: IQueryOptions

[everything](../modules/everything.md).IQueryOptions

## Table of contents

### Properties

- [compartment](everything.IQueryOptions.md#compartment)
- [end](everything.IQueryOptions.md#end)
- [includeValues](everything.IQueryOptions.md#includevalues)
- [limit](everything.IQueryOptions.md#limit)
- [lookupKeys](everything.IQueryOptions.md#lookupkeys)
- [order](everything.IQueryOptions.md#order)
- [start](everything.IQueryOptions.md#start)

## Properties

### compartment

• `Optional` **compartment**: `string`

#### Defined in

[src/services/persistence.ts:32](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L32)

___

### end

• `Optional` **end**: [`IQueryRangePart`](everything.IQueryRangePart.md)

#### Defined in

[src/services/persistence.ts:35](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L35)

___

### includeValues

• `Optional` **includeValues**: `boolean`

#### Defined in

[src/services/persistence.ts:38](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L38)

___

### limit

• **limit**: `number`

#### Defined in

[src/services/persistence.ts:36](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L36)

___

### lookupKeys

• **lookupKeys**: `string`[]

#### Defined in

[src/services/persistence.ts:33](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L33)

___

### order

• `Optional` **order**: ``"ascending"`` \| ``"descending"``

#### Defined in

[src/services/persistence.ts:37](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L37)

___

### start

• `Optional` **start**: [`IQueryRangePart`](everything.IQueryRangePart.md)

#### Defined in

[src/services/persistence.ts:34](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/services/persistence.ts#L34)
