[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [types](../modules/types.md) / IActorCreateOptions

# Interface: IActorCreateOptions

[types](../modules/types.md).IActorCreateOptions

## Table of contents

### Properties

- [id](types.IActorCreateOptions.md#id)
- [logger](types.IActorCreateOptions.md#logger)

## Properties

### id

• **id**: `string`[]

#### Defined in

[src/types.ts:11](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L11)

___

### logger

• **logger**: [`ILogger`](everything.ILogger.md)

#### Defined in

[src/types.ts:12](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L12)
