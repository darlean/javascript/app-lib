[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [types](../modules/types.md) / IBaseActionContext

# Interface: IBaseActionContext

[types](../modules/types.md).IBaseActionContext

Provides an action executor with information about the current action request and
with methods to perform additional functionality like performing other actions,
scheduling timers and storing state.

Typically, one action context instance is created per action request. The action context instance
contains information that is specific to that action request, like the 'call chain' that is used to
properly handle reentrancy. As a result, *action contexts should only be used within the code that
receives the context. They should not be stored and used again at a later moment.*

## Hierarchy

- **`IBaseActionContext`**

  ↳ [`IActionContext`](actorfactory.IActionContext.md)

## Table of contents

### Properties

- [clusterId](types.IBaseActionContext.md#clusterid)
- [fullAppId](types.IBaseActionContext.md#fullappid)
- [hopsRemaining](types.IBaseActionContext.md#hopsremaining)
- [sessionId](types.IBaseActionContext.md#sessionid)

### Methods

- [fireAction](types.IBaseActionContext.md#fireaction)
- [getService](types.IBaseActionContext.md#getservice)
- [performAction](types.IBaseActionContext.md#performaction)
- [scheduleTimer](types.IBaseActionContext.md#scheduletimer)

## Properties

### clusterId

• **clusterId**: `string`

#### Defined in

[src/types.ts:219](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L219)

___

### fullAppId

• **fullAppId**: `string`

#### Defined in

[src/types.ts:220](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L220)

___

### hopsRemaining

• **hopsRemaining**: `number`

#### Defined in

[src/types.ts:221](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L221)

___

### sessionId

• **sessionId**: `string`

#### Defined in

[src/types.ts:218](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L218)

## Methods

### fireAction

▸ **fireAction**<`Input`\>(`action`): `void`

Fires a sub-action without waiting for the result.

Warning: Because fireAction returns immediately, errors that occur during performing of the sub-action are absorbed
and not reported back to the calling code.

In other aspects, this method behaves similar to [performAction](types.IBaseActionContext.md#performaction).

#### Type parameters

| Name |
| :------ |
| `Input` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `action` | [`IActionOptions`](everything.IActionOptions.md)<`Input`\> | The action to be fired. |

#### Returns

`void`

Nothing. Even does not throw errors when network, processing or actor errors occur.

#### Defined in

[src/types.ts:285](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L285)

___

### getService

▸ **getService**(`name`): `unknown`

Requests a certain service that can be used to perform useful functionality. Currently supported services are:
* `state`: Returns an [IPersistenceService](everything.IPersistenceService.md) instance that can be used to load or store actor state
* `scheduling`: Returns an [ISchedulingService](everything.ISchedulingService.md) instance that can be used to schedule reminders
* `time`: Returns an [ITimeService](everything.ITimeService.md) instance that gives access to the current node time (and in the future
  to the cluster time as well) and allows an actor to sleep and to pause very briefly to keep the application
  responsive during long-running synchronous operations.

See also the convenience methods [IActionContext.state](actorfactory.IActionContext.md#state), [IActionContext.time](actorfactory.IActionContext.md#time) and [IActionContext.scheduling](actorfactory.IActionContext.md#scheduling)
that allow easy access to the available services.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `name` | `string` | The name of the service. Currently, `state`, `scheduling` and `time` are supported values. |

#### Returns

`unknown`

An instance of a IXyzService interface that is specific to this contexts action.

#### Defined in

[src/types.ts:310](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L310)

___

### performAction

▸ **performAction**<`Input`, `Output`\>(`action`): `Promise`<`Output` extends `Object` ? [`Struct`](../modules/structs.md#struct)<`Output`\> : `void`\>

Performs a certain sub-action on the same or a different actor.

#### Type parameters

| Name | Type |
| :------ | :------ |
| `Input` | extends `void` \| `Object` |
| `Output` | extends `void` \| `Object` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `action` | [`IActionOptions`](everything.IActionOptions.md)<`Input`\> | The sub-action to be invoked. When `actorType` is not set, the sub-action is performed on the actor type of this context's action. When `actorId` is not set, the id is copied from this context's action.  When performing a sub-action via `performAction`, reentrancy information is added to the action request so that the subaction that is being performed can perform actions on the original actor without causing a deadlock.  Example of invoking a sub-action: ``` const result = await context.performAction<{a: number, b: number}, {sum: number}>({   actorType: 'MyCalculator',   actorId: [],   actionName: 'add',   data: {     a: 3,     b: 4   } }); console.log(result.sum);  // Prints 7 when MyCalculator.add is implemented properly ```  Performed sub-actions can also receive and return attachments: ``` const imageBuffer = await fs.readFile('image.jpg', "binary"); const result = await context.performAction<{factor: number, image: IBinaryAttachmentRef}, {upsampled: IBinaryAttachmentRef}>({   actorType: 'MyImageFilter',   actorId: [],   actionName: 'upsample',   data: struct( (attach) => ({     factor: 2.5,     image: attach(imageBuffer);   }) }); const upsampledBuffer = result._att(result.upsampled).binary(); await fs.writeFile('upsampled.jpg', upsampledBuffer); ```  It is recommended to type performAction with the `Input` and `Output` types. When an action does not require input and/or return output, use `void`: ``` await context.performAction<void, void>({    ... }); ``` |

#### Returns

`Promise`<`Output` extends `Object` ? [`Struct`](../modules/structs.md#struct)<`Output`\> : `void`\>

A [Struct](../modules/structs.md#struct) with the returned data (or throws an error when something goes wrong).

#### Defined in

[src/types.ts:271](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L271)

___

### scheduleTimer

▸ **scheduleTimer**<`Input`\>(`options`): [`ITimer`](types.ITimer.md)

Schedules a timer that fires once or repeatedly until the timer is explicitly cancelled or the actor
is deactivated.

#### Type parameters

| Name |
| :------ |
| `Input` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `options` | [`IScheduleTimerOptions`](types.IScheduleTimerOptions.md)<`Input`\> | The timer options. |

#### Returns

[`ITimer`](types.ITimer.md)

A [ITimer](types.ITimer.md) instance that can be used to explicitly cancel the timer.

#### Defined in

[src/types.ts:294](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L294)
