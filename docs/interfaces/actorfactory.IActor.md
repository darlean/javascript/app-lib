[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [actorfactory](../modules/actorfactory.md) / IActor

# Interface: IActor

[actorfactory](../modules/actorfactory.md).IActor

## Implemented by

- [`BaseActor`](../classes/actorfactory.BaseActor.md)

## Table of contents

### Methods

- [activate](actorfactory.IActor.md#activate)
- [deactivate](actorfactory.IActor.md#deactivate)

## Methods

### activate

▸ `Optional` **activate**(`context`): `Promise`<`void`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `context` | [`IActionContext`](actorfactory.IActionContext.md) |

#### Returns

`Promise`<`void`\>

#### Defined in

[src/actorfactory.ts:84](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L84)

___

### deactivate

▸ `Optional` **deactivate**(`context`): `Promise`<`void`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `context` | [`IActionContext`](actorfactory.IActionContext.md) |

#### Returns

`Promise`<`void`\>

#### Defined in

[src/actorfactory.ts:85](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L85)
