[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](../modules/everything.md) / [interface\_locking](../modules/everything.interface_locking.md) / IReleaseLockRequest

# Interface: IReleaseLockRequest

[everything](../modules/everything.md).[interface_locking](../modules/everything.interface_locking.md).IReleaseLockRequest

## Table of contents

### Properties

- [id](everything.interface_locking.IReleaseLockRequest.md#id)
- [requester](everything.interface_locking.IReleaseLockRequest.md#requester)

## Properties

### id

• **id**: `string`[]

#### Defined in

[src/interfaces/locking.ts:26](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/locking.ts#L26)

___

### requester

• **requester**: `string`

#### Defined in

[src/interfaces/locking.ts:27](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/locking.ts#L27)
