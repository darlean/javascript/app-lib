@darlean/app-lib / [Modules](modules.md)

# Darlean JS App

Library for creating JavaScript/TypeScript Darlean apps

## Modules

> Note: Technically, this library consists of only one module, but it is better for the structure of the 
automatically generated documentation to artificially treat separate files as modules. 
When using the types and function of this library from within custom code, they are all simply part of the toplevel 
`@darlean/app-lib` module entry point.

## Functionality

The Darlean app-lib provides the following key functionality:
* [Creating](modules/actorfactory.md#actorfactory) regular and service actors that derive from [BaseActor](classes/actorfactory.BaseActor.md) in module [actorfactory](modules/actorfactory.md).

In addition to that it provides a lot of supportive functionality:
* Shared/exclusive locks with reentrancy and take-over functionality in module [selock](modules/selock.md)
* Serializeable data objects with support for text, binary and struct attachments in module [structs](modules/structs.md)
* Containers that contain and transfer text, binary and struct data over the network in module [containers](modules/containers.md)
* Binary encoding of containers in module [binaryencoding](modules/binaryencoding.md)
* Various types in module [types](modules/types.md)
* Various utility functions (like [parallel](modules/util.md#parallel), [replaceAll](modules/util.md#replaceall) and [wildcardMatch](modules/util.md#wildcardmatch)) in module [util](modules/util.md).
