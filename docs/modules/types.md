[@darlean/app-lib](../README.md) / [Modules](../modules.md) / types

# Module: types

## Table of contents

### Classes

- [DarleanError](../classes/types.DarleanError.md)

### Interfaces

- [IActorCreateOptions](../interfaces/types.IActorCreateOptions.md)
- [IActorRegistration](../interfaces/types.IActorRegistration.md)
- [IAppBinding](../interfaces/types.IAppBinding.md)
- [IAppInfo](../interfaces/types.IAppInfo.md)
- [IAttributes](../interfaces/types.IAttributes.md)
- [IBaseActionContext](../interfaces/types.IBaseActionContext.md)
- [IInstanceInfo](../interfaces/types.IInstanceInfo.md)
- [IPeerConfig](../interfaces/types.IPeerConfig.md)
- [IRegisterActorOptions](../interfaces/types.IRegisterActorOptions.md)
- [IRpcError](../interfaces/types.IRpcError.md)
- [IRuntime](../interfaces/types.IRuntime.md)
- [IScheduleTimerOptions](../interfaces/types.IScheduleTimerOptions.md)
- [ISettings](../interfaces/types.ISettings.md)
- [IShutdownInfo](../interfaces/types.IShutdownInfo.md)
- [ISupportedActor](../interfaces/types.ISupportedActor.md)
- [ITime](../interfaces/types.ITime.md)
- [ITimer](../interfaces/types.ITimer.md)

### Type aliases

- [ProcessErrorCode](types.md#processerrorcode)

### Variables

- [DEFAULT\_MAX\_HOP\_COUNT](types.md#default_max_hop_count)

### Functions

- [ProcessError](types.md#processerror)
- [ensureDarleanError](types.md#ensuredarleanerror)
- [fromRpcError](types.md#fromrpcerror)
- [isProcessError](types.md#isprocesserror)
- [toRpcError](types.md#torpcerror)

## Type aliases

### ProcessErrorCode

Ƭ **ProcessErrorCode**: ``"ACTOR_NOT_ACTIVE"`` \| ``"UNKNOWN_ACTION"`` \| ``"ACTOR_ACTIVATION_FAILED"`` \| ``"ACTOR_CREATE_FAILED"`` \| ``"ACTOR_TYPE_NOT_DEFINED"`` \| ``"ACTOR_TYPE_DISABLED"`` \| ``"ACTOR_LOCK_REFUSED"`` \| ``"UNEXPECTED_PERFORM_ACTION_ERROR"`` \| ``"INVALID_REQUEST"``

#### Defined in

[src/types.ts:45](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L45)

## Variables

### DEFAULT\_MAX\_HOP\_COUNT

• **DEFAULT\_MAX\_HOP\_COUNT**: ``128``

#### Defined in

[src/types.ts:364](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L364)

## Functions

### ProcessError

▸ **ProcessError**(`code`, `message`, `attributes?`, `nested?`): [`DarleanError`](../classes/types.DarleanError.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `code` | [`ProcessErrorCode`](types.md#processerrorcode) |
| `message` | `string` |
| `attributes?` | [`IAttributes`](../interfaces/types.IAttributes.md) |
| `nested?` | [`DarleanError`](../classes/types.DarleanError.md)[] |

#### Returns

[`DarleanError`](../classes/types.DarleanError.md)

#### Defined in

[src/types.ts:81](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L81)

___

### ensureDarleanError

▸ **ensureDarleanError**(`e`): [`DarleanError`](../classes/types.DarleanError.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `e` | `unknown` |

#### Returns

[`DarleanError`](../classes/types.DarleanError.md)

#### Defined in

[src/types.ts:94](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L94)

___

### fromRpcError

▸ **fromRpcError**(`error`): [`DarleanError`](../classes/types.DarleanError.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | [`IRpcError`](../interfaces/types.IRpcError.md) |

#### Returns

[`DarleanError`](../classes/types.DarleanError.md)

#### Defined in

[src/types.ts:138](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L138)

___

### isProcessError

▸ **isProcessError**(`error`): `boolean`

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | [`DarleanError`](../classes/types.DarleanError.md) |

#### Returns

`boolean`

#### Defined in

[src/types.ts:90](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L90)

___

### toRpcError

▸ **toRpcError**(`error`): [`IRpcError`](../interfaces/types.IRpcError.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | [`DarleanError`](../classes/types.DarleanError.md) |

#### Returns

[`IRpcError`](../interfaces/types.IRpcError.md)

#### Defined in

[src/types.ts:128](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/types.ts#L128)
