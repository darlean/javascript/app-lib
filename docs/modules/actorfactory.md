[@darlean/app-lib](../README.md) / [Modules](../modules.md) / actorfactory

# Module: actorfactory

Provides an actor factory and other types to base custom actors upon.
* Custom actors can be created by extending the [BaseActor](../classes/actorfactory.BaseActor.md) class.
* Custom actors can be registered by means of the [actorFactory](actorfactory.md#actorfactory) function that returns an [ActorFactory](../classes/actorfactory.ActorFactory.md) instance
* Decorators are provided for for regular actors ([@actor](actorfactory.md#actor)) and service actors ([@service](actorfactory.md#service))
* A decorator is provided for actor action implementation methods ([@action](actorfactory.md#action))
* An [IActionContext](../interfaces/actorfactory.IActionContext.md) (that extends [IBaseActionContext](../interfaces/types.IBaseActionContext.md)) is defined that allows
  action implementation code to interact with other actors and the Darlean platform.

## Table of contents

### Classes

- [ActorFactory](../classes/actorfactory.ActorFactory.md)
- [BaseActor](../classes/actorfactory.BaseActor.md)

### Interfaces

- [IActionContext](../interfaces/actorfactory.IActionContext.md)
- [IActionDecoration](../interfaces/actorfactory.IActionDecoration.md)
- [IActor](../interfaces/actorfactory.IActor.md)
- [IActorDecoration](../interfaces/actorfactory.IActorDecoration.md)
- [IBaseActorFactoryOptions](../interfaces/actorfactory.IBaseActorFactoryOptions.md)

### Functions

- [action](actorfactory.md#action)
- [actor](actorfactory.md#actor)
- [actorFactory](actorfactory.md#actorfactory)
- [service](actorfactory.md#service)

## Functions

### action

▸ **action**(`config?`): (`target`: `Object`, `propertyKey`: `string`, `descriptor`: `PropertyDescriptor`) => `void`

Decorator for an action method.

When the method name already matches with the action name, and no additional opions are required:
```
@action()
public myActor(...) {}
```

When the method name does not match with the action name, and/or when additional options are required:
```
@action({name: 'myAction', locking: 'shared'})
public myActorFunction(...) {}
```

For a list of options, see [IActionDecoration](../interfaces/actorfactory.IActionDecoration.md).

#### Parameters

| Name | Type |
| :------ | :------ |
| `config?` | [`IActionDecoration`](../interfaces/actorfactory.IActionDecoration.md) |

#### Returns

`fn`

▸ (`target`, `propertyKey`, `descriptor`): `void`

##### Parameters

| Name | Type |
| :------ | :------ |
| `target` | `Object` |
| `propertyKey` | `string` |
| `descriptor` | `PropertyDescriptor` |

##### Returns

`void`

#### Defined in

[src/actorfactory.ts:285](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L285)

___

### actor

▸ **actor**(`config`): (`constructor`: `Function`) => `void`

Decorator for a regular actor class.

Standard use:
```
@actor({name: 'MyActor'})
export class MyActor extends BaseActor {...}
```
For the list of options, see [IActorDecoration](../interfaces/actorfactory.IActorDecoration.md). To decorate a *service actor*, use [@service](actorfactory.md#service) instead.

#### Parameters

| Name | Type |
| :------ | :------ |
| `config` | [`IActorDecoration`](../interfaces/actorfactory.IActorDecoration.md) |

#### Returns

`fn`

▸ (`constructor`): `void`

##### Parameters

| Name | Type |
| :------ | :------ |
| `constructor` | `Function` |

##### Returns

`void`

#### Defined in

[src/actorfactory.ts:206](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L206)

___

### actorFactory

▸ **actorFactory**<`Options`\>(`logger`, `options`): [`ActorFactory`](../classes/actorfactory.ActorFactory.md)

Creates a factory for a customer actor that extends [BaseActor](../classes/actorfactory.BaseActor.md).

#### Type parameters

| Name |
| :------ |
| `Options` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `logger` | [`ILogger`](../interfaces/everything.ILogger.md) | The logger that is used to log factory events to |
| `options` | [`IBaseActorFactoryOptions`](../interfaces/actorfactory.IBaseActorFactoryOptions.md)<`Options`\> | The options that defines the actor for which a factory is to be created |

#### Returns

[`ActorFactory`](../classes/actorfactory.ActorFactory.md)

A newly created actor factory

#### Defined in

[src/actorfactory.ts:478](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L478)

___

### service

▸ **service**(`config`): (`constructor`: `Function`) => `void`

Decorator for a service actor class.

Standard use:
```
@service({name: 'MyService'})
export class MyService extends BaseActor {...}
```
For the list of options, see [IActorDecoration](../interfaces/actorfactory.IActorDecoration.md). To decorate a *regular (non-service) actor*, use [@actor](actorfactory.md#actor) instead.

#### Parameters

| Name | Type |
| :------ | :------ |
| `config` | [`IActorDecoration`](../interfaces/actorfactory.IActorDecoration.md) |

#### Returns

`fn`

▸ (`constructor`): `void`

##### Parameters

| Name | Type |
| :------ | :------ |
| `constructor` | `Function` |

##### Returns

`void`

#### Defined in

[src/actorfactory.ts:240](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/actorfactory.ts#L240)
