[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](everything.md) / interface\_scheduling

# Namespace: interface\_scheduling

[everything](everything.md).interface_scheduling

## Table of contents

### Interfaces

- [IScheduleReminderRequest](../interfaces/everything.interface_scheduling.IScheduleReminderRequest.md)
- [IScheduleReminderResponse](../interfaces/everything.interface_scheduling.IScheduleReminderResponse.md)
