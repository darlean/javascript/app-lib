[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](everything.md) / interface\_persistence

# Namespace: interface\_persistence

[everything](everything.md).interface_persistence

## Table of contents

### Interfaces

- [IDeleteRequest](../interfaces/everything.interface_persistence.IDeleteRequest.md)
- [IInspectRequest](../interfaces/everything.interface_persistence.IInspectRequest.md)
- [IInspectResponse](../interfaces/everything.interface_persistence.IInspectResponse.md)
- [IInspectResponseItem](../interfaces/everything.interface_persistence.IInspectResponseItem.md)
- [ILoadRequest](../interfaces/everything.interface_persistence.ILoadRequest.md)
- [ILoadResponse](../interfaces/everything.interface_persistence.ILoadResponse.md)
- [IQueryRangePart](../interfaces/everything.interface_persistence.IQueryRangePart.md)
- [IQueryRequest](../interfaces/everything.interface_persistence.IQueryRequest.md)
- [IQueryResponse](../interfaces/everything.interface_persistence.IQueryResponse.md)
- [IQueryResponseItem](../interfaces/everything.interface_persistence.IQueryResponseItem.md)
- [IStoreRequest](../interfaces/everything.interface_persistence.IStoreRequest.md)
