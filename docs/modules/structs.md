[@darlean/app-lib](../README.md) / [Modules](../modules.md) / structs

# Module: structs

Within the scope of Darlean, a *struct* is an object that can be converted from and to
a [container](../interfaces/containers.IContainer.md), and that can contain [attachments](../interfaces/structs.IAttachment.md)) that
can be binary, text or structs themselves (that then can again contain attachments, et
cetera).

Structs play an important role within Darlean, because they provide a simple and very efficient way
for user code to interact with the [containers](containers.md) that are used to transfer action requests and
responses over the network.

A regular object (like `{Hello: 'World'}`) can be turned into a struct by passing it to the
[struct](structs.md#struct) function. The object that is converted into a struct then implements [IStruct](../interfaces/structs.IStruct.md) because
the properties [__struct](../interfaces/structs.IStruct.md#__struct), [_att](../interfaces/structs.IStruct.md#_att) and [_cont](../interfaces/structs.IStruct.md#_cont) that make up [IStruct](../interfaces/structs.IStruct.md) are injected to the
object by the [struct](structs.md#struct) function.

Example: creating a struct from an object
```
const s = struct({
  hello: 'World'
});
console.log(s.hello);
// Prints: World
console.log(s);
// Prints: { hello: 'World', _cont: [Function], __struct: [Function], _att: [Function] }
```

Example: Working with structs
```
interface IAbc {a: number, b: number, c: IBinaryAttachmentRef};
function sum(input: Struct<IAbc>) {
  return input.a + input.b + input._att(input.c).length;
}
const s = struct<IAbc>( (attach) => ({
  a: 3,
  b: 4,
  c: attach( Buffer.from('Hello'))
}));
console.log( sum(s) );  // Prints 12
```
 Darlean provides functionality for:
 * Creating structs from objects (the [struct](structs.md#struct) function)
 * Creating structs with attachments ([struct](structs.md#struct) together with an [Attacher](structs.md#attacher) function)
 * Extracting an attachment from a struct ([IStruct._att](../interfaces/structs.IStruct.md#_att))
 * Extracting [binary](../interfaces/structs.IAttachment.md#binary), [text](../interfaces/structs.IAttachment.md#text) or [struct](../interfaces/structs.IAttachment.md#struct)
   data from [attachments](../interfaces/structs.IAttachment.md)
 * Retrieving the underlying container of a struct ([IStruct._cont](../interfaces/structs.IStruct.md#_cont))
 * Retrieving the underlying container of an attachment ([IAttachment._cont](../interfaces/structs.IAttachment.md#_cont))

## Table of contents

### Classes

- [ContainerAttachment](../classes/structs.ContainerAttachment.md)

### Interfaces

- [IAttachment](../interfaces/structs.IAttachment.md)
- [IAttachmentRef](../interfaces/structs.IAttachmentRef.md)
- [IStruct](../interfaces/structs.IStruct.md)

### Type aliases

- [Attacher](structs.md#attacher)
- [AttachmentData](structs.md#attachmentdata)
- [AttachmentLike](structs.md#attachmentlike)
- [AttachmentLike0](structs.md#attachmentlike0)
- [AttachmentType](structs.md#attachmenttype)
- [IAttachment0](structs.md#iattachment0)
- [IBinaryAttachmentRef](structs.md#ibinaryattachmentref)
- [IStructAttachmentRef](structs.md#istructattachmentref)
- [ITextAttachmentRef](structs.md#itextattachmentref)
- [Struct](structs.md#struct)
- [StructLike](structs.md#structlike)

### Functions

- [asAttachment](structs.md#asattachment)
- [isAttachment](structs.md#isattachment)
- [isStruct](structs.md#isstruct)
- [struct](structs.md#struct)

## Type aliases

### Attacher

Ƭ **Attacher**<`T`\>: (`attach`: (`att`: [`AttachmentLike`](structs.md#attachmentlike)<`T`\>) => [`IAttachmentRef`](../interfaces/structs.IAttachmentRef.md)) => `T`

#### Type parameters

| Name | Type |
| :------ | :------ |
| `T` | extends [`AnObject`](containers.md#anobject) |

#### Type declaration

▸ (`attach`): `T`

Function signature that can be used to add [attachment references|](../interfaces/structs.IAttachmentRef.md) to a struct.
See also the overloaded version of the [struct](structs.md#struct) function that accepts an Attacher as argument.

```
const myStruct = struct( (attach) => ({
  myBuffer: attach(Buffer.from('Hello'))
}));
```

##### Parameters

| Name | Type |
| :------ | :------ |
| `attach` | (`att`: [`AttachmentLike`](structs.md#attachmentlike)<`T`\>) => [`IAttachmentRef`](../interfaces/structs.IAttachmentRef.md) |

##### Returns

`T`

#### Defined in

[src/structs.ts:286](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L286)

___

### AttachmentData

Ƭ **AttachmentData**: `Buffer` \| `string` \| [`AnObject`](containers.md#anobject) \| `undefined`

#### Defined in

[src/structs.ts:67](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L67)

___

### AttachmentLike

Ƭ **AttachmentLike**<`StructType`\>: [`IAttachment`](../interfaces/structs.IAttachment.md)<`StructType`\> \| [`AttachmentData`](structs.md#attachmentdata) \| [`IContainer1`](containers.md#icontainer1)<`StructType`\>

Alias for types that can be converted into attachments. That are objects that implement [IAttachment](../interfaces/structs.IAttachment.md),
plain objects (that do not implement [IAttachment](../interfaces/structs.IAttachment.md), strings, Buffers, the value undefined, and IContainers
that hold objects).

#### Type parameters

| Name | Type |
| :------ | :------ |
| `StructType` | extends [`AnObject`](containers.md#anobject) |

#### Defined in

[src/structs.ts:219](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L219)

___

### AttachmentLike0

Ƭ **AttachmentLike0**: [`AttachmentLike`](structs.md#attachmentlike)<[`AnObject`](containers.md#anobject)\>

#### Defined in

[src/structs.ts:223](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L223)

___

### AttachmentType

Ƭ **AttachmentType**: ``"binary"`` \| ``"text"`` \| ``"struct"`` \| ``"empty"``

#### Defined in

[src/structs.ts:66](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L66)

___

### IAttachment0

Ƭ **IAttachment0**: [`IAttachment`](../interfaces/structs.IAttachment.md)<`unknown`\>

Alias for a struct attachment with unknown struct type, or for a non-struct attachment (for which
a struct type is not applicable).

#### Defined in

[src/structs.ts:212](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L212)

___

### IBinaryAttachmentRef

Ƭ **IBinaryAttachmentRef**: [`IAttachmentRef`](../interfaces/structs.IAttachmentRef.md)

Reference to a binary attachment. Useful to 'annotate' that certain struct fields expect a binary attachment.

#### Defined in

[src/structs.ts:83](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L83)

___

### IStructAttachmentRef

Ƭ **IStructAttachmentRef**<`T`\>: [`IAttachmentRef`](../interfaces/structs.IAttachmentRef.md)

Reference to a structured attachment of type T. Useful to 'annotate' that certain struct fields expect a
structured attachment.

#### Type parameters

| Name |
| :------ |
| `T` |

#### Defined in

[src/structs.ts:93](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L93)

___

### ITextAttachmentRef

Ƭ **ITextAttachmentRef**: [`IAttachmentRef`](../interfaces/structs.IAttachmentRef.md)

Reference to a text attachment. Useful to 'annotate' that certain struct fields expect a text attachment.

#### Defined in

[src/structs.ts:87](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L87)

___

### Struct

Ƭ **Struct**<`T`\>: `T` & [`IStruct`](../interfaces/structs.IStruct.md)<`T`\>

Type alias to indicate that a value is both a struct object itself as well as an IStruct. A
Struct can be created by means of the [struct](structs.md#struct) function.

Example:
```
interface Abc {a: number, b: number, c: IBinaryAttachmentRef};
function sum(input: Struct<Abc>) {
  return input.a + input.b + input._att(input.c).length;
}
const s = struct<Abc>( (attach) => ({
  a: 3,
  b: 4,
  c: attach( Buffer.from('Hello'))
}));
console.log( sum(s) );  // Prints 12
```

#### Type parameters

| Name | Type |
| :------ | :------ |
| `T` | extends [`AnObject`](containers.md#anobject) |

#### Defined in

[src/structs.ts:113](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L113)

___

### StructLike

Ƭ **StructLike**<`T`\>: `T` \| [`Struct`](structs.md#struct)<`T`\>

#### Type parameters

| Name | Type |
| :------ | :------ |
| `T` | extends [`AnObject`](containers.md#anobject) |

#### Defined in

[src/structs.ts:115](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L115)

## Functions

### asAttachment

▸ **asAttachment**<`T`\>(`value`): [`IAttachment`](../interfaces/structs.IAttachment.md)<`T`\>

Converts an attachment, containerizeable, struct, buffer, string, object or undefined into an attachment.
When the provided value already is an attachment, it is simply returned. When a container can be derived
because value is an instance of [IContainer](../interfaces/containers.IContainer.md) or [IContainerizeable](../interfaces/containers.IContainerizeable.md), the returned attachment will
use this exact same container. Otherwise, a new container is generated, and an attachment around that
container is returned.

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | [`AttachmentLike`](structs.md#attachmentlike)<`T`\> |

#### Returns

[`IAttachment`](../interfaces/structs.IAttachment.md)<`T`\>

#### Defined in

[src/structs.ts:295](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L295)

___

### isAttachment

▸ **isAttachment**(`value`): `boolean`

Returns whether value is an attachment.

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | [`AttachmentLike0`](structs.md#attachmentlike0) |

#### Returns

`boolean`

#### Defined in

[src/structs.ts:310](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L310)

___

### isStruct

▸ **isStruct**(`value`): `boolean`

Returns whether value is a struct.

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `unknown` |

#### Returns

`boolean`

#### Defined in

[src/structs.ts:317](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L317)

___

### struct

▸ **struct**<`T`\>(`value`): [`Struct`](structs.md#struct)<`T`\>

Converts the provided object vakue into a struct by injecting the [_cont](../interfaces/structs.IStruct.md#_cont), [_att](../interfaces/structs.IStruct.md#_att)
and [__struct](../interfaces/structs.IStruct.md#__struct) methods, thus effectively making it implement [IStruct](../interfaces/structs.IStruct.md).
When value already is a struct, it is simply returned. Otherwise, the provided object is converted into a struct that
is backed by a newly generated container.

```
const s = struct({
  hello: 'World'
});
console.log(s.hello);   // Prints: World
console.log(s);         // Prints: { hello: 'World', _cont: [Function], __struct: [Function], _att: [Function] }
```

#### Type parameters

| Name | Type |
| :------ | :------ |
| `T` | extends `Object` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | [`ContainerLike1`](containers.md#containerlike1)<`T`\> | The object or struct to be converted to a struct |

#### Returns

[`Struct`](structs.md#struct)<`T`\>

value (unmodified) if value already is a struct, or value augmented with the
fields of [IStruct](../interfaces/structs.IStruct.md) when value is not already a struct.

#### Defined in

[src/structs.ts:339](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L339)

▸ **struct**<`T`\>(`attacher`): [`Struct`](structs.md#struct)<`T`\>

Creates a struct that contains attachments.

Typical use:
```
const myStruct = struct( (attach) => ({
  myBuffer: attach(Buffer.from('Hello'))
}));
```

#### Type parameters

| Name | Type |
| :------ | :------ |
| `T` | extends `Object` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `attacher` | [`Attacher`](structs.md#attacher)<`T`\> | The attacher function (usually an arrow function) that returns an object that is then converted into a struct. Within the attacher function, the provided 'attach' function can be used to generate [IAttachmentRef](../interfaces/structs.IAttachmentRef.md) instances. |

#### Returns

[`Struct`](structs.md#struct)<`T`\>

#### Defined in

[src/structs.ts:354](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/structs.ts#L354)
