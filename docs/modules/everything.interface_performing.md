[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](everything.md) / interface\_performing

# Namespace: interface\_performing

[everything](everything.md).interface_performing

## Table of contents

### Interfaces

- [IActionHop](../interfaces/everything.interface_performing.IActionHop.md)
- [IPerformActionReq](../interfaces/everything.interface_performing.IPerformActionReq.md)
- [IPerformActionResp](../interfaces/everything.interface_performing.IPerformActionResp.md)

### Type aliases

- [IPerformActionReqC](everything.interface_performing.md#iperformactionreqc)
- [IPerformActionReqCL](everything.interface_performing.md#iperformactionreqcl)
- [IPerformActionRespC](everything.interface_performing.md#iperformactionrespc)
- [IPerformActionRespCL](everything.interface_performing.md#iperformactionrespcl)

## Type aliases

### IPerformActionReqC

Ƭ **IPerformActionReqC**<`Data`\>: [`IContainer`](../interfaces/containers.IContainer.md)<[`IPerformActionReq`](../interfaces/everything.interface_performing.IPerformActionReq.md), `Data`\>

#### Type parameters

| Name | Type |
| :------ | :------ |
| `Data` | extends [`AnObject`](containers.md#anobject) |

#### Defined in

[src/interfaces/performing.ts:21](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/performing.ts#L21)

___

### IPerformActionReqCL

Ƭ **IPerformActionReqCL**<`Data`\>: [`ContainerLike`](containers.md#containerlike)<[`IPerformActionReq`](../interfaces/everything.interface_performing.IPerformActionReq.md), `Data`\>

#### Type parameters

| Name | Type |
| :------ | :------ |
| `Data` | extends [`AnObject`](containers.md#anobject) |

#### Defined in

[src/interfaces/performing.ts:22](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/performing.ts#L22)

___

### IPerformActionRespC

Ƭ **IPerformActionRespC**<`Data`\>: [`IContainer`](../interfaces/containers.IContainer.md)<[`IPerformActionResp`](../interfaces/everything.interface_performing.IPerformActionResp.md), `Data`\>

#### Type parameters

| Name |
| :------ |
| `Data` |

#### Defined in

[src/interfaces/performing.ts:32](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/performing.ts#L32)

___

### IPerformActionRespCL

Ƭ **IPerformActionRespCL**<`Data`\>: [`ContainerLike`](containers.md#containerlike)<[`IPerformActionResp`](../interfaces/everything.interface_performing.IPerformActionResp.md), `Data`\>

#### Type parameters

| Name |
| :------ |
| `Data` |

#### Defined in

[src/interfaces/performing.ts:33](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/interfaces/performing.ts#L33)
