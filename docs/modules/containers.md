[@darlean/app-lib](../README.md) / [Modules](../modules.md) / containers

# Module: containers

Under water, containers play a key role in Darlean because they allow efficient transfer
of text, binary and structured information between apps. But they are almost hidden from the
regular app developer that uses [structs](structs.md) to directly interact with the contained binary,
text and structured data.

> In essence, containers and [structs](structs.md) deal with the same data, it is just that containers
are more low-level (and closely related to the underlying messaging layer) whereas [structs](structs.md)
are more high-level and easier to use in application code.

A container contains binary, text or structured data, or can be empty (contain no data). The
corresponding types are `Buffer`, `string`, `object` and `undefined`, respectively.
Containers can be nested because they can have zero or more child containers.

> Example of nested containers: a typical Darlean network message consists of a top level
struct container (C0) with the name of the receiver node as data. A child struct container (C1) contains
metadata about the message (like to which node to send delivery receipts). This container C1 then has
a struct child container (C2) with the action request data (actor type, actor id, action name). And this
action request container C2 has a struct child container that finally contains the data that should be
passed as input to the actor. When the actor also needs additional information (like binary blobs of data),
they can be attached as children of C2.

Containers can be serialized and deserialized very efficiently. In particular, containers make it
possible to only deserialize the information that your are interested in, instead of the entire message.
The same holds for serializing: when composing a new container that contains existing serialized containers as
their (sub)children, these (sub)children can be incorporated into the final container without the need to
serialize and deserialize them first.

> Example: At the messaging layer, a node only has to read the `receiver` field of the top-level container
to determine to which node it should forward the message to.
To do so, it only has to deserialize the very small *data section* of that top-level container. It then can create a
new container (possibly with a different `recipient` value), and just attach all child containers to that
new container without having to serialize and deserialize those child containers first.

Containers are created by means of the [container](containers.md#container) function. It expects the container data (a string, Buffer,
object or undefined) as input, and optionally expects an array of child container-likes. (A [ContainerLike](containers.md#containerlike) is
a value that can already is, or can easily be converted into a container, like strings, Buffers, [struct](structs.md#struct) objects
and regular objects).

A container implements [IContainer](../interfaces/containers.IContainer.md), which provides methods for accessing the container data and its child
containers.

The binarization of containers is implemented in [binaryencoding](binaryencoding.md).

## Table of contents

### Classes

- [EditableContainer](../classes/containers.EditableContainer.md)

### Interfaces

- [IContainer](../interfaces/containers.IContainer.md)
- [IContainerizeable](../interfaces/containers.IContainerizeable.md)

### Type aliases

- [AnObject](containers.md#anobject)
- [BufferEncoding](containers.md#bufferencoding)
- [ContainerData](containers.md#containerdata)
- [ContainerKind](containers.md#containerkind)
- [ContainerLike](containers.md#containerlike)
- [ContainerLike0](containers.md#containerlike0)
- [ContainerLike1](containers.md#containerlike1)
- [IContainer0](containers.md#icontainer0)
- [IContainer1](containers.md#icontainer1)
- [IContainerizeable0](containers.md#icontainerizeable0)
- [IContainerizeable1](containers.md#icontainerizeable1)

### Functions

- [asContainer](containers.md#ascontainer)
- [container](containers.md#container)
- [isContainer](containers.md#iscontainer)

## Type aliases

### AnObject

Ƭ **AnObject**: `Object`

#### Defined in

[src/containers.ts:52](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L52)

___

### BufferEncoding

Ƭ **BufferEncoding**: ``"none"`` \| ``"tlv"``

#### Defined in

[src/containers.ts:49](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L49)

___

### ContainerData

Ƭ **ContainerData**: [`AnObject`](containers.md#anobject) \| `Buffer` \| `string` \| `undefined`

#### Defined in

[src/containers.ts:54](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L54)

___

### ContainerKind

Ƭ **ContainerKind**: ``"empty"`` \| ``"binary"`` \| ``"text"`` \| ``"structure"``

#### Defined in

[src/containers.ts:47](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L47)

___

### ContainerLike

Ƭ **ContainerLike**<`A`, `B`\>: [`IContainer`](../interfaces/containers.IContainer.md)<`A`, `B`\> \| [`IContainerizeable`](../interfaces/containers.IContainerizeable.md)<`A`, `B`\> \| `A`

#### Type parameters

| Name | Type |
| :------ | :------ |
| `A` | extends [`ContainerData`](containers.md#containerdata) |
| `B` | extends [`ContainerData`](containers.md#containerdata) |

#### Defined in

[src/containers.ts:65](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L65)

___

### ContainerLike0

Ƭ **ContainerLike0**: [`ContainerLike`](containers.md#containerlike)<[`ContainerData`](containers.md#containerdata), [`ContainerData`](containers.md#containerdata)\>

#### Defined in

[src/containers.ts:69](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L69)

___

### ContainerLike1

Ƭ **ContainerLike1**<`D`\>: [`ContainerLike`](containers.md#containerlike)<`D`, [`ContainerData`](containers.md#containerdata)\>

#### Type parameters

| Name |
| :------ |
| `D` |

#### Defined in

[src/containers.ts:70](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L70)

___

### IContainer0

Ƭ **IContainer0**: [`IContainer`](../interfaces/containers.IContainer.md)<[`ContainerData`](containers.md#containerdata), [`ContainerData`](containers.md#containerdata)\>

#### Defined in

[src/containers.ts:56](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L56)

___

### IContainer1

Ƭ **IContainer1**<`T`\>: [`IContainer`](../interfaces/containers.IContainer.md)<`T`, [`ContainerData`](containers.md#containerdata)\>

#### Type parameters

| Name | Type |
| :------ | :------ |
| `T` | extends [`ContainerData`](containers.md#containerdata) |

#### Defined in

[src/containers.ts:57](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L57)

___

### IContainerizeable0

Ƭ **IContainerizeable0**: [`IContainerizeable`](../interfaces/containers.IContainerizeable.md)<[`ContainerData`](containers.md#containerdata), [`ContainerData`](containers.md#containerdata)\>

#### Defined in

[src/containers.ts:62](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L62)

___

### IContainerizeable1

Ƭ **IContainerizeable1**<`D`\>: [`IContainerizeable`](../interfaces/containers.IContainerizeable.md)<`D`, [`ContainerData`](containers.md#containerdata)\>

#### Type parameters

| Name | Type |
| :------ | :------ |
| `D` | extends [`ContainerData`](containers.md#containerdata) |

#### Defined in

[src/containers.ts:63](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L63)

## Functions

### asContainer

▸ **asContainer**<`Type`, `Child0Type`\>(`value?`): [`IContainer`](../interfaces/containers.IContainer.md)<`Type`, `Child0Type`\>

#### Type parameters

| Name | Type |
| :------ | :------ |
| `Type` | extends [`ContainerData`](containers.md#containerdata) |
| `Child0Type` | extends [`ContainerData`](containers.md#containerdata) |

#### Parameters

| Name | Type |
| :------ | :------ |
| `value?` | [`ContainerLike`](containers.md#containerlike)<`Type`, `Child0Type`\> |

#### Returns

[`IContainer`](../interfaces/containers.IContainer.md)<`Type`, `Child0Type`\>

#### Defined in

[src/containers.ts:92](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L92)

___

### container

▸ **container**<`Data`, `Child0Data`\>(`data?`, `children?`): [`IContainer`](../interfaces/containers.IContainer.md)<`Data`, `Child0Data`\>

#### Type parameters

| Name | Type |
| :------ | :------ |
| `Data` | extends [`ContainerData`](containers.md#containerdata) |
| `Child0Data` | extends [`ContainerData`](containers.md#containerdata) |

#### Parameters

| Name | Type |
| :------ | :------ |
| `data?` | `Data` |
| `children?` | [`ContainerLike0`](containers.md#containerlike0)[] |

#### Returns

[`IContainer`](../interfaces/containers.IContainer.md)<`Data`, `Child0Data`\>

#### Defined in

[src/containers.ts:104](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L104)

___

### isContainer

▸ **isContainer**(`value`): `boolean`

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `unknown` |

#### Returns

`boolean`

#### Defined in

[src/containers.ts:87](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/containers.ts#L87)
