[@darlean/app-lib](../README.md) / [Modules](../modules.md) / [everything](everything.md) / interface\_locking

# Namespace: interface\_locking

[everything](everything.md).interface_locking

## Table of contents

### Interfaces

- [IAcquireLockRequest](../interfaces/everything.interface_locking.IAcquireLockRequest.md)
- [IAcquireLockResponse](../interfaces/everything.interface_locking.IAcquireLockResponse.md)
- [IAggregatedSIngleLockHolder](../interfaces/everything.interface_locking.IAggregatedSIngleLockHolder.md)
- [IAggregatedSingleLock](../interfaces/everything.interface_locking.IAggregatedSingleLock.md)
- [IGetLockInfoRequest](../interfaces/everything.interface_locking.IGetLockInfoRequest.md)
- [IGetLockInfoResponse](../interfaces/everything.interface_locking.IGetLockInfoResponse.md)
- [IReleaseLockRequest](../interfaces/everything.interface_locking.IReleaseLockRequest.md)
