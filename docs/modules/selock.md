[@darlean/app-lib](../README.md) / [Modules](../modules.md) / selock

# Module: selock

Provides a [SharedExclusiveLock](../classes/selock.SharedExclusiveLock.md) implementation with support for reentrancy and take over.

## Table of contents

### Classes

- [SharedExclusiveLock](../classes/selock.SharedExclusiveLock.md)
