[@darlean/app-lib](../README.md) / [Modules](../modules.md) / everything

# Module: everything

## Table of contents

### References

- [ActorFactory](everything.md#actorfactory)
- [AnObject](everything.md#anobject)
- [Attacher](everything.md#attacher)
- [AttachmentData](everything.md#attachmentdata)
- [AttachmentLike](everything.md#attachmentlike)
- [AttachmentLike0](everything.md#attachmentlike0)
- [AttachmentType](everything.md#attachmenttype)
- [BaseActor](everything.md#baseactor)
- [BufferContainer](everything.md#buffercontainer)
- [BufferEncoding](everything.md#bufferencoding)
- [ContainerAttachment](everything.md#containerattachment)
- [ContainerData](everything.md#containerdata)
- [ContainerKind](everything.md#containerkind)
- [ContainerLike](everything.md#containerlike)
- [ContainerLike0](everything.md#containerlike0)
- [ContainerLike1](everything.md#containerlike1)
- [ContainerToBuffers](everything.md#containertobuffers)
- [DEFAULT\_MAX\_HOP\_COUNT](everything.md#default_max_hop_count)
- [DarleanError](everything.md#darleanerror)
- [EditableContainer](everything.md#editablecontainer)
- [IActionContext](everything.md#iactioncontext)
- [IActionDecoration](everything.md#iactiondecoration)
- [IActor](everything.md#iactor)
- [IActorCreateOptions](everything.md#iactorcreateoptions)
- [IActorDecoration](everything.md#iactordecoration)
- [IActorRegistration](everything.md#iactorregistration)
- [IAppBinding](everything.md#iappbinding)
- [IAppInfo](everything.md#iappinfo)
- [IAttachment](everything.md#iattachment)
- [IAttachment0](everything.md#iattachment0)
- [IAttachmentRef](everything.md#iattachmentref)
- [IAttributes](everything.md#iattributes)
- [IBaseActionContext](everything.md#ibaseactioncontext)
- [IBaseActorFactoryOptions](everything.md#ibaseactorfactoryoptions)
- [IBinaryAttachmentRef](everything.md#ibinaryattachmentref)
- [IContainer](everything.md#icontainer)
- [IContainer0](everything.md#icontainer0)
- [IContainer1](everything.md#icontainer1)
- [IContainerizeable](everything.md#icontainerizeable)
- [IContainerizeable0](everything.md#icontainerizeable0)
- [IContainerizeable1](everything.md#icontainerizeable1)
- [IInstanceInfo](everything.md#iinstanceinfo)
- [IPeerConfig](everything.md#ipeerconfig)
- [IRegisterActorOptions](everything.md#iregisteractoroptions)
- [IRpcError](everything.md#irpcerror)
- [IRuntime](everything.md#iruntime)
- [IScheduleTimerOptions](everything.md#ischeduletimeroptions)
- [ISettings](everything.md#isettings)
- [IShutdownInfo](everything.md#ishutdowninfo)
- [IStruct](everything.md#istruct)
- [IStructAttachmentRef](everything.md#istructattachmentref)
- [ISupportedActor](everything.md#isupportedactor)
- [ITaskResult](everything.md#itaskresult)
- [ITasksResult](everything.md#itasksresult)
- [ITextAttachmentRef](everything.md#itextattachmentref)
- [ITime](everything.md#itime)
- [ITimer](everything.md#itimer)
- [ParallelAbort](everything.md#parallelabort)
- [ParallelTask](everything.md#paralleltask)
- [ProcessError](everything.md#processerror)
- [ProcessErrorCode](everything.md#processerrorcode)
- [Struct](everything.md#struct)
- [StructLike](everything.md#structlike)
- [action](everything.md#action)
- [actionToString](everything.md#actiontostring)
- [actor](everything.md#actor)
- [actorFactory](everything.md#actorfactory)
- [actorToString](everything.md#actortostring)
- [asAttachment](everything.md#asattachment)
- [asContainer](everything.md#ascontainer)
- [container](everything.md#container)
- [decodeFromBinaryBuffer](everything.md#decodefrombinarybuffer)
- [decodeKey](everything.md#decodekey)
- [encodeKey](everything.md#encodekey)
- [encodeToBinaryBuffer](everything.md#encodetobinarybuffer)
- [encodeToBinaryBuffers](everything.md#encodetobinarybuffers)
- [ensureDarleanError](everything.md#ensuredarleanerror)
- [formatAllAttributes](everything.md#formatallattributes)
- [formatAttribute](everything.md#formatattribute)
- [fromRpcError](everything.md#fromrpcerror)
- [isAttachment](everything.md#isattachment)
- [isContainer](everything.md#iscontainer)
- [isProcessError](everything.md#isprocesserror)
- [isStruct](everything.md#isstruct)
- [parallel](everything.md#parallel)
- [replaceAll](everything.md#replaceall)
- [replaceArguments](everything.md#replacearguments)
- [service](everything.md#service)
- [sleep](everything.md#sleep)
- [struct](everything.md#struct)
- [toRpcError](everything.md#torpcerror)
- [wildcardMatch](everything.md#wildcardmatch)

### Namespaces

- [interface\_locking](everything.interface_locking.md)
- [interface\_performing](everything.interface_performing.md)
- [interface\_persistence](everything.interface_persistence.md)
- [interface\_scheduling](everything.interface_scheduling.md)

### Classes

- [ActorRunner](../classes/everything.ActorRunner.md)
- [Application](../classes/everything.Application.md)
- [Logger](../classes/everything.Logger.md)
- [RuntimeApplication](../classes/everything.RuntimeApplication.md)

### Interfaces

- [IActionHop](../interfaces/everything.IActionHop.md)
- [IActionOptions](../interfaces/everything.IActionOptions.md)
- [IActionResult](../interfaces/everything.IActionResult.md)
- [IDeleteOptions](../interfaces/everything.IDeleteOptions.md)
- [IInspectRequest](../interfaces/everything.IInspectRequest.md)
- [ILoadOptions](../interfaces/everything.ILoadOptions.md)
- [ILoadResult](../interfaces/everything.ILoadResult.md)
- [ILogEvent](../interfaces/everything.ILogEvent.md)
- [ILogMetric](../interfaces/everything.ILogMetric.md)
- [ILogger](../interfaces/everything.ILogger.md)
- [IPersistenceService](../interfaces/everything.IPersistenceService.md)
- [IQueryOptions](../interfaces/everything.IQueryOptions.md)
- [IQueryRangePart](../interfaces/everything.IQueryRangePart.md)
- [IQueryResult](../interfaces/everything.IQueryResult.md)
- [IQueryResultItem](../interfaces/everything.IQueryResultItem.md)
- [IRawActionResult](../interfaces/everything.IRawActionResult.md)
- [IScheduleReminderOptions](../interfaces/everything.IScheduleReminderOptions.md)
- [IScheduleReminderResult](../interfaces/everything.IScheduleReminderResult.md)
- [ISchedulingService](../interfaces/everything.ISchedulingService.md)
- [IStoreOptions](../interfaces/everything.IStoreOptions.md)
- [ITimeService](../interfaces/everything.ITimeService.md)

### Type aliases

- [PerformActionRequestWaitFor](everything.md#performactionrequestwaitfor)
- [TLogLevel](everything.md#tloglevel)

### Variables

- [DEBUG\_AND\_UP](everything.md#debug_and_up)
- [DEEP\_AND\_UP](everything.md#deep_and_up)
- [DO\_NOT\_LOG](everything.md#do_not_log)
- [ERROR\_AND\_UP](everything.md#error_and_up)
- [INFO\_AND\_UP](everything.md#info_and_up)
- [VERBOSE\_AND\_UP](everything.md#verbose_and_up)
- [WARNING\_AND\_UP](everything.md#warning_and_up)

## References

### ActorFactory

Re-exports [ActorFactory](../classes/actorfactory.ActorFactory.md)

___

### AnObject

Re-exports [AnObject](containers.md#anobject)

___

### Attacher

Re-exports [Attacher](structs.md#attacher)

___

### AttachmentData

Re-exports [AttachmentData](structs.md#attachmentdata)

___

### AttachmentLike

Re-exports [AttachmentLike](structs.md#attachmentlike)

___

### AttachmentLike0

Re-exports [AttachmentLike0](structs.md#attachmentlike0)

___

### AttachmentType

Re-exports [AttachmentType](structs.md#attachmenttype)

___

### BaseActor

Re-exports [BaseActor](../classes/actorfactory.BaseActor.md)

___

### BufferContainer

Re-exports [BufferContainer](../classes/binaryencoding.BufferContainer.md)

___

### BufferEncoding

Re-exports [BufferEncoding](containers.md#bufferencoding)

___

### ContainerAttachment

Re-exports [ContainerAttachment](../classes/structs.ContainerAttachment.md)

___

### ContainerData

Re-exports [ContainerData](containers.md#containerdata)

___

### ContainerKind

Re-exports [ContainerKind](containers.md#containerkind)

___

### ContainerLike

Re-exports [ContainerLike](containers.md#containerlike)

___

### ContainerLike0

Re-exports [ContainerLike0](containers.md#containerlike0)

___

### ContainerLike1

Re-exports [ContainerLike1](containers.md#containerlike1)

___

### ContainerToBuffers

Re-exports [ContainerToBuffers](../classes/binaryencoding.ContainerToBuffers.md)

___

### DEFAULT\_MAX\_HOP\_COUNT

Re-exports [DEFAULT_MAX_HOP_COUNT](types.md#default_max_hop_count)

___

### DarleanError

Re-exports [DarleanError](../classes/types.DarleanError.md)

___

### EditableContainer

Re-exports [EditableContainer](../classes/containers.EditableContainer.md)

___

### IActionContext

Re-exports [IActionContext](../interfaces/actorfactory.IActionContext.md)

___

### IActionDecoration

Re-exports [IActionDecoration](../interfaces/actorfactory.IActionDecoration.md)

___

### IActor

Re-exports [IActor](../interfaces/actorfactory.IActor.md)

___

### IActorCreateOptions

Re-exports [IActorCreateOptions](../interfaces/types.IActorCreateOptions.md)

___

### IActorDecoration

Re-exports [IActorDecoration](../interfaces/actorfactory.IActorDecoration.md)

___

### IActorRegistration

Re-exports [IActorRegistration](../interfaces/types.IActorRegistration.md)

___

### IAppBinding

Re-exports [IAppBinding](../interfaces/types.IAppBinding.md)

___

### IAppInfo

Re-exports [IAppInfo](../interfaces/types.IAppInfo.md)

___

### IAttachment

Re-exports [IAttachment](../interfaces/structs.IAttachment.md)

___

### IAttachment0

Re-exports [IAttachment0](structs.md#iattachment0)

___

### IAttachmentRef

Re-exports [IAttachmentRef](../interfaces/structs.IAttachmentRef.md)

___

### IAttributes

Re-exports [IAttributes](../interfaces/types.IAttributes.md)

___

### IBaseActionContext

Re-exports [IBaseActionContext](../interfaces/types.IBaseActionContext.md)

___

### IBaseActorFactoryOptions

Re-exports [IBaseActorFactoryOptions](../interfaces/actorfactory.IBaseActorFactoryOptions.md)

___

### IBinaryAttachmentRef

Re-exports [IBinaryAttachmentRef](structs.md#ibinaryattachmentref)

___

### IContainer

Re-exports [IContainer](../interfaces/containers.IContainer.md)

___

### IContainer0

Re-exports [IContainer0](containers.md#icontainer0)

___

### IContainer1

Re-exports [IContainer1](containers.md#icontainer1)

___

### IContainerizeable

Re-exports [IContainerizeable](../interfaces/containers.IContainerizeable.md)

___

### IContainerizeable0

Re-exports [IContainerizeable0](containers.md#icontainerizeable0)

___

### IContainerizeable1

Re-exports [IContainerizeable1](containers.md#icontainerizeable1)

___

### IInstanceInfo

Re-exports [IInstanceInfo](../interfaces/types.IInstanceInfo.md)

___

### IPeerConfig

Re-exports [IPeerConfig](../interfaces/types.IPeerConfig.md)

___

### IRegisterActorOptions

Re-exports [IRegisterActorOptions](../interfaces/types.IRegisterActorOptions.md)

___

### IRpcError

Re-exports [IRpcError](../interfaces/types.IRpcError.md)

___

### IRuntime

Re-exports [IRuntime](../interfaces/types.IRuntime.md)

___

### IScheduleTimerOptions

Re-exports [IScheduleTimerOptions](../interfaces/types.IScheduleTimerOptions.md)

___

### ISettings

Re-exports [ISettings](../interfaces/types.ISettings.md)

___

### IShutdownInfo

Re-exports [IShutdownInfo](../interfaces/types.IShutdownInfo.md)

___

### IStruct

Re-exports [IStruct](../interfaces/structs.IStruct.md)

___

### IStructAttachmentRef

Re-exports [IStructAttachmentRef](structs.md#istructattachmentref)

___

### ISupportedActor

Re-exports [ISupportedActor](../interfaces/types.ISupportedActor.md)

___

### ITaskResult

Re-exports [ITaskResult](../interfaces/util.ITaskResult.md)

___

### ITasksResult

Re-exports [ITasksResult](../interfaces/util.ITasksResult.md)

___

### ITextAttachmentRef

Re-exports [ITextAttachmentRef](structs.md#itextattachmentref)

___

### ITime

Re-exports [ITime](../interfaces/types.ITime.md)

___

### ITimer

Re-exports [ITimer](../interfaces/types.ITimer.md)

___

### ParallelAbort

Re-exports [ParallelAbort](util.md#parallelabort)

___

### ParallelTask

Re-exports [ParallelTask](util.md#paralleltask)

___

### ProcessError

Re-exports [ProcessError](types.md#processerror)

___

### ProcessErrorCode

Re-exports [ProcessErrorCode](types.md#processerrorcode)

___

### Struct

Re-exports [Struct](structs.md#struct)

___

### StructLike

Re-exports [StructLike](structs.md#structlike)

___

### action

Re-exports [action](actorfactory.md#action)

___

### actionToString

Re-exports [actionToString](util.md#actiontostring)

___

### actor

Re-exports [actor](actorfactory.md#actor)

___

### actorFactory

Re-exports [actorFactory](actorfactory.md#actorfactory)

___

### actorToString

Re-exports [actorToString](util.md#actortostring)

___

### asAttachment

Re-exports [asAttachment](structs.md#asattachment)

___

### asContainer

Re-exports [asContainer](containers.md#ascontainer)

___

### container

Re-exports [container](containers.md#container)

___

### decodeFromBinaryBuffer

Re-exports [decodeFromBinaryBuffer](binaryencoding.md#decodefrombinarybuffer)

___

### decodeKey

Re-exports [decodeKey](util.md#decodekey)

___

### encodeKey

Re-exports [encodeKey](util.md#encodekey)

___

### encodeToBinaryBuffer

Re-exports [encodeToBinaryBuffer](binaryencoding.md#encodetobinarybuffer)

___

### encodeToBinaryBuffers

Re-exports [encodeToBinaryBuffers](binaryencoding.md#encodetobinarybuffers)

___

### ensureDarleanError

Re-exports [ensureDarleanError](types.md#ensuredarleanerror)

___

### formatAllAttributes

Re-exports [formatAllAttributes](util.md#formatallattributes)

___

### formatAttribute

Re-exports [formatAttribute](util.md#formatattribute)

___

### fromRpcError

Re-exports [fromRpcError](types.md#fromrpcerror)

___

### isAttachment

Re-exports [isAttachment](structs.md#isattachment)

___

### isContainer

Re-exports [isContainer](containers.md#iscontainer)

___

### isProcessError

Re-exports [isProcessError](types.md#isprocesserror)

___

### isStruct

Re-exports [isStruct](structs.md#isstruct)

___

### parallel

Re-exports [parallel](util.md#parallel)

___

### replaceAll

Re-exports [replaceAll](util.md#replaceall)

___

### replaceArguments

Re-exports [replaceArguments](util.md#replacearguments)

___

### service

Re-exports [service](actorfactory.md#service)

___

### sleep

Re-exports [sleep](util.md#sleep)

___

### struct

Re-exports [struct](structs.md#struct)

___

### toRpcError

Re-exports [toRpcError](types.md#torpcerror)

___

### wildcardMatch

Re-exports [wildcardMatch](util.md#wildcardmatch)

## Type aliases

### PerformActionRequestWaitFor

Ƭ **PerformActionRequestWaitFor**: ``"nothing"`` \| ``"performed"``

#### Defined in

[src/application.ts:50](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/application.ts#L50)

___

### TLogLevel

Ƭ **TLogLevel**: ``"error"`` \| ``"warning"`` \| ``"info"`` \| ``"verbose"`` \| ``"debug"`` \| ``"deep"``

#### Defined in

[src/logging.ts:13](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/logging.ts#L13)

## Variables

### DEBUG\_AND\_UP

• **DEBUG\_AND\_UP**: [`TLogLevel`](everything.md#tloglevel)[]

#### Defined in

[src/logging.ts:20](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/logging.ts#L20)

___

### DEEP\_AND\_UP

• **DEEP\_AND\_UP**: [`TLogLevel`](everything.md#tloglevel)[]

#### Defined in

[src/logging.ts:21](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/logging.ts#L21)

___

### DO\_NOT\_LOG

• **DO\_NOT\_LOG**: [`TLogLevel`](everything.md#tloglevel)[] = `[]`

#### Defined in

[src/logging.ts:15](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/logging.ts#L15)

___

### ERROR\_AND\_UP

• **ERROR\_AND\_UP**: [`TLogLevel`](everything.md#tloglevel)[]

#### Defined in

[src/logging.ts:16](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/logging.ts#L16)

___

### INFO\_AND\_UP

• **INFO\_AND\_UP**: [`TLogLevel`](everything.md#tloglevel)[]

#### Defined in

[src/logging.ts:18](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/logging.ts#L18)

___

### VERBOSE\_AND\_UP

• **VERBOSE\_AND\_UP**: [`TLogLevel`](everything.md#tloglevel)[]

#### Defined in

[src/logging.ts:19](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/logging.ts#L19)

___

### WARNING\_AND\_UP

• **WARNING\_AND\_UP**: [`TLogLevel`](everything.md#tloglevel)[]

#### Defined in

[src/logging.ts:17](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/logging.ts#L17)
