[@darlean/app-lib](../README.md) / [Modules](../modules.md) / util

# Module: util

## Table of contents

### Interfaces

- [ITaskResult](../interfaces/util.ITaskResult.md)
- [ITasksResult](../interfaces/util.ITasksResult.md)

### Type aliases

- [ParallelAbort](util.md#parallelabort)
- [ParallelTask](util.md#paralleltask)

### Functions

- [actionToString](util.md#actiontostring)
- [actorToString](util.md#actortostring)
- [decodeKey](util.md#decodekey)
- [encodeKey](util.md#encodekey)
- [formatAllAttributes](util.md#formatallattributes)
- [formatAttribute](util.md#formatattribute)
- [parallel](util.md#parallel)
- [replaceAll](util.md#replaceall)
- [replaceArguments](util.md#replacearguments)
- [sleep](util.md#sleep)
- [wildcardMatch](util.md#wildcardmatch)

## Type aliases

### ParallelAbort

Ƭ **ParallelAbort**<`FinalResult`\>: (`finalResult?`: `FinalResult`) => `void`

#### Type parameters

| Name |
| :------ |
| `FinalResult` |

#### Type declaration

▸ (`finalResult?`): `void`

##### Parameters

| Name | Type |
| :------ | :------ |
| `finalResult?` | `FinalResult` |

##### Returns

`void`

#### Defined in

[src/util.ts:114](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/util.ts#L114)

___

### ParallelTask

Ƭ **ParallelTask**<`TaskResult`, `FinalResult`\>: (`abort`: [`ParallelAbort`](util.md#parallelabort)<`FinalResult`\>) => `Promise`<`TaskResult`\>

#### Type parameters

| Name |
| :------ |
| `TaskResult` |
| `FinalResult` |

#### Type declaration

▸ (`abort`): `Promise`<`TaskResult`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `abort` | [`ParallelAbort`](util.md#parallelabort)<`FinalResult`\> |

##### Returns

`Promise`<`TaskResult`\>

#### Defined in

[src/util.ts:115](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/util.ts#L115)

## Functions

### actionToString

▸ **actionToString**(`action`): `string`

#### Parameters

| Name | Type |
| :------ | :------ |
| `action` | [`IPerformActionReq`](../interfaces/everything.interface_performing.IPerformActionReq.md) |

#### Returns

`string`

#### Defined in

[src/util.ts:95](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/util.ts#L95)

___

### actorToString

▸ **actorToString**(`type`, `id`): `string`

#### Parameters

| Name | Type |
| :------ | :------ |
| `type` | `string` |
| `id` | `string`[] |

#### Returns

`string`

#### Defined in

[src/util.ts:99](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/util.ts#L99)

___

### decodeKey

▸ **decodeKey**(`key`): `string`[]

Decodes a key that was previously encoded with [encodeKey](util.md#encodekey).

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `key` | `string` | The key to be decoded |

#### Returns

`string`[]

The decoded key parts

#### Defined in

[src/util.ts:86](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/util.ts#L86)

___

### encodeKey

▸ **encodeKey**(`parts`): `string`

Encodes an actor key into a string. Encoding is performed in such a way that later decoding
produces exactly the same key as result, and that lexicographical ordering still preserves the
original order.

Warning: This function uses unicode characters 1-3 as delimiters and escape characters, so the output
of this function should only be used on places where these characters are accepted. Also, no escaping
of other 'typically unsafe' characters like `/` is performed. In particular, this means that the encoded
keys should not be used to create files/folders on file systems or web services like S3.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `parts` | `string`[] | The key parts |

#### Returns

`string`

The encoded string

#### Defined in

[src/util.ts:71](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/util.ts#L71)

___

### formatAllAttributes

▸ **formatAllAttributes**(`attributes?`): [`IAttributes`](../interfaces/types.IAttributes.md) \| `undefined`

#### Parameters

| Name | Type |
| :------ | :------ |
| `attributes?` | [`IAttributes`](../interfaces/types.IAttributes.md) |

#### Returns

[`IAttributes`](../interfaces/types.IAttributes.md) \| `undefined`

#### Defined in

[src/util.ts:283](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/util.ts#L283)

___

### formatAttribute

▸ **formatAttribute**(`attribute`): `string`

#### Parameters

| Name | Type |
| :------ | :------ |
| `attribute` | `any` |

#### Returns

`string`

#### Defined in

[src/util.ts:249](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/util.ts#L249)

___

### parallel

▸ **parallel**<`TaskResult`, `FinalResult`\>(`tasks`, `timeout`, `maxConcurrency?`): `Promise`<[`ITasksResult`](../interfaces/util.ITasksResult.md)<`TaskResult`, `FinalResult`\>\>

Execute a list of tasks in parallel, with an optional timeout. Tasks can decide to
abort the parallel processing early. They can then also specify a 'final result'
value that is then returned as part of the result.

#### Type parameters

| Name |
| :------ |
| `TaskResult` |
| `FinalResult` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `tasks` | [`ParallelTask`](util.md#paralleltask)<`TaskResult`, `FinalResult`\>[] |
| `timeout` | `number` |
| `maxConcurrency?` | `number` |

#### Returns

`Promise`<[`ITasksResult`](../interfaces/util.ITasksResult.md)<`TaskResult`, `FinalResult`\>\>

#### Defined in

[src/util.ts:123](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/util.ts#L123)

___

### replaceAll

▸ **replaceAll**(`input`, `search`, `replace`): `string`

Replaces all occurrances of `search` within `input` with `replace`. The replacement is
performed in one sweep (when the result of the replacement contains occurrances of
'input', they are not replaced again).

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `input` | `string` | The text in which the search and replace should take place |
| `search` | `string` | The text to search for |
| `replace` | `string` | The text to replace the search text with |

#### Returns

`string`

The input with all occurrances of search replaced by replace.

#### Defined in

[src/util.ts:29](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/util.ts#L29)

___

### replaceArguments

▸ **replaceArguments**(`value`, `attributes?`, `literal?`): `string`

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `value` | `string` | `undefined` |
| `attributes?` | [`IAttributes`](../interfaces/types.IAttributes.md) | `undefined` |
| `literal` | `boolean` | `false` |

#### Returns

`string`

#### Defined in

[src/util.ts:295](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/util.ts#L295)

___

### sleep

▸ **sleep**(`ms`): `Promise`<`void`\>

Sleeps for [[ms]] milliseconds. When ms is 0, `setImmediate` is used so that the function
returns immediately after giving the NodeJS event loop the chance to process pending events.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `ms` | `number` | The amount of milliseconds to wait |

#### Returns

`Promise`<`void`\>

#### Defined in

[src/util.ts:9](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/util.ts#L9)

___

### wildcardMatch

▸ **wildcardMatch**(`input`, `mask`): `boolean`

Performs a wildcard match on [[input]]. The [[mask]] can contain zero or more occurrances
of the wildcard character (`*`).

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `input` | `string` | The text that should be evaluated against the mask |
| `mask` | `string` | The mask that is evaluated against the input |

#### Returns

`boolean`

Whether text matches with the mask.

#### Defined in

[src/util.ts:40](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/util.ts#L40)
