[@darlean/app-lib](../README.md) / [Modules](../modules.md) / binaryencoding

# Module: binaryencoding

This module provides classes and methods for encoding and decoding containers from and to an
efficient human-readable binary format. See [containers](containers.md) for more info.

The *encoding* (serialization) of [IContainer](../interfaces/containers.IContainer.md) instances is performed by the [encodeToBinaryBuffers](binaryencoding.md#encodetobinarybuffers)
function. It converts a container (and its child containers) into an array of buffers. When it detects
that (child) containers already are encoded (serialized), their serialized contents is simply returned.
Otherwise, their contents is encoded and added to the buffer array.

The *decooding* (deserialization) is performed by means of the [decodeFromBinaryBuffer](binaryencoding.md#decodefrombinarybuffer) function that creates a new
[BufferContainer](../classes/binaryencoding.BufferContainer.md) instance around a `Buffer`. The returned [BufferContainer](../classes/binaryencoding.BufferContainer.md) implements [IContainer](../interfaces/containers.IContainer.md), so it
can be used directly everywhere [IContainer](../interfaces/containers.IContainer.md) is used. The BufferContainer performs
*lazy deserialization*. It only deserializes the parts of the buffer that are
needed by application code.

The binary data format of a container is:

| Field | Length | Description |
| --- | --- | --- |
| VERSION_MAJOR | 1 byte | Currently '0'. Implementations must reject a VERSION_MAJOR that is higher that their own supported VERSION_MAJOR. Update this field for backwards-*incompatible* changes to the data format. |
| VERSION_MINOR | 1 byte | Currently '0'. Update this field for backwards-*compatible* changes to the data format. |
| DATA_TYPE | 1 byte | 'e', 'b', 't' or 's' for empty, binary, text and structured data, respectively. |
| RESERVED | 1 byte | Must be ' ' (space, ascii 32). Reservered for future use. |
| TOTAL_SIZE | 10 bytes | The total size of this entire container blob, including all header fields, the data and all subcontainers, represented as ascii number (example: '0000000123') |
| DATA_SIZE | 10 bytes | The size of the data for this container, respresented as ascii number (example: '0000000042'). |
| DATA | \<DATA_SIZE\> | The actual container data. Text data must be utf-8 encoded, struct data must be utf-8 encoded JSON.=, binary data is just copied in literally, and for 'empty' data, this field is not present (0 bytes long). |

## Table of contents

### Classes

- [BufferContainer](../classes/binaryencoding.BufferContainer.md)
- [ContainerToBuffers](../classes/binaryencoding.ContainerToBuffers.md)

### Functions

- [decodeFromBinaryBuffer](binaryencoding.md#decodefrombinarybuffer)
- [encodeToBinaryBuffer](binaryencoding.md#encodetobinarybuffer)
- [encodeToBinaryBuffers](binaryencoding.md#encodetobinarybuffers)

## Functions

### decodeFromBinaryBuffer

▸ **decodeFromBinaryBuffer**<`Type`, `Child0Type`\>(`buffer`, `byteOffset`): [`IContainer`](../interfaces/containers.IContainer.md)<`Type`, `Child0Type`\>

Decodes buffer, that should previously be encoded using [encodeToBinaryBuffer](binaryencoding.md#encodetobinarybuffer), into a container.

#### Type parameters

| Name | Type |
| :------ | :------ |
| `Type` | extends [`ContainerData`](containers.md#containerdata) |
| `Child0Type` | extends [`ContainerData`](containers.md#containerdata) |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `buffer` | `Buffer` | The buffer that needs to be decoded. |
| `byteOffset` | `number` | The offset in bytes from the beginning of buffer where the decoding should start |

#### Returns

[`IContainer`](../interfaces/containers.IContainer.md)<`Type`, `Child0Type`\>

A container.

#### Defined in

[src/binaryencoding.ts:61](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L61)

___

### encodeToBinaryBuffer

▸ **encodeToBinaryBuffer**(`value`): `Buffer`

Encodes a container into one combined binary buffer. Just a wrapper around [encodeToBinaryBuffers](binaryencoding.md#encodetobinarybuffers).

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | [`ContainerLike0`](containers.md#containerlike0) | The value to be encoded. |

#### Returns

`Buffer`

A Buffer with the binary data.

#### Defined in

[src/binaryencoding.ts:88](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L88)

___

### encodeToBinaryBuffers

▸ **encodeToBinaryBuffers**(`value`, `buffers`): `void`

Encodes a container into a series of binary buffers and adds them to buffers.

The reason for 'returning' multiple buffers (instead of just one buffer) is performance. When encoding
multiple (sub)containers, it is more efficient to first collect all the individual small buffers in one
large array (and then concatenating all of these small buffers at once) than concatenating all small
buffers time and time again for every (sub)container.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | [`ContainerLike0`](containers.md#containerlike0) | The value to be encoded. |
| `buffers` | `Buffer`[] | The buffers array to which the resulting buffers are added. |

#### Returns

`void`

#### Defined in

[src/binaryencoding.ts:78](https://gitlab.com/darlean/javascript/app-lib/-/blob/8a73ae7/src/binaryencoding.ts#L78)
